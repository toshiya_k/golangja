## 翻訳ファイルの仕様

翻訳フォルダは，tra/src/{パッケージ} です。
その中に，ソースコード xxx.go に対応する xxx.txt という翻訳ファイルがあります。
翻訳ファイルの仕様は以下の通りです。

```
* 1      // 翻訳ファイルバージョン番号(固定)

#各行のプレフィックス#
原文（英語）
#
訳文（日本語）
#

```

翻訳が必要なのは，ドキュメントに出る部分です。
つまり，

 - 大文字で始まる関数，型，インターフェース

 - 大文字で始まる型の大文字で始まるメソッド

 - 大文字で始まるパッケージレベルの定数，変数

直前のコメントです。

 - また，example_xxx_test.go の場合，Output: 以外のすべてのコメントを訳します。

それ以外のコメント部分は，Google 翻訳のままでかまいません。



# 環境設定
release-branch.go(最新バージョン) ブランチの go のソースコードをダウンロードします。

英語と日本語の 2 つのツリーが必要です。

英語（原文）にある .go ファイルのコメントに翻訳ファイルを適用して，日本語のツリーに .go ファイルをコピーします。


# 翻訳作業の進め方

## 翻訳ファイルの準備
まず，以下を実行して，ソースコードのコメント部分を抽出します。

`go run prog/comment_main/comment_main.go -pkgs=expvar -tdir=/Users/juny/go/src/golangja/golangja/goja/tra/ -sdir=/Users/juny/Documents/github.com/go/`

pkgs は，翻訳したいパッケージ名

tdir は，翻訳ファイルのあるディレクトリ

sdir は， go ソースのディレクトリ

です。

パッケージのソースファイルと example_xxxx_test.go というファイルに対応する txt ファイルが　tra/src/{パッケージ} フォルダに生成されます。

 - xxx.s.txt

 - xxx.t.txt

 - xxx.ja.txt

というファイルが作成されます。

xxx.t.txt を Google 翻訳にかけて，xxx.ja.txt にコピーします。

その後，

`go run prog/merge_mt_main/merge_mt_main.go -pkgs=expvar -tdir=/Users/juny/go/src/golangja/golangja/goja/tra/ -terms=/Users/juny/go/src/golangja/golangja/goja/tra/terms.txt`

を実行すると，

xxx.s.txt と xxx.ja.txt を結合した xxx.mt.txt ができます。

xxx.mt.txt を xxx.txt と名前を手動で変更します。

xxx.txt が翻訳ファイルになります。

翻訳ファイルで翻訳した後，

`go run replace_main/replace_main.go -tdir=/Users/juny/go/src/golangja/golangja/goja/tra/ -sdir=/Users/juny/Documents/github.com/go/ -jdir=/Users/juny/Documents/github.com/go_ja/`

これで，jdir で指定した go のソースフォルダに，日本語訳が適用された .go ファイルが生成されます。

サイトでどのように表示されるか翻訳を確かめるには，godoc で goroot を指定します。

`godoc -http=:6060 -goroot=/Users/juny/Documents/github.com/go_ja/`

