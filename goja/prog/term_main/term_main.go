package main

import (
	"flag"
	"golangja/golangja/goja/prog/translation"
	"os"

	"github.com/atotto/clipboard"
)

var termsFilename string

func main() {
	flag.StringVar(&termsFilename, "term", "/Users/juny/go/src/golangja/golangja/goja/tra/terms.txt", "用語集ファイル")
	flag.Parse()

	var buf [1000]byte
	for {
		terminology := translation.OpenTermsFile(termsFilename)
		n, err := os.Stdin.Read(buf[:])
		if err != nil {
			panic(err)
		}
		o := string(buf[:n-1])
		if o == "" {
			o, err = clipboard.ReadAll()
			if err != nil {
				panic(err)
			}
		}
		s := terminology.Replace(o)
		println(s)
		clipboard.WriteAll(s)
	}
}
