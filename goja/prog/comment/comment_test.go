package comment

import (
	"testing"
)

func TestParseComments(t *testing.T) {
	ecs := []Comment{
		Comment{[]string{"A", "B"}, ""},
		Comment{[]string{"C"}, ""},
		Comment{[]string{"D", "E"}, "// "},
		Comment{[]string{"F"}, "// "},
		Comment{[]string{"G", "H"}, "	// "},
		Comment{[]string{"I"}, "	//	"},
		Comment{[]string{"J"}, "	s string // "},
		Comment{[]string{"K"}, "	t int    // "},
	}
	cs := ExtractComments(s)
	if len(cs) != len(ecs) {
		t.Errorf("サイズが違います。len=%d expected=%d, %#v", len(cs), len(ecs), cs)
	}
	for i := range cs {
		if cs[i].Prefix != ecs[i].Prefix {
			t.Errorf("Prefixが違います。[%d] %s: expeced=%s", i, cs[i].Prefix, ecs[i].Prefix)
		}
		if len(cs[i].Lines) != len(ecs[i].Lines) {
			t.Errorf("Linesの数が違います.[%d] %d expected=%d, %#v", i, len(cs[i].Lines), len(ecs[i].Lines), cs[i].Lines)
		}
		for j := range cs[i].Lines {
			if cs[i].Lines[j] != ecs[i].Lines[j] {
				t.Errorf("Lineが違います。[%d:%d] %s expected=%s", i, j, cs[i].Lines[j], ecs[i].Lines[j])
			}
		}
	}
	f := FormatComments(cs)
	if f != s1 {
		t.Errorf("Formatが違います.%s", f)
		for i := 0; i < len(f); i++ {
			if f[i] != s1[i] {
				t.Errorf("[%d] %s", i, f[i:])
			}
		}
	}
}

const s1 = `* 1

##
A
B
#
A B
#

##
C
#
C
#

#// #
D
E
#
D E
#

#// #
F
#
F
#

#	// #
G
H
#
G H
#

#	//	#
I
#
I
#

#	s string // #
J
#
J
#

#	t int    // #
K
#
K
#

`

const s = `
// Copyright 2018 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*
A
B

C
 */

// +build js,wasm

package abc

// D
// E
//
// F
func A() {
	// G
	// H
	//
	//	I
}

type B struct {
	s string // J
	t int    // K
}
`
