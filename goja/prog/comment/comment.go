package comment

import (
	"bytes"
	"strings"
)

/*
const (
	LevelPublic = iota
	LevelPrivate
)
*/

type Comment struct {
	Lines  []string
	Prefix string
	Tag    string
	//Level  int
}

func ExtractComments(s string, tag string) []Comment {
	var first = true
	var comments []Comment
	var lines = []string{}
	allLines := strings.Split(s, "\n")
	var c bool
	var cc bool // c タイプのコメント
	var prefix string
	var addcomment = func() {
		if len(lines) == 0 {
			return
		}
		if !(first && strings.HasPrefix(lines[0], "Copyright")) {
			comments = append(comments, Comment{lines, prefix, tag})
		}
		lines = []string{}
	}
	for _, l := range allLines {
		// c タイプのコメント
		if strings.TrimSpace(l) == "/*" {
			addcomment()
			c = false
			prefix = ""
			cc = true
			continue
		} else if strings.TrimSpace(l) == "*/" {
			cc = false
			continue
		} else if cc {
			if l == "" {
				addcomment()
			} else {
				lines = append(lines, l)
			}
			continue
		}
		// c++ タイプのコメント
		if strings.HasPrefix(l, "// +build") {
			addcomment()
			c = false
			prefix = ""
			continue
		}
		i1 := strings.Index(l, "// ") // 空白
		i2 := strings.Index(l, "//	") // タブ
		i := i1                       // i は上記のどちらか。
		if i < 0 {
			i = i2
		}
		if i >= 0 {
			p := l[:i+len("// ")]
			if p != prefix {
				addcomment()
			}
			lines = append(lines, strings.TrimPrefix(l, p))
			c = true
			prefix = p
		} else {
			if c {
				addcomment()
				c = false
				prefix = ""
			}
		}
	}
	return comments
}

/*
func FormatComments(comments []Comment) string {
	return formatComments(comments, true, true)
}
*/

func formatComments(comments []Comment, printSource, printTarget, isSourceTargetSame bool) string {
	var b bytes.Buffer
	b.WriteString("* 1\n\n")
	for _, c := range comments {
		if c.Tag != "" {
			b.WriteString("[" + c.Tag + "]\n")
		}
		b.WriteString("#" + c.Prefix + "#\n")
		if printSource {
			for _, s := range c.Lines {
				b.WriteString(strings.ReplaceAll(s, "#", "＃"))
				b.WriteString("\n")
			}
		}
		b.WriteString("#\n")
		if printTarget {
			if isSourceTargetSame {
				for _, s := range c.Lines {
					b.WriteString(strings.ReplaceAll(s, "#", "＃"))
					b.WriteString("\n")
				}
			} else {
				b.WriteString(strings.ReplaceAll(strings.Join(c.Lines, " "), "#", "＃"))
				b.WriteString("\n")
			}
		}
		b.WriteString("#\n\n")
	}
	return b.String()
}

func FormatCommentsSource(comments []Comment) string {
	return formatComments(comments, true, false, false)
}

func FormatCommentsSourceTarget(comments []Comment) string {
	return formatComments(comments, true, true, true)
}

func FormatCommentsTarget(comments []Comment) string {
	return formatComments(comments, false, true, false)
}

func FormatCommentsSource2(comments []Comment) string {
	var b bytes.Buffer
	for _, c := range comments {
		b.WriteString(strings.Join(c.Lines, " "))
		b.WriteString("\n")
	}
	return b.String()
}
