package main

import (
	"flag"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"log"
	"path/filepath"
)

var tBaseDir string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	//formatTranslationFile("/Users/juny/go/src/golangja/golangja/goja/tra/src/math/big/floatconv.txt")
	formatTranslationFilesInDir(filepath.Join(tBaseDir, "src"))
}

func formatTranslationFile(filename string) error {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	var tr = new(translation.Translation)
	tr.Filename = filename
	tr.ParseTranslationStr(string(bytes))
	tr.FormatTarget()
	log.Println(tr.String())
	err = ioutil.WriteFile(filename, []byte(tr.String()), 0666)
	if err != nil {
		return err
	}
	return nil
}

func formatTranslationFilesInDir(dir string) error {
	tfiles, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}
	for _, tfile := range tfiles {
		if tfile.IsDir() {
			formatTranslationFilesInDir(filepath.Join(dir, tfile.Name()))
		} else if filepath.Ext(tfile.Name()) == ".txt" {
			err = formatTranslationFile(filepath.Join(dir, tfile.Name()))
			if err != nil {
				return err
			}
		}
	}
	return nil
}
