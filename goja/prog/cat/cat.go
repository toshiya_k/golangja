package cat

import (
	"bytes"
	"index/suffixarray"
	"io/ioutil"
	"sort"
	"strings"
	"unicode/utf8"
)

const delimS = utf8.MaxRune
const delimT = utf8.MaxRune - 1

type Translation struct {
	Source string
	Target string
}

type TrMemory struct {
	Translations      []Translation
	TranslationIndice []int
	Index             *suffixarray.Index
}

func (tm *TrMemory) Lookup(s []byte, n int) []int {
	var ret []int
	result := tm.Index.Lookup(s, n)
	for _, x := range result {
		i := sort.SearchInts(tm.TranslationIndice, x)
		app := true
		for _, r := range ret {
			if r == i {
				app = false
				break
			}
		}
		if app {
			ret = append(ret, i)
		}
	}
	return ret
}

func (tm *TrMemory) Format(indice []int) string {
	var ret bytes.Buffer
	for _, i := range indice {
		ret.WriteString(tm.Translations[i].Source)
		ret.WriteString("\n")
		ret.WriteString(tm.Translations[i].Target)
		ret.WriteString("\n\n")
	}
	return ret.String()
}

func (tm *TrMemory) MakeIndex() {
	var buf bytes.Buffer
	buf.WriteRune(delimT)
	for _, t := range tm.Translations {
		buf.WriteString(t.Source)
		buf.WriteRune(delimS)
		buf.WriteString(t.Target)
		buf.WriteRune(delimT)
		tm.TranslationIndice = append(tm.TranslationIndice, buf.Len())
	}
	tm.Index = suffixarray.New(buf.Bytes())
}

func (tm *TrMemory) ReadTranslationFile(trFilepath string) error {
	translationBytes, err := ioutil.ReadFile(trFilepath)
	if err != nil {
		return err
	}

	translationStr := "\n" + string(translationBytes) + "\n"
	translationStr = strings.ReplaceAll(translationStr, "\n#\n#\n#\n", "\n")
	pairs := strings.Split(translationStr, "\n#\n")
	var isTarget bool
	var sourceLine string
	for _, s := range pairs {
		if strings.TrimSpace(s) == "" {
			isTarget = false
			continue
		}
		ss := "\n" + s + "\n"
		if !isTarget {
			sourceLine = ss
			isTarget = true
		} else {
			tm.Translations = append(tm.Translations, Translation{strings.TrimSpace(sourceLine), strings.TrimSpace(ss)})
			isTarget = false
		}
	}
	return nil
}
