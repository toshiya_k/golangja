package main

import (
	"flag"
	"golangja/golangja/goja/prog/extract"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
)

var filename string

func main() {
	flag.StringVar(&filename, "f", "/Users/juny/Documents/github.com/go/doc/effective_go.html", "ファイル名フルパス")

	b, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	tps, tpt := extract.ExtractParagraphs(string(b))
	var trs, trt translation.Translation
	trs.Pairs = tps
	trt.Pairs = tpt
	ioutil.WriteFile(filename+".s.txt", []byte(trs.String()), 0666)
	ioutil.WriteFile(filename+".t.txt", []byte(trt.String()), 0666)
	ioutil.WriteFile(filename+".ja.txt", []byte(""), 0666)
}
