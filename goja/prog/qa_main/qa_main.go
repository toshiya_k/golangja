package main

import (
	"flag"
	"fmt"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"path/filepath"
	"strings"
)

func main() {
	var dir string
	var termsFilename string
	flag.StringVar(&dir, "dir", "/Users/juny/go/src/golangja/golangja/goja/tra/src/", "ディレクトリ")
	flag.StringVar(&termsFilename, "term", "/Users/juny/go/src/golangja/golangja/goja/tra/terms.txt", "用語集ファイル")
	flag.Parse()
	terminology := translation.OpenTermsFile(termsFilename)
	runQA(dir, terminology)
}

func runQA(d string, t *translation.Terminology) {
	files, err := ioutil.ReadDir(d)
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		if f.IsDir() {
			runQA(filepath.Join(d, f.Name()), t)
		} else if strings.HasSuffix(f.Name(), ".txt") {
			tra := new(translation.Translation)
			path := filepath.Join(d, f.Name())
			b, err := ioutil.ReadFile(path)
			if err != nil {
				panic(err)
			}
			tra.Filename = path
			tra.ParseTranslationStr(string(b))
			res := t.QA(tra)
			for _, r := range res {
				fmt.Printf("%s:%d %s\n", path, r.Pair.LineNo, r.ForbiddenTerm.T)
			}
		}
	}
}
