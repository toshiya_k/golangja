package fetch

import (
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
)

func FetchHTML(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func SaveHTMLFile(url, filename string, replaceFunc func(string) string, translations []*translation.Translation) {
	html := FetchHTML(url)
	if replaceFunc != nil {
		html = replaceFunc(html)
	}
	for _, t := range translations {
		html = t.ApplyWithoutLastNewLine(html)
		// /src/へのリンクは golang.org へのリンクに変換する。
		html = strings.Replace(html, "href=\"/src/", "href=\"https://golang.org/src/", -1)
		r := regexp.MustCompile("\\?s=[0-9]+:[0-9]+#")
		html = r.ReplaceAllString(html, "#")
	}
	html = strings.Replace(html, "{{LinkToOriginalSource}}", "https://golang.org/"+url[len("http://localhost:6060/"):], 1)
	//log.Printf("filename: %s, %s", filename, html[0:10])
	err := ioutil.WriteFile(filename, []byte(html), 0666)
	if err != nil {
		panic(err)
	}
	rp := regexp.MustCompile("<p>[\x00-\x3b\x3d-\x7F]{10,}</p>")
	rpr := rp.FindString(html)
	if rpr != "" {
		log.Printf("アスキー文字のみのpがあります。 %s : %s", url, rpr[:15])
	}
	rpc := regexp.MustCompile(`<span class="comment">// [^ \t][\x00-\x3b\x3d-\x7F]{20,}</span>`)
	rpcr := rpc.FindString(html)
	if rpcr != "" && !strings.Contains(rpcr, "://") && !strings.Contains(rpcr, "contains filtered or unexported") && !strings.Contains(rpcr, "Go 1.") && !strings.ContainsAny(rpcr, ":_") {
		log.Printf("アスキー文字のみのコメントがあります。 %s : %s", url, rpcr)
	}
}

/*
func ApplyTranslation(sourceStr, translationStr string) string {
	translationStr = "\n" + translationStr + "\n"
	pairs := strings.Split(translationStr, "\n#\n")
	var isTarget bool
	var sourceLine string
	for _, s := range pairs {
		if strings.TrimSpace(s) == "" {
			isTarget = false
			continue
		}
		if !isTarget {
			sourceLine = s
			isTarget = true
		} else {
			sourceStr = strings.Replace(sourceStr, sourceLine, s, -1)
			isTarget = false
		}
	}
	// /src/へのリンクは golang.org へのリンクに変換する。
	sourceStr = strings.Replace(sourceStr, "href=\"/src/", "href=\"https://golang.org/src/", -1)
	r, err := regexp.Compile("\\?s=[0-9]+:[0-9]+#")
	if err != nil {
		panic(err)
	}
	sourceStr = r.ReplaceAllString(sourceStr, "#")

	return sourceStr
}
*/
