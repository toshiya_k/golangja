package fetch

import (
	"testing"
)

var tra = `
#
"Hello"
#
"ハロー"
#

`

func TestFetch(t *testing.T) {
	s := ApplyTranslation(`"Hello"`, tra)
	if s != `"ハロー"` {
		t.Errorf(s)
	}
}

// これは，フォルダを作成するためだけのテストファイル。
/*
func TestDir(t *testing.T) {
	for _, packageName := range prog.NotTranslatedPackageNames {
		dir := "/Users/juny/go/src/golangja/golangja/goja/appengine/site/pkg/" + packageName
		//dir := "/Users/juny/go/src/golangja/golangja/goja/tra/src/" + packageName
		_, err := os.Open(dir)
		if err != nil {
			e := err.(*os.PathError)
			t.Logf("%v, %s", e, e.Err)
			if e.Err.Error() == "no such file or directory" {
				err = os.Mkdir(dir, os.ModeDir|os.ModePerm)
				if err != nil {
					t.Logf("%v", err)
				}
			}
		}
	}
	t.Error("Error")
}
*/
