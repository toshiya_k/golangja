package main

import (
	"flag"
	"golangja/golangja/goja/prog"
	"golangja/golangja/goja/prog/cat"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func readDir(dir string, tm *cat.TrMemory) error {
	tfiles, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}
	for _, tfile := range tfiles {
		log.Printf("%s", tfile.Name())
		if filepath.Ext(tfile.Name()) == ".txt" {
			err := tm.ReadTranslationFile(filepath.Join(dir, tfile.Name()))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

var tBaseDir string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	flag.Parse()

	var tm cat.TrMemory
	for _, dir := range prog.PackageNames {
		readDir(tBaseDir+"src/"+dir, &tm)
	}
	readDir(tBaseDir+"doc/", &tm)
	tm.MakeIndex()

	var buf [1000]byte
	for {
		n, err := os.Stdin.Read(buf[:])
		if err != nil {
			panic(err)
		}
		indice := tm.Lookup(buf[:n-1], 3)
		s := tm.Format(indice)
		println(s)
	}
}
