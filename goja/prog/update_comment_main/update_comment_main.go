package main

import (
	"flag"
	"fmt"
	"golangja/golangja/goja/prog/comment"
	"golangja/golangja/goja/prog/fetch"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var tBaseDir string
var sBaseDir string
var translatingPackagesStr string
var translatingPackages []string
var filename string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	flag.StringVar(&sBaseDir, "sdir", "/Users/juny/Documents/github.com/go_ja/", "go ソースのディレクトリ")
	flag.StringVar(&translatingPackagesStr, "pkgs", "", "翻訳するパッケージ。カンマ区切りで，複数指定可能です。")
	flag.StringVar(&filename, "f", "/Users/juny/go/src/golangja/golangja/goja/tra/tmp/update_comment", "")
	flag.Parse()

	translatingPackages = strings.Split(translatingPackagesStr, ",")
	translatingPackages = []string{
		"archive",
		"archive/tar",
		"archive/zip",
		"bufio",
		"builtin",
		"bytes",
		"compress",
		"compress/bzip2",
		"compress/flate",
		"compress/gzip",
		"compress/lzw",
		"compress/zlib",
		"container",
		"container/heap",
		"container/list",
		"container/ring",
		"context",
		"crypto",
		"crypto/aes",
		"crypto/cipher",
		"crypto/des",
		"crypto/dsa",
		"crypto/ecdsa",
		"crypto/ed25519",
		"crypto/elliptic",
		"crypto/hmac",
		"crypto/md5",
		"crypto/rand",
		"crypto/rc4",
		"crypto/rsa",
		"crypto/sha1",
		"crypto/sha256",
		"crypto/sha512",
		"crypto/subtle",
		"crypto/tls",
		"crypto/x509",
		"crypto/x509/pkix",
		"database",
		"database/sql",
		"database/sql/driver",
		"debug",
		"debug/dwarf",
		"debug/elf",
		"debug/gosym",
		"debug/macho",
		"debug/pe",
		"debug/plan9obj",
		"encoding",
		"encoding/ascii85",
		"encoding/asn1",
		"encoding/base32",
		"encoding/base64",
		"encoding/binary",
		"encoding/csv",
		"encoding/gob",
		"encoding/hex",
		"encoding/json",
		"encoding/pem",
		"encoding/xml",
		"errors",
		"expvar",
		"flag",
		"fmt",
		"go",
		"go/ast",
		"go/build",
		"go/constant",
		"go/doc",
		"go/format",
		"go/importer",
		"go/parser",
		"go/printer",
		"go/scanner",
		"go/token",
		"go/types",
		"hash",
		"hash/adler32",
		"hash/crc32",
		"hash/crc64",
		"hash/fnv",
		"html",
		"html/template",
		"image",
		"image/color",
		"image/color/palette",
		"image/draw",
		"image/gif",
		"image/jpeg",
		"image/png",
		"index",
		"index/suffixarray",
		"io",
		"io/ioutil",
		"log",
		"log/syslog",
		"math",
		"math/big",
		"math/bits",
		"math/cmplx",
		"math/rand",
		"mime",
		"mime/multipart",
		"mime/quotedprintable",
		"net",
		"net/http",
		"net/http/cgi",
		"net/http/cookiejar",
		"net/http/fcgi",
		"net/http/httptest",
		"net/http/httptrace",
		"net/http/httputil",
		"net/http/pprof",
		"net/mail",
		"net/rpc",
		"net/rpc/jsonrpc",
		"net/smtp",
		"net/textproto",
		"net/url",
		"os",
		"os/exec",
		"os/signal",
		"os/user",
		"path",
		"path/filepath",
		"plugin",
		"reflect",
		"regexp",
		"regexp/syntax",
		"runtime",
		"runtime/cgo",
		"runtime/debug",
		"runtime/msan",
		"runtime/pprof",
		"runtime/race",
		"runtime/trace",
		"sort",
		"strconv",
		"strings",
		"sync",
		"sync/atomic",
		"syscall",
		"syscall/js",
		"testing",
		"testing/iotest",
		"testing/quick",
		"text",
		"text/scanner",
		"text/tabwriter",
		"text/template",
		"text/template/parse",
		"time",
		"unicode",
		"unicode/utf16",
		"unicode/utf8",
		"unsafe",
	}
	readPackages()
}

func readPackages() error {
	var updateComments []comment.Comment
	for _, packageName := range translatingPackages {
		html := fetch.FetchHTML("http://localhost:6060/pkg/" + packageName + "/")
		sDir := sBaseDir + "src/" + packageName
		tDir := tBaseDir + "src/" + packageName
		sfiles, err := ioutil.ReadDir(sDir)
		if err != nil {
			return err
		}
		for _, sfile := range sfiles {
			if filepath.Ext(sfile.Name()) == ".go" && (strings.Contains(html, ">"+sfile.Name()+"</a>") || strings.HasPrefix(sfile.Name(), "example")) {
				sBytes, err := ioutil.ReadFile(filepath.Join(sDir, sfile.Name()))
				if err != nil {
					return err
				}
				tag := packageName + "/" + sfile.Name()
				comments := comment.ExtractComments(string(sBytes), tag)

				var trans translation.Translation
				trans.Filename = filepath.Join(tDir, strings.ReplaceAll(sfile.Name(), ".go", ".txt"))
				bytes, err := ioutil.ReadFile(trans.Filename)
				if os.IsNotExist(err) {
					ioutil.WriteFile(trans.Filename, []byte("* 1\n\n\n"), 0666)
					updateComments = append(updateComments, comments...)
					continue
				} else if err != nil {
					panic(err)
				}
				trans.ParseTranslationStr(string(bytes))
				updateComments = append(updateComments, findNotTranslatedComments(comments, trans)...)
			}
		}
	}
	const maxPairNum = 100
	for i := 0; i < 1000; i++ {
		m := len(updateComments)
		cs := updateComments
		if m >= maxPairNum {
			cs = updateComments[:maxPairNum]
			updateComments = updateComments[maxPairNum:]
		}
		ioutil.WriteFile(fmt.Sprintf("%s.%d.s.txt", filename, i), []byte(comment.FormatCommentsSourceTarget(cs)), 0666)
		ioutil.WriteFile(fmt.Sprintf("%s.%d.t.txt", filename, i), []byte(comment.FormatCommentsSource2(cs)), 0666)
		ioutil.WriteFile(fmt.Sprintf("%s.%d.ja.txt", filename, i), []byte(""), 0666)
		if m < maxPairNum {
			break
		}
	}
	return nil
}

func findNotTranslatedComments(comments []comment.Comment, trans translation.Translation) []comment.Comment {
	var ret []comment.Comment
	for _, c := range comments {
		var found bool
		for _, l := range c.Lines {
			if strings.IndexFunc(l, func(r rune) bool {
				return (r > 0x7F)
			}) >= 0 {
				found = true
				break
			}
		}
		/*
			if c.Lines[0] == `  import "html/template"` {
				println(fmt.Sprintf("XXXX %+v", c))
			}
		*/
		for _, p := range trans.Pairs {
			if deepEqual(c.Lines, c.Prefix, p.SourceLines, p.Prefix) {
				println(fmt.Sprintf("%+v", c.Lines))
				found = true
				break
			}
		}
		if !found {
			ret = append(ret, c)
		}
	}
	return ret
}

func deepEqual(a []string, aprefix string, b []string, bprefix string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if aprefix+a[i] != bprefix+b[i] {
			return false
		}
	}
	return true
}
