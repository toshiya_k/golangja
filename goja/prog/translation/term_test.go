package translation

import "testing"

type D struct {
	in, out string
}

func TestReplace(t *testing.T) {
	data := []D{
		D{"Helloこんにちは", "Hello こんにちは"},
		D{"こんにちはHello", "こんにちは Hello"},
		D{"Hello こんにちは", "Hello こんにちは"},
		D{"こんにちは Hello", "こんにちは Hello"},
		D{"(a)", "(a)"},
		D{"a/b", "a/b"},
	}
	var te Terminology
	for _, d := range data {
		if d.out != te.Replace(d.in) {
			t.Errorf("in:%s, expected:%s, but:%s", d.in, d.out, te.Replace(d.in))
		}
	}
}
