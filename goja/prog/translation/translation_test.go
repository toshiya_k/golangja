package translation

import (
	"testing"
)

type testTranslationData struct {
	translationStr string
	tr             Translation
}

const ts1 = `* 1

##
A
#
あ
#

#//＃ #
C
D
#
い
う
#

`

func TestParseTranslationStr(t *testing.T) {
	var data = []testTranslationData{
		{
			translationStr: ts1,
			tr: Translation{Pairs: []TrPair{
				TrPair{sourceLines: []string{"A"}, targetLines: []string{"あ"}, Prefix: ""},
				TrPair{sourceLines: []string{"C", "D"}, targetLines: []string{"い", "う"}, Prefix: "//# "},
			}},
		},
	}
	for _, d := range data {
		tra := new(Translation)
		tra.ParseTranslationStr(d.translationStr)
		if len(tra.Pairs) != len(d.tr.Pairs) {
			t.Errorf("Pairs の数が違います。 tra.Pairs = %#v", tra.Pairs)
		}
		for i := range tra.Pairs {
			if tra.Pairs[i].Prefix != d.tr.Pairs[i].Prefix {
				t.Errorf("Prefix が違います。%s, %s", tra.Pairs[i].Prefix, d.tr.Pairs[i].Prefix)
			}
			if len(tra.Pairs[i].sourceLines) != len(d.tr.Pairs[i].sourceLines) {
				t.Errorf("sourceLines の数が違います。%d, %#v", i, tra.Pairs[i].sourceLines)
			}
			for j := range tra.Pairs[i].sourceLines {
				if tra.Pairs[i].sourceLines[j] != d.tr.Pairs[i].sourceLines[j] {
					t.Errorf("sourceLinesが違います。%d, %d, %s", i, j, tra.Pairs[i].sourceLines[j])
				}
			}
			if len(tra.Pairs[i].targetLines) != len(d.tr.Pairs[i].targetLines) {
				t.Errorf("targetLines の数が違います。%d, %#v", i, tra.Pairs[i].targetLines)
			}
			for j := range tra.Pairs[i].targetLines {
				if tra.Pairs[i].targetLines[j] != d.tr.Pairs[i].targetLines[j] {
					t.Errorf("targetLinesが違います。%d, %d, %s", i, j, tra.Pairs[i].targetLines[j])
				}
			}
		}
	}
}

type testData struct {
	tp        TrPair
	newTarget string
}

func TestFormatTarget(t *testing.T) {
	var data = []testData{
		{TrPair{targetLines: []string{"ｂ"}}, "b"},
		{TrPair{sourceLines: []string{"a < b"}, targetLines: []string{"a<b"}}, "a < b"},
		{TrPair{sourceLines: []string{"a, b"}, targetLines: []string{"a ， b"}}, "a, b"},
		{TrPair{sourceLines: []string{"point (fcount <= 0) and"}, targetLines: []string{"あ (fcount<=0) ，"}}, "あ (fcount <= 0) ，"},
		{TrPair{sourceLines: []string{"for p, q := uint64(0), uint64(1);"}, targetLines: []string{"pについて，q : =uint64 ( 0 ) ， uint64 ( 1 ) 。"}}, "pについて，q := uint64(0), uint64(1) 。"},
		{TrPair{sourceLines: []string{"bool-value"}, targetLines: []string{"ブール値"}}, "ブール値"},
		{TrPair{sourceLines: []string{"The returned *Float f is nil"}, targetLines: []string{"返される *Floatf は nil で"}}, "返される *Float f は nil で"},
		{TrPair{sourceLines: []string{"'int'"}, targetLines: []string{"' int '"}}, "'int'"},
	}
	for _, d := range data {
		d.tp.FormatTarget()
		if d.tp.targetLines[0] != d.newTarget {
			t.Errorf("expected:%s, got:%s", d.newTarget, d.tp.targetLines[0])
		}
	}
}

type testPrefixData struct {
	original TrPair
	expected TrPair
}

func TestExtractPrefix(t *testing.T) {
	var data = []testPrefixData{
		{original: TrPair{sourceLines: []string{"// A"}, targetLines: []string{"// B"}}, expected: TrPair{sourceLines: []string{"A"}, targetLines: []string{"B"}, Prefix: "// "}},
		{original: TrPair{sourceLines: []string{"// A"}, targetLines: []string{"// B"}, Prefix: "// "}, expected: TrPair{sourceLines: []string{"// A"}, targetLines: []string{"// B"}, Prefix: "// "}},
	}
	for _, d := range data {
		d.original.FormatTarget()
		if d.original.Prefix != d.expected.Prefix {
			t.Errorf("Prefix が違います。%s, %s", d.original.Prefix, d.expected.Prefix)
		}
		if len(d.original.sourceLines) != len(d.expected.sourceLines) {
			t.Errorf("sourceLines の数が違います。%#v", d.original.sourceLines)
		}
		for j := range d.original.sourceLines {
			if d.original.sourceLines[j] != d.expected.sourceLines[j] {
				t.Errorf("sourceLinesが違います。%d, %s", j, d.original.sourceLines[j])
			}
		}
		if len(d.original.targetLines) != len(d.expected.targetLines) {
			t.Errorf("targetLines の数が違います。%#v", d.original.targetLines)
		}
		for j := range d.original.targetLines {
			if d.original.targetLines[j] != d.expected.targetLines[j] {
				t.Errorf("targetLinesが違います。%d, %s", j, d.original.targetLines[j])
			}
		}

	}
}
