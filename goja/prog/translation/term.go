package translation

import (
	"bytes"
	"io/ioutil"
	"log"
	"sort"
	"strings"
	"unicode"
)

type Terminology struct {
	terms []Term
}

type TermType int

const (
	TReplace TermType = iota
	Trans
	TForbidden
	TTwoLines
)

type Term struct {
	S        string
	T        string
	TermType TermType
}

var BasicTerms = []Term{
	Term{"！", "!", TReplace},
	Term{"？", "?", TReplace},
	Term{"：", ":", TReplace},
	Term{"（", "(", TReplace},
	Term{"）", ")", TReplace},
	Term{"［", "[", TReplace},
	Term{"］", "]", TReplace},
	Term{"｛", "{", TReplace},
	Term{"｝", "}", TReplace},
	Term{"＝", "=", TReplace},
	Term{"｜", "|", TReplace},
	Term{"．", ".", TReplace},
	Term{"％", "%", TReplace},
	Term{"＆", "&", TReplace},
	Term{"＄", "$", TReplace},
	Term{"＃", "#", TReplace},
	Term{"〜", "~", TReplace},
	Term{"＾", "^", TReplace},
	//Term{"ー", "-"},
	Term{"＋", "+", TReplace},
	Term{"＊", "*", TReplace},
	Term{"＜", "<", TReplace},
	Term{"＞", ">", TReplace},
	Term{"／", "/", TReplace},
	Term{"＿", "_", TReplace},
	Term{"｀", "`", TReplace},
	Term{"０", "0", TReplace},
	Term{"１", "1", TReplace},
	Term{"２", "2", TReplace},
	Term{"３", "3", TReplace},
	Term{"４", "4", TReplace},
	Term{"５", "5", TReplace},
	Term{"６", "6", TReplace},
	Term{"７", "7", TReplace},
	Term{"８", "8", TReplace},
	Term{"９", "9", TReplace},
	Term{"ａ", "a", TReplace},
	Term{"ｂ", "b", TReplace},
	Term{"ｃ", "c", TReplace},
	Term{"ｄ", "d", TReplace},
	Term{"ｅ", "e", TReplace},
	Term{"ｆ", "f", TReplace},
	Term{"ｇ", "g", TReplace},
	Term{"ｈ", "h", TReplace},
	Term{"ｉ", "i", TReplace},
	Term{"ｊ", "j", TReplace},
	Term{"ｋ", "k", TReplace},
	Term{"ｌ", "l", TReplace},
	Term{"ｍ", "m", TReplace},
	Term{"ｎ", "n", TReplace},
	Term{"ｏ", "o", TReplace},
	Term{"ｐ", "p", TReplace},
	Term{"ｑ", "q", TReplace},
	Term{"ｒ", "r", TReplace},
	Term{"ｓ", "s", TReplace},
	Term{"ｔ", "t", TReplace},
	Term{"ｕ", "u", TReplace},
	Term{"ｖ", "v", TReplace},
	Term{"ｗ", "w", TReplace},
	Term{"ｘ", "x", TReplace},
	Term{"ｙ", "y", TReplace},
	Term{"ｚ", "z", TReplace},
	Term{"Ａ", "A", TReplace},
	Term{"Ｂ", "B", TReplace},
	Term{"Ｃ", "C", TReplace},
	Term{"Ｄ", "D", TReplace},
	Term{"Ｅ", "E", TReplace},
	Term{"Ｆ", "F", TReplace},
	Term{"Ｇ", "G", TReplace},
	Term{"Ｈ", "H", TReplace},
	Term{"Ｉ", "I", TReplace},
	Term{"Ｊ", "J", TReplace},
	Term{"Ｋ", "K", TReplace},
	Term{"Ｌ", "L", TReplace},
	Term{"Ｍ", "M", TReplace},
	Term{"Ｎ", "N", TReplace},
	Term{"Ｏ", "O", TReplace},
	Term{"Ｐ", "P", TReplace},
	Term{"Ｑ", "Q", TReplace},
	Term{"Ｒ", "R", TReplace},
	Term{"Ｓ", "S", TReplace},
	Term{"Ｔ", "T", TReplace},
	Term{"Ｕ", "U", TReplace},
	Term{"Ｖ", "V", TReplace},
	Term{"Ｗ", "W", TReplace},
	Term{"Ｘ", "X", TReplace},
	Term{"Ｙ", "Y", TReplace},
	Term{"Ｚ", "Z", TReplace},
}

func OpenTermsFile(filename string) *Terminology {
	var terms = append([]Term{}, BasicTerms...)
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(bs), "\n")
	for _, l := range lines {
		l = strings.TrimSpace(l)
		if l == "" {
			continue
		}
		two := strings.Split(l, ":")
		if len(two) != 2 {
			log.Printf(":がありません: %s", l)
			continue
		}
		termtype := TReplace
		if strings.HasPrefix(two[0], "!") {
			termtype = TForbidden
			two[0] = two[0][1:]
		}
		ss := strings.Split(two[0], ",")
		for _, sss := range ss {
			v := strings.TrimSpace(sss)
			if v != "" {
				terms = append(terms, Term{v, strings.TrimSpace(two[1]), termtype})
			}
		}
	}
	sort.Slice(terms, func(i, j int) bool {
		return len(terms[i].S) > len(terms[j].S)
	})
	return &Terminology{terms}
}

func isASCII(r rune) bool {
	return r <= unicode.MaxLatin1 || r == '±'
}

func (t *Terminology) Replace(s string) string {
	for _, u := range t.terms {
		if u.TermType != TReplace {
			continue
		}
		s = strings.Replace(s, u.S, u.T, -1)
	}
	// 英単語の前後には半角スペースを挿入する
	var b bytes.Buffer
	var j bool
	var space = true
	for _, r := range s {
		if !space && !unicode.IsSpace(r) {
			if isASCII(r) && j {
				b.WriteRune(' ')
			}
			if !isASCII(r) && !j {
				b.WriteRune(' ')
			}
		}

		b.WriteRune(r)
		j = r > unicode.MaxASCII
		space = unicode.IsSpace(r)
	}
	return b.String()
}

type QAResult struct {
	Pair          TrPair
	ForbiddenTerm Term
}

var TwoLinesWithLetterPrefix Term = Term{S: "", T: "TWO LINES", TermType: TTwoLines}

func (t *Terminology) QA(tra *Translation) []QAResult {
	var ret []QAResult
	for _, p := range tra.Pairs {
		if strings.IndexFunc(p.Prefix, func(r rune) bool {
			return unicode.IsLetter(r)
		}) >= 0 && len(p.TargetLines) > 1 {
			ret = append(ret, QAResult{p, TwoLinesWithLetterPrefix})
		}
		for _, u := range t.terms {
			if u.TermType != TForbidden {
				continue
			}
			if (u.S == "" || strings.Contains(p.SourceWithoutPrefix(), u.S)) && strings.Contains(p.TargetWithoutPrefix(), u.T) {
				ret = append(ret, QAResult{p, u})
			}
		}
	}
	return ret
}
