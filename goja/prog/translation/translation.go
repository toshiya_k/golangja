package translation

import (
	"bytes"
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"
)

type TrPair struct {
	SourceLines []string
	TargetLines []string
	LineNo      int
	Prefix      string
	Tag         string
}

func (t *TrPair) CopyTarget(target TrPair) {
	t.TargetLines = append([]string{}, target.TargetLines...)
}

func (t *TrPair) SourceWithoutPrefix() string {
	if len(t.SourceLines) == 0 {
		return ""
	}
	return strings.Join(t.SourceLines, "\n") + "\n"
}

func (t *TrPair) TargetWithoutPrefix() string {
	if len(t.TargetLines) == 0 {
		return ""
	}
	return strings.Join(t.TargetLines, "\n") + "\n"
}

func (t *TrPair) SourceWithPrefix() string {
	if len(t.SourceLines) == 0 {
		return ""
	}
	return t.Prefix + strings.Join(t.SourceLines, "\n"+t.Prefix) + "\n"
}

func (t *TrPair) TargetWithPrefix() string {
	if len(t.TargetLines) == 0 {
		return ""
	}
	return t.Prefix + strings.Join(t.TargetLines, "\n"+t.Prefix) + "\n"
}

func (t *TrPair) SourceWithPrefixWithoutLastNewLine() string {
	if len(t.SourceLines) == 0 {
		return ""
	}
	return t.Prefix + strings.Join(t.SourceLines, "\n"+t.Prefix)
}

func (t *TrPair) TargetWithPrefixWithoutLastNewLine() string {
	if len(t.TargetLines) == 0 {
		return ""
	}
	return t.Prefix + strings.Join(t.TargetLines, "\n"+t.Prefix)
}

const (
	DupNone      = iota // 重複していない
	DupSame             // 重複している(訳文は全くおなじ)
	DupDifferent        // 重複している(訳文が異なる)
)

func (t *TrPair) CheckDup(o *TrPair) int {
	if t.Prefix != o.Prefix {
		return DupNone
	}
	if len(t.SourceLines) != len(o.SourceLines) {
		return DupNone
	}
	for i := range t.SourceLines {
		if t.SourceLines[i] != o.SourceLines[i] {
			return DupNone
		}
	}
	if len(t.TargetLines) != len(o.TargetLines) {
		return DupDifferent
	}
	for i := range t.TargetLines {
		if t.TargetLines[i] != o.TargetLines[i] {
			return DupDifferent
		}
	}
	return DupSame
}

func EscapeSharp(s string) string {
	return strings.ReplaceAll(s, "#", "＃")
}

func UnescapeSharp(s string) string {
	return strings.ReplaceAll(s, "＃", "#")
}

type Translation struct {
	Pairs    []TrPair
	Filename string
}

const (
	SectionNone = iota
	SectionSource
	SectionTarget
)

func (t *Translation) AppendPair(tp TrPair) {
	for _, p := range t.Pairs {
		if p.IsSameSource(&tp) {
			fmt.Printf("すでに同じ原文の対訳が存在します。 old:%+v new:%+v\n", p, tp)
			return
		}
	}
	t.Pairs = append(t.Pairs, tp)
}

func (p *TrPair) IsSameSource(o *TrPair) bool {
	return p.SourceWithPrefix() == o.SourceWithPrefix()
}

func (t *Translation) ParseTranslationStr(translationStr string) {
	lines := strings.Split(translationStr, "\n")
	var n int
	var version string
	var section = SectionNone
	var prefix string
	if len(lines) > 0 && strings.HasPrefix(lines[0], "*") {
		version = strings.TrimSpace(lines[0][1:])
		n = 1
	}
	_ = version // 今は，バージョンによって処理を変化させていない
	var trLines []string
	var SourceLines []string
	var pairs []TrPair
	var lineNo int
	var tag string
	for ; n < len(lines); n++ {
		if strings.HasPrefix(lines[n], "#") {
			if section == SectionNone {
				prefix = UnescapeSharp(strings.TrimSuffix(strings.TrimSpace(lines[n])[1:], "#"))
				section = SectionSource
				lineNo = n + 1
			} else if section == SectionSource {
				SourceLines = append([]string{}, trLines...)
				trLines = []string{}
				section = SectionTarget
			} else {
				// ＃ を # に変換する
				for i := range SourceLines {
					SourceLines[i] = strings.ReplaceAll(SourceLines[i], "＃", "#")
				}
				for i := range trLines {
					trLines[i] = strings.ReplaceAll(trLines[i], "＃", "#")
				}
				trPair := TrPair{SourceLines, append([]string{}, trLines...), lineNo, prefix, tag}
				trLines = []string{}
				SourceLines = []string{}
				tag = ""
				/*
					for _, p := range pairs {
						dup := trPair.CheckDup(&p)
						if dup != DupNone {
							fmt.Printf("翻訳が重複しています： %s:%d ,%d, %s, %s\n", t.Filename, lineNo, p.LineNo, trPair.SourceWithoutPrefix(), trPair.TargetWithoutPrefix())
							break
						}
					}
				*/
				pairs = append(pairs, trPair)
				section = SectionNone
			}
		} else {
			if section == SectionNone {
				l := strings.TrimSpace(lines[n])
				if strings.HasPrefix(l, "[") && strings.HasSuffix(l, "]") {
					tag = strings.TrimSuffix(strings.TrimPrefix(l, "["), "]")
				} else if strings.HasPrefix(l, "<!--") && strings.HasSuffix(l, "-->") {
					fmt.Printf("%s:%d  %s\n", t.Filename, n+1, lines[n])
				} else if l != "" {
					fmt.Printf("警告: 本来空行であるべき部分に文字があります。 %s:%d  %s\n", t.Filename, n+1, lines[n])
					panic(lines[n])
				}
				continue
			} else {
				trLines = append(trLines, lines[n])
			}
		}
	}
	t.Pairs = pairs
}

var printNotAppliedTranslation = false

func (t *Translation) Apply(s string) string {
	for _, p := range t.Pairs {
		st := p.SourceWithPrefix()
		tt := p.TargetWithPrefix()
		snew := strings.ReplaceAll(s, st, tt)
		if printNotAppliedTranslation && snew == s && st != "" && st != tt {
			log.Printf("Info: 翻訳が適用されていません。 %s:%d ; %s : %s", t.Filename, p.LineNo, st, tt)
		}
		s = snew
	}
	return s
}

func (t *Translation) ApplyWithoutLastNewLine(s string) string {
	for _, p := range t.Pairs {
		st := p.SourceWithPrefixWithoutLastNewLine()
		tt := p.TargetWithPrefixWithoutLastNewLine()
		snew := strings.ReplaceAll(s, st, tt)
		/*
			if snew == s && st != "" && st != tt {
				log.Printf("Info: 翻訳が適用されていません。%s[%d]: %s : %s", t.Filename, p.LineNo, st, tt)
			}
		*/
		s = snew
	}
	return s
}

func (t *Translation) FormatTarget() {
	for i := range t.Pairs {
		t.Pairs[i].FormatTarget()
	}
}

func (t *Translation) ApplyTerminology(terminology *Terminology) {
	for i := range t.Pairs {
		t.Pairs[i].replaceByTerminology(terminology)
	}
}

func (t *Translation) String() string {
	var b bytes.Buffer
	b.WriteString("* 1\n\n")
	for _, p := range t.Pairs {
		if p.Tag != "" {
			b.WriteString("[" + p.Tag + "]\n")
		}
		b.WriteString("#" + EscapeSharp(p.Prefix) + "#\n")
		b.WriteString(EscapeSharp(p.SourceWithoutPrefix()))
		b.WriteString("#\n")
		b.WriteString(EscapeSharp(p.TargetWithoutPrefix()))
		b.WriteString("#\n\n")
	}
	return b.String()
}

func (t *Translation) RemoveDuplication() {
	var pairs []TrPair
	for i := range t.Pairs {
		var dup bool
		for j := 0; j < i; j++ {
			if t.Pairs[i].SourceWithPrefix() == t.Pairs[j].SourceWithPrefix() {
				dup = true
			}
		}
		if !dup {
			pairs = append(pairs, t.Pairs[i])
		}
	}
	t.Pairs = pairs
}

func (t *TrPair) FormatTarget() {
	t.replaceByBasicTerms()
	t.replaceParen()
	t.replaceSameSource(re1)
	t.replaceSameSource(re2)
	t.extractPrefix()
}

var prefixCandidates = []string{
	"  ",
	"    ",
	"      ",
	"        ",
	"\t",
	"\t\t",
	"\t\t\t",
	"// ",
	"//   ",
	"//     ",
	"  // ",
	"  //   ",
	"    // ",
	"      // ",
	"//\t",
	"// \t",
	"\t// ",
	"\t// \t",
	"\t//\t",
	"\t\t// ",
	"\t\t// \t",
	"\t\t//\t",
}

type LengthSort []string

func (l LengthSort) Len() int           { return len(l) }
func (l LengthSort) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l LengthSort) Less(i, j int) bool { return len(l[i]) > len(l[j]) }

func init() {
	sort.Sort(LengthSort(prefixCandidates))
}

func (t *TrPair) extractPrefix() {
	// すでに Prefix がある場合には変更しない
	if t.Prefix != "" {
		return
	}
	if len(t.SourceLines) == 0 {
		return
	}
	prefix := ""
	for _, p := range prefixCandidates {
		ok := true
		for _, s := range t.SourceLines {
			if !strings.HasPrefix(s, p) {
				ok = false
				break
			}
		}
		for _, s := range t.TargetLines {
			if !strings.HasPrefix(s, p) {
				ok = false
				break
			}
		}
		if ok {
			prefix = p
			break
		}
	}
	if prefix != "" {
		for i := range t.SourceLines {
			t.SourceLines[i] = strings.TrimPrefix(t.SourceLines[i], prefix)
		}
		for i := range t.TargetLines {
			t.TargetLines[i] = strings.TrimPrefix(t.TargetLines[i], prefix)
		}
		t.Prefix = prefix
	}
}

// replaceByBasicTerms は，全角英数字を半角英数字に置換する。
func (t *TrPair) replaceByBasicTerms() {
	for i := range t.TargetLines {
		s := t.TargetLines[i]
		for _, t := range BasicTerms {
			s = strings.Replace(s, t.S, t.T, -1)
		}
		t.TargetLines[i] = s
	}
}

func (t *TrPair) replaceByTerminology(terminology *Terminology) {
	for i := range t.TargetLines {
		s := t.TargetLines[i]
		s = terminology.Replace(s)
		t.TargetLines[i] = s
	}
}

// replaceParen は，"( "を"("に，" )" を ")"に変換します。
func (t *TrPair) replaceParen() {
	for i := range t.TargetLines {
		t.TargetLines[i] = strings.ReplaceAll(t.TargetLines[i], "( ", "(")
		t.TargetLines[i] = strings.ReplaceAll(t.TargetLines[i], " )", ")")
		t.TargetLines[i] = strings.ReplaceAll(t.TargetLines[i], "[ ", "[")
		t.TargetLines[i] = strings.ReplaceAll(t.TargetLines[i], " ]", "]")
	}
	/*
		var open bool
		for i := range t.TargetLines {
			var b bytes.Buffer
			var r0, r1 rune
			for _, r := range t.TargetLines[i] {
				var skip bool
				if r1 == '"' {
					open = !open
					if !open && r0 == ' ' {

					}
				}
				if r0 == ' ' && r1 == '"' {

				}
				r0, r1 = r1, r
			}
		}
	*/
}

var reg1 = `['"±|0-9a-zA-Z!=?<>/:;(){}%.&$#~\-+_*\[\],， ]+`
var reg2 = strings.ReplaceAll(reg1, "，", "")

var re1 = regexp.MustCompile(reg1)
var re2 = regexp.MustCompile(reg2)

// replaceSameSource は，source と target に同じ表現がある場合，source と同じように空白を入れる。
func (t *TrPair) replaceSameSource(re *regexp.Regexp) {
	ss := []string{}
	originalTarget := t.TargetWithoutPrefix()
	originalSource := t.SourceWithoutPrefix()
	matches := re.FindAllStringIndex(originalTarget, -1)
	ts := strings.Split(originalSource, "")
	for _, m := range matches {
		sm := strings.Trim(originalTarget[m[0]:m[1]], "， ")
		if len(sm) <= 1 {
			continue
		}
		s := strings.Split(sm, "")
		for p := 0; p < len(ts); p++ {
			if ts[p] != s[0] {
				continue
			}
			var i = 1
			var j = p + 1
			for j < len(originalSource) && i < len(s) {
				if s[i] == " " {
					i++
					continue
				}
				if ts[j] == " " {
					j++
					continue
				}
				if s[i] == ts[j] || (s[i] == "，" && ts[j] == ",") {
					i++
					j++
				} else {
					break
				}
			}
			if i == len(s) {
				ss = append(ss, strings.Join(ts[p:j], ""), sm)
			}
		}
	}
	for i := range t.TargetLines {
		line := t.TargetLines[i]
		for k := 0; k < len(ss)-1; k += 2 {
			line = strings.ReplaceAll(line, ss[k+1], ss[k])
		}
		t.TargetLines[i] = line
	}
}
