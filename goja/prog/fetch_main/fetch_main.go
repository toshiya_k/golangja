package main

import (
	"bytes"
	"flag"
	"golangja/golangja/goja/prog"
	"golangja/golangja/goja/prog/fetch"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

type Doc struct {
	URL      string
	filename string
}

func (d *Doc) Filename() string {
	if d.filename == "" {
		return d.URL
	}
	return d.filename
}

var docURL = []Doc{
	{"doc/asm", "doc/asm.html"},
	{"doc/cmd", "doc/cmd.html"},
	{"doc/code.html", ""},
	{"conduct", "doc/conduct.html"},
	{"project/", "doc/contrib.html"},
	{"doc/contribute.html", ""},
	{"doc/gdb", "doc/debugging_with_gdb.html"},
	{"doc/diagnostics.html", ""},
	{"doc/", "doc/docs.html"},
	{"doc/editors.html", ""},
	{"doc/effective_go.html", ""},
	{"doc/install/gccgo", "doc/gccgo_install.html"},
	{"doc/faq", "doc/go_faq.html"},
	{"ref/mem", "doc/go_mem.html"},
	{"ref/spec", "doc/go_spec.html"},
	{"doc/devel/release.html", ""},
	{"doc/go1.1", "doc/go1.1.html"},
	{"doc/go1.2", "doc/go1.2.html"},
	{"doc/go1.3", "doc/go1.3.html"},
	{"doc/go1.4", "doc/go1.4.html"},
	{"doc/go1.5", "doc/go1.5.html"},
	{"doc/go1.6", "doc/go1.6.html"},
	{"doc/go1.7", "doc/go1.7.html"},
	{"doc/go1.8", "doc/go1.8.html"},
	{"doc/go1.9", "doc/go1.9.html"},
	{"doc/go1.10", "doc/go1.10.html"},
	{"doc/go1.11", "doc/go1.11.html"},
	{"doc/go1.12", "doc/go1.12.html"},
	{"doc/go1", "doc/go1.html"},
	{"doc/go1compat", "doc/go1compat.html"},
	{"help/", "doc/help.html"},
	{"doc/install/source", "doc/install-source.html"},
	{"doc/install", "doc/install.html"},
	{"security", "doc/security.html"},
	{"doc/articles/go_command.html", ""},
	{"doc/articles/race_detector.html", ""},
}

var localhost = "http://localhost:6060/"

/*
var site = "/Users/juny/go/src/golangja/golangja/goja/appengine/site/"
var commonFilename = "/Users/juny/go/src/golangja/golangja/goja/prog/fetch/common.txt"
var rootFilename = "/Users/juny/go/src/golangja/golangja/goja/prog/fetch/root.txt"
var pkgFilename = "/Users/juny/go/src/golangja/golangja/goja/prog/fetch/pkg.txt"
*/

var gojaDir string

func main() {
	flag.StringVar(&gojaDir, "gojadir", "/Users/juny/go/src/golangja/golangja/goja/", "goja ディレクトリ")
	flag.Parse()

	site := filepath.Join(gojaDir, "appengine/site/")
	commonFilename := filepath.Join(gojaDir, "prog/fetch/common.txt")
	rootFilename := filepath.Join(gojaDir, "prog/fetch/root.txt")
	pkgFilename := filepath.Join(gojaDir, "prog/fetch/pkg.txt")

	commonTraB, err := ioutil.ReadFile(commonFilename)
	if err != nil {
		panic(err)
	}
	commonTra := new(translation.Translation)
	commonTra.ParseTranslationStr(string(commonTraB))
	rootTraB, err := ioutil.ReadFile(rootFilename)
	if err != nil {
		panic(err)
	}
	rootTra := new(translation.Translation)
	rootTra.ParseTranslationStr(string(rootTraB))
	pkgTraB, err := ioutil.ReadFile(pkgFilename)
	if err != nil {
		panic(err)
	}
	pkgTra := new(translation.Translation)
	pkgTra.ParseTranslationStr(string(pkgTraB))

	for _, d := range prog.PackageNames {
		fetch.SaveHTMLFile(localhost+"pkg/"+d+"/", filepath.Join(site, "pkg/", d, "/index.html"), nil, []*translation.Translation{commonTra})
	}
	for _, d := range prog.NotTranslatedPackageNames {
		fetch.SaveHTMLFile(localhost+"pkg/"+d+"/", filepath.Join(site, "pkg/", d, "/index.html"), nil, []*translation.Translation{commonTra})
	}
	for _, f := range docURL {
		fetch.SaveHTMLFile(localhost+f.URL, filepath.Join(site, f.Filename()), nil, []*translation.Translation{commonTra})
	}
	fetch.SaveHTMLFile(localhost, filepath.Join(site, "index.html"), nil, []*translation.Translation{rootTra, commonTra})
	fetch.SaveHTMLFile(localhost+"pkg/", filepath.Join(site, "pkg/index.html"), pkgReplace, []*translation.Translation{pkgTra, commonTra})

	MakeSitemap(site)
}

func pkgReplace(s string) string {
	i := strings.Index(s, `		<div id="thirdparty" class="toggleVisible">`)
	j := strings.Index(s, `	<h2 id="other">Other packages</h2>`)
	if i < 0 || j < 0 {
		log.Panicf("pkgReplace: i=%d, j=%d", i, j)
	}
	ret := s[:i] + s[j:]

	for _, pn := range prog.NotTranslatedPackageNames {
		ns := strings.Split(pn, "/")
		olds := `<a href="` + pn + `/">` + ns[len(ns)-1]
		news := `<a href="` + pn + `/">` + ns[len(ns)-1] + " *"
		ret = strings.Replace(ret, olds, news, 1)
	}
	return ret
}

func MakeSitemap(site string) {
	var b bytes.Buffer
	b.WriteString(baseSitemap)
	for _, pkg := range prog.PackageNames {
		b.WriteString("https://go言語.com/pkg/" + pkg + "/\n")
	}
	ioutil.WriteFile(filepath.Join(site, "sitemap.txt"), b.Bytes(), 0666)
}

var baseSitemap = `https://go言語.com/
https://go言語.com/pkg/
https://go言語.com/doc/
https://go言語.com/doc/install
https://go言語.com/doc/code.html
https://go言語.com/doc/editors.html
https://go言語.com/doc/cmd
https://go言語.com/project/
https://go言語.com/help/
`
