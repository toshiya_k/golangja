package main

import (
	"flag"
	"fmt"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

var tBaseDir string
var termsFilename string
var translatingPackagesStr string
var translatingPackages []string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	flag.StringVar(&termsFilename, "terms", "/Users/juny/go/src/golangja/golangja/goja/tra/terms.txt", "用語集ファイル名")
	flag.StringVar(&translatingPackagesStr, "pkgs", "", "翻訳するパッケージ カンマ区切りで指定できます。")
	flag.Parse()
	filename := "/Users/juny/go/src/golangja/golangja/goja/tra/tmp/update_comment"
	for i := 0; i < 100; i++ {
		sF := fmt.Sprintf("%s.%d.s.txt", filename, i)
		jaF := fmt.Sprintf("%s.%d.ja.txt", filename, i)
		mtF := fmt.Sprintf("%s.%d.mt.txt", filename, i)
		merge2(sF, jaF, mtF)
	}
	//merge2("/Users/juny/Documents/github.com/go/doc/effective_go.html.s.txt", "/Users/juny/Documents/github.com/go/doc/effective_go.html.ja.txt", "/Users/juny/Documents/github.com/go/doc/effective_go.html.mt.txt")
	//merge("/Users/juny/Documents/github.com/go/doc/effective_go.html.s.txt", "/Users/juny/Documents/github.com/go/doc/effective_go.html.ja.txt", "/Users/juny/Documents/github.com/go/doc/effective_go.html.mt.txt")
	/*
		translatingPackages = strings.Split(translatingPackagesStr, ",")
		translatingPackages = []string{
			"html/template",
			/*
				"database/sql/driver",
				"debug/dwarf",
				"debug/elf",
				"debug/gosym",
				"debug/macho",
				"debug/pe",
				"debug/plan9obj",
				"flag",
				"fmt",
				"go/ast",
				"go/build",
				"go/constant",
				"go/doc",
				"go/format",
				"go/importer",
				"go/parser",
				"go/printer",
				"go/scanner",
				"go/token",
				"go/types",
				"hash",
				"hash/adler32",
				"hash/crc32",
				"hash/crc64",
				"hash/fnv",
				"html/template",
				"image",
				"image/color",
				"image/color/palette",
				"image/draw",
				"image/gif",
				"image/jpeg",
				"image/png",
				"log/syslog",
				"math",
				"math/big",
				"math/bits",
				"math/cmplx",
				"math/rand",
				"mime",
				"mime/multipart",
				"mime/quotedprintable",
				"net",
				"net/http/cgi",
				"net/http/cookiejar",
				"net/http/fcgi",
				"net/http/httptest",
				"net/http/httptrace",
				"net/http/httputil",
				"net/http/pprof",
				"net/mail",
				"net/rpc",
				"net/rpc/jsonrpc",
				"net/smtp",
				"net/textproto",
				"net/url",
				"os",
				"os/exec",
				"os/signal",
				"os/user",
				"plugin",
				"reflect",
				"regexp",
				"regexp/syntax",
				"runtime",
				"runtime/cgo",
				"runtime/debug",
				"runtime/msan",
				"runtime/pprof",
				"runtime/race",
				"runtime/trace",
				"sync",
				"sync/atomic",
				"syscall",
				"syscall/js",
				"testing",
				"testing/iotest",
				"testing/quick",
				"text/scanner",
				"text/tabwriter",
				"text/template",
				"text/template/parse",
				"unicode",
				"unicode/utf16",
				"unicode/utf8",
				"unsafe",
	*/
	// }
	//mergePackages()
}

func merge2(enFile, jaFile, mtFile string) {
	enB, err := ioutil.ReadFile(enFile)
	if err != nil {
		panic(err)
	}
	jaB, err := ioutil.ReadFile(jaFile)
	if err != nil {
		panic(err)
	}
	terminology := translation.OpenTermsFile(termsFilename)

	var transEn = new(translation.Translation)
	transEn.Filename = enFile
	transEn.ParseTranslationStr(string(enB))
	println(transEn.String())
	var transJas = strings.Split(strings.ReplaceAll(string(jaB), "＃", "#"), "\n")
	if len(transEn.Pairs) != len(transJas) {
		log.Printf("Pairsの数が合いません。とばします。%s, en=%d, ja=%d", enFile, len(transEn.Pairs), len(transJas))
		return
	}
	for i := range transEn.Pairs {
		transEn.Pairs[i].TargetLines = []string{transJas[i]}
	}
	transEn.ApplyTerminology(terminology)
	transEn.FormatTarget()
	transEn.RemoveDuplication()
	err = ioutil.WriteFile(mtFile, []byte(transEn.String()), 0666)
	if err != nil {
		panic(err)
	}
}

func merge(enFile, jaFile, mtFile string) {
	enB, err := ioutil.ReadFile(enFile)
	if err != nil {
		panic(err)
	}
	jaB, err := ioutil.ReadFile(jaFile)
	if err != nil {
		panic(err)
	}
	terminology := translation.OpenTermsFile(termsFilename)

	var transEn = new(translation.Translation)
	transEn.Filename = enFile
	transEn.ParseTranslationStr(string(enB))
	println(transEn.String())
	var transJa = new(translation.Translation)
	transJa.Filename = jaFile
	transJa.ParseTranslationStr(strings.ReplaceAll(string(jaB), "＃", "#"))
	println(transJa.String())
	if len(transEn.Pairs) != len(transJa.Pairs) {
		log.Printf("Pairsの数が合いません。とばします。%s, en=%d, ja=%d", enFile, len(transEn.Pairs), len(transJa.Pairs))
		return
	}
	for i := range transEn.Pairs {
		transEn.Pairs[i].CopyTarget(transJa.Pairs[i])
	}
	transEn.ApplyTerminology(terminology)
	transEn.FormatTarget()
	transEn.RemoveDuplication()
	err = ioutil.WriteFile(mtFile, []byte(transEn.String()), 0666)
	if err != nil {
		panic(err)
	}
}

func mergePackages() error {
	for _, packageName := range translatingPackages {
		tDir := tBaseDir + "src/" + packageName
		sfiles, err := ioutil.ReadDir(tDir)
		if err != nil {
			return err
		}
		for _, sfile := range sfiles {
			if strings.HasSuffix(sfile.Name(), ".ja.txt") {
				jaFile := filepath.Join(tDir, sfile.Name())
				enFile := strings.ReplaceAll(jaFile, ".ja.txt", ".s.txt")
				mtFile := strings.ReplaceAll(jaFile, ".ja.txt", ".mt.txt")
				merge(enFile, jaFile, mtFile)
			}
		}
	}
	return nil
}
