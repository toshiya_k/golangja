package main

import (
	"flag"
	"golangja/golangja/goja/prog"
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

var sBaseDir string
var tBaseDir string
var jBaseDir string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	flag.StringVar(&sBaseDir, "sdir", "/Users/juny/Documents/github.com/go/", "go ソースコードのあるディレクトリ")
	flag.StringVar(&jBaseDir, "jdir", "/Users/juny/Documents/github.com/go_ja/", "日本語に翻訳した go ソースコードのあるディレクトリ")
	flag.Parse()

	var packageNames = []string{
		"log/syslog",
		/*
			"math",
			"math/big",
			"math/bits",
			"math/cmplx",
			"math/rand",
			"mime",
			"mime/multipart",
			"mime/quotedprintable",
			"net",
			"net/http",
			"net/http/cgi",
			"net/http/cookiejar",
			"net/http/fcgi",
			"net/http/httptest",
			"net/http/httptrace",
			"net/http/httputil",
			"net/http/pprof",
			"net/mail",
			"net/rpc",
			"net/rpc/jsonrpc",
			"net/smtp",
			"net/textproto",
			"net/url",
			"os",
			"os/exec",
			"os/signal",
			"os/user",
			"plugin",
			"reflect",
			"regexp",
			"regexp/syntax",
			"runtime",
			"runtime/cgo",
			"runtime/debug",
			"runtime/msan",
			"runtime/pprof",
			"runtime/race",
			"runtime/trace",
			"sync",
			"sync/atomic",
			"syscall",
			"syscall/js",
			"testing",
			"testing/iotest",
			"testing/quick",
			"text/scanner",
			"text/tabwriter",
			"text/template",
			"text/template/parse",
			"unicode",
			"unicode/utf16",
			"unicode/utf8",
			"unsafe",
		*/
	}

	packageNames = append(packageNames, prog.PackageNames...)
	packageNames = append(packageNames, prog.NotTranslatedPackageNames...)

	for _, packageName := range packageNames {
		err := createTranslatedFilesInDir(filepath.Join(sBaseDir, "src/", packageName), filepath.Join(tBaseDir, "src/", packageName), filepath.Join(jBaseDir, "src/", packageName), ".go")
		if err != nil {
			log.Fatalf("%#v", err)
		}
	}

	/*
		for _, packageName := range prog.PackageNames {
			err := createTranslatedFilesInDir(filepath.Join(sBaseDir, "src/", packageName), filepath.Join(tBaseDir, "src/", packageName), filepath.Join(jBaseDir, "src/", packageName), ".go")
			if err != nil {
				log.Fatalf("%#v", err)
			}
		}

		for _, packageName := range prog.NotTranslatedPackageNames {
			err := createTranslatedFilesInDir(filepath.Join(sBaseDir, "src/", packageName), filepath.Join(tBaseDir, "src/", packageName), filepath.Join(jBaseDir, "src/", packageName), ".go")
			if err != nil {
				log.Fatalf("%#v", err)
			}
		}
	*/

	err := createTranslatedFilesInDir(filepath.Join(sBaseDir, "doc/"), filepath.Join(tBaseDir, "doc/"), filepath.Join(jBaseDir, "doc/"), ".html")
	if err != nil {
		log.Fatalf("%#v", err)
	}

	err = createTranslatedFilesInDir(filepath.Join(sBaseDir, "doc/articles/"), filepath.Join(tBaseDir, "doc/articles/"), filepath.Join(jBaseDir, "doc/articles/"), ".html")
	if err != nil {
		log.Fatalf("%#v", err)
	}
}

func createTranslatedFile(sourceFilename, translationFilename, target string) error {
	//println(sourceFilename)
	sourceBytes, err := ioutil.ReadFile(sourceFilename)
	if err != nil {
		return err
	}
	translationBytes, err := ioutil.ReadFile(translationFilename)
	if err != nil {
		return err
	}
	var tr = new(translation.Translation)
	tr.Filename = translationFilename
	tr.ParseTranslationStr(string(translationBytes))
	targetStr := tr.Apply(string(sourceBytes))
	err = ioutil.WriteFile(target, []byte(targetStr), 0666)
	if err != nil {
		return err
	}
	return nil
}

func createTranslatedFilesInDir(sourceDir, translationDir, targetDir string, ext string) error {
	tfiles, err := ioutil.ReadDir(translationDir)
	if err != nil {
		return err
	}
	for _, tfile := range tfiles {
		if filepath.Ext(tfile.Name()) == ".txt" && len(strings.Split(tfile.Name(), ".")) == 2 {
			goFilename := strings.Replace(tfile.Name(), ".txt", ext, 1)
			err = createTranslatedFile(filepath.Join(sourceDir, goFilename), filepath.Join(translationDir, tfile.Name()), filepath.Join(targetDir, goFilename))
			if err != nil {
				return err
			}
		}
	}
	return nil
}
