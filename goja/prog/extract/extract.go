package extract

import (
	"golangja/golangja/goja/prog/translation"
	"strings"
)

func ExtractParagraphs(h string) ([]translation.TrPair, []translation.TrPair) {
	tps := []translation.TrPair{}
	tpt := []translation.TrPair{}
	ss := []string{}
	var inP bool
	lines := strings.Split(h, "\n")
	for i := range lines {
		if lines[i] == "<p>" {
			inP = true
			continue
		} else if lines[i] == "</p>" {
			inP = false
			if len(ss) > 0 {
				tps = append(tps, translation.TrPair{SourceLines: ss})
				tpt = append(tpt, translation.TrPair{TargetLines: []string{strings.Join(ss, " ")}})
			}
			ss = []string{}
			continue
		}
		if inP {
			ss = append(ss, lines[i])
		}
	}
	return tps, tpt
}
