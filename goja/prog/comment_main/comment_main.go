package main

import (
	"flag"
	"golangja/golangja/goja/prog/comment"
	"golangja/golangja/goja/prog/fetch"
	"io/ioutil"
	"path/filepath"
	"strings"
)

var tBaseDir string
var sBaseDir string
var translatingPackagesStr string
var translatingPackages []string
var fileFilter string

func main() {
	flag.StringVar(&tBaseDir, "tdir", "/Users/juny/go/src/golangja/golangja/goja/tra/", "翻訳ファイルのあるディレクトリ")
	flag.StringVar(&sBaseDir, "sdir", "/Users/juny/Documents/github.com/go/", "go ソースのディレクトリ")
	flag.StringVar(&translatingPackagesStr, "pkgs", "", "翻訳するパッケージ。カンマ区切りで，複数指定可能です。")
	flag.Parse()

	translatingPackages = strings.Split(translatingPackagesStr, ",")
	translatingPackages = []string{
		"html/template",
	}
	fileFilter = "examplefiles_test.go"
	readPackages()

	/*
		o, err := clipboard.ReadAll()
		if err != nil {
			panic(err)
		}
		comments := comment.ExtractComments(o)
		s := comment.FormatComments(comments)
		clipboard.WriteAll(s)
	*/
}

func readPackages() error {
	for _, packageName := range translatingPackages {
		html := fetch.FetchHTML("http://localhost:6060/pkg/" + packageName + "/")
		sDir := sBaseDir + "src/" + packageName
		tDir := tBaseDir + "src/" + packageName
		sfiles, err := ioutil.ReadDir(sDir)
		if err != nil {
			return err
		}
		for _, sfile := range sfiles {
			if filepath.Ext(sfile.Name()) == ".go" && (strings.Contains(html, ">"+sfile.Name()+"</a>") || strings.HasPrefix(sfile.Name(), "example")) && (fileFilter == "" || sfile.Name() == fileFilter) {
				sBytes, err := ioutil.ReadFile(filepath.Join(sDir, sfile.Name()))
				if err != nil {
					return err
				}
				comments := comment.ExtractComments(string(sBytes))
				err = ioutil.WriteFile(filepath.Join(tDir, strings.ReplaceAll(sfile.Name(), ".go", ".s.txt")), []byte(comment.FormatCommentsSource(comments)), 0666)
				if err != nil {
					return err
				}
				err = ioutil.WriteFile(filepath.Join(tDir, strings.ReplaceAll(sfile.Name(), ".go", ".t.txt")), []byte(comment.FormatCommentsTarget(comments)), 0666)
				if err != nil {
					return err
				}
				err = ioutil.WriteFile(filepath.Join(tDir, strings.ReplaceAll(sfile.Name(), ".go", ".ja.txt")), []byte(""), 0666)
			}
		}
	}
	return nil
}
