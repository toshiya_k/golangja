package main

import (
	"golangja/golangja/goja/prog/translation"
	"io/ioutil"
	"path/filepath"
	"strings"
)

func main() {
	translatedFilename := "/Users/juny/go/src/golangja/golangja/goja/tra/tmp/translated.txt"
	traBase := "/Users/juny/go/src/golangja/golangja/goja/tra/src/"
	tB, err := ioutil.ReadFile(translatedFilename)
	if err != nil {
		panic(err)
	}
	var trans translation.Translation
	trans.Filename = translatedFilename
	trans.ParseTranslationStr(string(tB))
	var transMap = make(map[string]*translation.Translation)
	for _, p := range trans.Pairs {
		if transMap[p.Tag] == nil {
			fn := strings.ReplaceAll(filepath.Join(traBase, p.Tag), ".go", ".txt")
			b, err := ioutil.ReadFile(fn)
			if err != nil {
				panic(err)
			}
			var t translation.Translation
			t.Filename = fn
			t.ParseTranslationStr(string(b))
			transMap[p.Tag] = &t
		}
		transMap[p.Tag].AppendPair(p)
	}
	for key, tr := range transMap {
		fn := strings.ReplaceAll(filepath.Join(traBase, key), ".go", ".txt")
		ioutil.WriteFile(fn, []byte(tr.String()), 0666)
	}
}
