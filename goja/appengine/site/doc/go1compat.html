<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-19425926-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-19425926-3');
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#375EAB">

  <title>Go 1 そして Go プログラムの未来 - Go 言語</title>

<link type="text/css" rel="stylesheet" href="/lib/godoc/style.css">


<script>window.initFuncs = [];</script>

<script src="/lib/godoc/jquery.js" defer></script>



<script>var goVersion = "go1.12.7";</script>
<script src="/lib/godoc/godocs.js" defer></script>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-6364714413801130",
          enable_page_level_ads: true
     });
</script>
</head>
<body>

<div id='lowframe' style="position: fixed; bottom: 0; left: 0; height: 0; width: 100%; border-top: thin solid grey; background-color: white; overflow: auto;">
...
</div><!-- #lowframe -->

<div id="topbar" class="wide"><div class="container">
<div class="top-heading" id="heading-wide"><a href="/">Go 言語</a></div>
<div class="top-heading" id="heading-narrow"><a href="/">Go 言語</a></div>
<a href="#" id="menu-button"><span id="menu-button-arrow">&#9661;</span></a>
<form method="GET" action="/search">
<div id="menu">
<a href="/doc/">ドキュメント</a>
<a href="/pkg/">パッケージ</a>
<a href="/project/">プロジェクト</a>
<a href="/help/">困ったら</a>


<span class="search-box" style="width:0;visibility: hidden;"><input type="search" id="search" name="q" placeholder="Search" aria-label="Search" required><button type="submit"><span><!-- magnifying glass: --><svg width="24" height="24" viewBox="0 0 24 24"><title>submit search</title><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path d="M0 0h24v24H0z" fill="none"/></svg></span></button></span>
</div>
</form>

</div></div>



<div id="page" class="wide">
<div class="container">


  <h1>
    Go 1 そして Go プログラムの未来
    <span class="text-muted"></span>
  </h1>







<div id="nav"></div>




<h2 id="introduction">Introduction</h2>
<p>
The release of Go version 1, Go 1 for short, is a major milestone
in the development of the language. Go 1 is a stable platform for
the growth of programs and projects written in Go.
</p>

<p>
Go 1 defines two things: first, the specification of the language;
and second, the specification of a set of core APIs, the "standard
packages" of the Go library. The Go 1 release includes their
implementation in the form of two compiler suites (gc and gccgo),
and the core libraries themselves.
</p>

<p>
It is intended that programs written to the Go 1 specification will
continue to compile and run correctly, unchanged, over the lifetime
of that specification. At some indefinite point, a Go 2 specification
may arise, but until that time, Go programs that work today should
continue to work even as future "point" releases of Go 1 arise (Go
1.1, Go 1.2, etc.).
</p>

<p>
Compatibility is at the source level. Binary compatibility for
compiled packages is not guaranteed between releases. After a point
release, Go source will need to be recompiled to link against the
new release.
</p>

<p>
The APIs may grow, acquiring new packages and features, but not in
a way that breaks existing Go 1 code.
</p>

<h2 id="expectations">Expectations</h2>

<p>
Although we expect that the vast majority of programs will maintain
this compatibility over time, it is impossible to guarantee that
no future change will break any program. This document is an attempt
to set expectations for the compatibility of Go 1 software in the
future. There are a number of ways in which a program that compiles
and runs today may fail to do so after a future point release. They
are all unlikely but worth recording.
</p>

<ul>
<li>
Security. A security issue in the specification or implementation
may come to light whose resolution requires breaking compatibility.
We reserve the right to address such security issues.
</li>

<li>
Unspecified behavior. The Go specification tries to be explicit
about most properties of the language, but there are some aspects
that are undefined. Programs that depend on such unspecified behavior
may break in future releases.
</li>

<li>
Specification errors. If it becomes necessary to address an
inconsistency or incompleteness in the specification, resolving the
issue could affect the meaning or legality of existing programs.
We reserve the right to address such issues, including updating the
implementations. Except for security issues, no incompatible changes
to the specification would be made.
</li>

<li>
Bugs. If a compiler or library has a bug that violates the
specification, a program that depends on the buggy behavior may
break if the bug is fixed. We reserve the right to fix such bugs.
</li>

<li>
Struct literals. For the addition of features in later point
releases, it may be necessary to add fields to exported structs in
the API. Code that uses unkeyed struct literals (such as pkg.T{3,
"x"}) to create values of these types would fail to compile after
such a change. However, code that uses keyed literals (pkg.T{A:
3, B: "x"}) will continue to compile after such a change. We will
update such data structures in a way that allows keyed struct
literals to remain compatible, although unkeyed literals may fail
to compile. (There are also more intricate cases involving nested
data structures or interfaces, but they have the same resolution.)
We therefore recommend that composite literals whose type is defined
in a separate package should use the keyed notation.
</li>

<li>
Methods. As with struct fields, it may be necessary to add methods
to types.
Under some circumstances, such as when the type is embedded in
a struct along with another type,
the addition of the new method may break
the struct by creating a conflict with an existing method of the other
embedded type.
We cannot protect against this rare case and do not guarantee compatibility
should it arise.
</li>

<li>
Dot imports. If a program imports a standard package
using <code>import . "path"</code>, additional names defined in the
imported package in future releases may conflict with other names
defined in the program.  We do not recommend the use of <code>import .</code>
outside of tests, and using it may cause a program to fail
to compile in future releases.
</li>

<li>
Use of package <code>unsafe</code>. Packages that import
<a href="/pkg/unsafe/"><code>unsafe</code></a>
may depend on internal properties of the Go implementation.
We reserve the right to make changes to the implementation
that may break such programs.
</li>

</ul>

<p>
Of course, for all of these possibilities, should they arise, we
would endeavor whenever feasible to update the specification,
compilers, or libraries without affecting existing code.
</p>

<p>
These same considerations apply to successive point releases. For
instance, code that runs under Go 1.2 should be compatible with Go
1.2.1, Go 1.3, Go 1.4, etc., although not necessarily with Go 1.1
since it may use features added only in Go 1.2
</p>

<p>
Features added between releases, available in the source repository
but not part of the numbered binary releases, are under active
development. No promise of compatibility is made for software using
such features until they have been released.
</p>

<p>
Finally, although it is not a correctness issue, it is possible
that the performance of a program may be affected by
changes in the implementation of the compilers or libraries upon
which it depends.
No guarantee can be made about the performance of a
given program between releases.
</p>

<p>
Although these expectations apply to Go 1 itself, we hope similar
considerations would be made for the development of externally
developed software based on Go 1.
</p>

<h2 id="subrepos">Sub-repositories</h2>

<p>
Code in sub-repositories of the main go tree, such as
<a href="//golang.org/x/net">golang.org/x/net</a>,
may be developed under
looser compatibility requirements. However, the sub-repositories
will be tagged as appropriate to identify versions that are compatible
with the Go 1 point releases.
</p>

<h2 id="operating_systems">Operating systems</h2>

<p>
It is impossible to guarantee long-term compatibility with operating
system interfaces, which are changed by outside parties.
The <a href="/pkg/syscall/"><code>syscall</code></a> package
is therefore outside the purview of the guarantees made here.
As of Go version 1.4, the <code>syscall</code> package is frozen.
Any evolution of the system call interface must be supported elsewhere,
such as in the
<a href="//golang.org/x/sys">go.sys</a> subrepository.
For details and background, see
<a href="//golang.org/s/go1.4-syscall">this document</a>.
</p>

<h2 id="tools">Tools</h2>

<p>
Finally, the Go toolchain (compilers, linkers, build tools, and so
on) is under active development and may change behavior. This
means, for instance, that scripts that depend on the location and
properties of the tools may be broken by a point release.
</p>

<p>
These caveats aside, we believe that Go 1 will be a firm foundation
for the development of Go and its ecosystem.
</p>


<div id="footer">
Build version go1.12.7.<br>
<a href="https://golang.org/doc/go1compat">翻訳元ページ</a><br>
Copyright 2019 Toshiya Kazuyoshi. All rights reserved.<br>
The content of this page is Japanese translation of <a href="https://golang.org">golang.org</a> website. Any use of this content is not permitted. 
The code of this page is licenced under <a href="/licence.txt">this license</a>.<br>
<a href="/privacy.html">Privacy Policy</a>
</div>

</div><!-- .container -->
</div><!-- #page -->

</body>
</html>

