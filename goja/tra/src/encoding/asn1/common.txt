* 1

#// #
ASN.1 objects have metadata preceding them:
  the tag: the type of the object
  a flag denoting if this object is compound or not
  the class type: the namespace of the tag
  the length of the object, in bytes
#
ASN.1 オブジェクトはメタデータを持っています:
  タグ: オブジェクトの型
  このオブジェクトが複合型であるかどうかを示すフラグ
  クラス型: タグの名前空間
  オブジェクトの長さ (バイト単位)
#

#// #
Here are some standard tags and classes
#
これが標準的なタグとクラスです。
#

#// #
ASN.1 tags represent the type of the following object.
#
ASN.1 タグは，次のオブジェクトの種類を表します。
#

#// #
ASN.1 class types represent the namespace of the tag.
#
ASN.1 クラス型はタグの名前空間を表します。
#

#// #
ASN.1 has IMPLICIT and EXPLICIT tags, which can be translated as "instead
of" and "in addition to". When not specified, every primitive type has a
default tag in the UNIVERSAL class.
#
ASN.1 には IMPLICIT タグと EXPLICIT タグがあり，これらは "の代わりに" と "に加えて" と翻訳できます。
指定しない場合，すべてのプリミティブ型には UNIVERSAL クラスのデフォルトタグがあります。
#

#// #
For example: a BIT STRING is tagged [UNIVERSAL 3] by default (although ASN.1
doesn't actually have a UNIVERSAL keyword). However, by saying [IMPLICIT
CONTEXT-SPECIFIC 42], that means that the tag is replaced by another.
#
例: デフォルトでは BIT STRING は [UNIVERSAL 3] とタグ付けされています (ただし， ASN.1 は実際には UNIVERSAL キーワードを持っていません) 。
しかし， [IMPLICIT CONTEXT-SPECIFIC 42] と言うことは，タグが別のタグに置き換えられることを意味します。
#

#// #
On the other hand, if it said [EXPLICIT CONTEXT-SPECIFIC 10], then an
/additional/ tag would wrap the default tag. This explicit tag will have the
compound flag set.
#
一方， [EXPLICIT CONTEXT-SPECIFIC 10] と表示されている場合， /additional/ タグはデフォルトタグをラップします。
この明示的タグには複合フラグが設定されます。
#

#// #
(This is used in order to remove ambiguity with optional elements.)
#
(これはオプションの要素のあいまいさを取り除くために使用されます)
#

#// #
You can layer EXPLICIT and IMPLICIT tags to an arbitrary depth, however we
don't support that here. We support a single layer of EXPLICIT or IMPLICIT
tagging with tag strings on the fields of a structure.
#
EXPLICIT タグと IMPLICIT タグを任意の深さまで重ねることができますが，ここではサポートしません。
構造体のフィールドにタグ文字列を使用して，単一層の EXPLICIT または IMPLICIT タグ付けをサポートします。
#

#// #
fieldParameters is the parsed representation of tag string from a structure field.
#
fieldParameters は，構造体フィールドからのタグ文字列の解析済み表現です。
#

#	optional     bool   // #
true iff the field is OPTIONAL
#
フィールドが OPTIONAL の場合は true
#

#	explicit     bool   // #
true iff an EXPLICIT tag is in use.
#
EXPLICIT タグが使用されている場合は true
#

#	application  bool   // #
true iff an APPLICATION tag is in use.
#
APPLICATION タグが使用されている場合は true
#

#	private      bool   // #
true iff a PRIVATE tag is in use.
#
PRIVATE タグが使用されている場合は true
#

#	defaultValue *int64 // #
a default value for INTEGER typed fields (maybe nil).
#
INTEGER 型付きフィールドのデフォルト値 (おそらく nil) 。
#

#	tag          *int   // #
the EXPLICIT or IMPLICIT tag (maybe nil).
#
EXPLICIT または IMPLICIT タグ (おそらく nil)
#

#	stringType   int    // #
the string tag to use when marshaling.
#
マーシャリング時に使用する文字列タグ
#

#	timeType     int    // #
the time tag to use when marshaling.
#
マーシャリング時に使用するタイムタグ。
#

#	set          bool   // #
true iff this should be encoded as a SET
#
これを SET としてエンコードする必要がある場合は true
#

#	omitEmpty    bool   // #
true iff this should be omitted if empty when marshaling.
#
マーシャリング時に空の場合，これを省略する必要がある場合は true
#

#	// #
Invariants:
  if explicit is set, tag is non-nil.
#
不変式 :explicit が設定されている場合， tag は nil 以外です。
#

#// #
Given a tag string with the format specified in the package comment,
parseFieldParameters will parse it into a fieldParameters structure,
ignoring unknown parts of the string.
#
パッケージのコメントで指定されたフォーマットのタグ文字列が与えられた場合， parseFieldParameters はそれを fieldParameters 構造に解析し，文字列の未知の部分を無視します。
#

#// #
Given a reflected Go type, getUniversalType returns the default tag number
and expected compound flag.
#
反射型 Go を指定すると， getUniversalType はデフォルトのタグ番号と予想される複合フラグを返します。
#

