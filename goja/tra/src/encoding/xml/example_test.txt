* 1

#// #
This example demonstrates unmarshaling an XML excerpt into a value with
some preset fields. Note that the Phone field isn't modified and that
the XML <Company> element is ignored. Also, the Groups field is assigned
considering the element path provided in its tag.
#
この例では， XML の抜粋をいくつかの事前設定フィールドを持つ値に非整列化する方法を示します。
Phone フィールドは変更されず， XML の <Company> 要素は無視されます。
また， Groups フィールドは，そのタグに指定されている要素パスを考慮して割り当てられます。
#

[encoding/xml/example_test.go]
#	// #
Output:
  <person id="13">
      <name>
          <first>John</first>
          <last>Doe</last>
      </name>
      <age>42</age>
      <Married>false</Married>
      <City>Hanga Roa</City>
      <State>Easter Island</State>
      <!-- Need more details. -->
  </person>
#
Output:
  <person id="13">
      <name>
          <first>John</first>
          <last>Doe</last>
      </name>
      <age>42</age>
      <Married>false</Married>
      <City>Hanga Roa</City>
      <State>Easter Island</State>
      <!-- Need more details. -->
  </person>
#

[encoding/xml/example_test.go]
#	// #
Output:
  <person id="13">
      <name>
          <first>John</first>
          <last>Doe</last>
      </name>
      <age>42</age>
      <Married>false</Married>
      <City>Hanga Roa</City>
      <State>Easter Island</State>
      <!-- Need more details. -->
  </person>
#
Output:
  <person id="13">
      <name>
          <first>John</first>
          <last>Doe</last>
      </name>
      <age>42</age>
      <Married>false</Married>
      <City>Hanga Roa</City>
      <State>Easter Island</State>
      <!-- Need more details. -->
  </person>
#

[encoding/xml/example_test.go]
#	// #
Output:
XMLName: xml.Name{Space:"", Local:"Person"}
Name: "Grace R. Emlin"
Phone: "none"
Email: [{home gre@example.com} {work gre@work.com}]
Groups: [Friends Squash]
Address: {Hanga Roa Easter Island}
#
Output:
XMLName: xml.Name{Space:"", Local:"Person"}
Name: "Grace R. Emlin"
Phone: "none"
Email: [{home gre@example.com} {work gre@work.com}]
Groups: [Friends Squash]
Address: {Hanga Roa Easter Island}
#


