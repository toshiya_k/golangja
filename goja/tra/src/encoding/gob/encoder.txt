* 1

#// #
An Encoder manages the transmission of type and data information to the
other side of a connection.  It is safe for concurrent use by multiple
goroutines.
#
Encoder は，接続の反対側への型とデータ情報の送信を管理します。
複数のゴルーチンによる平行使用は安全です。
#

#	mutex      sync.Mutex              // #
each item must be sent atomically
#
各項目はアトミックに送信する必要があります
#

#	w          []io.Writer             // #
where to send the data
#
データの送信先
#

#	sent       map[reflect.Type]typeId // #
which types we've already sent
#
私達が既に送った型
#

#	countState *encoderState           // #
stage for writing counts
#
カウントを書くための段階
#

#	freeList   *encoderState           // #
list of free encoderStates; avoids reallocation
#
無料の encoderStates のリスト。 再割り当てを回避
#

#	byteBuf    encBuffer               // #
buffer for top-level encoderState
#
最上位の encoderState のバッファ
#

#// #
Before we encode a message, we reserve space at the head of the
buffer in which to encode its length. This means we can use the
buffer to assemble the message without another allocation.
#
メッセージをエンコードする前に，その長さをエンコードするためにバッファーの先頭にスペースを予約します。
これは，他の割り当てなしでメッセージを組み立てるためにバッファを使用できることを意味します。
#

#const maxLength = 9 // #
Maximum size of an encoded length.
#
エンコードされた長さの最大サイズ
#

#// #
NewEncoder returns a new encoder that will transmit on the io.Writer.
#
NewEncoder は， io.Writer で送信する新しいエンコーダを返します。
#

#// #
writer() returns the innermost writer the encoder is using
#
writer() は，エンコーダが使用している最も内側のライターを返します。
#

#// #
pushWriter adds a writer to the encoder.
#
pushWriter はエンコーダーにライターを追加します。
#

#// #
popWriter pops the innermost writer.
#
popWriter は最も内側のライターをポップします。
#

#	if enc.err == nil { // #
remember the first.
#
最初を覚えています。
#

#// #
writeMessage sends the data item preceded by a unsigned count of its length.
#
writeMessage は，符号なしの長さのデータを先頭に付けてデータ項目を送信します。
#

#	// #
Space has been reserved for the length at the head of the message.
This is a little dirty: we grab the slice from the bytes.Buffer and massage
it by hand.
#
メッセージの先頭の長さのためにスペースが予約されています。
これは少し汚れています。
バイトからスライスを取得し，手でマッサージします。
#

#	// #
Length cannot be bigger than the decoder can handle.
#
長さは，デコーダが処理できるよりも大きくすることはできません。
#

#	// #
Encode the length.
#
長さをエンコードします。
#

#	// #
Copy the length to be a prefix of the message.
#
長さをコピーしてメッセージのプレフィックスにします。
#

#	// #
Write the data.
#
データを書き込みます。
#

#	// #
Drain the buffer and restore the space at the front for the count of the next message.
#
バッファを排出し，次のメッセージの数のために先頭のスペースを復元します。
#

#// #
sendActualType sends the requested type, without further investigation, unless
it's been sent before.
#
sendActualType は，それが以前に送信された場合を除き，リクエストされた型をさらに調査することなく送信します。
#

#	// #
Send the pair (-id, type)
Id:
#
ペアを送信します (-id ， type)
Id:
#

#	// #
Type:
#
型:
#

#	// #
Remember we've sent this type, both what the user gave us and the base type.
#
ユーザーが提供したものと基本型の両方をこの型で送信したことを忘れないでください。
#

#	// #
Now send the inner types
#
今すぐ内部型を送る
#

#// #
sendType sends the type info to the other side, if necessary.
#
必要に応じて， sendType は型情報を相手に送信します。
#

#		// #
The rules are different: regardless of the underlying type's representation,
we need to tell the other side that the base type is a GobEncoder.
#
基本的な型の表現に関係なく，基本型が GobEncoder であることを反対側に伝える必要があります。
#

#	// #
It's a concrete value, so drill down to the base type.
#
それは具体的な値なので，基本型にドリルダウンします。
#

#		// #
Basic types and interfaces do not need to be described.
#
基本的な型とインターフェースは説明する必要はありません。
#

#		// #
If it's []uint8, don't send; it's considered basic.
#
もしそれが []uint8 なら，送信しないでください。
それは基本的だと考えられています。
#

#		// #
Otherwise we do send.
#
それ以外の場合は送信します。
#

#		// #
arrays must be sent so we know their lengths and element types.
#
配列は長さと要素型がわかるように送信する必要があります。
#

#		// #
maps must be sent so we know their lengths and key/value types.
#
マップは長さとキー / 値型がわかるように送信する必要があります。
#

#		// #
structs must be sent so we know their fields.
#
構造体を送信する必要がありますので，私たちは彼らの分野を知っています。
#

#		// #
If we get here, it's a field of a struct; ignore it.
#
ここに来たら，それは構造体のフィールドです。
それを無視します。
#

#// #
Encode transmits the data item represented by the empty interface value,
guaranteeing that all necessary type information has been transmitted first.
Passing a nil pointer to Encoder will panic, as they cannot be transmitted by gob.
#
Encode は空のインターフェース値で表されるデータ項目を送信し，必要なすべての型情報が最初に送信されたことを保証します。
エンコーダに nil ポインタを渡すと，それらが gob によって送信され得ないのでパニックします。
#

#// #
sendTypeDescriptor makes sure the remote side knows about this type.
It will send a descriptor if this is the first time the type has been
sent.
#
sendTypeDescriptor は，リモート側がこの型を確実に知っているようにします。
型が初めて送信された場合は，記述子を送信します。
#

#	// #
Make sure the type is known to the other side.
First, have we already sent this type?
#
型が反対側に知られていることを確認してください。
まず，この型をすでに送信したことがありますか。
#

#		// #
No, so send it.
#
いいえ，送信してください。
#

#		// #
If the type info has still not been transmitted, it means we have
a singleton basic type (int, []byte etc.) at top level. We don't
need to send the type info but we do need to update enc.sent.
#
型情報がまだ送信されていない場合は，最上位にシングルトン基本型 (int, []byte など) があることを意味します。
型情報を送信する必要はありませんが， enc.sent を更新する必要があります。
#

#// #
sendTypeId sends the id, which must have already been defined.
#
sendTypeId は ID を送信します。
これはすでに定義されている必要があります。
#

#	// #
Identify the type of this top-level value.
#
この最上位値の型を識別します。
#

#// #
EncodeValue transmits the data item represented by the reflection value,
guaranteeing that all necessary type information has been transmitted first.
Passing a nil pointer to EncodeValue will panic, as they cannot be transmitted by gob.
#
EncodeValue はリフレクション値で表されるデータ項目を送信し，必要なすべての型情報が最初に送信されたことを保証します。
EncodeValue に nil ポインタを渡すと，それらが gob によって送信されることができないのでパニックします。
#

#	// #
Make sure we're single-threaded through here, so multiple
goroutines can share an encoder.
#
ここでシングルスレッドであることを確認してください。
そうすれば，複数のゴルーチンがエンコーダーを共有できます。
#

#	// #
Remove any nested writers remaining due to previous errors.
#
以前のエラーのために残っている入れ子のライターを削除します。
#

#	// #
Encode the object.
#
オブジェクトをエンコードします。
#

