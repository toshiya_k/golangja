* 1

#// #
Compact appends to dst the JSON-encoded src with
insignificant space characters elided.
#
Compact は，意味のない空白文字を取り除いた， JSON エンコードされた src を dst に追加します。
#

#		// #
Convert U+2028 and U+2029 (E2 80 A8 and E2 80 A9).
#
U+2028 と U+2029 を変換します (E2 80 A8 と E2 80 A9) 。
#

#// #
Indent appends to dst an indented form of the JSON-encoded src.
Each element in a JSON object or array begins on a new,
indented line beginning with prefix followed by one or more
copies of indent according to the indentation nesting.
The data appended to dst does not begin with the prefix nor
any indentation, to make it easier to embed inside other formatted JSON data.
Although leading space characters (space, tab, carriage return, newline)
at the beginning of src are dropped, trailing space characters
at the end of src are preserved and copied to dst.
For example, if src has no trailing spaces, neither will dst;
if src ends in a trailing newline, so will dst.
#
Indent は JSON エンコードされた，インデント付きの src を dst に追加します。
JSON オブジェクトまたは配列の各要素は，prefix で始まる新しいインデント行で始まり，
その後にインデントのネストに従って 1 つ以上の indent のコピーが続きます。
dst に追加されるデータは，他のフォーマットされた JSON データ内に埋め込むのを容易にするために，prefix やインデントで始まっていません。
src の先頭にある先頭の空白文字 (スペース，タブ，キャリッジリターン，改行) は削除されますが， src の末尾にある空白文字は保持され， dst にコピーされます。
たとえば， src に末尾のスペースがない場合は， dst の末尾にもスペースはありません。src の末尾が改行で終わっていれば， dst も末尾が改行で終わります。
#

#		// #
Emit semantically uninteresting bytes
(in particular, punctuation in strings) unmodified.
#
意味的に興味のないバイト (特に，文字列の句読点) を変更せずに出力します。
#

#		// #
Add spacing around real punctuation.
#
実際の句読点の周りにスペースを追加します。
#

#			// #
delay indent so that empty object and array are formatted as {} and [].
#
空のオブジェクトと配列が {} と [] としてフォーマットされるようにインデントを遅らせます。
#

#				// #
suppress indent in empty object/array
#
空のオブジェクト / 配列のインデントを抑制
#

