* 1

#// #
This file implements the Check function, which drives type-checking.
#
このファイルは型チェックを駆動する Check 関数を実装します。
#

#// #
debugging/development support
#
デバッグ / 開発サポート
#

#	debug = false // #
leave on during development
#
開発中はそのまま
#

#	trace = false // #
turn on for detailed type resolution traces
#
詳細な型解決トレースをオンにする
#

#// #
If Strict is set, the type-checker enforces additional
rules not specified by the Go 1 spec, but which will
catch guaranteed run-time errors if the respective
code is executed. In other words, programs passing in
Strict mode are Go 1 compliant, but not all Go 1 programs
will pass in Strict mode. The additional rules are:
#
strict が設定されている場合，型チェッカーは Go 1 仕様では指定されていない追加の規則を適用しますが，それぞれのコードが実行された場合に保証された実行時エラーを捕捉します。
つまり，厳密モードで渡されるプログラムは Go 1 に準拠していますが，すべての Go 1 プログラムが厳密モードで渡されるわけではありません。
追加の規則は以下のとおりです。
#

#// #
- A type assertion x.(T) where T is an interface type
  is invalid if any (statically known) method that exists
  for both x and T have different signatures.
#
- x と T の両方に存在する (静的に知られている) メソッドのシグニチャが異なる場合， T がインターフェース型である型アサーション x.(T) は無効です。
#

#// #
exprInfo stores information about an untyped expression.
#
exprInfo は型なし式に関する情報を格納します。
#

#	isLhs bool // #
expression is lhs operand of a shift with delayed type-check
#
expression が遅延型チェックを伴うシフトの lhs オペランドである
#

#	val   constant.Value // #
constant value; or nil (if not a constant)
#
定数値または nil (定数ではない場合)
#

#// #
A context represents the context within which an object is type-checked.
#
コンテキストは，オブジェクトが型チェックされるコンテキストを表します。
#

#	decl          *declInfo              // #
package-level declaration whose init expression/function body is checked
#
init 式 / 関数本体がチェックされるパッケージレベルの宣言
#

#	scope         *Scope                 // #
top-most scope for lookups
#
検索の最上位スコープ
#

#	pos           token.Pos              // #
if valid, identifiers are looked up as if at position pos (used by Eval)
#
有効な場合，識別子は pos の位置にあるかのように検索されます (Eval によって使用されます) 。
#

#	iota          constant.Value         // #
value of iota in a constant declaration; nil otherwise
#
定数宣言における iota の値それ以外の場合はゼロ
#

#	sig           *Signature             // #
function signature if inside a function; nil otherwise
#
関数内にある場合は関数シグネチャ。 それ以外の場合はゼロ
#

#	isPanic       map[*ast.CallExpr]bool // #
set of panic call expressions (used for termination check)
#
パニックコール表現のセット (終了チェックに使用)
#

#	hasLabel      bool                   // #
set if a function makes use of labels (only ~1% of functions); unused outside functions
#
関数がラベルを利用する場合に設定されます (関数の ~1% のみ) 。 未使用の外部関数
#

#	hasCallOrRecv bool                   // #
set if an expression contains a function call or channel receive operation
#
式に関数呼び出しまたはチャンネル受信操作が含まれる場合に設定されます
#

#// #
lookup looks up name in the current context and returns the matching object, or nil.
#
lookup は現在のコンテキストで name を検索し，一致するオブジェクトを返します。
または nil を返します。
#

#// #
An importKey identifies an imported package by import path and source directory
(directory containing the file containing the import). In practice, the directory
may always be the same, or may not matter. Given an (import path, directory), an
importer must always return the same package (but given two different import paths,
an importer may still return the same package by mapping them to the same package
paths).
#
importKey は，インポートパスとソースディレクトリ (インポートを含むファイルを含むディレクトリ) によってインポートされたパッケージを識別します。
実際には，ディレクトリは常に同じ場合もあれば，関係ない場合もあります。
(インポートパス，ディレクトリ) が与えられると，インポーターは常に同じパッケージを返さなければなりません (ただし， 2 つの異なるインポートパスが与えられても，インポーターは同じパッケージパスにマッピングすることによって同じパッケージを返すことができます) 。
#

#// #
A Checker maintains the state of the type checker.
It must be created with NewChecker.
#
Checker は型チェッカーの状態を管理します。
NewChecker で作成する必要があります。
#

#	// #
package information
(initialized by NewChecker, valid for the life-time of checker)
#
パッケージ情報 (NewChecker によって初期化され， checker の有効期間に有効)
#

#	objMap map[Object]*declInfo   // #
maps package-level objects and (non-interface) methods to declaration info
#
パッケージレベルのオブジェクトと (インターフェースではない) メソッドを宣言情報にマッピングします。
#

#	impMap map[importKey]*Package // #
maps (import path, source directory) to (complete or fake) package
#
(インポートパス，ソースディレクトリ) を (完全または偽の) パッケージにマッピングします。
#

#	// #
information collected during type-checking of a set of package files
(initialized by Files, valid only for the duration of check.Files;
maps and lists are allocated on demand)
#
一連のパッケージファイルの型チェック中に収集された情報 (Files によって初期化されます。
チェックの間だけ有効です。
ファイル ; マップとリストはリクエストに応じて割り当てられます)
#

#	files            []*ast.File                       // #
package files
#
パッケージファイル
#

#	unusedDotImports map[*Scope]map[*Package]token.Pos // #
positions of unused dot-imported packages for each file scope
#
各ファイルスコープに対する未使用のドットインポートパッケージの位置
#

#	firstErr error                 // #
first error encountered
#
最初のエラーが発生しました
#

#	methods  map[*TypeName][]*Func // #
maps package scope type names to associated non-blank, non-interface methods
#
パッケージスコープの型名を，関連付けられている空白以外の非インターフェースメソッドにマッピングします。
#

#	// #
TODO(gri) move interfaces up to the group of fields persistent across check.Files invocations (see also comment in Checker.initFiles)
#
TODO(gri) は， check.Files を呼び出しても持続するフィールドのグループにインターフェースを移動します (Checker.initFiles のコメントも参照) 。
#

#	interfaces map[*TypeName]*ifaceInfo // #
maps interface type names to corresponding interface infos
#
インターフェース型名を対応するインターフェース情報にマッピングする
#

#	untyped    map[ast.Expr]exprInfo    // #
map of expressions without final type
#
最終型なしの式のマップ
#

#	delayed    []func()                 // #
stack of delayed actions
#
遅延アクションのスタック
#

#	objPath    []Object                 // #
path of object dependencies during type inference (for cycle reporting)
#
型推論中のオブジェクト依存関係のパス (サイクルレポート用)
#

#	// #
context within which the current object is type-checked
(valid only for the duration of type-checking a specific object)
#
現在のオブジェクトが型チェックされているコンテキスト (特定のオブジェクトの型チェックが行われている間だけ有効)
#

#	// #
debugging
#
デバッグ
#

#	indent int // #
indentation for tracing
#
トレース用のインデント
#

#// #
addUnusedImport adds the position of a dot-imported package
pkg to the map of dot imports for the given file scope.
#
addUnusedImport は，ドットでインポートされたパッケージ pkg の位置を，指定されたファイルスコープのドットインポートのマップに追加します。
#

#// #
addDeclDep adds the dependency edge (check.decl -> to) if check.decl exists
#
check.decl が存在する場合， addDeclDep は依存関係エッジ (check.decl -> to) を追加します。
#

#		return // #
not in a package-level init expression
#
パッケージレベルの init 式にはない
#

#		return // #
to is not a package-level object
#
to はパッケージレベルのオブジェクトではありません
#

#// #
later pushes f on to the stack of actions that will be processed later;
either at the end of the current statement, or in case of a local constant
or variable declaration, before the constant or variable is in scope
(so that f still sees the scope before any new declarations).
#
後で f は後で処理されるアクションのスタックにプッシュします。
現在の文の末尾，またはローカル定数または変数宣言の場合は，定数または変数が有効範​​囲内になる前 (つまり， f は新しい宣言の前に有効範囲を認識します) 。
#

#// #
push pushes obj onto the object path and returns its index in the path.
#
push は， obj をオブジェクトパスにプッシュし，そのインデックスをパスに返します。
#

#// #
pop pops and returns the topmost object from the object path.
#
pop は，オブジェクトパスから一番上のオブジェクトをポップして返します。
#

#// #
NewChecker returns a new Checker instance for a given package.
Package files may be added incrementally via checker.Files.
#
NewChecker は与えられたパッケージの新しい Checker インスタンスを返します。
パッケージファイルは checker.Files を介して段階的に追加することができます。
#

#	// #
make sure we have a configuration
#
設定があることを確認してください
#

#	// #
make sure we have an info struct
#
情報構造体があることを確認してください
#

#// #
initFiles initializes the files-specific portion of checker.
The provided files must all belong to the same package.
#
initFiles は checker のファイル固有の部分を初期化します。
渡されたファイルはすべて同じパッケージに属している必要があります。
#

#	// #
start with a clean slate (check.Files may be called multiple times)
#
きれいな状態で始めてください (check.Files は複数回呼ばれるかもしれません)
#

#	// #
Don't clear the interfaces cache! It's important that we don't recompute
ifaceInfos repeatedly (due to multiple check.Files calls) because when
they are recomputed, they are not used in the context of their original
declaration (because those types are already type-checked, typically) and
then they will get the wrong receiver types, which matters for go/types
clients. It is also safe to not reset the interfaces cache because files
added to a package cannot change (add methods to) existing interface types;
they can only add new interfaces. See also the respective comment in
checker.infoFromTypeName (interfaces.go). Was bug - see issue #29029.
#
インターフェースのキャッシュをクリアしないでください。
ifaceInfos を繰り返し再計算しないことが重要です (複数の check.Files 呼び出しのため) 。
これらは再計算されるときに元の宣言のコンテキストでは使用されず (通常，これらの型は既に型チェックされているため) ，彼らは間違った受信機型を取得するでしょう，これは go/types クライアントにとって重要です。
パッケージに追加されたファイルは既存のインターフェース型を変更 (メソッドを追加) することができないため，インターフェースキャッシュをリセットしない方が安全です。
新しいインターフェースを追加することしかできません。
checker.infoFromTypeName (interfaces.go) のそれぞれのコメントも参照してください。
バグでした - issue #29029 を参照してください。
#

#	// #
determine package name and collect valid files
#
パッケージ名を決めて有効なファイルを集める
#

#			// #
ignore this file
#
このファイルを無視する
#

#// #
A bailout panic is used for early termination.
#
救済パニックは早期終了のために使用されます。
#

#		// #
normal return or early exit
#
通常の復帰または早期終了
#

#		// #
re-panic
#
再パニック
#

#// #
Files checks the provided files as part of the checker's package.
#
ファイルは渡されたファイルをチェッカーのパッケージの一部としてチェックします。
#

#	check.processDelayed(0) // #
incl. all functions
#
すべての関数を含む
#

#		return // #
nothing to do
#
何もしない
#

#		return // #
omit
#
省略
#

#	// #
f must be a (possibly parenthesized) identifier denoting a built-in
(built-ins in package unsafe always produce a constant result and
we don't record their signatures, so we don't see qualified idents
here): record the signature for f and possible children.
#
f は組み込みを示す (おそらく括弧でくくられた) 識別子でなければなりません (パッケージ内の組み込みは安全ではないため常に定数の結果が得られます。
署名は記録されないため，ここでは修飾識別子は表示されません) 。
f と可能な子供たち。
#

#			return // #
we're done
#
終了
#

#			assert(tv.Type != nil) // #
should have been recorded already
#
すでに記録されているはずです
#

#			// #
if x is a parenthesized expression (p.X), update p.X
#
x が括弧で囲まれた式 (p.X) の場合， p.X を更新する
#

