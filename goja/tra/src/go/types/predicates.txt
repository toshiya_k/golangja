* 1

#// #
This file implements commonly used type predicates.
#
このファイルはよく使われる型述語を実装します。
#

#// #
IsInterface reports whether typ is an interface type.
#
IsInterface は， typ がインターフェース型かどうかを報告します。
#

#// #
Comparable reports whether values of type T are comparable.
#
Comparable は，T 型の値が比較可能かどうかを報告します。
#

#		// #
assume invalid types to be comparable
to avoid follow-up errors
#
フォローアップエラーを避けるために，無効な型は同程度であると仮定する
#

#// #
hasNil reports whether a type includes the nil value.
#
hasNil は，型に nil 値が含まれているかどうかを報告します。
#

#// #
Identical reports whether x and y are identical types.
Receivers of Signature types are ignored.
#
Identical は，x と y が同じ型であるかどうかを報告します。
Signature 型の受信者は無視されます。
#

#// #
IdenticalIgnoreTags reports whether x and y are identical types if tags are ignored.
Receivers of Signature types are ignored.
#
IdenticalIgnoreTags は，タグが無視された場合に x と y が同じ型かどうかを報告します。
Signature 型の受信者は無視されます。
#

#// #
An ifacePair is a node in a stack of interface type pairs compared for identity.
#
ifacePair は，識別のために比較されるインターフェース型ペアのスタック内のノードです。
#

#		// #
Basic types are singletons except for the rune and byte
aliases, thus we cannot solely rely on the x == y check
above. See also comment in TypeName.IsAlias.
#
基本的な型はルーンとバイトのエイリアスを除いてシングルトンです。
したがって，上記の x == y チェックだけに頼ることはできません。
TypeName.IsAlias のコメントも参照してください。
#

#		// #
Two array types are identical if they have identical element types
and the same array length.
#
2 つの配列型が同じ要素型と同じ配列長を持つ場合，それらは同じです。
#

#			// #
If one or both array lengths are unknown (< 0) due to some error,
assume they are the same to avoid spurious follow-on errors.
#
片方または両方の配列長が何らかの誤差のために未知数 (< 0) である場合，誤った後続誤差を避けるためにそれらが同じであると仮定します。
#

#		// #
Two slice types are identical if they have identical element types.
#
2 つのスライス型が同じ要素型を持つ場合，それらは同じです。
#

#		// #
Two struct types are identical if they have the same sequence of fields,
and if corresponding fields have the same names, and identical types,
and identical tags. Two embedded fields are considered to have the same
name. Lower-case field names from different packages are always different.
#
2 つの構造体型が同じシーケンスのフィールドを持ち，対応するフィールドが同じ名前，同じ型，同じタグを持つ場合，それらは同じです。
2 つの埋め込みフィールドは同じ名前を持つと見なされます。
異なるパッケージからの小文字のフィールド名は常に異なります。
#

#		// #
Two pointer types are identical if they have identical base types.
#
2 つのポインタ型が同じ基本型を持つ場合，それらは同じです。
#

#		// #
Two tuples types are identical if they have the same number of elements
and corresponding elements have identical types.
#
2 つのタプル型が同じ数の要素を持ち，対応する要素が同じ型を持つ場合，それらは同じです。
#

#		// #
Two function types are identical if they have the same number of parameters
and result values, corresponding parameter and result types are identical,
and either both functions are variadic or neither is. Parameter and result
names are not required to match.
#
2 つの関数型が同じ数のパラメータと結果値を持ち，対応するパラメータと結果型が同じで，どちらの関数も可変長であるか，どちらでもない場合，それらは同じです。
パラメータ名と結果名は一致する必要はありません。
#

#		// #
Two interface types are identical if they have the same set of methods with
the same names and identical function types. Lower-case method names from
different packages are always different. The order of the methods is irrelevant.
#
2 つのインターフェース型が同じ名前と同じ関数型を持つ同じメソッドセットを持つ場合，それらは同じです。
異なるパッケージからの小文字のメソッド名は常に異なります。
メソッドの順序は関係ありません。
#

#				// #
Interface types are the only types where cycles can occur
that are not "terminated" via named types; and such cycles
can only be created via method parameter types that are
anonymous interfaces (directly or indirectly) embedding
the current interface. Example:
#
名前付きの型を介して " 終了 " されないサイクルが発生する可能性があるのは，インターフェース型だけです。
このようなサイクルは，現在のインターフェースを埋め込む匿名インターフェース (直接または間接) であるメソッドパラメータ型を介してのみ作成できます。
例 :
#

#				// #
If two such (differently named) interfaces are compared,
endless recursion occurs if the cycle is not detected.
#
そのような (異なる名前の) インターフェースが 2 つ比較されると，サイクルが検出されないと無限の再帰が発生します。
#

#				// #
If x and y were compared before, they must be equal
(if they were not, the recursion would have stopped);
search the ifacePair stack for the same pair.
#
x と y が以前に比較された場合，それらは等しくなければなりません (もしそうでなければ，再帰は停止していたでしょう) 。
ifacePair スタックで同じペアを検索してください。
#

#				// #
This is a quadratic algorithm, but in practice these stacks
are extremely short (bounded by the nesting depth of interface
type declarations that recur via parameter types, an extremely
rare occurrence). An alternative implementation might use a
"visited" map, but that is probably less efficient overall.
#
これは 2 次アルゴリズムですが，実際には，これらのスタックは極端に短くなります (パラメータ型を介して再帰するインターフェース型宣言の入れ子の深さによって制限されます) 。
別の実装では "visit" マップを使用するかもしれませんが，それはおそらく全体的に効率が悪いです。
#

#						return true // #
same pair was compared before
#
同じペアが以前に比較されました
#

#		// #
Two map types are identical if they have identical key and value types.
#
2 つのマップ型が同じキー型と値型を持つ場合，それらは同じです。
#

#		// #
Two channel types are identical if they have identical value types
and the same direction.
#
2 つのチャンネル型が同じ値型と同じ方向を持つ場合，それらは同じです。
#

#		// #
Two named types are identical if their type names originate
in the same type declaration.
#
2 つの名前付き型は，それらの型名が同じ型宣言に由来する場合，同一です。
#

#// #
Default returns the default "typed" type for an "untyped" type;
it returns the incoming type for all other types. The default type
for untyped nil is untyped nil.
#
Default は， " 型なし " 型のデフォルトの " 型付き " 型を返します。
他のすべての型の着信型を返します。
untyped nil のデフォルトの型は untyped nil です。
#

#			return universeRune // #
use 'rune' name
#
'rune' 名を使用
#

[go/types/predicates.go]
#				// #
   type T interface {
       m() interface{T}
   }
#
   type T interface {
       m() interface{T}
   }
#

