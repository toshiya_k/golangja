* 1

#	// #
src is the input that we want to tokenize.
#
src はトークン化したい入力です。
#

#	// #
Initialize the scanner.
#
スキャナを初期化します。
#

#	fset := token.NewFileSet()                      // #
positions are relative to fset
#
位置は fset に対する相対位置です
#

#	file := fset.AddFile("", fset.Base(), len(src)) // #
register input "file"
#
入力 "ファイル" を登録
#

#	// #
Repeated calls to Scan yield the token sequence found in the input.
#
Scan を繰り返し呼び出すと，入力に含まれるトークンシーケンスが得られます。
#

[go/scanner/example_test.go]
#	src := []byte("cos(x) + 1i*sin(x) // #
Euler")
#
Euler")
#

[go/scanner/example_test.go]
#	// #
output:
1:1	IDENT	"cos"
1:4	(	""
1:5	IDENT	"x"
1:6	)	""
1:8	+	""
1:10	IMAG	"1i"
1:12	*	""
1:13	IDENT	"sin"
1:16	(	""
1:17	IDENT	"x"
1:18	)	""
1:20	;	"\n"
1:20	COMMENT	"// Euler"
#
output:
1:1	IDENT	"cos"
1:4	(	""
1:5	IDENT	"x"
1:6	)	""
1:8	+	""
1:10	IMAG	"1i"
1:12	*	""
1:13	IDENT	"sin"
1:16	(	""
1:17	IDENT	"x"
1:18	)	""
1:20	;	"\n"
1:20	COMMENT	"// Euler"
#

