* 1

#// #
A Visitor's Visit method is invoked for each node encountered by Walk.
If the result visitor w is not nil, Walk visits each of the children
of node with the visitor w, followed by a call of w.Visit(nil).
#
Visitorの Visit メソッドは， Walk によって検出された各ノードに対して呼び出されます。
結果のVisitor w が nil でない場合， Walk はVisitor w と共に node の子のそれぞれを訪問し，続いて w.Visit(nil) を呼び出します。
#

#// #
Helper functions for common node lists. They may be empty.
#
共通ノードリスト用のヘルパー関数。 それらは空かもしれません。
#

#// #
TODO(gri): Investigate if providing a closure to Walk leads to
           simpler use (and may help eliminate Inspect in turn).
#
TODO(gri): Walk にクロージャを提供することがより簡単な使用につながるかどうかを調査します
(そして順番に Inspect を排除するのを助けるかもしれません) 。
#

#// #
Walk traverses an AST in depth-first order: It starts by calling
v.Visit(node); node must not be nil. If the visitor w returned by
v.Visit(node) is not nil, Walk is invoked recursively with visitor
w for each of the non-nil children of node, followed by a call of
w.Visit(nil).
#
Walk は AST を深さ優先の順序で走査します。
それは v.Visit(node) を呼び出すことで始まります。
node を nil にしないでください。
v.Visit(node) によって返されたVisitor w が nil ではない場合， node の nil 以外の子のそれぞれについて，Visitor w を使って Walk が再帰的に呼び出され，その後に w.Visit(nil) が呼び出されます。
#

#	// #
walk children
(the order of the cases matches the order
of the corresponding node types in ast.go)
#
子を歩く (ケースの順序は ast.go 内の対応するノード型の順序と一致します)
#

#	// #
Comments and fields
#
コメントとフィールド
#

#		// #
nothing to do
#
何もしない
#

#	// #
Expressions
#
式
#

#	// #
Types
#
型
#

#	// #
Statements
#
ステートメント
#

#	// #
Declarations
#
宣言
#

#	// #
Files and packages
#
ファイルとパッケージ
#

#		// #
don't walk n.Comments - they have been
visited already through the individual
nodes
#
n.Comments を歩いてはいけません - それらはすでに個々のノードを通して訪問されています
#

#// #
Inspect traverses an AST in depth-first order: It starts by calling
f(node); node must not be nil. If f returns true, Inspect invokes f
recursively for each of the non-nil children of node, followed by a
call of f(nil).
#
Inspect は AST を深さ優先の順序で走査します。
それは f(node) を呼び出すことから始まります。
node を nil にしないでください。
f が true を返す場合， Inspect は node の nil 以外の子のそれぞれに対して再帰的に f を呼び出し，その後に f(nil) を呼び出します。
#

