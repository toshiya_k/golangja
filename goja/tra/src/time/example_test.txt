* 1

#	// #
The package also accepts the incorrect but common prefix u for micro.
#
このパッケージは正しくはありませんが一般に使われる，マイクロを表す u も受け付けます。
#

#	// #
Parse a time value from a string in the standard Unix format.
#
標準の Unix フォーマットの文字列からタイム値を解析します。
#

#	if err != nil { // #
Always check errors even if they should not happen.
#
発生しないはずの場合でも，エラーを必ず確認してください。
#

#	// #
time.Time's Stringer method is useful without any format.
#
time.Time の Stringer メソッドはフォーマットなしで役に立ちます。
#

#	// #
Predefined constants in the package implement common layouts.
#
パッケージ内の定義済み定数は共通のレイアウトを実装します。
#

#	// #
The time zone attached to the time value affects its output.
#
タイム値に付加されたタイムゾーンはその出力に影響します。
#

#	// #
The rest of this function demonstrates the properties of the
layout string used in the format.
#
この関数の残りの部分は，フォーマットで使用されるレイアウト文字列のプロパティを示しています。
#

#	// #
The layout string used by the Parse function and Format method
shows by example how the reference time should be represented.
We stress that one must show how the reference time is formatted,
not a time of the user's choosing. Thus each layout string is a
representation of the time stamp,
#
Parse 関数と Format メソッドで使用されるレイアウト文字列は，
例を使って標準時刻を表しています。
任意の時間ではなく，標準時間がどのようにフォーマットされているかを
示す必要があります。
それで，各レイアウト文字列はタイムスタンプの表現です。
#

#	// #
An easy way to remember this value is that it holds, when presented
in this order, the values (lined up with the elements above):
#
この値を簡単に覚えるには，数字が順序に並んでいることに着目します。
#

#	// #
There are some wrinkles illustrated below.
#
以下に，コツをいくつかお教えしましょう。
#

#	// #
Most uses of Format and Parse use constant layout strings such as
the ones defined in this package, but the interface is flexible,
as these examples show.
#
Format と Parse のほとんどの使用法は，
このパッケージで定義されているような定数のレイアウト文字列を使用しますが，
例が示すようにインターフェースは柔軟です。
#

#	// #
Define a helper function to make the examples' output look nice.
#
例の出力を見栄えよくするために，ヘルパー関数を定義します。
#

#	// #
Print a header in our output.
#
出力にヘッダを表示します。
#

#	// #
A simple starter example.
#
簡単な例
#

#	// #
For fixed-width printing of values, such as the date, that may be one or
two characters (7 vs. 07), use an _ instead of a space in the layout string.
Here we print just the day, which is 2 in our layout string and 7 in our
value.
#
日付など， 1 文字または 2 文字 (7 と 07) の値を固定幅で表示するには，
レイアウト文字列のスペースの代わりに _ を使用します。
ここでは，日だけを表示します。
これは，レイアウト文字列では 2 ，値では 7 です。
#

#	// #
An underscore represents a space pad, if the date only has one digit.
#
日付が 1 桁の場合，アンダースコアはスペースパディングを表します。
#

#	// #
A "0" indicates zero padding for single-digit values.
#
"0" は， 1 桁の値に対するゼロパディングを示します。
#

#	// #
If the value is already the right width, padding is not used.
For instance, the second (05 in the reference time) in our value is 39,
so it doesn't need padding, but the minutes (04, 06) does.
#
値がすでに正しい幅の場合，パディングは使用されません。
たとえば，2 番目 (標準時刻の 05) の値は 39 なので，
パディングは不要です。ただし，分 (04, 06) はパディングされます。
#

#	// #
The predefined constant Unix uses an underscore to pad the day.
Compare with our simple starter example.
#
あらかじめ定義された定数 Unix は日をパディングするためにアンダースコアを使います。簡単な例と比較してください。
#

#	// #
The hour of the reference time is 15, or 3PM. The layout can express
it either way, and since our value is the morning we should see it as
an AM time. We show both in one format string. Lower case too.
#
基準時間の時間は 15 ，または午後 3 時です。レイアウトはどちらの方法でも表現することができ，私たちの価値は朝なので，私たちはそれを AM 時間と見なすべきです。両方を 1 つのフォーマット文字列で表示します。小文字も。
#

#	// #
When parsing, if the seconds value is followed by a decimal point
and some digits, that is taken as a fraction of a second even if
the layout string does not represent the fractional second.
Here we add a fractional second to our time value used above.
#
パース時に，秒の値の後に小数点と数桁が続く場合は，
レイアウト文字列が小数秒を表していなくても，
1 秒の何分の 1 と見なされます。
ここで，上で使用した時間値に小数秒を追加します。
#

#	// #
It does not appear in the output if the layout string does not contain
a representation of the fractional second.
#
レイアウト文字列に小数秒の表現が含まれていない場合，
出力に表示されません。
#

#	// #
Fractional seconds can be printed by adding a run of 0s or 9s after
a decimal point in the seconds value in the layout string.
If the layout digits are 0s, the fractional second is of the specified
width. Note that the output has a trailing zero.
#
小数秒は，レイアウト文字列の秒数の小数点の後に 0 または 9 の数字列を追加することによって表示できます。
レイアウト桁が 0 の場合，小数秒は指定された幅になります。
出力の末尾にゼロがあることに注意してください。
#

#	// #
If the fraction in the layout is 9s, trailing zeros are dropped.
#
レイアウトの小数部が 9 の場合，末尾のゼロは削除されます。
#

#	// #
See the example for Time.Format for a thorough description of how
to define the layout string to parse a time.Time value; Parse and
Format use the same model to describe their input and output.
#
time.Time 値をパースするためのレイアウト文字列の定義方法の詳細については，
Time.Format の例を参照してください。
パースとフォーマットは，入力と出力を記述するために同じモデルを使用します。
#

#	// #
longForm shows by example how the reference time would be represented in
the desired layout.
#
longForm は，例として，
標準時刻が希望のレイアウトでどのように表されるかを示します。
#

#	// #
shortForm is another way the reference time would be represented
in the desired layout; it has no time zone present.
Note: without explicit zone, returns time in UTC.
#
shortForm は，標準時間を目的のレイアウトで表現するもう 1 つの方法です。
タイムゾーンはありません。
注:明示的ゾーンなしでは， UTC で時間を返します。
#

#	// #
Some valid layouts are invalid time values, due to format specifiers
such as _ for space padding and Z for zone information.
For example the RFC3339 layout 2006-01-02T15:04:05Z07:00
contains both Z and a time zone offset in order to handle both valid options:
2006-01-02T15:04:05Z
2006-01-02T15:04:05+07:00
#
有効なレイアウトの中には無効な時間値があるものがあります。
なぜなら，スペースの埋め込みには _ ，ゾーン情報には Z などのフォーマット指定子があるためです。
たとえば， RFC3339 のレイアウト 2006-01-02T15:04:05Z07:00 には，
次の 2 つの有効な文字列を処理するために Z とタイムゾーンオフセットの両方が含まれています。
2006-01-02T15:04:05Z
2006-01-02T15:04:05+07:00
#

#	fmt.Println("error", err) // #
Returns an error as the layout is not a valid time value
#
レイアウトが有効な時間値ではないため，エラーを返します
#

#	// #
Note: without explicit zone, returns time in given location.
#
注:明示的なタイムゾーンなしで，与えられたロケーションで時間を返します。
#

#	// #
1 billion seconds of Unix, three ways.
#
10 億秒の Unix時間, 3 つの方法。
#

#	fmt.Println(time.Unix(1e9, 0).UTC())     // #
1e9 seconds
#
1e9 秒
#

#	fmt.Println(time.Unix(0, 1e18).UTC())    // #
1e18 nanoseconds
#
1e18 ナノ秒
#

#	fmt.Println(time.Unix(2e9, -1e18).UTC()) // #
2e9 seconds - 1e18 nanoseconds
#
2e9 秒 - 1e18 ナノ秒
#

#	fmt.Println(t.Unix())     // #
seconds since 1970
#
1970 年からの秒数
#

#	fmt.Println(t.UnixNano()) // #
nanoseconds since 1970
#
1970 年からのナノ秒数
#

#	// #
To round to the last midnight in the local timezone, create a new Date.
#
現地のタイムゾーンの最後の真夜中に丸めるため，新しい Date を作成します。
#

#	// #
China doesn't have daylight saving. It uses a fixed 8 hour offset from UTC.
#
中国には夏時間がありません。 UTC からの固定 8 時間オフセットを使用します。
#

#	// #
If the system has a timezone database present, it's possible to load a location
from that, e.g.:
#
システムにタイムゾーンデータベースが存在する場合は，そこからロケーションをロードすることができます。例:
#

#	// #
Creating a time requires a location. Common locations are time.Local and time.UTC.
#
時間を作成するにはロケーションが必要です。一般的なロケーションは time.Local と time.UTC です。
#

#	// #
Although the UTC clock time is 1200 and the Beijing clock time is 2000, Beijing is
8 hours ahead so the two dates actually represent the same instant.
#
UTC の時刻は 1200 ，北京の時刻は 2000 ですが，北京は 8 時間先であるため， 2 つの日付は実際には同じ時刻を表します。
#

#	// #
Unlike the equal operator, Equal is aware that d1 and d2 are the
same instant but in different time zones.
#
== 演算子とは異なり， Equal は d1 と d2 が同じ時刻であるが異なる時間帯にあることを認識しています。
#

[time/example_test.go]
#	isYear3000AfterYear2000 := year3000.After(year2000) // #
True
#
True
#

[time/example_test.go]
#	isYear2000AfterYear3000 := year2000.After(year3000) // #
False
#
False
#

[time/example_test.go]
#	// #
Output:
year3000.After(year2000) = true
year2000.After(year3000) = false
#
Output:
year3000.After(year2000) = true
year2000.After(year3000) = false
#

[time/example_test.go]
#	isYear2000BeforeYear3000 := year2000.Before(year3000) // #
True
#
True
#

[time/example_test.go]
#	isYear3000BeforeYear2000 := year3000.Before(year2000) // #
False
#
False
#

[time/example_test.go]
#	// #
Output:
year2000.Before(year3000) = true
year3000.Before(year2000) = false
#
Output:
year2000.Before(year3000) = true
year3000.Before(year2000) = false
#

[time/example_test.go]
#	// #
Output:
year = 2000
month = February
day = 1
#
Output:
year = 2000
month = February
day = 1
#

[time/example_test.go]
#	// #
Output:
day = 1
#
Output:
day = 1
#

[time/example_test.go]
#	// #
Output:
datesEqualUsingEqualOperator = false
datesEqualUsingFunction = true
#
Output:
datesEqualUsingEqualOperator = false
datesEqualUsingFunction = true
#

[time/example_test.go]
#	// #
Output:
withNanoseconds = 2000-02-01 12:13:14.000000015 +0000 UTC
withoutNanoseconds = 2000-02-01 12:13:14 +0000 UTC
#
Output:
withNanoseconds = 2000-02-01 12:13:14.000000015 +0000 UTC
withoutNanoseconds = 2000-02-01 12:13:14 +0000 UTC
#

[time/example_test.go]
#	// #
Output:
difference = 12h0m0s
#
Output:
difference = 12h0m0s
#

[time/example_test.go]
#	// #
Output:
Time: 11:00AM
#
Output:
Time: 11:00AM
#

[time/example_test.go]
#	// #
Output: The time is: 10 Nov 09 23:00 UTC-8
#
Output: The time is: 10 Nov 09 23:00 UTC-8
#

[time/example_test.go]
#	// #
Output: 4440h0m0s
#
Output: 4440h0m0s
#

[time/example_test.go]
#	// #
Output: I've got 4.5 hours of work left.
#
Output: I've got 4.5 hours of work left.
#

[time/example_test.go]
#	// #
Output: The movie is 90 minutes long.
#
Output: The movie is 90 minutes long.
#

[time/example_test.go]
#	// #
Output:
One microsecond is 1000 nanoseconds.
#
Output:
One microsecond is 1000 nanoseconds.
#

[time/example_test.go]
#	// #
Output: Take off in t-90 seconds.
#
Output: Take off in t-90 seconds.
#

[time/example_test.go]
#	// #
Output: Go launched at 2009-11-10 15:00:00 -0800 PST
#
Output: Go launched at 2009-11-10 15:00:00 -0800 PST
#

[time/example_test.go]
#	//	#
Jan 2 15:04:05 2006 MST
#
Jan 2 15:04:05 2006 MST
#

[time/example_test.go]
#	//	#
  1 2  3  4  5    6  -7
#
  1 2  3  4  5    6  -7
#

[time/example_test.go]
#	// #
Output:
default format: 2015-03-07 11:06:39 -0800 PST
Unix format: Sat Mar  7 11:06:39 PST 2015
Same, in UTC: Sat Mar  7 19:06:39 UTC 2015
#
Output:
default format: 2015-03-07 11:06:39 -0800 PST
Unix format: Sat Mar  7 11:06:39 PST 2015
Same, in UTC: Sat Mar  7 19:06:39 UTC 2015
#

[time/example_test.go]
#	// #
Formats:
#
Formats:
#

[time/example_test.go]
#	// #
Basic           "Mon Jan 2 15:04:05 MST 2006" gives "Sat Mar 7 11:06:39 PST 2015"
No pad          "<2>" gives "<7>"
Spaces          "<_2>" gives "< 7>"
Zeros           "<02>" gives "<07>"
Suppressed pad  "04:05" gives "06:39"
Unix            "Mon Jan _2 15:04:05 MST 2006" gives "Sat Mar  7 11:06:39 PST 2015"
AM/PM           "3PM==3pm==15h" gives "11AM==11am==11h"
No fraction     "Mon Jan _2 15:04:05 MST 2006" gives "Sat Mar  7 11:06:39 PST 2015"
0s for fraction "15:04:05.00000" gives "11:06:39.12340"
9s for fraction "15:04:05.99999999" gives "11:06:39.1234"
#
Basic           "Mon Jan 2 15:04:05 MST 2006" gives "Sat Mar 7 11:06:39 PST 2015"
No pad          "<2>" gives "<7>"
Spaces          "<_2>" gives "< 7>"
Zeros           "<02>" gives "<07>"
Suppressed pad  "04:05" gives "06:39"
Unix            "Mon Jan _2 15:04:05 MST 2006" gives "Sat Mar  7 11:06:39 PST 2015"
AM/PM           "3PM==3pm==15h" gives "11AM==11am==11h"
No fraction     "Mon Jan _2 15:04:05 MST 2006" gives "Sat Mar  7 11:06:39 PST 2015"
0s for fraction "15:04:05.00000" gives "11:06:39.12340"
9s for fraction "15:04:05.99999999" gives "11:06:39.1234"
#

[time/example_test.go]
#	// #
Output:
2013-02-03 19:54:00 -0800 PST
2013-02-03 00:00:00 +0000 UTC
2006-01-02 15:04:05 +0000 UTC
2006-01-02 15:04:05 +0700 +0700
error parsing time "2006-01-02T15:04:05Z07:00": extra text: 07:00
#
Output:
2013-02-03 19:54:00 -0800 PST
2013-02-03 00:00:00 +0000 UTC
2006-01-02 15:04:05 +0000 UTC
2006-01-02 15:04:05 +0700 +0700
error parsing time "2006-01-02T15:04:05Z07:00": extra text: 07:00
#

[time/example_test.go]
#	// #
Output:
2012-07-09 05:02:00 +0200 CEST
2012-07-09 00:00:00 +0200 CEST
#
Output:
2012-07-09 05:02:00 +0200 CEST
2012-07-09 00:00:00 +0200 CEST
#

[time/example_test.go]
#	// #
Output:
2001-09-09 01:46:40 +0000 UTC
2001-09-09 01:46:40 +0000 UTC
2001-09-09 01:46:40 +0000 UTC
1000000000
1000000000000000000
#
Output:
2001-09-09 01:46:40 +0000 UTC
2001-09-09 01:46:40 +0000 UTC
2001-09-09 01:46:40 +0000 UTC
1000000000
1000000000000000000
#

[time/example_test.go]
#	// #
Output: 2018-08-30 05:00:00 -0700 PDT
#
Output: 2018-08-30 05:00:00 -0700 PDT
#

[time/example_test.go]
#	// #
Output:
true
#
Output:
true
#

[time/example_test.go]
#	// #
Output:
start = 2009-01-01 12:00:00 +0000 UTC
start.Add(time.Second * 10) = 2009-01-01 12:00:10 +0000 UTC
start.Add(time.Minute * 10) = 2009-01-01 12:10:00 +0000 UTC
start.Add(time.Hour * 10) = 2009-01-01 22:00:00 +0000 UTC
start.Add(time.Hour * 24 * 10) = 2009-01-11 12:00:00 +0000 UTC
#
Output:
start = 2009-01-01 12:00:00 +0000 UTC
start.Add(time.Second * 10) = 2009-01-01 12:00:10 +0000 UTC
start.Add(time.Minute * 10) = 2009-01-01 12:10:00 +0000 UTC
start.Add(time.Hour * 10) = 2009-01-01 22:00:00 +0000 UTC
start.Add(time.Hour * 24 * 10) = 2009-01-11 12:00:00 +0000 UTC
#

[time/example_test.go]
#	// #
Output:
oneDayLater: start.AddDate(0, 0, 1) = 2009-01-02 00:00:00 +0000 UTC
oneMonthLater: start.AddDate(0, 1, 0) = 2009-02-01 00:00:00 +0000 UTC
oneYearLater: start.AddDate(1, 0, 0) = 2010-01-01 00:00:00 +0000 UTC
#
Output:
oneDayLater: start.AddDate(0, 0, 1) = 2009-01-02 00:00:00 +0000 UTC
oneMonthLater: start.AddDate(0, 1, 0) = 2009-02-01 00:00:00 +0000 UTC
oneYearLater: start.AddDate(1, 0, 0) = 2010-01-01 00:00:00 +0000 UTC
#

