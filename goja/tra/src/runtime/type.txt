* 1

#// #
Runtime type representation.
#
ランタイム型の表現
#

#// #
tflag is documented in reflect/type.go.
#
tflag は reflect/type.go に文書化されています。
#

#// #
tflag values must be kept in sync with copies in:
#
tflag 値は，次の場所にあるコピーと同期している必要があります。
#

#	ptrdata    uintptr // #
size of memory prefix holding all pointers
#
すべてのポインタを保持するメモリプレフィックスのサイズ
#

#	// #
gcdata stores the GC type data for the garbage collector.
If the KindGCProg bit is set in kind, gcdata is a GC program.
Otherwise it is a ptrmask bitmap. See mbitmap.go for details.
#
gcdata はガベージコレクタの GC 型データを格納します。
KindGCProg ビットがその種類で設定されている場合， gcdata は GC プログラムです。
それ以外の場合は ptrmask ビットマップです。
詳細は mbitmap.go を見てください。
#

#// #
pkgpath returns the path of the package where t was defined, if
available. This is not the same as the reflect package's PkgPath
method, in that it returns the package path for struct and interface
types, not just named types.
#
pkg パスは， t が定義されているパッケージのパスがあればそれを返します。
これはリフレクトパッケージの PkgPath メソッドと同じではありません。
名前付きの型だけでなく，構造体型とインターフェース型のパッケージパスを返すという点です。
#

#// #
reflectOffs holds type offsets defined at run time by the reflect package.
#
reflectOffs は，実行時に reflect パッケージによって定義された型オフセットを保持します。
#

#// #
When a type is defined at run time, its *rtype data lives on the heap.
There are a wide range of possible addresses the heap may use, that
may not be representable as a 32-bit offset. Moreover the GC may
one day start moving heap memory, in which case there is no stable
offset that can be defined.
#
型が実行時に定義されると，その *rtype データはヒープ上に存在します。
ヒープが使用する可能性のあるアドレスは広範囲にありますが， 32 ビットオフセットとして表現できない場合があります。
さらに， GC はいつかヒープメモリの移動を開始する場合があります。
その場合，定義できる安定したオフセットはありません。
#

#// #
To provide stable offsets, we add pin *rtype objects in a global map
and treat the offset as an identifier. We use negative offsets that
do not overlap with any compile-time module offsets.
#
安定したオフセットを提供するために，グローバルマップに pin *rtype オブジェクトを追加し，オフセットを識別子として扱います。
コンパイル時のモジュールオフセットと重ならない負のオフセットを使用します。
#

#// #
Entries are created by reflect.addReflectOff.
#
エントリは reflect.addReflectOff によって作成されます。
#

#	// #
No module found. see if it is a run time name.
#
モジュールが見つかりませんでした。
実行時の名前かどうかを確認してください。
#

#	// #
The text, or instruction stream is generated as one large buffer.  The off (offset) for a method is
its offset within this buffer.  If the total text size gets too large, there can be issues on platforms like ppc64 if
the target of calls are too far for the call instruction.  To resolve the large text issue, the text is split
into multiple text sections to allow the linker to generate long calls when necessary.  When this happens, the vaddr
for each text section is set to its offset within the text.  Each method's offset is compared against the section
vaddrs and sizes to determine the containing section.  Then the section relative offset is added to the section's
relocated baseaddr to compute the method addess.
#
テキストまたは命令ストリームは， 1 つの大きなバッファとして生成されます。
メソッドの off (オフセット) は，このバッファ内のオフセットです。
合計テキストサイズが大きくなりすぎると，呼び出し先が呼び出し命令に対して遠すぎる場合， ppc64 などのプラットフォームで問題が発生する可能性があります。
大きなテキストの問題を解決するために，テキストは複数のテキストセクションに分割され，必要に応じてリンカが長い呼び出しを生成できるようになります。
これが起こると，各テキストセクションの vaddr はテキスト内のオフセットに設定されます。
各メソッドのオフセットは，セクションの vaddrs とサイズと比較されて，含まれるセクションが決定されます。
次に，セクションの相対オフセットがセクションの再配置 baseaddr に加算され，メソッド addess が計算されます。
#

#		// #
single text section
#
単一テキストセクション
#

#	if res > md.etext && GOARCH != "wasm" { // #
on wasm, functions do not live in the same address space as the linear memory
#
wasm では，関数は線形メモリと同じアドレス空間に存在しません。
#

#	// #
See funcType in reflect/type.go for details on data layout.
#
データレイアウトの詳細については， reflect/type.go の funcType を参照してください。
#

#	mcount  uint16 // #
number of methods
#
メソッド数
#

#	xcount  uint16 // #
number of exported methods
#
エクスポートされたメソッドの数
#

#	moff    uint32 // #
offset from this uncommontype to [mcount]method
#
この uncommontype から [mcount]method へのオフセット
#

#	_       uint32 // #
unused
#
未使用
#

#	bucket     *_type // #
internal type representing a hash bucket
#
ハッシュバケットを表す内部型
#

#	keysize    uint8  // #
size of key slot
#
キースロットのサイズ
#

#	valuesize  uint8  // #
size of value slot
#
値スロットのサイズ
#

#	bucketsize uint16 // #
size of bucket
#
バケツのサイズ
#

#// #
Note: flag values must match those used in the TMAP case
in ../cmd/compile/internal/gc/reflect.go:dtypesym.
#
注 : フラグ値は， ../cmd/compile/internal/gc/reflect.go:dtypesym の TMAP のケースで使用されている値と一致する必要があります。
#

#func (mt *maptype) indirectkey() bool { // #
store ptr to key instead of key itself
#
キー自体ではなくキーに ptr を格納する
#

#func (mt *maptype) indirectvalue() bool { // #
store ptr to value instead of value itself
#
値そのものではなく値に ptr を格納する
#

#func (mt *maptype) reflexivekey() bool { // #
true if k==k for all keys
#
すべてのキーに対して k==k の場合は true
#

#func (mt *maptype) needkeyupdate() bool { // #
true if we need to update key on an overwrite
#
上書き時にキーを更新する必要がある場合は true
#

#func (mt *maptype) hashMightPanic() bool { // #
true if hash function might panic
#
ハッシュ関数がパニックになる可能性がある場合は true
#

#// #
name is an encoded type name with optional extra data.
See reflect/type.go for details.
#
name は，オプションの追加データを含むエンコードされた型名です。
詳細は reflect/type.go を見てください。
#

#// #
typelinksinit scans the types from extra modules and builds the
moduledata typemap used to de-duplicate type pointers.
#
typelinksinit は追加のモジュールから型をスキャンし，型ポインタの重複を排除するために使用されるモジュール型データ型マップを作成します。
#

#		// #
Collect types from the previous module into typehash.
#
前のモジュールの型を typehash に集めます。
#

#			// #
Add to typehash if not seen before.
#
前に見たことがなければ typehash に追加してください。
#

#			// #
If any of this module's typelinks match a type from a
prior module, prefer that prior type by adding the offset
to this module's typemap.
#
このモジュールの型リンクのいずれかが前のモジュールの型と一致する場合は，このモジュールの型マップにオフセットを追加して，その前の型を優先します。
#

#// #
typesEqual reports whether two types are equal.
#
typesEqual は， 2 つの型が等しいかどうかを報告します。
#

#// #
Everywhere in the runtime and reflect packages, it is assumed that
there is exactly one *_type per Go type, so that pointer equality
can be used to test if types are equal. There is one place that
breaks this assumption: buildmode=shared. In this case a type can
appear as two different pieces of memory. This is hidden from the
runtime and reflect package by the per-module typemap built in
typelinksinit. It uses typesEqual to map types from later modules
back into earlier ones.
#
ランタイムのどこでも，そしてパッケージを反映して， Go 型ごとに正確に 1 つの *_type があると仮定されます，それで，型が等しいかどうかテストするためにポインター等価が使用できるように。
この仮定を破る場所が 1 つあります。
buildmode=shared です。
この場合，型は 2 つの異なるメモリとして現れることがあります。
これはランタイムから隠されており， typelinksinit に組み込まれたモジュールごとの型マップによってパッケージを反映します。
これは typesEqual を使用して，後のモジュールから前のモジュールに型をマッピングします。
#

#// #
Only typelinksinit needs this function.
#
typelinksinit だけがこの関数を必要とします。
#

#	// #
mark these types as seen, and thus equivalent which prevents an infinite loop if
the two types are identical, but recursively defined and loaded from
different modules
#
これらの型を見られたものとしてマークし， 2 つの型が同一であるが再帰的に定義され異なるモジュールからロードされた場合に無限ループを防止する等価
#

#			// #
Note the mhdr array can be relocated from
another module. See ＃17724.
#
mhdr 配列は他のモジュールから移動できることに注意してください。
#

[runtime/type.go]
#//	#
cmd/compile/internal/gc/reflect.go
cmd/link/internal/ld/decodesym.go
reflect/type.go
#
cmd/compile/internal/gc/reflect.go
cmd/link/internal/ld/decodesym.go
reflect/type.go
#

[runtime/type.go]
#// #
Needs to be in sync with ../cmd/link/internal/ld/decodesym.go:/^func.commonsize,
../cmd/compile/internal/gc/reflect.go:/^func.dcommontype and
../reflect/type.go:/^type.rtype.
#
Needs to be in sync with ../cmd/link/internal/ld/decodesym.go:/^func.commonsize,
../cmd/compile/internal/gc/reflect.go:/^func.dcommontype and
../reflect/type.go:/^type.rtype.
#

[runtime/type.go]
#	elemsize   uint8  // #
size of elem slot
#
elem スロットのサイズ
#

[runtime/type.go]
#func (mt *maptype) indirectelem() bool { // #
store ptr to elem instead of elem itself
#
elem 自体ではなく elem に ptr を保存します
#

