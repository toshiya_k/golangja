* 1

#// #
Numbers fundamental to the encoding.
#
エンコーディングの基本となる番号
#

#	runeError = '\uFFFD'     // #
the "error" Rune or "Unicode replacement character"
#
" エラー " ルーンまたは "Unicode 置換文字 "
#

#	runeSelf  = 0x80         // #
characters below Runeself are represented as themselves in a single byte.
#
Runeself より下の文字は，それ自体として 1 バイトで表されます。
#

#	maxRune   = '\U0010FFFF' // #
Maximum valid Unicode code point.
#
最大有効 Unicode コードポイント。
#

#// #
Code points in the surrogate range are not valid for UTF-8.
#
サロゲート範囲内のコードポイントは， UTF-8 には無効です。
#

#	// #
The default lowest and highest continuation byte.
#
デフォルトの最小継続バイトと最大継続バイト。
#

#// #
countrunes returns the number of runes in s.
#
countrunes は s のルーン文字数を返します。
#

#// #
decoderune returns the non-ASCII rune at the start of
s[k:] and the index after the rune in s.
#
decoderune は， s[k:] の先頭にある ASCII 以外のルーン文字と， s のルーン文字の後のインデックスを返します。
#

#// #
decoderune assumes that caller has checked that
the to be decoded rune is a non-ASCII rune.
#
decoderune は，呼び出し元がデコードされるルーンが非 ASCII ルーンであることを確認したと仮定します。
#

#// #
If the string appears to be incomplete or decoding problems
are encountered (runeerror, k + 1) is returned to ensure
progress when decoderune is used to iterate over a string.
#
文字列が不完全であると思われる場合，またはデコードの問題が発生した場合 (runeerror, k + 1) は， decoderune を使用して文字列を反復処理するときの進捗を確認するために返されます。
#

#		// #
0080-07FF two byte sequence
#
0080-07FF2 バイトシーケンス
#

#		// #
0800-FFFF three byte sequence
#
0800-FFFF3 バイトシーケンス
#

#		// #
10000-1FFFFF four byte sequence
#
10000-1FFFFF4 バイトシーケンス
#

#// #
encoderune writes into p (which must be large enough) the UTF-8 encoding of the rune.
It returns the number of bytes written.
#
encoderune は，ルーン文字の UTF-8 エンコーディングを p (十分に大きい必要があります) に書き込みます。
書き込まれたバイト数を返します。
#

#	// #
Negative values are erroneous. Making it unsigned addresses the problem.
#
負の値は誤りです。
符号なしにすると問題が解決します。
#

#		_ = p[1] // #
eliminate bounds checks
#
境界チェックを排除
#

#		_ = p[2] // #
eliminate bounds checks
#
境界チェックを排除
#

#		_ = p[3] // #
eliminate bounds checks
#
境界チェックを排除
#

[runtime/utf8.go]
#	t1 = 0x00 // #
0000 0000
#
0000 0000
#

[runtime/utf8.go]
#	tx = 0x80 // #
1000 0000
#
1000 0000
#

[runtime/utf8.go]
#	t2 = 0xC0 // #
1100 0000
#
1100 0000
#

[runtime/utf8.go]
#	t3 = 0xE0 // #
1110 0000
#
1110 0000
#

[runtime/utf8.go]
#	t4 = 0xF0 // #
1111 0000
#
1111 0000
#

[runtime/utf8.go]
#	t5 = 0xF8 // #
1111 1000
#
1111 1000
#

[runtime/utf8.go]
#	maskx = 0x3F // #
0011 1111
#
0011 1111
#

[runtime/utf8.go]
#	mask2 = 0x1F // #
0001 1111
#
0001 1111
#

[runtime/utf8.go]
#	mask3 = 0x0F // #
0000 1111
#
0000 1111
#

[runtime/utf8.go]
#	mask4 = 0x07 // #
0000 0111
#
0000 0111
#

[runtime/utf8.go]
#	locb = 0x80 // #
1000 0000
#
1000 0000
#

[runtime/utf8.go]
#	hicb = 0xBF // #
1011 1111
#
1011 1111
#

