* 1

#// #
This file implements runtime support for signal handling.
#
このファイルはシグナル処理のためのランタイムサポートを実装します。
#

#// #
Most synchronization primitives are not available from
the signal handler (it cannot block, allocate memory, or use locks)
so the handler communicates with a processing goroutine
via struct sig, below.
#
ほとんどの同期プリミティブはシグナルハンドラからは利用できません (ブロックすることも，メモリを割り当てることも，ロックを使用することもできません) 。
そのため，ハンドラは下記の struct sig を介して処理グループと通信します。
#

#// #
sigsend is called by the signal handler to queue a new signal.
signal_recv is called by the Go program to receive a newly queued signal.
Synchronization between sigsend and signal_recv is based on the sig.state
variable. It can be in 3 states: sigIdle, sigReceiving and sigSending.
sigReceiving means that signal_recv is blocked on sig.Note and there are no
new pending signals.
sigSending means that sig.mask *may* contain new pending signals,
signal_recv can't be blocked in this state.
sigIdle means that there are no new pending signals and signal_recv is not blocked.
Transitions between states are done atomically with CAS.
When signal_recv is unblocked, it resets sig.Note and rechecks sig.mask.
If several sigsends and signal_recv execute concurrently, it can lead to
unnecessary rechecks of sig.mask, but it cannot lead to missed signals
nor deadlocks.
#
sigsend は，新しいシグナルをキューに入れるためにシグナルハンドラによって呼び出されます。
signal_recv は，新しくキューに入れられたシグナルを受け取るために Go プログラムによって呼び出されます。
sigsend と signal_recv の間の同期は， sig.state 変数に基づいています。
それは 3 つの状態にある場合があります : sigIdle, sigReceiving および sigSending 。
sigReceiving は， signal_recv が sig.Note でブロックされており，新しい保留中のシグナルがないことを意味します。
sigSending は， sig.mask * が新しい保留中のシグナルを含む可能性があることを意味します。
signal_recv はこの状態ではブロックできません。
sigIdle は，新しい保留中のシグナルがなく， signal_recv がブロックされていないことを意味します。
状態間の遷移は CAS を使ってアトミックに行われます。
signal_recv のブロックが解除されると， sig.Note がリセットされ， sig.mask が再チェックされます。
複数の sigsends と signal_recv が平行に実行されると，不必要に sig.mask が再チェックされる可能性がありますが，見逃したシグナルやデッドロックが発生することはありません。
#

#	_ "unsafe" // #
for go:linkname
#
リンク名
#

#// #
sig handles communication between the signal handler and os/signal.
Other than the inuse and recv fields, the fields are accessed atomically.
#
sig はシグナルハンドラと os/signal 間の通信を処理します。
inuse フィールドと recv フィールド以外に，フィールドはアトミックにアクセスされます。
#

#// #
The wanted and ignored fields are only written by one goroutine at
a time; access is controlled by the handlers Mutex in os/signal.
The fields are only read by that one goroutine and by the signal handler.
We access them atomically to minimize the race between setting them
in the goroutine calling os/signal and the signal handler,
which may be running in a different thread. That race is unavoidable,
as there is no connection between handling a signal and receiving one,
but atomic instructions should minimize it.
#
求められて無視されたフィールドは一度に一つの goroutine によって書かれるだけです。
アクセスは os/signal でハンドラ Mutex によって制御されます。
フィールドはその 1 つのゴルーチンとシグナルハンドラによってのみ読み込まれます。
それらをアトミックにアクセスして，それらを別のスレッドで実行されている可能性がある os/signal を呼び出す goroutine で設定することと signal handler で設定することの間の競合を最小限に抑えます。
シグナルの処理と受信の間には関連性がないので，その競合は避けられませんが，アトミックな命令はそれを最小限に抑えるべきです。
#

#// #
sigsend delivers a signal from sighandler to the internal signal delivery queue.
It reports whether the signal was sent. If not, the caller typically crashes the program.
It runs from the signal handler, so it's limited in what it can do.
#
sigsend は sighandler からのシグナルを内部シグナル配信キューに配信します。
シグナルが送信されたかどうかを報告します。
そうでない場合，呼び出し元は通常プログラムをクラッシュさせます。
シグナルハンドラから実行されるので，実行できることには制限があります。
#

#	// #
We are running in the signal handler; defer is not available.
#
シグナルハンドラで実行しています。
延期はできません。
#

#	// #
Add signal to outgoing queue.
#
発信キューにシグナルを追加します。
#

#			return true // #
signal already in queue
#
シグナルはすでにキューに入っています
#

#	// #
Notify receiver that queue has new bit.
#
受信側にキューに新しいビットがあることを通知します。
#

#			// #
notification already pending
#
通知はすでに保留中です
#

#// #
Called to receive the next queued signal.
Must only be called from a single goroutine at a time.
#
次のキューシグナルを受信するために呼び出されます。
一度に一つの goroutine から呼ばれなければなりません。
#

#		// #
Serve any signals from local copy.
#
ローカルコピーからのシグナルを処理します。
#

#		// #
Wait for updates to be available from signal sender.
#
シグナル送信側からアップデートが入手可能になるのを待ちます。
#

#		// #
Incorporate updates from sender into local copy.
#
送信者からの更新をローカルコピーに組み込みます。
#

#// #
signalWaitUntilIdle waits until the signal delivery mechanism is idle.
This is used to ensure that we do not drop a signal notification due
to a race between disabling a signal and receiving a signal.
This assumes that signal delivery has already been disabled for
the signal(s) in question, and here we are just waiting to make sure
that all the signals have been delivered to the user channels
by the os/signal package.
#
signalWaitUntilIdle は，シグナル配信メカニズムがアイドル状態になるまで待機します。
これは，シグナルを無効にしてからシグナルを受信するまでの間の競合のためにシグナル通知がドロップされないようにするために使用されます。
これは，問題のシグナルのシグナル配信がすでに無効になっていることを前提としています。
ここでは， os/signal パッケージによってすべてのシグナルがユーザーチャンネルに配信されていることを確認します。
#

#	// #
Although the signals we care about have been removed from
sig.wanted, it is possible that another thread has received
a signal, has read from sig.wanted, is now updating sig.mask,
and has not yet woken up the processor thread. We need to wait
until all current signal deliveries have completed.
#
気になるシグナルは sig.wanted から削除されましたが，別のスレッドがシグナルを受信し， sig.wanted から読み取り，現在 sig.mask を更新していて，まだプロセッサスレッドを起こしていない可能性があります。
現在のシグナル配信がすべて完了するまで待つ必要があります。
#

#	// #
Although WaitUntilIdle seems like the right name for this
function, the state we are looking for is sigReceiving, not
sigIdle.  The sigIdle state is really more like sigProcessing.
#
WaitUntilIdle はこの関数の正しい名前のように見えますが，探している状態は sigIce ではなく sigReceiving です。
sigIdle 状態は，本当に sigProcessing に似ています。
#

#// #
Must only be called from a single goroutine at a time.
#
一度に一つの goroutine から呼ばれなければなりません。
#

#		// #
The first call to signal_enable is for us
to use for initialization. It does not pass
signal information in m.
#
signal_enable への最初の呼び出しは初期化に使用するためのものです。
シグナル情報を m に渡しません。
#

#		sig.inuse = true // #
enable reception of signals; cannot disable
#
シグナルの受信を有効にします。 無効にすることはできません
#

#// #
sigInitIgnored marks the signal as already ignored. This is called at
program start by initsig. In a shared library initsig is called by
libpreinit, so the runtime may not be initialized yet.
#
sigInitIgnored はシグナルを既に無視されたものとしてマークします。
これはプログラムの開始時に initsig によって呼び出されます。
共有ライブラリでは， initsig は libpreinit によって呼び出されるため，ランタイムはまだ初期化されていない可能性があります。
#

#// #
Checked by signal handlers.
#
シグナルハンドラによってチェックされます。
#

