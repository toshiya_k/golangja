* 1

#// #
These functions cannot have go:noescape annotations,
because while ptr does not escape, new does.
If new is marked as not escaping, the compiler will make incorrect
escape analysis decisions about the pointer value being stored.
#
これらの関数は， go:noescape アノテーションを持つことはできません。
ptr はエスケープしないのに対して， new はエスケープするからです。
new がエスケープしていないとマークされていると，コンパイラーは，保管されているポインター値について誤ったエスケープ分析の決定を行います。
#

#// #
atomicwb performs a write barrier before an atomic pointer write.
The caller should guard the call with "if writeBarrier.enabled".
#
アトミック wb は，アトミックポインタ書き込みの前に書き込みバリアを実行します。
呼び出し側は "if writeBarrier.enabled" で呼び出しを保護する必要があります。
#

#// #
atomicstorep performs *ptr = new atomically and invokes a write barrier.
#
アトミック storep は， *ptr = new をアトミックに実行し，書き込みバリアを呼び出します。
#

#// #
Like above, but implement in terms of sync/atomic's uintptr operations.
We cannot just call the runtime routines, because the race detector expects
to be able to intercept the sync/atomic forms but not the runtime forms.
#
上記と同じですが， sync/ アトミックの uintptr 操作に関して実装します。
我々はただランタイムルーチンを呼び出すことはできません，なぜなら競合検出器は同期 / アトミックフォームを傍受することができるがランタイムフォームを傍受することができないと期待するからです。
#

