* 1

#		// #
One-bucket table.
#
1 バケットテーブル
#

#			// #
short key, doing lots of comparisons is ok
#
短いキー，たくさんの比較をしても大丈夫です
#

#		// #
long key, try not to do more comparisons than necessary
#
長いキー，必要以上に比較しないようにしてください。
#

#			// #
check first 4 bytes
#
最初の 4 バイトを確認
#

#			// #
check last 4 bytes
#
最後の 4 バイトをチェック
#

#				// #
Two keys are potential matches. Use hash to distinguish them.
#
2 つのキーが潜在的な一致です。
それらを区別するためにハッシュを使用してください。
#

#			// #
There used to be half as many buckets; mask down one more power of two.
#
以前は半分の数のバケットがありました。
もう 2 つの力を覆い隠す。
#

#	// #
Set hashWriting after calling alg.hash for consistency with mapassign.
#
mapassign との整合性のために alg.hash を呼び出した後に hashWriting を設定してください。
#

#		h.buckets = newobject(t.bucket) // #
newarray(t.bucket, 1)
#
newarray(t.bucket, 1)
#

#			// #
already have a mapping for key. Update it.
#
すでにキーのマッピングがあります。
それを更新してください。
#

#	// #
Did not find mapping for key. Allocate new cell & add entry.
#
キーのマッピングが見つかりませんでした。
新しいセルを割り当ててエントリを追加します。
#

#	// #
If we hit the max load factor or we have too many overflow buckets,
and we're not already in the middle of growing, start growing.
#
最大負荷率に達した場合，またはオーバーフローバケットが多すぎて，まだ成長の途中ではない場合は，成長を始めます。
#

#		goto again // #
Growing the table invalidates everything, so try again
#
テーブルを大きくするとすべてが無効になるので，もう一度試してください。
#

#		// #
all current buckets are full, allocate a new one.
#
現在のバケットはすべていっぱいになりました。
新しいバケットを割り当てます。
#

#		inserti = 0 // #
not necessary, but avoids needlessly spilling inserti
#
必須ではありませんが，不必要に挿入物をこぼさないようにします
#

#	insertb.tophash[inserti&(bucketCnt-1)] = top // #
mask inserti to avoid bounds checks
#
境界チェックを回避するために inserti をマスクする
#

#	// #
store new key at insert position
#
新しいキーを挿入位置に格納する
#

#	// #
Set hashWriting after calling alg.hash for consistency with mapdelete
#
mapdelete との一貫性のために alg.hash を呼び出した後に hashWriting を設定する
#

#			// #
Clear key's pointer.
#
キーのポインタを消去します。
#

#			// #
If the bucket now ends in a bunch of emptyOne states,
change those to emptyRest states.
#
バケットが一連の emptyOne 状態で終わる場合は，それらを emptyRest 状態に変更します。
#

#						break // #
beginning of initial bucket, we're done.
#
最初のバケツの始め，これで終わりです。
#

#					// #
Find previous bucket, continue at its last entry.
#
前のバケットを見つけて，最後のエントリから続けます。
#

#	// #
make sure we evacuate the oldbucket corresponding
to the bucket we're about to use
#
使用しようとしているバケットに対応する oldbucket を確実に退避させる
#

#	// #
evacuate one more oldbucket to make progress on growing
#
成長を進めるためにもう 1 つ oldbucket を退避させる
#

#		// #
TODO: reuse overflow buckets instead of using new ones, if there
is no iterator using the old buckets.  (If !oldIterator.)
#
TODO: 古いバケットを使うイテレータがない場合は，新しいバケットを使う代わりにオーバーフローバケットを再利用します。
(if!oldIterator です。)
#

#		// #
xy contains the x and y (low and high) evacuation destinations.
#
xy には， x および y (低および高) の避難先が含まれています。
#

#			// #
Only calculate y pointers if we're growing bigger.
Otherwise GC can see bad pointers.
#
私たちが大きく成長している場合にのみ y ポインタを計算します。
そうでなければ GC は悪いポインタを見ることができます。
#

#					// #
Compute hash to make our evacuation decision (whether we need
to send this key/value to bucket x or bucket y).
#
ハッシュを計算して避難決定を行います (このキー / 値をバケット x またはバケット y のどちらに送信する必要があるかどうか) 。
#

#				b.tophash[i] = evacuatedX + useY // #
evacuatedX + 1 == evacuatedY, enforced in makemap
#
evacuatedX + 1 == evacuatedY ，メイクマップで強制
#

#				dst := &xy[useY]                 // #
evacuation destination
#
避難先
#

#				dst.b.tophash[dst.i&(bucketCnt-1)] = top // #
mask dst.i as an optimization, to avoid a bounds check
#
境界チェックを回避するために， dst.i を最適化としてマスクします。
#

#				// #
Copy key.
#
コピーキー
#

#				// #
These updates might push these pointers past the end of the
key or value arrays.  That's ok, as we have the overflow pointer
at the end of the bucket to protect against pointing past the
end of the bucket.
#
これらの更新により，これらのポインタはキー配列または値配列の末尾を超えることがあります。
バケツの端を越えて指し示すことから保護するためにバケツの端にオーバーフローポインタがあるので，それは問題ありません。
#

#		// #
Unlink the overflow buckets & clear key/value to help GC.
Unlink the overflow buckets & clear key/value to help GC.
#
オーバーフローバケットのリンクを解除し， GC を助けるためにキー / 値をクリアします。
オーバーフローバケットのリンクを解除し， GC を助けるためにキー / 値をクリアします。
#

#			// #
Preserve b.tophash because the evacuation
state is maintained there.
#
避難状態はそこで維持されるので b.tophash を保存しなさい。
#

[runtime/map_faststr.go]
#					// #
Compute hash to make our evacuation decision (whether we need
to send this key/elem to bucket x or bucket y).
#
ハッシュを計算して避難決定を行います (このキー /elem をバケット x またはバケット y に送信する必要があるかどうか) 。
#

[runtime/map_faststr.go]
#				// #
These updates might push these pointers past the end of the
key or elem arrays.  That's ok, as we have the overflow pointer
at the end of the bucket to protect against pointing past the
end of the bucket.
#
これらの更新により，これらのポインターがキーまたは elem 配列の終わりを超える可能性があります。バケットの終わりにオーバーフローポインターがあり，バケットの終わりを超えて指すのを防ぐため，これで問題ありません。
#

[runtime/map_faststr.go]
#		// #
Unlink the overflow buckets & clear key/elem to help GC.
#
オーバーフローバケットのリンクを解除し，キー / エレムをクリアして GC を支援します。
#

