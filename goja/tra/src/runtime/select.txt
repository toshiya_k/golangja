* 1

#// #
This file contains the implementation of Go select statements.
#
このファイルには Go の select 文の実装が含まれています。
#

#// #
scase.kind values.
Known to compiler.
Changes here must also be made in src/cmd/compile/internal/gc/select.go's walkselect.
#
scase.kind の値コンパイラに知られています。
ここでの変更も src/cmd/compile/internal/gc/select.go の walkselect で行わなければなりません。
#

#// #
Select case descriptor.
Known to compiler.
Changes here must also be made in src/cmd/internal/gc/select.go's scasetype.
#
ケース記述子を選択してください。
コンパイラに知られています。
ここでの変更も src/cmd/internal/gc/select.go の scasetype で行わなければなりません。
#

#	c           *hchan         // #
chan
#
チャンネル
#

#	elem        unsafe.Pointer // #
data element
#
データ要素
#

#	pc          uintptr // #
race pc (for race detector / msan)
#
race pc (レースディテクタ / msan 用)
#

#	// #
We must be very careful here to not touch sel after we have unlocked
the last lock, because sel can be freed right after the last unlock.
Consider the following situation.
First M calls runtime·park() in runtime·selectgo() passing the sel.
Once runtime·park() has unlocked the last lock, another M makes
the G that calls select runnable again and schedules it for execution.
When the G runs on another M, it locks all the locks and frees sel.
Now if the first M touches sel, it will access freed memory.
#
最後のロックを解除した直後に sel を解放することができるので，最後のロックを解除した後に sel に触れないように，ここでは細心の注意を払う必要があります。
次のような状況を考えます。
最初の M はランタイム・ selectgo() でランタイム・ park() を呼び出し， sel を渡します。
runtime ・ park() が最後のロックを解除すると，別の M が select を呼び出す G を再度実行可能にして，実行をスケジュールします。
G が別の M の上を走ると，すべてのロックをロックして sel を解放します。
最初の M が sel に触れると，解放されたメモリにアクセスします。
#

#			continue // #
will unlock it on the next iteration
#
次の繰り返しでロックを解除します
#

#	// #
This must not access gp's stack (see gopark). In
particular, it must not access the *hselect. That's okay,
because by the time this is called, gp.waiting has all
channels in lock order.
#
これは gp のスタックにアクセスしてはいけません (gopark を参照) 。
特に， *hselect にアクセスしてはいけません。
これは， gp.waiting が呼び出されるときまでに，すべてのチャンネルをロック順にしているため，問題ありません。
#

#			// #
As soon as we unlock the channel, fields in
any sudog with that channel may change,
including c and waitlink. Since multiple
sudogs may have the same channel, we unlock
only after we've passed the last instance
of a channel.
#
チャンネルのロックを解除するとすぐに，そのチャンネルを持つすべての Sudog 内のフィールド (c や waitlink など) が変わる可能性があります。
複数の Sudog が同じチャンネルを持っているかもしれないので，私たちはチャンネルの最後のインスタンスを通過した後にのみロックを解除します。
#

#	gopark(nil, nil, waitReasonSelectNoCases, traceEvGoStop, 1) // #
forever
#
永遠に
#

#// #
selectgo implements the select statement.
#
selectgo は select 文を実装します。
#

#// #
cas0 points to an array of type [ncases]scase, and order0 points to
an array of type [2*ncases]uint16. Both reside on the goroutine's
stack (regardless of any escaping in selectgo).
#
cas0 は型 [ncases]scase の配列を指し， order0 は型 [2*ncases]uint16 の配列を指します。
どちらも goroutine のスタックに常駐しています (selectgo でのエスケープに関係なく) 。
#

#// #
selectgo returns the index of the chosen scase, which matches the
ordinal position of its respective select{recv,send,default} call.
Also, if the chosen scase was a receive operation, it reports whether
a value was received.
#
selectgo は選択された scase のインデックスを返します。
これはそれぞれの select{recv,send,default} 呼び出しの序数位置と一致します。
また，選択したケースが受信操作だった場合は，値が受信されたかどうかを報告します。
#

#	// #
Replace send/receive cases involving nil channels with
caseNil so logic below can assume non-nil channel.
#
以下のロジックが nil 以外のチャンネルを想定できるように， nil チャンネルを含む送受信ケースを caseNil に置き換えます。
#

#	// #
The compiler rewrites selects that statically have
only 0 or 1 cases plus default into simpler constructs.
The only way we can end up with such small sel.ncase
values here is for a larger select in which most channels
have been nilled out. The general code handles those
cases correctly, and they are rare enough not to bother
optimizing (and needing to test).
#
コンパイラーは，静的に 0 または 1 のケースのみを含み，デフォルトが単純な構成になるように選択を書き換えます。
私たちがここでそのような小さい sel.ncase 値に終わることができる唯一の方法は大部分のチャンネルが無駄にされたより大きな選択のためのものです。
一般的なコードはこれらのケースを正しく処理し，最適化を邪魔しない (そしてテストする必要がある) ほど十分にまれです。
#

#	// #
generate permuted order
#
順列を生成する
#

#	// #
sort the cases by Hchan address to get the locking order.
simple heap sort, to guarantee n log n time and constant stack footprint.
#
ロック順を得るために， Hchan アドレスでケースをソートします。
n log n 時間と一定のスタックフットプリントを保証するための単純なヒープソート。
#

#		// #
Start with the pollorder to permute cases on the same channel.
#
同じチャンネルでケースを並べ替えるには， pollorder から始めます。
#

#	// #
lock all the channels involved in the select
#
選択に含まれるすべてのチャンネルをロックする
#

#	// #
pass 1 - look for something already waiting
#
パス 1 - すでに待っているものを探す
#

#	// #
pass 2 - enqueue on all chans
#
パス 2 - すべてのチャンでエンキュー
#

#		// #
No stack splits between assigning elem and enqueuing
sg on gp.waiting where copystack can find it.
#
コピースタックがそれを見つけることができるところで gp.waiting に elem を割り当てることと sg をエンキューすることの間でスタックは分割されません。
#

#		// #
Construct waiting list in lock order.
#
ロック順に待機リストを作成します。
#

#	// #
wait for someone to wake us up
#
誰かが私たちを目覚めさせるのを待つ
#

#	// #
pass 3 - dequeue from unsuccessful chans
otherwise they stack up on quiet channels
record the successful case, if any.
We singly-linked up the SudoGs in lock order.
#
パス 3 - 失敗したチャンからデキューし，それ以外の場合は，静かなチャンネルにスタックして，成功した場合があればそれを記録します。
ロック順に SudoG を単独でリンクしました。
#

#	// #
Clear all elem before unlinking from gp.waiting.
#
gp.waiting からリンク解除する前に，すべての elem をクリアしてください。
#

#			// #
sg has already been dequeued by the G that woke us up.
#
sg はすでに私たちを起こした G によってデキューされました。
#

#		// #
We can wake up with gp.param == nil (so cas == nil)
when a channel involved in the select has been closed.
It is easiest to loop and re-run the operation;
we'll see that it's now closed.
Maybe some day we can signal the close explicitly,
but we'd have to distinguish close-on-reader from close-on-writer.
It's easiest not to duplicate the code and just recheck above.
We know that something closed, and things never un-close,
so we won't block again.
#
選択に関与しているチャンネルが閉じられたとき， gp.param == nil (つまり cas == nil) で目を覚ますことができます。
操作をループして再実行するのが最も簡単です。
閉店したことがわかります。
いつか近いうちにクローズを明示的に知らせることができるかもしれませんが，クローズアップオンリーダーとクローズアップオンライターを区別する必要があります。
コードを複製しないで，上記を再確認するのが最も簡単です。
私たちは，何かが閉じたこと，そして閉じたことがないことを知っているので，再びブロックすることはありません。
#

#	// #
can receive from buffer
#
バッファから受け取ることができます
#

#	// #
can send to buffer
#
バッファに送信できます
#

#	// #
can receive from sleeping sender (sg)
#
眠っている送り手から受け取ることができます (sg)
#

#	// #
read at end of closed channel
#
閉じたチャンネルの終わりで読む
#

#	// #
can send to a sleeping receiver (sg)
#
眠っている受信機 (sg) に送信することができます
#

#	// #
send on closed channel
#
クローズドチャンネルで送信
#

#	// #
TODO(khr): if we have a moving garbage collector, we'll need to
change this function.
#
TODO(khr): 動いているガベージコレクタがあるのなら，この関数を変更する必要があるでしょう。
#

#// #
A runtimeSelect is a single case passed to rselect.
This must match ../reflect/value.go:/runtimeSelect
#
runtimeSelect は， rselect に渡される 1 つのケースです。
これは ../reflect/value.go:/runtimeSelect と一致する必要があります
#

#	typ unsafe.Pointer // #
channel type (not used here)
#
チャンネル型 (ここでは使用されていません)
#

#	ch  *hchan         // #
channel
#
チャンネル
#

#	val unsafe.Pointer // #
ptr to data (SendDir) or ptr to receive buffer (RecvDir)
#
データへの ptr(SendDir) または受信バッファーへの ptr(RecvDir)
#

#// #
These values must match ../reflect/value.go:/SelectDir.
#
これらの値は ../reflect/value.go:/SelectDir と一致する必要があります。
#

#	selectSend              // #
case Chan <- Send
#
ケースチャン <- 送信
#

#	selectRecv              // #
case <-Chan:
#
ケース <- チャン :
#

#	selectDefault           // #
default
#
デフォルト
#

#			// #
middle of queue
#
キューの途中
#

#		// #
end of queue
#
キューの終わり
#

#		// #
start of queue
#
キューの開始
#

#	// #
x==y==nil. Either sgp is the only element in the queue,
or it has already been removed. Use q.first to disambiguate.
#
x==y==nil 。
sgp がキュー内の唯一の要素であるか，またはすでに削除されています。
曖昧さを解消するために q.first を使用してください。
#

