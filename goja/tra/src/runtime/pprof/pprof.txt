* 1

#// #
Package pprof writes runtime profiling data in the format expected
by the pprof visualization tool.
#
pprof パッケージは，pprof 視覚化ツールのフォーマットでランタイムプロファイルデータを出力します。
#

#// #
Profiling a Go program
#
Go プログラムのプロファイリング
#

#// #
The first step to profiling a Go program is to enable profiling.
Support for profiling benchmarks built with the standard testing
package is built into go test. For example, the following command
runs benchmarks in the current directory and writes the CPU and
memory profiles to cpu.prof and mem.prof:
#
Go プログラムをプロファイリングするための最初のステップは，プロファイリングを有効にすることです。
標準テストパッケージで構築されたプロファイリングベンチマークのサポートは go test に組み込まれています。
たとえば，次のコマンドは現在のディレクトリでベンチマークを実行し， CPU とメモリのプロファイルを cpu.prof と mem.prof に書き込みます。
#

#// #
To add equivalent profiling support to a standalone program, add
code like the following to your main function:
#
スタンドアロンプ​​ログラムに同等のプロファイリングサポートを追加するには，メイン関数に次のようなコードを追加します。
#

#// #
       // ... rest of the program ...
#
       // ... プログラムの残りの部分 ...
#

#// #
           runtime.GC() // get up-to-date statistics
#
           runtime.GC() // 最新統計を取得する
#

#// #
There is also a standard HTTP interface to profiling data. Adding
the following line will install handlers under the /debug/pprof/
URL to download live profiles:
#
データをプロファイリングするための標準 HTTP インターフェースもあります。
次の行を追加すると，ライブプロファイルをダウンロードするために /debug/pprof/URL の下にハンドラがインストールされます。
#

#// #
See the net/http/pprof package for more details.
#
詳細は net/http/pprof パッケージを見てください。
#

#// #
Profiles can then be visualized with the pprof tool:
#
プロファイルは pprof ツールで視覚化することができます。
#

#// #
There are many commands available from the pprof command line.
Commonly used commands include "top", which prints a summary of the
top program hot-spots, and "web", which opens an interactive graph
of hot-spots and their call graphs. Use "help" for information on
all pprof commands.
#
pprof コマンドラインから使用できるコマンドはたくさんあります。
一般的に使用されるコマンドには，トッププログラムのホットスポットの概要を表示する "top" ，およびホットスポットとそれらのコールグラフのインタラクティブなグラフを開く "web" があります。
すべての pprof コマンドについては， "help" を使用してください。
#

#// #
For more information about pprof, see
https://github.com/google/pprof/blob/master/doc/README.md.
#
pprof の詳細については， https://github.com/google/pprof/blob/master/doc/README.md を参照してください。
#

#// #
BUG(rsc): Profiles are only as good as the kernel support used to generate them.
See https://golang.org/issue/13841 for details about known problems.
#
BUG (rsc): プロファイルはそれらを生成するのに使用されたカーネルサポートと同じくらい良いです。
既知の問題についての詳細は https://golang.org/issue/13841 を参照してください。
#

#// #
A Profile is a collection of stack traces showing the call sequences
that led to instances of a particular event, such as allocation.
Packages can create and maintain their own profiles; the most common
use is for tracking resources that must be explicitly closed, such as files
or network connections.
#
Profile は，割り当てなど，特定のイベントのインスタンスにつながった呼び出しシーケンスを示すスタックトレースの集まりです。
パッケージは独自のプロファイルを作成および管理できます。
最も一般的な用途は，ファイルやネットワーク接続など，明示的に閉じる必要があるリソースを追跡することです。
#

#// #
A Profile's methods can be called from multiple goroutines simultaneously.
#
Profile のメソッドは，複数のゴルーチンから平行に呼び出すことができます。
#

#// #
Each Profile has a unique name. A few profiles are predefined:
#
各プロファイルには一意の名前があります。
いくつかのプロファイルが事前定義されています。
#

#//	#
goroutine    - stack traces of all current goroutines
heap         - a sampling of memory allocations of live objects
allocs       - a sampling of all past memory allocations
threadcreate - stack traces that led to the creation of new OS threads
block        - stack traces that led to blocking on synchronization primitives
mutex        - stack traces of holders of contended mutexes
#
goroutine    - 現在のすべての goroutine のスタックトレース
heap         - ライブオブジェクトのメモリ割り当てのサンプリング
alloc        - 過去のすべてのメモリ割り当てのサンプリング
threadcreate - 新しい OS スレッドの作成につながったスタックトレース
block        - 同期プリミティブのブロックにつながったスタックトレース
mutex        - 競合ミューテックスの所有者のスタックトレース
#

#// #
These predefined profiles maintain themselves and panic on an explicit
Add or Remove method call.
#
これらの定義済みプロファイルはそれ自体を維持し，明示的な Add または Remove メソッド呼び出しでパニックします。
#

#// #
The heap profile reports statistics as of the most recently completed
garbage collection; it elides more recent allocation to avoid skewing
the profile away from live data and toward garbage.
If there has been no garbage collection at all, the heap profile reports
all known allocations. This exception helps mainly in programs running
without garbage collection enabled, usually for debugging purposes.
#
ヒーププロファイルは，最後に完了したガベージコレクションの統計を報告します。
プロファイルをライブデータからゴミに傾けないようにするために，最近の割り当てを省略します。
ガベージコレクションがまったく行われていない場合，ヒーププロファイルはすべての既知の割り当てを報告します。
この例外は，主にガベージコレクションを有効にせずに実行されているプログラムで，通常はデバッグ目的で役立ちます。
#

#// #
The heap profile tracks both the allocation sites for all live objects in
the application memory and for all objects allocated since the program start.
Pprof's -inuse_space, -inuse_objects, -alloc_space, and -alloc_objects
flags select which to display, defaulting to -inuse_space (live objects,
scaled by size).
#
ヒーププロファイルは，アプリケーションメモリ内のすべてのライブオブジェクトと，プログラムの開始以降に割り当てられたすべてのオブジェクトの両方の割り当てサイトを追跡します。
Pprof の -inuse_space, -inuse_objects, -alloc_space ，および -alloc_objects の各フラグは，どちらを表示するかを選択します。
デフォルトでは， -inuse_space (ライブオブジェクト，サイズでスケーリング) に設定されています。
#

#// #
The allocs profile is the same as the heap profile but changes the default
pprof display to -alloc_space, the total number of bytes allocated since
the program began (including garbage-collected bytes).
#
allocs プロファイルはヒーププロファイルと同じですが，デフォルトの pprof 表示を，プログラムの開始以降に割り当てられた総バイト数 (ガベージコレクションされたバイトを含む) の -alloc_space に変更します。
#

#// #
The CPU profile is not available as a Profile. It has a special API,
the StartCPUProfile and StopCPUProfile functions, because it streams
output to a writer during profiling.
#
CPU プロファイルはプロファイルとしては使用できません。
プロファイリング中に出力をライターにストリームするため，特別な API ， StartCPUProfile 関数と StopCPUProfile 関数があります。
#

#// #
profiles records all registered profiles.
#
profiles はすべての登録済みプロファイルを記録します。
#

#	count: countHeap, // #
identical to heap profile
#
ヒーププロファイルと同じ
#

#		// #
Initial built-in profiles.
#
初期組み込みプロファイル
#

#// #
NewProfile creates a new profile with the given name.
If a profile with that name already exists, NewProfile panics.
The convention is to use a 'import/path.' prefix to create
separate name spaces for each package.
For compatibility with various tools that read pprof data,
profile names should not contain spaces.
#
NewProfile は，指定された名前で新しいプロファイルを作成します。
その名前のプロファイルが既に存在する場合， NewProfile はパニックします。
慣用的に 'import/path' を使用します。
各パッケージに別々のネームスペースを作成するための接頭辞。
pprof データを読み取るさまざまなツールとの互換性のために，プロファイル名にスペースを入れないでください。
#

#// #
Lookup returns the profile with the given name, or nil if no such profile exists.
#
Lookup は与えられた名前のプロファイルを返します。
そのようなプロファイルが存在しない場合は nil を返します。
#

#// #
Profiles returns a slice of all the known profiles, sorted by name.
#
Profiles は，名前順にソートされた，既知のすべてのプロファイルのスライスを返します。
#

#// #
Name returns this profile's name, which can be passed to Lookup to reobtain the profile.
#
Name はこのプロファイルの名前を返します。
これは，プロファイルを再取得するために Lookup に渡すことができます。
#

#// #
Count returns the number of execution stacks currently in the profile.
#
Count は，現在プロファイル内にある実行スタックの数を返します。
#

#// #
Add adds the current execution stack to the profile, associated with value.
Add stores value in an internal map, so value must be suitable for use as
a map key and will not be garbage collected until the corresponding
call to Remove. Add panics if the profile already contains a stack for value.
#
Add は現在の実行スタックを value に関連付けられたプロファイルに追加します。
値を内部マップに格納するので， value はマップキーとしての使用に適している必要があり，対応する Remove の呼び出しまでガベージコレクションされません。
プロファイルにすでに値のスタックが含まれている場合はパニックを追加します。
#

#// #
The skip parameter has the same meaning as runtime.Caller's skip
and controls where the stack trace begins. Passing skip=0 begins the
trace in the function calling Add. For example, given this
execution stack:
#
skip パラメータは， runtime.Caller の skip と同じ意味を持ち，スタックトレースの開始位置を制御します。
skip=0 を渡すと， Add を呼び出す関数内でトレースが開始されます。
たとえば，次の実行スタックがあるとします。
#

#//	#
Add
called from rpc.NewClient
called from mypkg.Run
called from main.main
#
Add
called from rpc.NewClient
called from mypkg.Run
called from main.main
#

#// #
Passing skip=0 begins the stack trace at the call to Add inside rpc.NewClient.
Passing skip=1 begins the stack trace at the call to NewClient inside mypkg.Run.
#
skip=0 を渡すと， rpc.NewClient 内で Add を呼び出したときにスタックトレースが開始されます。
skip=1 を渡すと， mypkg.Run 内の NewClient の呼び出し時にスタックトレースが開始されます。
#

#		// #
The value for skip is too large, and there's no stack trace to record.
#
skip の値が大きすぎるため，記録するスタックトレースがありません。
#

#// #
Remove removes the execution stack associated with value from the profile.
It is a no-op if the value is not in the profile.
#
Remove は，プロファイルから値に関連付けられた実行スタックを削除します。
値がプロファイルにない場合は，何もしません。
#

#// #
WriteTo writes a pprof-formatted snapshot of the profile to w.
If a write to w returns an error, WriteTo returns that error.
Otherwise, WriteTo returns nil.
#
WriteTo は，プロファイルの pprof 形式のスナップショットを w に書き込みます。
w への書き込みがエラーを返す場合， WriteTo はそのエラーを返します。
そうでなければ， WriteTo は nil を返します。
#

#// #
The debug parameter enables additional output.
Passing debug=0 prints only the hexadecimal addresses that pprof needs.
Passing debug=1 adds comments translating addresses to function names
and line numbers, so that a programmer can read the profile without tools.
#
debug パラメータは追加の出力を有効にします。
debug=0 を渡すと， pprof が必要とする 16 進数アドレスのみが出力されます。
debug=1 を渡すと，アドレスを関数名と行番号に変換するコメントが追加されるため，プログラマはツールなしでプロファイルを読むことができます。
#

#// #
The predefined profiles may assign meaning to other debug values;
for example, when printing the "goroutine" profile, debug=2 means to
print the goroutine stacks in the same form that a Go program uses
when dying due to an unrecovered panic.
#
事前定義プロファイルは他のデバッグ値に意味を割り当てることができます。
例えば， "goroutine" プロファイルを表示するとき， debug=2 は， Go プログラムが回復不能なパニックのために死ぬときに使用するのと同じ形式で goroutine スタックを表示することを意味します。
#

#	// #
Obtain consistent snapshot under lock; then process without lock.
#
ロックされた状態で一貫したスナップショットを取得します。
それからロックなしで処理します。
#

#	// #
Map order is non-deterministic; make output deterministic.
#
マップの順序は決定論的ではありません。
出力を確定的にします。
#

#// #
A countProfile is a set of stack traces to be printed as counts
grouped by stack trace. There are multiple implementations:
all that matters is that we can find out how many traces there are
and obtain each trace in turn.
#
countProfile は，スタックトレースでグループ化されたカウントとして表示されるスタックトレースのセットです。
複数の実装があります。
重要なことは，トレースの数を調べて各トレースを順番に取得できることです。
#

#// #
printCountCycleProfile outputs block profile records (for block or mutex profiles)
as the pprof-proto format output. Translations from cycle count to time duration
are done because The proto expects count and time (nanoseconds) instead of count
and the number of cycles for block, contention profiles.
Possible 'scaler' functions are scaleBlockProfile and scaleMutexProfile.
#
printCountCycleProfile は，ブロックプロファイルレコード (ブロックプロファイルまたはミューテックスプロファイルの場合) を pprof-proto 形式の出力として出力します。
プロトはブロック，競合プロファイルのカウントとサイクル数ではなく，カウントと時間 (ナノ秒) を想定しているため，サイクル数から期間への変換が行われます。
可能な ' スケーラ ' 関数は， scaleBlockProfile と scaleMutexProfile です。
#

#	// #
Output profile in protobuf form.
#
protobuf 形式の出力プロファイル
#

#			// #
For count profiles, all stack addresses are
return PCs, which is what locForPC expects.
#
カウントプロファイルの場合，すべてのスタックアドレスはリターン PC です。
これは， locForPC が想定するものです。
#

#			if l == 0 { // #
runtime.goexit
#
runtime.goexit
#

#// #
printCountProfile prints a countProfile at the specified debug level.
The profile will be in compressed proto format unless debug is nonzero.
#
printCountProfile は，指定されたデバッグレベルで countProfile を表示します。
debug が 0 以外の場合を除き，プロファイルは圧縮プロト形式になります。
#

#	// #
Build count of each stack.
#
各スタックのビルド数。
#

#		// #
Print debug profile in legacy format
#
デバッグプロファイルを従来の形式で表示する
#

#// #
keysByCount sorts keys with higher counts first, breaking ties by key string order.
#
keysByCount は最初にキーの数が多いキーをソートし，キー文字列の順序で関係を壊します。
#

#// #
printStackRecord prints the function + source line information
for a single stack trace.
#
printStackRecord は，単一スタックトレースの function + ソース行情報を出力します。
#

#			// #
Hide runtime.goexit and any runtime functions at the beginning.
This is useful mainly for allocation traces.
#
最初に runtime.goexit とランタイム関数を非表示にします。
これは主に割り当てトレースに役立ちます。
#

#		// #
We didn't print anything; do it again,
and this time include runtime functions.
#
何も表示しませんでした。
もう一度実行して，今度はランタイム関数を含めます。
#

#// #
Interface to system profiles.
#
システムプロファイルへのインターフェース。
#

#// #
WriteHeapProfile is shorthand for Lookup("heap").WriteTo(w, 0).
It is preserved for backwards compatibility.
#
WriteHeapProfile は Lookup(" ヒープ ") の省略形です WriteTo(w, 0) 。
下位互換性のために残されています。
#

#// #
countHeap returns the number of records in the heap profile.
#
countHeap は，ヒーププロファイル内のレコード数を返します。
#

#// #
writeHeap writes the current runtime heap profile to w.
#
writeHeap は，現在のランタイムヒーププロファイルを w に書き込みます。
#

#// #
writeAlloc writes the current runtime heap profile to w
with the total allocation space as the default sample type.
#
writeAlloc は，現在のランタイムヒーププロファイルを合計割り当てスペースをデフォルトのサンプル型として w に書き込みます。
#

#		// #
Read mem stats first, so that our other allocations
do not appear in the statistics.
#
最初に mem stats を読み，他の割り当てが統計に現れないようにします。
#

#	// #
Find out how many records there are (MemProfile(nil, true)),
allocate that many records, and get the data.
There's a race—more records might be added between
the two calls—so allocate a few extra records for safety
and also try again if we're very unlucky.
The loop should only execute one iteration in the common case.
#
(MemProfile(nil, true)) レコードがいくつあるか調べ，そのレコードをいくつ割り当て，そしてデータを取得します。
2 つの呼び出しの間にレコードが追加される可能性があるため，競合が発生します。
安全のために追加のレコードをいくつか割り当て，運が悪ければもう一度試してください。
一般的なケースでは，ループは 1 回だけ反復を実行します。
#

#		// #
Allocate room for a slightly bigger profile,
in case a few more entries have been added
since the call to MemProfile.
#
MemProfile の呼び出し以降にさらにいくつかのエントリが追加されている場合に備えて，やや大きいプロファイル用にスペースを割り当てます。
#

#		// #
Profile grew; try again.
#
プロファイルは成長しました。
再試行する。
#

#	// #
Technically the rate is MemProfileRate not 2*MemProfileRate,
but early versions of the C++ heap profiler reported 2*MemProfileRate,
so that's what pprof has come to expect.
#
技術的には，そのレートは 2*MemProfileRate ではなく MemProfileRate ですが， C++ ヒーププロファイラーの初期のバージョンでは 2*MemProfileRate が報告されていたので， pprof はこれを期待しています。
#

#	// #
Print memstats information too.
Pprof will ignore, but useful for people
#
memstats 情報も表示します。
Pprof は無視しますが，人々にとって有用です
#

#// #
countThreadCreate returns the size of the current ThreadCreateProfile.
#
countThreadCreate は現在の ThreadCreateProfile のサイズを返します。
#

#// #
writeThreadCreate writes the current runtime ThreadCreateProfile to w.
#
writeThreadCreate は，現在のランタイム ThreadCreateProfile を w に書き込みます。
#

#// #
countGoroutine returns the number of goroutines.
#
countGoroutine はゴルーチンの数を返します。
#

#// #
writeGoroutine writes the current runtime GoroutineProfile to w.
#
writeGoroutine は現在のランタイム GoroutineProfile を w に書き込みます。
#

#	// #
We don't know how big the buffer needs to be to collect
all the goroutines. Start with 1 MB and try a few times, doubling each time.
Give up and use a truncated trace if 64 MB is not enough.
#
すべてのゴルーチンを集めるためにバッファがどのくらいの大きさである必要があるかわかりません。
1 MB から始めて，毎回 2 倍にして，数回試してください。
64 MB で十分でない場合は，切り捨てたトレースを破棄して使用してください。
#

#			// #
Filled 64 MB - stop there.
#
64 MB いっぱい - そこでやめてください。
#

#	// #
Find out how many records there are (fetch(nil)),
allocate that many records, and get the data.
There's a race—more records might be added between
the two calls—so allocate a few extra records for safety
and also try again if we're very unlucky.
The loop should only execute one iteration in the common case.
#
レコードがいくつあるか (fetch(nil)) を調べ，そのレコードをいくつ割り当て，そしてデータを取得します。
2 つの呼び出しの間にレコードが追加される可能性があるため，競合が発生します。
安全のために追加のレコードをいくつか割り当て，運が悪ければもう一度試してください。
一般的なケースでは，ループは 1 回だけ反復を実行します。
#

#		// #
Allocate room for a slightly bigger profile,
in case a few more entries have been added
since the call to ThreadProfile.
#
ThreadProfile の呼び出し以降にさらにいくつかのエントリが追加されている場合に備えて，やや大きいプロファイル用にスペースを割り当てます。
#

#// #
StartCPUProfile enables CPU profiling for the current process.
While profiling, the profile will be buffered and written to w.
StartCPUProfile returns an error if profiling is already enabled.
#
StartCPUProfile は，現在のプロセスの CPU プロファイリングを有効にします。
プロファイリングしている間，プロファイルはバッファされて w に書き込まれます。
プロファイリングがすでに有効になっている場合， StartCPUProfile はエラーを返します。
#

#// #
On Unix-like systems, StartCPUProfile does not work by default for
Go code built with -buildmode=c-archive or -buildmode=c-shared.
StartCPUProfile relies on the SIGPROF signal, but that signal will
be delivered to the main program's SIGPROF signal handler (if any)
not to the one used by Go. To make it work, call os/signal.Notify
for syscall.SIGPROF, but note that doing so may break any profiling
being done by the main program.
#
Unix 系システムでは， StartCPUProfile は， -buildmode=c- アーカイブまたは -buildmode=c-shared でビルドされた Go コードに対してはデフォルトでは機能しません。
StartCPUProfile は SIGPROF シグナルに依存しますが，そのシグナルは Go で使用されるものではなくメインプログラムの SIGPROF シグナルハンドラ (もしあれば) に配信されます。
これを機能させるには， syscall.SIGPROF に対して os/signal.Notify を呼び出しますが，これを行うとメインプログラムによって行われているプロファイリングが壊れる可能性があることに注意してください。
#

#	// #
The runtime routines allow a variable profiling rate,
but in practice operating systems cannot trigger signals
at more than about 500 Hz, and our processing of the
signal is not cheap (mostly getting the stack trace).
100 Hz is a reasonable choice: it is frequent enough to
produce useful data, rare enough not to bog down the
system, and a nice round number to make it easy to
convert sample counts to seconds. Instead of requiring
each client to specify the frequency, we hard code it.
#
ランタイムルーチンは可変のプロファイリングレートを可能にします，しかし実際にはオペレーティングシステムは約 500 Hz 以上でシグナルを引き起こすことができません，そしてシグナルの私達の処理は安価ではありません (大抵スタックトレースを取得する) 。
100 Hz が妥当な選択です。
有用なデータを生成するのに十分な頻度で，システムを使い果たしないほどまれに，サンプル数を簡単に秒単位に変換できるようにするための適切なラウンド数です。
各クライアントに頻度の指定をリクエストするのではなく，ハードコードします。
#

#	// #
Double-check.
#
再確認してください。
#

#// #
readProfile, provided by the runtime, returns the next chunk of
binary CPU profiling stack trace data, blocking until data is available.
If profiling is turned off and all the profile data accumulated while it was
on has been returned, readProfile returns eof=true.
The caller must save the returned data and tags before calling readProfile again.
#
ランタイムによって渡される readProfile は，バイナリ CPU プロファイリングスタックトレースデータの次のチャンクを返し，データが利用可能になるまでブロックします。
プロファイリングがオフになっていて，オンになっている間に累積されたすべてのプロファイルデータが返された場合， readProfile は eof=true を返します。
呼び出し元は， readProfile を再度呼び出す前に，返されたデータとタグを保存する必要があります。
#

#		// #
The runtime should never produce an invalid or truncated profile.
It drops records that can't fit into its log buffers.
#
ランタイムは，無効または切り捨てられたプロファイルを作成してはいけません。
ログバッファに収まらないレコードを削除します。
#

#// #
StopCPUProfile stops the current CPU profile, if any.
StopCPUProfile only returns after all the writes for the
profile have completed.
#
StopCPUProfile は，現在の CPU プロファイルがあればそれを停止します。
StopCPUProfile は，プロファイルに対するすべての書き込みが完了した後にのみ戻ります。
#

#// #
countBlock returns the number of records in the blocking profile.
#
countBlock は，ブロッキングプロファイル内のレコード数を返します。
#

#// #
countMutex returns the number of records in the mutex profile.
#
countMutex は， mutex プロファイル内のレコード数を返します。
#

#// #
writeBlock writes the current blocking profile to w.
#
writeBlock は現在のブロッキングプロファイルを w に書き込みます。
#

#	// #
Do nothing.
The current way of block profile sampling makes it
hard to compute the unsampled number. The legacy block
profile parse doesn't attempt to scale or unsample.
#
何もしない。
ブロックプロファイルサンプリングの現在の方法は，サンプリングされていない数を計算することを困難にしている。
レガシーブロックプロファイルの解析では，スケーリングやアンサンプリングは行われません。
#

#// #
writeMutex writes the current mutex profile to w.
#
writeMutex は現在のミューテックスプロファイルを w に書き込みます。
#

#	// #
TODO(pjw): too much common code with writeBlock. FIX!
#
TODO(pjw):writeBlock であまりにも一般的なコード。
FIX!
#

[runtime/pprof/pprof.go]
#// #
    go test -cpuprofile cpu.prof -memprofile mem.prof -bench .
#
    go test -cpuprofile cpu.prof -memprofile mem.prof -bench .
#

[runtime/pprof/pprof.go]
#// #
   var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
   var memprofile = flag.String("memprofile", "", "write memory profile to `file`")
#
   var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
   var memprofile = flag.String("memprofile", "", "write memory profile to `file`")
#

[runtime/pprof/pprof.go]
#// #
   func main() {
       flag.Parse()
       if *cpuprofile != "" {
           f, err := os.Create(*cpuprofile)
           if err != nil {
               log.Fatal("could not create CPU profile: ", err)
           }
           defer f.Close()
           if err := pprof.StartCPUProfile(f); err != nil {
               log.Fatal("could not start CPU profile: ", err)
           }
           defer pprof.StopCPUProfile()
       }
#
   func main() {
       flag.Parse()
       if *cpuprofile != "" {
           f, err := os.Create(*cpuprofile)
           if err != nil {
               log.Fatal("could not create CPU profile: ", err)
           }
           defer f.Close()
           if err := pprof.StartCPUProfile(f); err != nil {
               log.Fatal("could not start CPU profile: ", err)
           }
           defer pprof.StopCPUProfile()
       }
#

[runtime/pprof/pprof.go]
#// #
   import _ "net/http/pprof"
#
   import _ "net/http/pprof"
#

[runtime/pprof/pprof.go]
#// #
   go tool pprof cpu.prof
#
   go tool pprof cpu.prof
#

