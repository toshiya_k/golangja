* 1

#// #
LabelSet is a set of labels.
#
LabelSet はラベルのセットです。
#

#// #
labelContextKey is the type of contextKeys used for profiler labels.
#
labelContextKey は，プロファイララベルに使用される contextKeys の型です。
#

#// #
labelMap is the representation of the label set held in the context type.
This is an initial implementation, but it will be replaced with something
that admits incremental immutable modification more efficiently.
#
labelMap は，コンテキスト型に保持されているラベルセットの表現です。
これは最初の実装ですが，増分不変変更をより効率的に認めるものに置き換えられます。
#

#// #
WithLabels returns a new context.Context with the given labels added.
A label overwrites a prior label with the same key.
#
WithLabels は与えられたラベルを追加した新しい context.Context を返します。
ラベルは前のラベルを同じキーで上書きします。
#

#	// #
TODO(matloob): replace the map implementation with something
more efficient so creating a child context WithLabels doesn't need
to clone the map.
#
TODO(matloob): マップの実装をより効率的なものに置き換えて，子コンテキスト WithLabels を作成することでマップを複製する必要がなくなりました。
#

#// #
Labels takes an even number of strings representing key-value pairs
and makes a LabelSet containing them.
A label overwrites a prior label with the same key.
#
Labels はキーと値のペアを表す偶数個の文字列を受け取り，それらを含む LabelSet を作成します。
ラベルは前のラベルを同じキーで上書きします。
#

#// #
Label returns the value of the label with the given key on ctx, and a boolean indicating
whether that label exists.
#
Label は， ctx に指定されたキーを持つラベルの値と，そのラベルが存在するかどうかを示すブール値を返します。
#

#// #
ForLabels invokes f with each label set on the context.
The function f should return true to continue iteration or false to stop iteration early.
#
ForLabels は，各ラベルをコンテキストに設定して f を呼び出します。
関数 f は，繰り返しを継続するには true を返し，繰り返しを早く停止するには false を返す必要があります。
#

