* 1

#// #
Parse the type units stored in a DWARF4 .debug_types section. Each
type unit defines a single primary type and an 8-byte signature.
Other sections may then use formRefSig8 to refer to the type.
#
DWARF4 の .debug_types セクションに格納されている型単位を解析します。
各型ユニットは， 1 つの基本型と 8 バイトのシグネチャを定義します。
他のセクションでは，その型を参照するために formRefSig8 を使用できます。
#

#// #
The typeUnit format is a single type with a signature. It holds
the same data as a compilation unit.
#
typeUnit の形式は，署名付きの単一型です。
コンパイル単位と同じデータを保持します。
#

#	toff  Offset // #
Offset to signature type within data.
#
データ内の署名型へのオフセット。
#

#	name  string // #
Name of .debug_type section.
#
.debug_type セクションの名前
#

#	cache Type   // #
Cache the type, nil to start.
#
型をキャッシュし，開始するには nil 。
#

#// #
Parse a .debug_types section.
#
.debug_types セクションを解析してください。
#

#// #
Return the type for a type signature.
#
型シグネチャの型を返します。
#

#// #
typeUnitReader is a typeReader for a tagTypeUnit.
#
typeUnitReader は tagTypeUnit の typeReader です。
#

#// #
Seek to a new position in the type unit.
#
型ユニット内の新しい位置にシークします。
#

#// #
AddressSize returns the size in bytes of addresses in the current type unit.
#
AddressSize は，現在の型単位のアドレスのサイズをバイト数で返します。
#

#// #
Next reads the next Entry from the type unit.
#
Next は型ユニットから次のエントリを読み込みます。
#

#// #
clone returns a new reader for the type unit.
#
clone は型ユニットの新しいリーダーを返します。
#

#// #
offset returns the current offset.
#
offset は現在のオフセットを返します。
#

