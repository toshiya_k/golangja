* 1

#// #
DWARF debug information entry parser.
An entry is a sequence of data items of a given format.
The first word in the entry is an index into what DWARF
calls the ``abbreviation table.''  An abbreviation is really
just a type descriptor: it's an array of attribute tag/value format pairs.
#
DWARF デバッグ情報エントリパーサー。
エントリは，与えられたフォーマットの一連のデータ項目です。
エントリの最初の単語は， DWARF が " 省略形テーブル " と呼ぶものへのインデックスです。
省略形は，実際は単なる型記述子です。
属性タグと値の形式のペアの配列です。
#

#// #
a single entry's description: a sequence of attributes
#
単一エントリの説明 : 一連の属性
#

#// #
a map from entry format ids to their descriptions
#
エントリー形式 ID からその説明へのマップ
#

#// #
ParseAbbrev returns the abbreviation table that starts at byte off
in the .debug_abbrev section.
#
ParseAbbrev は， .debug_abbrev セクションのバイトオフから始まる省略形テーブルを返します。
#

#	// #
Error handling is simplified by the buf getters
returning an endless stream of 0s after an error.
#
エラー処理の後， buf getter が無限の 0 のストリームを返すことでエラー処理が簡単になります。
#

#		// #
Table ends with id == 0.
#
テーブルは id == 0 で終わります。
#

#		// #
Walk over attributes, counting.
#
数えて，属性を見ていく。
#

#		b1 := b // #
Read from copy of b.
#
b のコピーから読み取る。
#

#		// #
Walk over attributes again, this time writing them down.
#
今度は属性を書き留めて，属性をもう一度調べます。
#

#// #
attrIsExprloc indicates attributes that allow exprloc values that
are encoded as block values in DWARF 2 and 3. See DWARF 4, Figure
20.
#
attrIsExprloc は， DWARF 2 および 3 でブロック値としてエンコードされる exprloc 値を許可する属性を示します。
図 20 の DWARF 4 を参照してください。
#

#// #
attrPtrClass indicates the *ptr class of attributes that have
encoding formSecOffset in DWARF 4 or formData* in DWARF 2 and 3.
#
attrPtrClass は， DWARF 4 では formSecOffset ， DWARF 2 および 3 では formData* のエンコーディングを持つ属性の *ptr クラスを示します。
#

#// #
formToClass returns the DWARF 4 Class for the given form. If the
DWARF version is less then 4, it will disambiguate some forms
depending on the attribute.
#
formToClass は与えられたフォームの DWARF 4 クラスを返します。
DWARF のバージョンが 4 未満の場合，属性によってはいくつかの形式が明確になります。
#

#		// #
In DWARF 2 and 3, ClassExprLoc was encoded as a
block. DWARF 4 distinguishes ClassBlock and
ClassExprLoc, but there are no attributes that can
be both, so we also promote ClassBlock values in
DWARF 4 that should be ClassExprLoc in case
producers get this wrong.
#
DWARF 2 および 3 では， ClassExprLoc はブロックとしてエンコードされていました。
DWARF 4 は ClassBlock と ClassExprLoc を区別しますが，両方になることができる属性がないので，プロデューサーがこれを誤解した場合には ClassExprLoc であるべき DWARF 4 の ClassBlock 値も推奨します。
#

#		// #
In DWARF 2 and 3, ClassPtr was encoded as a
constant. Unlike ClassExprLoc/ClassBlock, some
DWARF 4 attributes need to distinguish Class*Ptr
from ClassConstant, so we only do this promotion
for versions 2 and 3.
#
DWARF 2 および 3 では， ClassPtr は定数としてエンコードされていました。
ClassExprLoc/ClassBlock とは異なり，一部の DWARF 4 属性は Class*Ptr を ClassConstant と区別する必要があるため，この昇格はバージョン 2 および 3 に対してのみ行います。
#

#		// #
DWARF 4 defines four *ptr classes, but doesn't
distinguish them in the encoding. Disambiguate
these classes using the attribute.
#
DWARF 4 では 4 つの *ptr クラスが定義されていますが，エンコーディングでは区別されません。
属性を使用してこれらのクラスを明確にします。
#

#// #
An entry is a sequence of attribute/value pairs.
#
エントリは，属性と値のペアのシーケンスです。
#

#	Offset   Offset // #
offset of Entry in DWARF info
#
DWARF 情報のエントリのオフセット
#

#	Tag      Tag    // #
tag (kind of Entry)
#
タグ (エントリーの種類)
#

#	Children bool   // #
whether Entry is followed by children
#
エントリーの後に子供がいるかどうか
#

#// #
A Field is a single attribute/value pair in an Entry.
#
Field (フィールド) は，エントリ内の 1 つの属性と値のペアです。
#

#// #
A value can be one of several "attribute classes" defined by DWARF.
The Go types corresponding to each class are:
#
値は， DWARF によって定義されたいくつかの " 属性クラス " の 1 つになります。
各クラスに対応する Go 型は以下のとおりです。
#

#// #
   DWARF class       Go type        Class
#
   DWARF クラス       Go 型          Class
#

#// #
For unrecognized or vendor-defined attributes, Class may be
ClassUnknown.
#
認識されない属性またはベンダー定義の属性の場合， Class は ClassUnknown になります。
#

#// #
A Class is the DWARF 4 class of an attribute value.
#
Class (クラス) は，属性値の DWARF 4 クラスです。
#

#// #
In general, a given attribute's value may take on one of several
possible classes defined by DWARF, each of which leads to a
slightly different interpretation of the attribute.
#
一般に，与えられた属性の値は， DWARF によって定義されたいくつかの可能なクラスのうちの 1 つをとることができ，それぞれがわずかに異なる属性の解釈を導きます。
#

#// #
DWARF version 4 distinguishes attribute value classes more finely
than previous versions of DWARF. The reader will disambiguate
coarser classes from earlier versions of DWARF into the appropriate
DWARF 4 class. For example, DWARF 2 uses "constant" for constants
as well as all types of section offsets, but the reader will
canonicalize attributes in DWARF 2 files that refer to section
offsets to one of the Class*Ptr classes, even though these classes
were only defined in DWARF 3.
#
DWARF バージョン 4 は，属性値クラスを以前のバージョンの DWARF よりも細かく区別します。
リーダーは， DWARF の以前のバージョンからのより粗いクラスを適切な DWARF 4 クラスに明確にします。
たとえば， DWARF 2 は定数およびすべての型のセクションオフセットに " 定数 " を使用しますが，セクションオフセットが Class*Ptr クラスの 1 つだけである場合でも， DWARF 2 ファイル内の属性を正規化します。
DWARF 3 で定義されています。
#

#	// #
ClassUnknown represents values of unknown DWARF class.
#
ClassUnknown は，未知の DWARF クラスの値を表します。
#

#	// #
ClassAddress represents values of type uint64 that are
addresses on the target machine.
#
ClassAddress は，ターゲットマシンのアドレスである uint64 型の値を表します。
#

#	// #
ClassBlock represents values of type []byte whose
interpretation depends on the attribute.
#
ClassBlock は，その解釈が属性に依存する type []byte の値を表します。
#

#	// #
ClassConstant represents values of type int64 that are
constants. The interpretation of this constant depends on
the attribute.
#
ClassConstant は，定数である int64 型の値を表します。
この定数の解釈は属性によって異なります。
#

#	// #
ClassExprLoc represents values of type []byte that contain
an encoded DWARF expression or location description.
#
ClassExprLoc は，エンコードされた DWARF 式または場所の説明を含む []byte の値を表します。
#

#	// #
ClassFlag represents values of type bool.
#
ClassFlag は bool 型の値を表します。
#

#	// #
ClassLinePtr represents values that are an int64 offset
into the "line" section.
#
ClassLinePtr は， "line" セクションへの int64 オフセットの値を表します。
#

#	// #
ClassLocListPtr represents values that are an int64 offset
into the "loclist" section.
#
ClassLocListPtr は， "loclist" セクションへの int64 オフセットである値を表します。
#

#	// #
ClassMacPtr represents values that are an int64 offset into
the "mac" section.
#
ClassMacPtr は， "mac" セクションへの int64 オフセットである値を表します。
#

#	// #
ClassMacPtr represents values that are an int64 offset into
the "rangelist" section.
#
ClassMacPtr は， "rangelist" セクションへの int64 オフセットである値を表します。
#

#	// #
ClassReference represents values that are an Offset offset
of an Entry in the info section (for use with Reader.Seek).
The DWARF specification combines ClassReference and
ClassReferenceSig into class "reference".
#
ClassReference は， info セクション内のエントリのオフセットオフセットである値を表します (Reader.Seek と共に使用します) 。
DWARF 仕様では， ClassReference と ClassReferenceSig をクラス "reference" にまとめています。
#

#	// #
ClassReferenceSig represents values that are a uint64 type
signature referencing a type Entry.
#
ClassReferenceSig は，エントリ型を参照する uint64 型のシグニチャである値を表します。
#

#	// #
ClassString represents values that are strings. If the
compilation unit specifies the AttrUseUTF8 flag (strongly
recommended), the string value will be encoded in UTF-8.
Otherwise, the encoding is unspecified.
#
ClassString は文字列である値を表します。
コンパイル単位が AttrUseUTF8 フラグを指定している場合 (強くお勧めします) ，文字列値は UTF-8 でエンコードされます。
それ以外の場合，エンコーディングは指定されていません。
#

#	// #
ClassReferenceAlt represents values of type int64 that are
an offset into the DWARF "info" section of an alternate
object file.
#
ClassReferenceAlt は，代替オブジェクトファイルの DWARF "info" セクションへのオフセットである int64 型の値を表します。
#

#	// #
ClassStringAlt represents values of type int64 that are an
offset into the DWARF string section of an alternate object
file.
#
ClassStringAlt は，代替オブジェクトファイルの DWARF 文字列セクションへのオフセットである int64 型の値を表します。
#

#// #
Val returns the value associated with attribute Attr in Entry,
or nil if there is no such attribute.
#
Val は Entry の属性 Attr に関連付けられた値を返します。
そのような属性がない場合は nil を返します。
#

#// #
A common idiom is to merge the check for nil return with
the check that the value has the expected dynamic type, as in:
#
nil return のチェックと，値が予想される動的型であることのチェックを同時に行う一般的な方法は以下です。
#

#// #
AttrField returns the Field associated with attribute Attr in
Entry, or nil if there is no such attribute.
#
AttrField は Entry の属性 Attr に関連付けられた Field を返します。
そのような属性がない場合は nil を返します。
#

#// #
An Offset represents the location of an Entry within the DWARF info.
(See Reader.Seek.)
#
Offset (オフセット) は， DWARF 情報内のエントリの場所を表します。
(Reader.Seek を参照してください。)
#

#// #
Entry reads a single entry from buf, decoding
according to the given abbreviation table.
#
Entry は buf から 1 つのエントリを読み込み，与えられた略語の表に従ってデコードします。
#

#		// #
address
#
アドレス
#

#		// #
block
#
ブロック
#

#		// #
constant
#
定数
#

#		// #
flag
#
フラグ
#

#		// #
New in DWARF 4.
#
DWARF 4 の新機能
#

#			// #
The attribute is implicitly indicated as present, and no value is
encoded in the debugging information entry itself.
#
属性は存在すると暗黙的に示され，値はデバッグ情報項目自体にはエンコードされません。
#

#		// #
reference to other entry
#
他のエントリへの参照
#

#		// #
string
#
文字列
#

#			var off uint64 // #
offset into .debug_str
#
.debug_str へのオフセット
#

#		// #
lineptr, loclistptr, macptr, rangelistptr
New in DWARF 4, but clang can generate them with -gdwarf-2.
Section reference, replacing use of formData4 and formData8.
#
lineptr, loclistptr, macptr, rangelistptr
DWARF 4 の新機能ですが， clang は -gdwarf-2 を使用して生成できます。
formData4 と formData8 の使用に代わるセクション参照。
#

#			// #
64-bit type signature.
#
64 ビット型の署名
#

#// #
A Reader allows reading Entry structures from a DWARF ``info'' section.
The Entry structures are arranged in a tree. The Reader's Next function
return successive entries from a pre-order traversal of the tree.
If an entry has children, its Children field will be true, and the children
follow, terminated by an Entry with Tag 0.
#
Reader は DWARF の ``info'' セクションから Entry 構造体を読むことを可能にします。
エントリ構造はツリー状に配置されています。
Reader の Next 関数は，ツリーの事前注文走査から連続したエントリを返します。
エントリに子がある場合，その Children フィールドは true になり，子はそれに続き， Tag 0 のエントリで終了します。
#

#	lastChildren bool   // #
.Children of last entry returned by Next
#
Next によって返された最後のエントリの .Children
#

#	lastSibling  Offset // #
.Val(AttrSibling) of last entry returned by Next
#
Next によって返される最後のエントリの .Val(AttrSibling)
#

#// #
Reader returns a new Reader for Data.
The reader is positioned at byte offset 0 in the DWARF ``info'' section.
#
Reader は新しい Reader for Data を返します。
リーダーは， DWARF の " 情報 " セクションのバイトオフセット 0 に配置されています。
#

#// #
AddressSize returns the size in bytes of addresses in the current compilation
unit.
#
AddressSize は，現在のコンパイル単位のアドレスのサイズをバイト数で返します。
#

#// #
Seek positions the Reader at offset off in the encoded entry stream.
Offset 0 can be used to denote the first entry.
#
Seek (シーク) は，エンコードされたエントリストリーム内で Reader をオフセット off に配置します。
オフセット 0 を使用して最初の項目を示すことができます。
#

#// #
maybeNextUnit advances to the next unit if this one is finished.
#
これが終了すると， maybeNextUnit は次のユニットに進みます。
#

#// #
Next reads the next entry from the encoded entry stream.
It returns nil, nil when it reaches the end of the section.
It returns an error if the current offset is invalid or the data at the
offset cannot be decoded as a valid Entry.
#
Next は，エンコードされたエントリストリームから次のエントリを読み込みます。
セクションの終わりに達すると， nil, nil を返します。
現在のオフセットが無効であるか，オフセットのデータが有効なエントリとしてデコードできない場合はエラーを返します。
#

#// #
SkipChildren skips over the child entries associated with
the last Entry returned by Next. If that Entry did not have
children or Next has not been called, SkipChildren is a no-op.
#
SkipChildren は， Next によって返された最後のエントリに関連付けられている子エントリをスキップします。
そのエントリが子を持っていなかったり Next が呼び出されていなかった場合， SkipChildren は何もしません。
#

#	// #
If the last entry had a sibling attribute,
that attribute gives the offset of the next
sibling, so we can avoid decoding the
child subtrees.
#
最後のエントリに兄弟属性がある場合，その属性は次の兄弟のオフセットを与えるので，子サブツリーのデコードを避けることができます。
#

#// #
clone returns a copy of the reader. This is used by the typeReader
interface.
#
clone はリーダーのコピーを返します。
これは typeReader インターフェースによって使用されます。
#

#// #
offset returns the current buffer offset. This is used by the
typeReader interface.
#
offset は現在のバッファオフセットを返します。
これは typeReader インターフェースによって使用されます。
#

#// #
SeekPC returns the Entry for the compilation unit that includes pc,
and positions the reader to read the children of that unit.  If pc
is not covered by any unit, SeekPC returns ErrUnknownPC and the
position of the reader is undefined.
#
SeekPC は， pc を含むコンパイル単位のエントリを返し，その単位の子を読むようにリーダーを配置します。
pc がどのユニットにも覆われていない場合， SeekPC は ErrUnknownPC を返し，リーダーの位置は未定義です。
#

#// #
Because compilation units can describe multiple regions of the
executable, in the worst case SeekPC must search through all the
ranges in all the compilation units. Each call to SeekPC starts the
search at the compilation unit of the last call, so in general
looking up a series of PCs will be faster if they are sorted. If
the caller wishes to do repeated fast PC lookups, it should build
an appropriate index using the Ranges method.
#
コンパイル単位は実行可能ファイルの複数の領域を記述できるため，最悪の場合， SeekPC はすべてのコンパイル単位内のすべての範囲を検索する必要があります。
SeekPC を呼び出すたびに，最後の呼び出しのコンパイル単位で検索が開始されるため，一般的に，一連の PC をソートすると検索が速くなります。
呼び出し元が高速の PC ルックアップを繰り返し行う場合は， Ranges メソッドを使用して適切なインデックスを作成してください。
#

#// #
Ranges returns the PC ranges covered by e, a slice of [low,high) pairs.
Only some entry types, such as TagCompileUnit or TagSubprogram, have PC
ranges; for others, this will return nil with no error.
#
Ranges は， [low,high) ペアのスライスである e でカバーされる PC 範囲を返します。
TagCompileUnit や TagSub プログラムなどの一部のエントリ型にのみ PC 範囲があります。
他の人にとっては，これはエラーなしで nil を返すでしょう。
#

#		// #
The initial base address is the lowpc attribute
of the enclosing compilation unit.
Although DWARF specifies the lowpc attribute,
comments in gdb/dwarf2read.c say that some versions
of GCC use the entrypc attribute, so we check that too.
#
初期ベースアドレスは，囲んでいるコンパイル単位の lowpc 属性です。
DWARF は lowpc 属性を指定していますが， gdb/dwarf2read.c のコメントでは， GCC の一部のバージョンでは entrypc 属性が使用されているとのことです。
#

[debug/dwarf/entry.go]
#//	#
v, ok := e.Val(AttrSibling).(int64)
#
v, ok := e.Val(AttrSibling).(int64)
#

