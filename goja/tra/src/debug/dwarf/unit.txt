* 1

#// #
DWARF debug info is split into a sequence of compilation units.
Each unit has its own abbreviation table and address size.
#
DWARF デバッグ情報は一連のコンパイル単位に分割されます。
各ユニットには，独自の略語表とアドレスサイズがあります。
#

#	base   Offset // #
byte offset of header within the aggregate info
#
集約情報内のヘッダーのバイトオフセット
#

#	off    Offset // #
byte offset of data within the aggregate info
#
集約情報内のデータのバイトオフセット
#

#	is64   bool // #
True for 64-bit DWARF format
#
64 ビット DWARF フォーマットの場合は真
#

#// #
Implement the dataFormat interface.
#
dataFormat インターフェースを実装します。
#

#	// #
Count units.
#
単位を数えます。
#

#	// #
Again, this time writing them down.
#
もう一度，今度はそれらを書き留めます。
#

#// #
offsetToUnit returns the index of the unit containing offset off.
It returns -1 if no unit contains this offset.
#
offsetToUnit は，オフセット off を含むユニットのインデックスを返します。
このオフセットを含むユニットがない場合は -1 を返します。
#

#	// #
Find the unit after off
#
オフ後にユニットを探す
#

