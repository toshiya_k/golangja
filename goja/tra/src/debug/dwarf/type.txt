* 1

#// #
DWARF type information structures.
The format is heavily biased toward C, but for simplicity
the String methods use a pseudo-Go syntax.
#
DWARF 型の情報構造形式は C に大きく偏っていますが，簡単にするために String メソッドは疑似 Go 構文を使用します。
#

#// #
A Type conventionally represents a pointer to any of the
specific Type structures (CharType, StructType, etc.).
#
Type は通常，特定の Type 構造体 (CharType, StructType など) のいずれかへのポインタを表します。
#

#// #
A CommonType holds fields common to multiple types.
If a field is not known or not applicable for a given type,
the zero value is used.
#
CommonType は，複数の型に共通のフィールドを保持します。
フィールドが不明であるか特定の型に適用できない場合は，ゼロ値が使用されます。
#

#	ByteSize int64  // #
size of value of this type, in bytes
#
この型の値のサイズ (バイト)
#

#	Name     string // #
name that can be used to refer to type
#
型を参照するために使用できる名前
#

#// #
Basic types
#
基本型
#

#// #
A BasicType holds fields common to all basic types.
#
BasicType は，すべての基本型に共通のフィールドを保持します。
#

#// #
A CharType represents a signed character type.
#
CharType は符号付き文字型を表します。
#

#// #
A UcharType represents an unsigned character type.
#
UcharType は，符号なし文字型を表します。
#

#// #
An IntType represents a signed integer type.
#
IntType は符号付き整数型を表します。
#

#// #
A UintType represents an unsigned integer type.
#
UintType は符号なし整数型を表します。
#

#// #
A FloatType represents a floating point type.
#
FloatType は浮動小数点型を表します。
#

#// #
A ComplexType represents a complex floating point type.
#
ComplexType は，複素数浮動小数点型を表します。
#

#// #
A BoolType represents a boolean type.
#
BoolType はブール型を表します。
#

#// #
An AddrType represents a machine address type.
#
AddrType はマシンアドレス型を表します。
#

#// #
An UnspecifiedType represents an implicit, unknown, ambiguous or nonexistent type.
#
UnspecifiedType は，暗黙的，不明，あいまいまたは存在しない型を表します。
#

#// #
qualifiers
#
限定子
#

#// #
A QualType represents a type that has the C/C++ "const", "restrict", or "volatile" qualifier.
#
QualType は， C/C++ の "const", "restrict" ，または "volatile" 修飾子を持つ型を表します。
#

#// #
An ArrayType represents a fixed size array type.
#
ArrayType は，固定サイズの配列型を表します。
#

#	StrideBitSize int64 // #
if > 0, number of bits to hold each element
#
0 より大きい場合，各要素を保持するビット数
#

#	Count         int64 // #
if == -1, an incomplete array, like char x[].
#
if == -1 ， char x[] のような不完全な配列。
#

#// #
A VoidType represents the C void type.
#
VoidType は C の void 型を表します。
#

#// #
A PtrType represents a pointer type.
#
PtrType はポインタ型を表します。
#

#// #
A StructType represents a struct, union, or C++ class type.
#
StructType は，構造体，共用体，または C++ クラス型を表します。
#

#	Kind       string // #
"struct", "union", or "class".
#
"struct", "union" ，または "class" 。
#

#	Incomplete bool // #
if true, struct, union, class is declared but not defined
#
true の場合， struct, union, class は宣言されているが未定義
#

#// #
A StructField represents a field in a struct, union, or C++ class type.
#
StructField は，構造体，共用体，または C++ クラス型のフィールドを表します。
#

#	ByteSize   int64 // #
usually zero; use Type.Size() for normal fields
#
通常はゼロです。 通常のフィールドには Type.Size() を使用してください。
#

#	BitOffset  int64 // #
within the ByteSize bytes at ByteOffset
#
ByteOffset の ByteSize バイト以内
#

#	BitSize    int64 // #
zero if not a bit field
#
ビットフィールドでない場合はゼロ
#

#// #
An EnumType represents an enumerated type.
The only indication of its native integer type is its ByteSize
(inside CommonType).
#
EnumType は列挙型を表します。
そのネイティブの整数型の唯一の指示は，その ByteSize(CommonType 内部) です。
#

#// #
An EnumValue represents a single enumeration value.
#
EnumValue は 1 つの列挙値を表します。
#

#// #
A FuncType represents a function type.
#
FuncType は関数型を表します。
#

#// #
A DotDotDotType represents the variadic ... function parameter.
#
DotDotDotType は可変個の ... 関数パラメータを表します。
#

#// #
A TypedefType represents a named type.
#
TypedefType は名前付き型を表します。
#

#// #
An UnsupportedType is a placeholder returned in situations where we
encounter a type that isn't supported.
#
UnsupportedType は，サポートされていない型に遭遇した場合に返されるプレースホルダーです。
#

#// #
typeReader is used to read from either the info section or the
types section.
#
typeReader は， info セクションまたは types セクションから読み取るために使用されます。
#

#	// #
AddressSize returns the size in bytes of addresses in the current
compilation unit.
#
AddressSize は，現在のコンパイル単位のアドレスのサイズをバイト数で返します。
#

#// #
Type reads the type at off in the DWARF ``info'' section.
#
Type は DWARF の ``info'' セクションの off の位置にある型を読み込みます。
#

#// #
readType reads a type from r at off of name. It adds types to the
type cache, appends new typedef types to typedefs, and computes the
sizes of types. Callers should pass nil for typedefs; this is used
for internal recursion.
#
readType は，名前から離れた位置にある r から型を読み取ります。
型キャッシュに型を追加し，新しい typedef 型を typedef に追加し，型のサイズを計算します。
呼び出し元は typedef に nil を渡すべきです。
これは内部再帰に使われます。
#

#	// #
If this is the root of the recursion, prepare to resolve
typedef sizes once the recursion is done. This must be done
after the type graph is constructed because it may need to
resolve cycles in a different order than readType
encounters them.
#
これが再帰の根源である場合は，再帰が完了したら typedef サイズを解決する準備をしてください。
readType が遭遇するのとは異なる順序でサイクルを解決する必要があるかもしれないので，これは型グラフが構築された後に行われなければなりません。
#

#	// #
Parse type from Entry.
Must always set typeCache[off] before calling
d.readType recursively, to handle circular types correctly.
#
Entry から型を解析します。
循環型を正しく処理するには， d.readType を再帰的に呼び出す前に必ず typeCache[off] を設定する必要があります。
#

#	// #
Get next child; set err if error happens.
#
次の子を入手してください。
エラーが発生した場合は err を設定します。
#

#		// #
Only return direct children.
Skip over composite entries that happen to be nested
inside this one. Most DWARF generators wouldn't generate
such a thing, but clang does.
See golang.org/issue/6472.
#
直接の子供だけを返します。
このエントリの中に入れ子になっている可能性がある複合エントリをスキップします。
ほとんどの DWARF ジェネレータはそのようなものを生成しませんが， clang は生成します。
golang.org/issue/6472 を参照してください。
#

#	// #
Get Type referred to by Entry's AttrType field.
Set err if error happens. Not having a type is an error.
#
Entry の AttrType フィールドによって参照される Get Type 。
エラーが発生した場合は err を設定します。
型がないとエラーになります。
#

#			// #
It appears that no Type means "void".
#
どの Type も "void" を意味しないようです。
#

#		// #
Multi-dimensional array.  (DWARF v2 §5.4)
Attributes:
#
多次元配列 (DWARF v2 §5.4) 属性 :
#

#		//	#
AttrType:subtype [required]
AttrStrideSize: size in bits of each element of the array
AttrByteSize: size of entire array
#
AttrType:subtype [必須]
AttrStrideSize: 配列の各要素のサイズ (ビット単位)
AttrByteSize: 配列全体のサイズ
#

#		// #
Children:
#
子供 :
#

#		//	#
TagSubrangeType or TagEnumerationType giving one dimension.
dimensions are in left to right order.
#
1 つの次元を与える TagSubrangeType または TagEnumerationType 。
寸法は左から右の順です。
#

#		// #
Accumulate dimensions,
#
寸法を累計する
#

#			// #
TODO(rsc): Can also be TagEnumerationType
but haven't seen that in the wild yet.
#
TODO(rsc): TagEnumerationType でも構いませんが，まだ実際には使用されていません。
#

#					// #
Old binaries may have an upper bound instead.
#
古いバイナリには上限があるかもしれません。
#

#						count++ // #
Length is one more than upper bound.
#
長さは上限より 1 つ多いです。
#

#						count = -1 // #
As in x[].
#
x[] と同じです。
#

#			// #
LLVM generates this for x[].
#
LLVM はこれを x[] に対して生成します。
#

#		// #
Basic type.  (DWARF v2 §5.1)
Attributes:
#
基本型 (DWARF v2§ 5.1) 属性 :
#

#		//	#
AttrName: name of base type in programming language of the compilation unit [required]
AttrEncoding: encoding value for type (encFloat etc) [required]
AttrByteSize: size of type in bytes [required]
AttrBitOffset: for sub-byte types, size in bits
AttrBitSize: for sub-byte types, bit offset of high order bit in the AttrByteSize bytes
#
AttrName: コンパイル単位のプログラミング言語での基本型の名前 [必須]
AttrEncoding: 型のエンコーディング値 (encFloat など) [必須]
AttrByteSize: バイト単位の型のサイズ [必須]
AttrBitOffset: サブバイト型の場合，ビット単位のサイズ
AttrBitSize: サブバイト型の場合， AttrByteSize バイトの上位ビットのビットオフセット
#

#				// #
clang writes out 'complex' instead of 'complex float' or 'complex double'.
clang also writes out a byte size that we can use to distinguish.
See issue 8694.
#
clang は， 'complex float' または 'complex double' の代わりに 'complex' を書き出します。
clang は，区別するために使用できるバイトサイズも書き出します。
Issue 8694 を参照してください。
#

#		// #
Structure, union, or class type.  (DWARF v2 §5.5)
Attributes:
#
構造体，共用体，またはクラス型。 (DWARF v2 §5.5)
属性 :
#

#		//	#
AttrName: name of struct, union, or class
AttrByteSize: byte size [required]
AttrDeclaration: if true, struct/union/class is incomplete
#
AttrName: 構造体，共用体，またはクラスの名前
AttrByteSize: バイトサイズ [必須]
AttrDeclaration:true の場合，構造体 / 共用体 / クラスは不完全
#

#		//	#
TagMember to describe one member.
	AttrName: name of member [required]
	AttrType: type of member [required]
	AttrByteSize: size in bytes
	AttrBitOffset: bit offset within bytes for bit fields
	AttrBitSize: bit size for bit fields
	AttrDataMemberLoc: location within struct [required for struct, class]
#
1 人のメンバーを説明する TagMember 。
	AttrName: メンバーの名前 [必須]
	AttrType: メンバーの種類 [必須]
	AttrByteSize: ビットフィールドのバイト内のビットオフセット
	AttrBitSize: ビットフィールドのビット内のビットサイズ
	AttrDataMemberLoc: 構造体内の位置 [構造体，クラスに必要]
#

#		// #
There is much more to handle C++, all ignored for now.
#
C++ を処理するためのもっと多くのものがあり，今のところすべて無視されています。
#

#				// #
TODO: Should have original compilation
unit here, not unknownFormat.
#
TODO:unknownFormat ではなく，ここに元のコンパイル単位が必要です。
#

#				// #
Last field was zero width. Fix array length.
(DWARF writes out 0-length arrays as if they were 1-length arrays.)
#
最後のフィールドはゼロ幅でした。
配列の長さを修正してください。
(DWARF は，長さ 0 の配列を長さ 1 の配列のように書き出します。)
#

#				// #
Final field must be zero width. Fix array length.
#
最終フィールドはゼロ幅でなければなりません。 配列の長さを修正してください。
#

#		// #
Type modifier (DWARF v2 §5.2)
Attributes:
#
型修飾子 (DWARF v2 §5.2) 属性 :
#

#		//	#
AttrType: subtype
#
AttrType: サブ型
#

#		// #
Enumeration type (DWARF v2 §5.6)
Attributes:
#
列挙型 (DWARF v2 §5.6) 属性 :
#

#		//	#
AttrName: enum name if any
AttrByteSize: bytes required to represent largest value
#
AttrName: 列挙型の名前(あれば)
AttrByteSize: 最大値を表すために必要なバイト
#

#		//	#
TagEnumerator:
	AttrName: name of constant
	AttrConstValue: value of constant
#
TagEnumerator:
	AttrName: 定数の名前
	AttrConstValue: 定数の値
#

#		//	#
AttrType: subtype [not required!  void* has no AttrType]
AttrAddrClass: address class [ignored]
#
AttrType: サブ型 [必須ではありません。 void* には AttrType がありません。]
AttrAddrClass: アドレスクラス [無視]
#

#		// #
Subroutine type.  (DWARF v2 §5.7)
Attributes:
#
サブルーチン型 (DWARF v2 §5.7) 属性 :
#

#		//	#
AttrType: type of return value if any
AttrName: possible name of type [ignored]
AttrPrototyped: whether used ANSI C prototype [ignored]
#
AttrType: 存在する場合は戻り値の型
AttrName: 型の可能な名前 [無視]
AttrPrototyped:ANSIC プロト型を使用したかどうか [無視]
#

#		//	#
TagFormalParameter: typed parameter
	AttrType: type of parameter
TagUnspecifiedParameter: final ...
#
TagFormalParameter: 型付きパラメータ
	AttrType: パラメータの型
TagUnspecifiedParameter: final ...
#

#		// #
Typedef (DWARF v2 §5.3)
Attributes:
#
Typedef (DWARF v2 §5.3)
属性 :
#

#		//	#
AttrName: name [required]
AttrType: type definition [required]
#
AttrName: 名前 [必須]
AttrType: 型定義 [必須]
#

#		// #
Unspecified type (DWARF v3 §5.2)
Attributes:
#
未指定型 (DWARF v3 §5.2)
属性 :
#

#		//	#
AttrName: name
#
AttrName: 名前
#

#				// #
Record that we need to resolve this
type's size once the type graph is
constructed.
#
型グラフが作成されたら，この型のサイズを解決する必要があることを記録します。
#

#	// #
If the parse fails, take the type out of the cache
so that the next call with this offset doesn't hit
the cache and return success.
#
解析が失敗した場合は，このオフセットを持つ次の呼び出しがキャッシュにヒットして成功を返さないように，キャッシュから型を取り出します。
#

#	// #
Make a copy to avoid invalidating typeCache.
#
typeCache を無効にしないようにコピーを作成してください。
#

[debug/dwarf/type.go]
#		// #
This is some other type DIE that we're currently not
equipped to handle. Return an abstract "unsupported type"
object in such cases.
#
これは，現在処理する能力がない他の型の DIE です。そのような場合，抽象的な " サポートされていない型 " オブジェクトを返します。
#

