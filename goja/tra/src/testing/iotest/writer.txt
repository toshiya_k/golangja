* 1

#// #
TruncateWriter returns a Writer that writes to w
but stops silently after n bytes.
#
TruncateWriter は， w に書き込みますが， n バイト後に黙って停止する Writer を返します。
#

#	// #
real write
#
本物の書き込み
#

