* 1

#// #
NewWriteLogger returns a writer that behaves like w except
that it logs (using log.Printf) each write to standard error,
printing the prefix and the hexadecimal data written.
#
NewWriteLogger は， (log.Printf を使用して) 各書き込みを標準エラーに記録し，書き込まれたプレフィックスと 16 進数データを出力することを除いて， w のように動作するライターを返します。
#

#// #
NewReadLogger returns a reader that behaves like r except
that it logs (using log.Printf) each read to standard error,
printing the prefix and the hexadecimal data read.
#
NewReadLogger は， r のように動作するリーダーを返します。
ただし， (log.Printf を使用して) 読み取った値を標準エラーに記録し，読み取ったプレフィックスと 16 進数のデータを出力します。
#

