* 1

#// #
Package lzw implements the Lempel-Ziv-Welch compressed data format,
described in T. A. Welch, ``A Technique for High-Performance Data
Compression'', Computer, 17(6) (June 1984), pp 8-19.
#
lzw パッケージは Lempel-Ziv-Welch 圧縮データフォーマットを実装します。
このフォーマットは以下の記事で記述されています。 T. A. Welch, ``A Technique for High-Performance Data
Compression'', Computer, 17(6) (June 1984), pp 8-19.
#

#// #
In particular, it implements LZW as used by the GIF and PDF file
formats, which means variable-width codes up to 12 bits and the first
two non-literal codes are a clear code and an EOF code.
#
特に，GIF や PDF ファイルフォーマットで使われている LZW を実装します。
つまり，12 ビットまでの長さ可変コードで，最初の 2 つの非リテラルコードは
クリアコードと EOF コードです。
#

#// #
The TIFF file format uses a similar but incompatible version of the LZW
algorithm. See the golang.org/x/image/tiff/lzw package for an
implementation.
#
TIFF ファイルフォーマットは，類似した，しかし互換性のないバージョンの LZW アルゴリズムを使っています。
golang.org/x/image/tiff/lzw に実装があります。
#

#// #
TODO(nigeltao): check that PDF uses LZW in the same way as GIF,
modulo LSB/MSB packing order.
#
TODO(nigeltao): PDF が GIF ，モジュロ LSB/MSB パッキング順序と同じ方法で LZW を使用することを確認。
#

#// #
Order specifies the bit ordering in an LZW data stream.
#
Order は， LZW データストリームのビット順序を指定します。
#

#	// #
LSB means Least Significant Bits first, as used in the GIF file format.
#
LSB は，GIF ファイル形式で使用されているように，最下位ビットファーストを意味します。
#

#	// #
MSB means Most Significant Bits first, as used in the TIFF and PDF
file formats.
#
MSB は，TIFF および PDF ファイル形式で使用されているように， 最上位ビットファーストを意味します。
#

#// #
decoder is the state from which the readXxx method converts a byte
stream into a code stream.
#
decoder は readXxx メソッドがバイトストリームをコードストリームに変換する状態です。
#

#	read     func(*decoder) (uint16, error) // #
readLSB or readMSB
#
readLSB または readMSB
#

#	litWidth int                            // #
width in bits of literal codes
#
リテラルコードのビット幅
#

#	// #
The first 1<<litWidth codes are literal codes.
The next two codes mean clear and EOF.
Other valid codes are in the range [lo, hi] where lo := clear + 2,
with the upper bound incrementing on each code seen.
#
最初の 1<<litWidth コードはリテラルコードです。
次の 2 つのコードは，クリアと EOF を意味します。
他の有効なコードは [lo, hi] の範囲にあります。
ここで lo := clear + 2 で，見られるそれぞれのコードで上限が増加します。
#

#	// #
overflow is the code at which hi overflows the code width. It always
equals 1 << width.
#
overflow は， hi がコード幅をオーバーフローさせるコードです。
これは常に 1 << width に等しくなります。
#

#	// #
last is the most recently seen code, or decoderInvalidCode.
#
last は最後に見たコード，または decoderInvalidCode です。
#

#	// #
An invariant is that
#
不変式
#

#	// #
Each code c in [lo, hi] expands to two or more bytes. For c != hi:
  suffix[c] is the last of these bytes.
  prefix[c] is the code for all but the last byte.
  This code can either be a literal code or another code in [lo, c).
The c == hi case is a special case.
#
[lo, hi] の各コード c は 2 バイト以上に拡張されます。
c != hi の場合:
  suffix[c] がこれらのバイトの最後です。
  prefix[c] は最後のバイト以外のすべてのコードです。
  このコードはリテラルコードか [lo, c) の別のコードのどちらかです。
c == hi は特別な場合です。
#

#	// #
output is the temporary output buffer.
Literal codes are accumulated from the start of the buffer.
Non-literal codes decode to a sequence of suffixes that are first
written right-to-left from the end of the buffer before being copied
to the start of the buffer.
It is flushed when it contains >= 1<<maxWidth bytes,
so that there is always room to decode an entire code.
#
output は一時出力バッファーです。
リテラルコードはバッファの先頭から累積されます。
非リテラルコードは，バッファの先頭にコピーされる前に，バッファの末尾から右から左に最初に書き込まれる一連の接尾辞にデコードされます。
1<<maxWidth 以上のバイトが含まれている場合はフラッシュされます。
そのため，常にコード全体をデコードする余地があります。
#

#	o      int    // #
write index into output
#
インデックスを出力に書き込む
#

#	toRead []byte // #
bytes to return from Read
#
Read から返されるバイト数
#

#// #
readLSB returns the next code for "Least Significant Bits first" data.
#
readLSB は "Least Significant Bits first" データの次のコードを返します。
#

#// #
readMSB returns the next code for "Most Significant Bits first" data.
#
readMSB は "Most Significant Bits first" データの次のコードを返します。
#

#// #
decode decompresses bytes from r and leaves them in d.toRead.
read specifies how to decode bytes into codes.
litWidth is the width in bits of literal codes.
#
decode は r からバイトを解凍し， d.toRead に残します。
read はバイトをコードにデコードする方法を指定します。
litWidth は，リテラルコードのビット数です。
#

#	// #
Loop over the code stream, converting codes into decompressed bytes.
#
コードストリームをループ処理し，コードを圧縮解除バイトに変換します。
#

#			// #
We have a literal code.
#
リテラルコードがあります。
#

#				// #
Save what the hi code expands to.
#
hi コードが展開したものを保存する。
#

#				// #
code == hi is a special case which expands to the last expansion
followed by the head of the last expansion. To find the head, we walk
the prefix chain until we find a literal code.
#
code == hi は，最後の展開の先頭に続いて最後の展開まで展開する特別な場合です。
先頭を見つけるために，リテラルコードが見つかるまでプレフィックスチェーンをたどります。
#

#			// #
Copy the suffix chain into output and then write that to w.
#
接尾辞チェーンを出力にコピーしてから w に書き込みます。
#

#				// #
Undo the d.hi++ a few lines above, so that (1) we maintain
the invariant that d.hi <= d.overflow, and (2) d.hi does not
eventually overflow a uint16.
#
数行前の d.hi++ をアンドゥします。それにより，
(1) d.hi <= d.overflow という不変式を維持し，
(2) d.hi が最終的には uint16 をオーバーフローしない。
#

#	// #
Flush pending output.
#
保留中の出力をフラッシュします。
#

#	d.err = errClosed // #
in case any Reads come along
#
何らかの読み取りが発生した場合
#

#// #
NewReader creates a new io.ReadCloser.
Reads from the returned io.ReadCloser read and decompress data from r.
If r does not also implement io.ByteReader,
the decompressor may read more data than necessary from r.
It is the caller's responsibility to call Close on the ReadCloser when
finished reading.
The number of bits to use for literal codes, litWidth, must be in the
range [2,8] and is typically 8. It must equal the litWidth
used during compression.
#
NewReader は新しい io.ReadCloser を作成します。
返り値の io.ReadCloser からの Read は， r からデータを読み取り，解凍します。
r が io.ByteReader を実装していない場合，解凍器は r から必要以上のデータを読み取る可能性があります。
読み終わったら ReadCloser の Close を呼び出すのは呼び出し側の責任です。
リテラルコードに使用するビット数 litWidth は， [2,8] の範囲内でなければならず，通常は 8 です。
これは，圧縮中に使用される litWidth と等しくなければなりません。
#

