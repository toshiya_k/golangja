* 1

#// #
A writer is a buffered, flushable writer.
#
writer は，バッファリングされた，フラッシュ可能なライターです。
#

#// #
An errWriteCloser is an io.WriteCloser that always returns a given error.
#
errWriteCloser は，常に特定のエラーを返す io.WriteCloser です。
#

#	// #
A code is a 12 bit value, stored as a uint32 when encoding to avoid
type conversions when shifting bits.
#
コードは 12 ビット値で，ビットシフト時の型変換を避けるためにエンコード時に uint32 として格納されます。
#

#	// #
There are 1<<12 possible codes, which is an upper bound on the number of
valid hash table entries at any given point in time. tableSize is 4x that.
#
1<<12 の可能なコードがあります。
これは，任意の時点における有効なハッシュテーブルエントリ数の上限です。
tableSize はその 4 倍です。
#

#	// #
A hash table entry is a uint32. Zero is an invalid entry since the
lower 12 bits of a valid entry must be a non-literal code.
#
ハッシュテーブルエントリは uint32 です。
有効なエントリの下位 12 ビットは非リテラルコードでなければならないため，ゼロは無効なエントリです。
#

#// #
encoder is LZW compressor.
#
エンコーダは LZW 圧縮器です。
#

#	// #
w is the writer that compressed bytes are written to.
#
w は圧縮バイトの書き込み先のライターです。
#

#	// #
order, write, bits, nBits and width are the state for
converting a code stream into a byte stream.
#
order ， write ， bits ， nBits ， width は，コードストリームをバイトストリームに変換するための状態です。
#

#	// #
litWidth is the width in bits of literal codes.
#
litWidth は，リテラルコードのビット数です。
#

#	// #
hi is the code implied by the next code emission.
overflow is the code at which hi overflows the code width.
#
hiこんにちはは，次回のコード発行で暗示されるコードです。
overflow は， hi がコード幅をオーバーフローさせるコードです。
#

#	// #
savedCode is the accumulated code at the end of the most recent Write
call. It is equal to invalidCode if there was no such call.
#
savedCode は，最新の Write 呼び出しの最後に蓄積されたコードです。
そのような呼び出しがなかった場合は， invalidCode と同じです。
#

#	// #
err is the first error encountered during writing. Closing the encoder
will make any future Write calls return errClosed
#
err は書き込み中に発生した最初のエラーです。
エンコーダを閉じると，今後の Write 呼び出しで errClosed が返されます。
#

#	// #
table is the hash table from 20-bit keys to 12-bit values. Each table
entry contains key<<12|val and collisions resolve by linear probing.
The keys consist of a 12-bit code prefix and an 8-bit byte suffix.
The values are a 12-bit code.
#
table は， 20 ビットキーから 12 ビット値へのハッシュテーブルです。
各テーブルエントリは key<<12|val を含み，衝突は線形プロービングによって解決されます。
キーは， 12 ビットのコードプレフィックスと 8 ビットのバイトサフィックスで構成されています。
値は 12 ビットコードです。
#

#// #
writeLSB writes the code c for "Least Significant Bits first" data.
#
writeLSB は， "Least Significant Bits first" データのコード c を書き込みます。
#

#// #
writeMSB writes the code c for "Most Significant Bits first" data.
#
writeMSB は， "Most Significant Bits first" データのコード c を書き込みます。
#

#// #
errOutOfCodes is an internal error that means that the encoder has run out
of unused codes and a clear code needs to be sent next.
#
errOutOfCodes は内部エラーで，エンコーダの未使用コードがなくなったため，次にクリアコードを送信する必要があります。
#

#// #
incHi increments e.hi and checks for both overflow and running out of
unused codes. In the latter case, incHi sends a clear code, resets the
encoder state and returns errOutOfCodes.
#
incHi は e.hi をインクリメントし，オーバーフローと未使用コードの不足の両方をチェックします。
後者の場合， incHi はクリアコードを送信し，エンコーダの状態をリセットして errOutOfCodes を返します。
#

#// #
Write writes a compressed representation of p to e's underlying writer.
#
Write は， p の圧縮表現を e の内部のライターに書き込みます。
#

#		// #
The first code sent is always a literal code.
#
最初に送信されたコードは常にリテラルコードです。
#

#		// #
If there is a hash table hit for this key then we continue the loop
and do not emit a code yet.
#
このキーに対してヒットしたハッシュテーブルがある場合は，ループを継続し，まだコードを発行しません。
#

#		// #
Otherwise, write the current code, and literal becomes the start of
the next emitted code.
#
そうでなければ，現在のコードを書くと，リテラルが次に発行されたコードの先頭になります。
#

#		// #
Increment e.hi, the next implied code. If we run out of codes, reset
the encoder state (including clearing the hash table) and continue.
#
次の暗黙のコード e.hi を増分します。
コードが足りなくなったら，エンコーダの状態をリセットし (ハッシュテーブルのクリアを含む) ，続行します。
#

#		// #
Otherwise, insert key -> e.hi into the map that e.table represents.
#
そうでなければ， key -> e.hi を e.table が表すマップに挿入してください。
#

#// #
Close closes the encoder, flushing any pending output. It does not close or
flush e's underlying writer.
#
Close はエンコーダを閉じて，保留中の出力をフラッシュします。
e の内部のライターを閉じたりフラッシュしたりしません。
#

#	// #
Make any future calls to Write return errClosed.
#
今後 Write を呼び出すと， errClosed が返されます。
#

#	// #
Write the savedCode if valid.
#
有効であれば savedCode を書きます。
#

#	// #
Write the eof code.
#
eof コードを書きます。
#

#	// #
Write the final bits.
#
最後のビットを書きます。
#

#// #
NewWriter creates a new io.WriteCloser.
Writes to the returned io.WriteCloser are compressed and written to w.
It is the caller's responsibility to call Close on the WriteCloser when
finished writing.
The number of bits to use for literal codes, litWidth, must be in the
range [2,8] and is typically 8. Input bytes must be less than 1<<litWidth.
#
NewWriter は新しい io.WriteCloser を作成します。
返された io.WriteCloser への書き込みは圧縮されて， w に書き込まれます。
書き込みが終わったら WriteCloser の Close を呼び出すのは呼び出し側の責任です。
リテラルコードに使用するビット数 litWidth は [2,8] の範囲内でなければならず，通常は 8 です。
入力バイトは 1<<litWidth 未満でなければなりません。
#

