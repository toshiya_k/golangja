* 1

#// #
Unix environment variables.
#
Unix 環境変数
#

#	// #
envOnce guards initialization by copyenv, which populates env.
#
envOnce は， env を生成する copyenv による初期化を保護します。
#

#	// #
envLock guards env and envs.
#
envLock は env と envs を保護します。
#

#	// #
env maps from an environment variable to its first occurrence in envs.
#
env は，環境変数から envs 内の最初の出現位置にマップします。
#

#	// #
envs is provided by the runtime. elements are expected to
be of the form "key=value". An empty string means deleted
(or a duplicate to be ignored).
#
envs はランタイムによって渡されます。
要素は "key=value" の形式であることが期待されています。
空の文字列は削除されたことを意味します (または重複して無視されます) 。
#

#func runtime_envs() []string // #
in package runtime
#
パッケージランタイム内
#

#// #
setenv_c and unsetenv_c are provided by the runtime but are no-ops
if cgo isn't loaded.
#
setenv_c と unsetenv_c はランタイムによって渡されますが， cgo がロードされていない場合は何もしません。
#

#					env[key] = i // #
first mention of key
#
キーの最初の言及
#

#					// #
Clear duplicate keys. This permits Unsetenv to
safely delete only the first item without
worrying about unshadowing a later one,
which might be a security problem.
#
重複したキーを消去します。
これは Unsetenv が最初のアイテムだけを安全に削除することを可能にします。
セキュリティ上の問題となる可能性があります。
#

#	envOnce.Do(copyenv) // #
prevent copyenv in Getenv/Setenv
#
Getenv/Setenv で copyenv を防止する
#

