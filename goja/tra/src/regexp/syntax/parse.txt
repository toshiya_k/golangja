* 1

#// #
An Error describes a failure to parse a regular expression
and gives the offending expression.
#
エラーは，正規表現を解析できなかったことを表し，問題のある表現を表します。
#

#// #
An ErrorCode describes a failure to parse a regular expression.
#
ErrorCode は，正規表現を解析できなかったことを表します。
#

#	// #
Unexpected error
#
予期しないエラー
#

#	// #
Parse errors
#
解析エラー
#

#// #
Flags control the behavior of the parser and record information about regexp context.
#
フラグはパーサーの振る舞いを制御し，正規表現コンテキストに関する情報を記録します。
#

#	FoldCase      Flags = 1 << iota // #
case-insensitive match
#
大文字と小文字を区別しない
#

#	Literal                         // #
treat pattern as literal string
#
パターンをリテラル文字列として扱う
#

#	ClassNL                         // #
allow character classes like [^a-z] and [[:space:]] to match newline
#
[^a-z] や [[:space:]] のような文字クラスを改行にマッチさせる
#

#	DotNL                           // #
allow . to match newline
#
許可します。 改行に合わせる
#

#	OneLine                         // #
treat ^ and $ as only matching at beginning and end of text
#
^ と $ をテキストの最初と最後でのみ一致するものとして扱います。
#

#	NonGreedy                       // #
make repetition operators default to non-greedy
#
繰り返し演算子をデフォルトで非欲張りにする
#

#	PerlX                           // #
allow Perl extensions
#
Perl 拡張を許可する
#

#	UnicodeGroups                   // #
allow \p{Han}, \P{Han} for Unicode group and negation
#
Unicode グループと否定に \p{Han} ， \P{Han} を許可
#

#	WasDollar                       // #
regexp OpEndText was $, not \z
#
正規表現 OpEndText は $ ではなく， \z でした
#

#	Simple                          // #
regexp contains no counted repetition
#
正規表現にカウントされた繰り返しが含まれていません
#

#	Perl        = ClassNL | OneLine | PerlX | UnicodeGroups // #
as close to Perl as possible
#
できるだけ Perl に近い
#

#	POSIX Flags = 0                                         // #
POSIX syntax
#
POSIX の構文
#

#// #
Pseudo-ops for parsing stack.
#
スタックを解析するための疑似命令。
#

#	flags       Flags     // #
parse mode flags
#
解析モードフラグ
#

#	stack       []*Regexp // #
stack of parsed expressions
#
解析された式のスタック
#

#	numCap      int // #
number of capturing groups seen
#
見られたキャプチャグループの数
#

#	tmpClass    []rune // #
temporary char class work space
#
一時文字クラス作業スペース
#

#// #
Parse stack manipulation.
#
スタック操作を解析します。
#

#// #
push pushes the regexp re onto the parse stack and returns the regexp.
#
push は正規表現 re をパーススタックにプッシュし，その正規表現を返します。
#

#		// #
Single rune.
#
シングルルーン。
#

#		// #
Case-insensitive rune like [Aa] or [Δδ].
#
[Aa] または [Δδ] のように大文字と小文字を区別しないルーン。
#

#		// #
Rewrite as (case-insensitive) literal.
#
(大文字と小文字を区別しない) リテラルとして書き換えます。
#

#		// #
Incremental concatenation.
#
インクリメンタル連結。
#

#// #
maybeConcat implements incremental concatenation
of literal runes into string nodes. The parser calls this
before each push, so only the top fragment of the stack
might need processing. Since this is called before a push,
the topmost literal is no longer subject to operators like *
(Otherwise ab* would turn into (ab)*.)
If r >= 0 and there's a node left over, maybeConcat uses it
to push r with the given flags.
maybeConcat reports whether r was pushed.
#
maybeConcat は，文字列ノードへのリテラルルーンの増分連結を実装します。
パーサーは各プッシュの前にこれを呼び出すので，スタックの一番上のフラグメントだけが処理を必要とするかもしれません。
これはプッシュの前に呼び出されるので，一番上のリテラルは * のような演算子の影響を受けなくなります (そうでなければ ab* は (ab)* になります) 。
r >= 0 で残りノードがある場合， maybeConcat はそれを使って r を押します。
与えられたフラグを使って。
maybeConcat は， r がプッシュされたかどうかを報告します。
#

#	// #
Push re1 into re2.
#
re1 を re2 に押し込みます。
#

#	// #
Reuse re1 if possible.
#
可能であれば re1 を再利用してください。
#

#	return false // #
did not push r
#
r を押しませんでした
#

#// #
newLiteral returns a new OpLiteral Regexp with the given flags
#
newLiteral は与えられたフラグを持つ新しい OpLiteral 正規表現を返します
#

#// #
minFoldRune returns the minimum rune fold-equivalent to r.
#
minFoldRune は， r と等価な最小ルーンの倍数を返します。
#

#// #
literal pushes a literal regexp for the rune r on the stack
and returns that regexp.
#
literal は，ルーン r のリテラル正規表現をスタックにプッシュし，その正規表現を返します。
#

#// #
op pushes a regexp with the given op onto the stack
and returns that regexp.
#
op は与えられた op を持つ正規表現をスタックにプッシュし，その正規表現を返します。
#

#// #
repeat replaces the top stack element with itself repeated according to op, min, max.
before is the regexp suffix starting at the repetition operator.
after is the regexp suffix following after the repetition operator.
repeat returns an updated 'after' and an error, if any.
#
repeat は， op, min, max に従って，先頭のスタック要素をそれ自体が繰り返されたものに置き換えます。
before は繰り返し演算子で始まる正規表現の接尾辞です。
after は，繰り返し演算子の後に続く正規表現接尾辞です。
repeat は更新された 'after' と，もしあればエラーを返します。
#

#			// #
In Perl it is not allowed to stack repetition operators:
a** is a syntax error, not a doubled star, and a++ means
something else entirely, which we don't support!
#
Perl では，繰り返し演算子をスタックすることは許可されていません。
** は構文エラーであり，二重星ではありません。
また， ++ は完全に別のものを意味します。
#

#// #
repeatIsValid reports whether the repetition re is valid.
Valid means that the combination of the top-level repetition
and any inner repetitions does not exceed n copies of the
innermost thing.
This function rewalks the regexp tree and is called for every repetition,
so we have to worry about inducing quadratic behavior in the parser.
We avoid this by only calling repeatIsValid when min or max >= 2.
In that case the depth of any >= 2 nesting can only get to 9 without
triggering a parse error, so each subtree can only be rewalked 9 times.
#
repeatIsValid は繰り返し re が有効かどうかを報告します。
有効とは，最上位レベルの繰り返しと任意の内側の繰り返しの組み合わせが，最も内側のものの n コピーを超えないことを意味します。
この関数は正規表現ツリーを書き換えて，繰り返しごとに呼び出されるので，パーサーで 2 次の振る舞いを引き起こすことを心配する必要があります。
min または max >= 2 のときに repeatIsValid を呼び出すだけでこれを回避できます。
その場合， >>2 のネスティングの深さは解析エラーを引き起こさずに 9 までしか得られないため，各サブツリーは 9 回だけ再探索できます。
#

#// #
concat replaces the top of the stack (above the topmost '|' or '(') with its concatenation.
#
concat はスタックの最上部 (最上部の '|' または '(' の上) をその連結で置き換えます。
#

#	// #
Scan down to find pseudo-operator | or (.
#
下にスキャンして疑似演算子を見つけます。
または ((。
#

#	// #
Empty concatenation is special case.
#
空の連結は特別な場合です。
#

#// #
alternate replaces the top of the stack (above the topmost '(') with its alternation.
#
alternate は，スタックの最上部 (最上部の '(') の上にあるものをその代替物に置き換えます。
#

#	// #
Scan down to find pseudo-operator (.
There are no | above (.
#
下にスキャンして疑似演算子 (を見つけます。
#

#	// #
Make sure top class is clean.
All the others already are (see swapVerticalBar).
#
トップクラスがきれいであることを確認してください。
他のものはすべてすでに存在しています (swapVerticalBar を参照) 。
#

#	// #
Empty alternate is special case
(shouldn't happen but easy to handle).
#
空の代替案は特別な場合です (起こるべきではないが扱いやすい) 。
#

#// #
cleanAlt cleans re for eventual inclusion in an alternation.
#
cleanAlt は，代替に最終的に含まれるように re をクリーンアップします。
#

#			// #
re.Rune will not grow any more.
Make a copy or inline to reclaim storage.
#
re.Rune はもう成長しません。
コピーを作成するかインラインにしてストレージを再利用します。
#

#// #
collapse returns the result of applying op to sub.
If sub contains op nodes, they all get hoisted up
so that there is never a concat of a concat or an
alternate of an alternate.
#
collapse は sub に op を適用した結果を返します。
sub に op ノードが含まれる場合，それらはすべて連結され，連結の代替または代替の代替が存在することはありません。
#

#// #
factor factors common prefixes from the alternation list sub.
It returns a replacement list that reuses the same storage and
frees (passes to p.reuse) any removed *Regexps.
#
代替リスト sub からの共通の接頭辞を因数分解します。
同じ記憶域を再利用し，削除された *Regexps を解放する (p.reuse に渡す) 置換リストを返します。
#

#// #
For example,
    ABC|ABD|AEF|BCX|BCY
simplifies by literal prefix extraction to
    A(B(C|D)|EF)|BC(X|Y)
which simplifies by character class introduction to
    A(B[CD]|EF)|BC[XY]
#
たとえば， ABC|ABD|AEF|BCX|BCY はリテラルプレフィックス抽出によって A(B(C|D)|EF)|BC(X|Y) に簡略化され， A(B[CD]|B|CD||) に簡略化されます。
EF)|BC[XY]
#

#	// #
Round 1: Factor out common literal prefixes.
#
ラウンド 1: 一般的なリテラルプレフィックスを除外します。
#

#		// #
Invariant: the Regexps that were in sub[0:start] have been
used or marked for reuse, and the slice space has been reused
for out (len(out) <= start).
#
不変式 :sub[0:start] に含まれていた正規表現が使用されているか，再使用のためにマークされていて，スライススペースが out として再使用されている (len(out) <= start) 。
#

#		// #
Invariant: sub[start:i] consists of regexps that all begin
with str as modified by strflags.
#
不変式 : sub[start:i] は，すべて strflags で変更された str で始まる正規表現で構成されています。
#

#					// #
Matches at least one rune in current range.
Keep going around.
#
現在の範囲内の少なくとも 1 つのルーンに一致します。
歩き続けなさい。
#

#		// #
Found end of a run with common leading literal string:
sub[start:i] all begin with str[0:len(str)], but sub[i]
does not even begin with str[0].
#
共通の先行リテラル文字列で実行の終わりが見つかりました :sub[start:i] すべて str[0:len(str)] で始まりますが， sub[i] は str[0] で始まっていません。
#

#		// #
Factor out common string and append factored expression to out.
#
共通の文字列を取り除き，因数分解された式を out に追加します。
#

#			// #
Nothing to do - run of length 0.
#
何もしない - 長さ 0 のラン。
#

#			// #
Just one: don't bother factoring.
#
一つだけ : ファクタリングを邪魔しないでください。
#

#			// #
Construct factored form: prefix(suffix1|suffix2|...)
#
因数分解された形式を構築します。
prefix(suffix1|suffix2|...)
#

#			suffix := p.collapse(sub[start:i], OpAlternate) // #
recurse
#
再帰
#

#		// #
Prepare for next iteration.
#
次の繰り返しに備えます。
#

#	// #
Round 2: Factor out common simple prefixes,
just the first piece of each concatenation.
This will be good enough a lot of the time.
#
ラウンド 2: 一般的な単純接頭辞を除外し，各連結の最初の部分だけにします。
これは十分な時間があるでしょう。
#

#	// #
Complex subexpressions (e.g. involving quantifiers)
are not safe to factor because that collapses their
distinct paths through the automaton, which affects
correctness in some cases.
#
複雑な部分式 (例えば，数量詞を含む) は，オートマトンを通るそれらの異なる経路を崩壊させるため，因数分解に対して安全ではありません。
#

#		// #
Invariant: sub[start:i] consists of regexps that all begin with ifirst.
#
不変式 : sub[start:i] は，すべて ifirst で始まる正規表現で構成されています。
#

#				// #
first must be a character class OR a fixed repeat of a character class.
#
最初は文字クラスまたは文字クラスの固定反復でなければなりません。
#

#		// #
Found end of a run with common leading regexp:
sub[start:i] all begin with first but sub[i] does not.
#
一般的な先頭の regexp で実行の終わりが見つかりました :sub[start:i] すべて first で始まりますが， sub[i] ではありません。
#

#		// #
Factor out common regexp and append factored expression to out.
#
一般的な正規表現を除外し，因数分解された式を out に追加します。
#

#				reuse := j != start // #
prefix came from sub[start]
#
接頭辞は sub から来ました [start]
#

#	// #
Round 3: Collapse runs of single literals into character classes.
#
ラウンド 3: 単一リテラルの実行を文字クラスに折りたたみます。
#

#		// #
Invariant: sub[start:i] consists of regexps that are either
literal runes or character classes.
#
不変式 : sub[start:i] は，リテラルルーンまたは文字クラスのいずれかである正規表現で構成されています。
#

#		// #
sub[i] is not a char or char class;
emit char class for sub[start:i]...
#
sub[i] は char でも char クラスでもありません。
sub[start:i] の char クラスを発行します ...
#

#			// #
Make new char class.
Start with most complex regexp in sub[start].
#
新しい char クラスを作ります。
sub[start] で最も複雑な正規表現から始めてください。
#

#		// #
... and then emit sub[i].
#
... そして sub[i] を発行します。
#

#	// #
Round 4: Collapse runs of empty matches into a single empty match.
#
ラウンド 4: 空のマッチの連続を 1 つの空のマッチにまとめます。
#

#// #
leadingString returns the leading literal string that re begins with.
The string refers to storage in re or its children.
#
leadingString は， re で始まる先頭のリテラル文字列を返します。
文字列は re またはその子の中のストレージを表します。
#

#// #
removeLeadingString removes the first n leading runes
from the beginning of re. It returns the replacement for re.
#
removeLeadingString は， re の先頭から最初の n 個の先頭ルーン文字を削除します。
re の代わりを返します。
#

#		// #
Removing a leading string in a concatenation
might simplify the concatenation.
#
連結で先頭の文字列を削除すると，連結が簡単になる場合があります。
#

#				// #
Impossible but handle.
#
不可能だが扱う。
#

#// #
leadingRegexp returns the leading regexp that re begins with.
The regexp refers to storage in re or its children.
#
leadingRegexp は， re で始まる先頭の正規表現を返します。
正規表現は re またはその子の中のストレージを指します。
#

#// #
removeLeadingRegexp removes the leading regexp in re.
It returns the replacement for re.
If reuse is true, it passes the removed regexp (if no longer needed) to p.reuse.
#
removeLeadingRegexp は， re の先頭の正規表現を削除します。
re の代わりを返します。
もし再利用が真ならば，削除された正規表現 (もはや必要でなければ) を p.reuse に渡します。
#

#	re.Rune = re.Rune0[:0] // #
use local storage for small strings
#
小さな文字列にローカルストレージを使用する
#

#			// #
string is too long to fit in Rune0.  let Go handle it
#
文字列が長すぎるため， Rune0 に収まりません。
Go に任せましょう
#

#// #
Parsing.
#
解析しています。
#

#// #
Parse parses a regular expression string s, controlled by the specified
Flags, and returns a regular expression parse tree. The syntax is
described in the top-level comment.
#
Parse は，指定された Flags によって制御されている正規表現文字列 s を解析し，正規表現解析木を返します。
構文はトップレベルのコメントで説明されています。
#

#		// #
Trivial parser for literal string.
#
リテラル文字列のための些細なパーサ。
#

#	// #
Otherwise, must do real work.
#
そうでなければ，本物の仕事をしなければならない。
#

#				// #
Flag changes and non-capturing groups.
#
変更と非キャプチャグループにフラグを立てます。
#

#				// #
If the repeat cannot be parsed, { is a literal.
#
繰り返しを解析できない場合， { はリテラルです。
#

#				// #
Numbers were too big, or max is present and min > max.
#
数値が大きすぎるか， max が存在し， min > max です。
#

#					// #
any byte; not supported
#
任意のバイトサポートされていません
#

#					// #
\Q ... \E: the ... is always literals
#
\Q ...\E:... は常にリテラルです
#

#			// #
Look for Unicode character group like \p{Han}
#
\p{Han} のような Unicode 文字グループを探してください
#

#			// #
Perl character class escape.
#
Perl 文字クラスのエスケープ
#

#			// #
Ordinary single-character escape.
#
通常の 1 文字のエスケープ
#

#		// #
pop vertical bar
#
ポップ垂直バー
#

#// #
parseRepeat parses {min} (max=min) or {min,} (max=-1) or {min,max}.
If s is not of that form, it returns ok == false.
If s has the right form but the values are too big, it returns min == -1, ok == true.
#
parseRepeat は {min} (max=min) または {min,} (max=-1) または {min,max} を解析します。
s がその形式でなければ， ok == false を返します。
s の形式は正しいが値が大きすぎる場合は， min == -1, ok == true を返します。
#

#			// #
parseInt found too big a number
#
parseInt が大きすぎる数値を見つけました
#

#// #
parsePerlFlags parses a Perl flag setting or non-capturing group or both,
like (?i) or (?: or (?i:.  It removes the prefix from s and updates the parse state.
The caller must have ensured that s begins with "(?".
#
parsePerlFlags は， (?i) ， (?: ， (?i: のように)Perl フラグ設定または非キャプチャー・グループ，あるいはその両方を解析します。
これは， s から接頭部を除去し，解析状態を更新します。
"(?"
#

#	// #
Check for named captures, first introduced in Python's regexp library.
As usual, there are three slightly different syntaxes:
#
Python の正規表現ライブラリで最初に導入された名前付きキャプチャをチェックします。
いつものように， 3 つのわずかに異なる構文があります。
#

#	// #
  (?P<name>expr)   the original, introduced by Python
  (?<name>expr)    the .NET alteration, adopted by Perl 5.10
  (?'name'expr)    another .NET alteration, adopted by Perl 5.10
#
(?P<name>expr) オリジナルの， Python で導入された (?<name>expr).NET の変更， Perl 5.10 で採用 (?'name'expr) 別の .NET の変更， Perl 5.10 で採用
#

#	// #
Perl 5.10 gave in and implemented the Python version too,
but they claim that the last two are the preferred forms.
PCRE and languages based on it (specifically, PHP and Ruby)
support all three as well. EcmaScript 4 uses only the Python form.
#
Perl 5.10 も Python バージョンを提供し，実装しましたが，彼らは最後の 2 つが好ましい形式であると主張します。
PCRE とそれを基にした言語 (特に PHP と Ruby) も 3 つすべてをサポートしています。
EcmaScript 4 は Python 形式のみを使用しています。
#

#	// #
In both the open source world (via Code Search) and the
Google source tree, (?P<expr>name) is the dominant form,
so that's the one we implement. One is enough.
#
オープンソースの世界 (コード検索による) と Google のソースツリーの両方で， (?P<expr> の名前) が優勢な形式なので，それが私たちが実装しているものです。
一つで十分です。
#

#		// #
Pull out name.
#
名前を抜く
#

#		capture := t[:end+1] // #
"(?P<name>"
#
"(?P< 名前 >"
#

#		name := t[4:end]     // #
"name"
#
" 名 "
#

#		// #
Like ordinary capture, but named.
#
通常の捕獲と似ていますが，名前が付けられています。
#

#	// #
Non-capturing group. Might also twiddle Perl flags.
#
非捕獲グループ Perl のフラグも回転させる可能性があります。
#

#	t = t[2:] // #
skip (?
#
スキップ (?
#

#		// #
Flags.
#
フラグ
#

#		// #
Switch to negation.
#
否定に切り替えます。
#

#			// #
Invert flags so that | above turn into &^ and vice versa.
We'll invert flags again before using it below.
#
次のようにフラグを反転します。
上記は& ^ に変わり，その逆も同様です。
以下で使用する前に，フラグをもう一度反転します。
#

#		// #
End of flags, starting group or not.
#
フラグの終わり，開始グループかどうか
#

#				// #
Open new group
#
新しいグループを開く
#

#// #
isValidCaptureName reports whether name
is a valid capture name: [A-Za-z0-9_]+.
PCRE limits names to 32 bytes.
Python rejects names starting with digits.
We don't enforce either of those.
#
isValidCaptureName は， name が有効なキャプチャ名かどうかを報告します : [A-Za-z0-9_]+ 。
PCRE は名前を 32 バイトに制限します。
Python は数字で始まる名前を拒否します。
どちらも強制しません。
#

#// #
parseInt parses a decimal integer.
#
parseInt は 10 進整数を解析します。
#

#	// #
Disallow leading zeros.
#
先行ゼロを許可しません。
#

#	// #
Have digits, compute value.
#
数字があり，値を計算します。
#

#		// #
Avoid overflow.
#
オーバーフローを避けます。
#

#// #
can this be represented as a character class?
single-rune literal string, char class, ., and .|\n.
#
これは文字クラスとして表現できますか ? 単一ルーンのリテラル文字列， char クラス，。
，および。
|\n 。
#

#// #
does re match r?
#
r と一致しますか ?
#

#// #
parseVerticalBar handles a | in the input.
#
parseVerticalBar は a を処理します。
入力で。
#

#	// #
The concatenation we just parsed is on top of the stack.
If it sits above an opVerticalBar, swap it below
(things below an opVerticalBar become an alternation).
Otherwise, push a new vertical bar.
#
今解析した連結はスタックの一番上にあります。
それが opVerticalBar の上にあるならば，それを下に交換してください (opVerticalBar の下のものは交互になります) 。
そうでない場合は，新しい垂直バーを押します。
#

#// #
mergeCharClass makes dst = dst|src.
The caller must ensure that dst.Op >= src.Op,
to reduce the amount of copying.
#
mergeCharClass は dst = dst|src にします。
呼び出し元は，コピー量を減らすために， dst.Op >= src.Op であることを確認する必要があります。
#

#		// #
src doesn't add anything.
#
src は何も追加しません。
#

#		// #
src might add \n
#
src が追加する可能性があります \n
#

#		// #
src is simpler, so either literal or char class
#
src はもっと単純なので，リテラルか char クラス
#

#		// #
both literal
#
両方とも文字通り
#

#// #
If the top of the stack is an element followed by an opVerticalBar
swapVerticalBar swaps the two and returns true.
Otherwise it returns false.
#
スタックの最上部が opVerticalBar が続く要素である場合， swapVerticalBar は 2 つを入れ替えて true を返します。
そうでなければ false を返します。
#

#	// #
If above and below vertical bar are literal or char class,
can merge into a single char class.
#
垂直バーの上下がリテラルまたは char クラスの場合は， 1 つの char クラスに結合できます。
#

#		// #
Make re3 the more complex of the two.
#
2 つのうち， re3 をもっと複雑にします。
#

#				// #
Now out of reach.
Clean opportunistically.
#
手が届かなくなりました。
日和見的にきれいにしなさい。
#

#// #
parseRightParen handles a ) in the input.
#
parseRightParen は入力で a ) を処理します。
#

#	// #
Restore flags at time of paren.
#
親のいないときにフラグを復元します。
#

#		// #
Just for grouping.
#
グループ化だけのために。
#

#// #
parseEscape parses an escape sequence at the beginning of s
and returns the rune.
#
parseEscape は， s の先頭でエスケープシーケンスを解析してルーン文字を返します。
#

#			// #
Escaped non-word characters are always themselves.
PCRE is not quite so rigorous: it accepts things like
\q, but we don't. We once rejected \_, but too many
programs and people insist on using it, so allow \_.
#
エスケープされた非単語文字は常にそれ自体です。
PCRE はそれほど厳密ではありません。
\q のようなものを受け入れますが，受け入れません。
私たちはかつて拒否しましたが，あまりにも多くのプログラムや人々がそれを使うことを主張しているので，許可してください。
#

#	// #
Octal escapes.
#
8 進数のエスケープ
#

#		// #
Single non-zero digit is a backreference; not supported
#
1 桁のゼロ以外の数字は後方参照です。
サポートされていません
#

#		// #
Consume up to three octal digits; already have one.
#
最大 8 桁の 8 進数を消費します。
すでに持っています。
#

#	// #
Hexadecimal escapes.
#
16 進数のエスケープ
#

#			// #
Any number of digits in braces.
Perl accepts any text at all; it ignores all text
after the first non-hex digit. We require only hex digits,
and at least one.
#
中かっこ内の任意の桁数。
Perl はどんなテキストも受け入れます。
最初の 16 進以外の数字の後のテキストはすべて無視されます。
必要なのは 16 進数と少なくとも 1 つだけです。
#

#		// #
Easy case: two hex digits.
#
簡単な場合 :2 桁の 16 進数。
#

#	// #
C escapes. There is no case 'b', to avoid misparsing
the Perl word-boundary \b as the C backspace \b
when in POSIX mode. In Perl, /\b/ means word-boundary
but /[\b]/ means backspace. We don't support that.
If you want a backspace, embed a literal backspace
character or use \x08.
#
C は逃げる。
POSIX モードのときに Perl の単語境界 \b を C のバックスペースとして誤って解析しないようにするための 'b' はありません。
Perl では， /\b/ は単語境界を意味しますが， /[\b]/ はバックスペースを意味します。
我々はそれを支持しません。
バックスペースが必要な場合は，リテラルバックスペース文字を埋め込むか， \x08 を使用してください。
#

#// #
parseClassChar parses a character class character at the beginning of s
and returns it.
#
parseClassChar は， s の先頭にある文字クラス character を解析して返します。
#

#	// #
Allow regular escape sequences even though
many need not be escaped in this context.
#
この文脈では多くをエスケープする必要がない場合でも，通常のエスケープシーケンスを許可します。
#

#// #
parsePerlClassEscape parses a leading Perl character class escape like \d
from the beginning of s. If one is present, it appends the characters to r
and returns the new slice r and the remainder of the string.
#
parsePerlClassEscape は， s の先頭から \d のような先頭の Perl 文字クラスのエスケープを解析します。
存在する場合は，文字を r に追加して，新しいスライス r と文字列の残りの部分を返します。
#

#// #
parseNamedClass parses a leading POSIX named character class like [:alnum:]
from the beginning of s. If one is present, it appends the characters to r
and returns the new slice r and the remainder of the string.
#
parseNamedClass は， [:alnum:] のような先頭の POSIX 名前付き文字クラスを s の先頭から解析します。
存在する場合は，文字を r に追加して，新しいスライス r と文字列の残りの部分を返します。
#

#// #
unicodeTable returns the unicode.RangeTable identified by name
and the table of additional fold-equivalent code points.
#
unicodeTable は，名前によって識別される unicode.RangeTable と，その他の折りたたみ可能なコードポイントのテーブルを返します。
#

#	// #
Special case: "Any" means any.
#
特別な場合 : "Any" は any を意味します。
#

#// #
parseUnicodeClass parses a leading Unicode character class like \p{Han}
from the beginning of s. If one is present, it appends the characters to r
and returns the new slice r and the remainder of the string.
#
parseUnicodeClass は， \p{Han} のような主要な Unicode 文字クラスを s の先頭から解析します。
存在する場合は，文字を r に追加して，新しいスライス r と文字列の残りの部分を返します。
#

#	// #
Committed to parse or return error.
#
解析またはエラーを返すことを約束します。
#

#		// #
Single-letter name.
#
1 文字の名前
#

#		// #
Name is in braces.
#
名前は中かっこで囲みます。
#

#	// #
Group can have leading negation too.  \p{^Han} == \P{Han}, \P{^Han} == \p{Han}.
#
グループも先導的否定を持つことができます。
\p{^Han} ==\P{Han} ， \P{^Han} ==\p{Han} 。
#

#		// #
Merge and clean tab and fold in a temporary buffer.
This is necessary for the negative case and just tidy
for the positive case.
#
タブをマージしてきれいにし，一時的なバッファに入れます。
これは否定的な場合には必要で，肯定的な場合には片付けるだけです。
#

#// #
parseClass parses a character class at the beginning of s
and pushes it onto the parse stack.
#
parseClass は， s の先頭で文字クラスを解析し，それを解析スタックにプッシュします。
#

#	t := s[1:] // #
chop [
#
チョップ [
#

#		// #
If character class does not match \n, add it here,
so that negation later will do the right thing.
#
文字クラスが \n と一致しない場合は，ここで追加してください。
そうすれば，後で否定しても正しい結果が得られます。
#

#	first := true // #
] and - are okay as first char in class
#
] と - はクラスの最初の文字として大丈夫です
#

#		// #
POSIX: - is only okay unescaped as first or last in class.
Perl: - is okay anywhere.
#
POSIX: - クラスの先頭または末尾がエスケープされていなくても問題ありません。
Perl: - どこでもいいです。
#

#		// #
Look for POSIX [:alnum:] etc.
#
POSIX [:alnum:] などを探してください。
#

#		// #
Look for Unicode character group like \p{Han}.
#
\p{Han} のような Unicode 文字グループを探してください。
#

#		// #
Look for Perl character class symbols (extension).
#
Perl の文字クラスシンボル (拡張子) を探してください。
#

#		// #
Single character or simple range.
#
単一文字または単純範囲
#

#		// #
[a-] means (a|-) so check for final ].
#
[a-] は (a|-) を意味するので， final をチェックしてください。
#

#	t = t[1:] // #
chop ]
#
チョップ]
#

#	// #
Use &re.Rune instead of &class to avoid allocation.
#
割り当てを避けるには，&class の代わりに&re.Rune を使用してください。
#

#// #
cleanClass sorts the ranges (pairs of elements of r),
merges them, and eliminates duplicates.
#
cleanClass は範囲 (r の要素のペア) をソートし，それらをマージして，重複を排除します。
#

#	// #
Sort by lo increasing, hi decreasing to break ties.
#
lo の昇順，降順の並べ替えでネクタイを破ります。
#

#	// #
Merge abutting, overlapping.
#
隣接して，重なり合ってマージします。
#

#	w := 2 // #
write index
#
インデックスを書く
#

#			// #
merge with previous range
#
前の範囲とマージする
#

#		// #
new disjoint range
#
互いに重ならない範囲
#

#// #
appendLiteral returns the result of appending the literal x to the class r.
#
appendLiteral は，リテラル x をクラス r に追加した結果を返します。
#

#// #
appendRange returns the result of appending the range lo-hi to the class r.
#
appendRange は，範囲 lo-hi をクラス r に追加した結果を返します。
#

#	// #
Expand last range or next to last range if it overlaps or abuts.
Checking two ranges helps when appending case-folded
alphabets, so that one range can be expanding A-Z and the
other expanding a-z.
#
最後の範囲または最後の範囲が重なっている，または隣接している場合は，最後の範囲の次に拡張します。
2 つの範囲をチェックすると，大文字に折り畳まれたアルファベットを追加するときに役立ちます。
そのため，一方の範囲は A-Z を拡大し，もう一方の範囲は a-z を拡大できます。
#

#	for i := 2; i <= 4; i += 2 { // #
twice, using i=2, i=4
#
2 回， i=2, i=4 を使用
#

#	// #
minimum and maximum runes involved in folding.
checked during test.
#
折り畳みに関与する最小および最大ルーン。
テスト中に確認
#

#// #
appendFoldedRange returns the result of appending the range lo-hi
and its case folding-equivalent runes to the class r.
#
appendFoldedRange は，範囲 lo-hi とその大文字小文字の区別が同じルーンをクラス r に追加した結果を返します。
#

#	// #
Optimizations.
#
最適化
#

#		// #
Range is full: folding can't add more.
#
範囲がいっぱいです : 折りたたみはそれ以上追加できません。
#

#		// #
Range is outside folding possibilities.
#
範囲は折りたたみ可能範囲外です。
#

#		// #
[lo, minFold-1] needs no folding.
#
[lo, minFold-1] は折りたたみが不要です。
#

#		// #
[maxFold+1, hi] needs no folding.
#
[maxFold+1, hi] は折りたたみが不要です。
#

#	// #
Brute force. Depend on appendRange to coalesce ranges on the fly.
#
ブルートフォース。その場で範囲を合体させるには appendRange を用います。
#

#// #
appendClass returns the result of appending the class x to the class r.
It assume x is clean.
#
appendClass は，クラス x をクラス r に追加した結果を返します。
x がクリーンだと仮定します。
#

#// #
appendFolded returns the result of appending the case folding of the class x to the class r.
#
appendFolded は，クラス x の大文字小文字変換をクラス r に追加した結果を返します。
#

#// #
appendNegatedClass returns the result of appending the negation of the class x to the class r.
It assumes x is clean.
#
appendNegatedClass は，クラス x の否定をクラス r に追加した結果を返します。
x がクリーンだと仮定します。
#

#// #
appendTable returns the result of appending x to the class r.
#
appendTable は， x をクラス r に追加した結果を返します。
#

#// #
appendNegatedTable returns the result of appending the negation of x to the class r.
#
appendNegatedTable は， x の否定をクラス r に追加した結果を返します。
#

#	nextLo := '\u0000' // #
lo end of next class to add
#
次に追加するクラスの終わり
#

#// #
negateClass overwrites r and returns r's negation.
It assumes the class r is already clean.
#
negateClass は r を上書きして r の否定を返します。
クラス r がすでにクリーンであると仮定します。
#

#	w := 0             // #
write index
#
インデックスを書く
#

#		// #
It's possible for the negation to have one more
range - this one - than the original class, so use append.
#
否定は元のクラスよりももう 1 つの範囲 - これ - を持つことが可能ですので， append を使用してください。
#

#// #
ranges implements sort.Interface on a []rune.
The choice of receiver type definition is strange
but avoids an allocation since we already have
a *[]rune.
#
範囲は []rune に sort.Interface を実装します。
レシーバ型定義の選択は奇妙ですが， *[]rune がすでにあるので割り当てを避けます。
#

