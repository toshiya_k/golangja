* 1

#// #
The original C code and the comment below are from
FreeBSD's /usr/src/lib/msun/src/e_remainder.c and came
with this notice. The go code is a simplified version of
the original C.
#
オリジナルの C コードと以下のコメントは FreeBSD の /usr/src/lib/msun/src/e_remainder.c からのもので，この通知とともに来ました。
go コードはオリジナルの C の簡易版です。
#

#// #
Remainder returns the IEEE 754 floating-point remainder of x/y.
#
Remainder は， x/y の IEEE 754 浮動小数点余りを返します。
#

#// #
Special cases are:
#
特別な場合は :
#

#	// #
special cases
#
特殊なケース
#

[math/remainder.go]
#// #
====================================================
Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
#
====================================================
Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
#

[math/remainder.go]
#// #
Developed at SunPro, a Sun Microsystems, Inc. business.
Permission to use, copy, modify, and distribute this
software is freely granted, provided that this notice
is preserved.
====================================================
#
Developed at SunPro, a Sun Microsystems, Inc. business.
Permission to use, copy, modify, and distribute this
software is freely granted, provided that this notice
is preserved.
====================================================
#

[math/remainder.go]
#// #
__ieee754_remainder(x,y)
Return :
     returns  x REM y  =  x - [x/y]*y  as if in infinite
     precision arithmetic, where [x/y] is the (infinite bit)
     integer nearest x/y (in half way cases, choose the even one).
Method :
     Based on Mod() returning  x - [x/y]chopped * y  exactly.
#
__ieee754_remainder(x,y)
Return :
     returns  x REM y  =  x - [x/y]*y  as if in infinite
     precision arithmetic, where [x/y] is the (infinite bit)
     integer nearest x/y (in half way cases, choose the even one).
Method :
     Based on Mod() returning  x - [x/y]chopped * y  exactly.
#

[math/remainder.go]
#		Tiny    = 4.45014771701440276618e-308 // #
0x0020000000000000
#
0x0020000000000000
#

[math/remainder.go]
#		x = Mod(x, y+y) // #
now x < 2y
#
now x < 2y
#

