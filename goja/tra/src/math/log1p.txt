* 1

#// #
The original C code, the long comment, and the constants
below are from FreeBSD's /usr/src/lib/msun/src/s_log1p.c
and came with this notice. The go code is a simplified
version of the original C.
#
以下のオリジナルの C コード，長いコメント，そして定数は FreeBSD の /usr/src/lib/msun/src/s_log1p.c からのものであり，この通知とともに来ました。
go コードはオリジナルの C の簡易版です。
#

#// #
Log1p returns the natural logarithm of 1 plus its argument x.
It is more accurate than Log(1 + x) when x is near zero.
#
Log1p は， 1 の自然対数とその引数 x を返します。
x がゼロに近い場合， Log(1 + x) よりも正確です。
#

#// #
Special cases are:
#
特別な場合は :
#

#	// #
special cases
#
特殊なケース
#

#	case x < -1 || IsNaN(x): // #
includes -Inf
#
-Inf を含みます
#

#			// #
correction term
#
補正項
#

#		if iu < 0x0006a09e667f3bcd { // #
mantissa of Sqrt(2)
#
Sqrt の仮数 (2)
#

#			u = Float64frombits(iu | 0x3ff0000000000000) // #
normalize u
#
正規化
#

#			u = Float64frombits(iu | 0x3fe0000000000000) // #
normalize u/2
#
u/2 を正規化
#

#		R = hfsq * (1.0 - 0.66666666666666666*f) // #
avoid division
#
分裂を避ける
#

[math/log1p.go]
#// #
====================================================
Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
#
====================================================
Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
#

[math/log1p.go]
#// #
Developed at SunPro, a Sun Microsystems, Inc. business.
Permission to use, copy, modify, and distribute this
software is freely granted, provided that this notice
is preserved.
====================================================
#
Developed at SunPro, a Sun Microsystems, Inc. business.
Permission to use, copy, modify, and distribute this
software is freely granted, provided that this notice
is preserved.
====================================================
#

[math/log1p.go]
#// #
double log1p(double x)
#
double log1p(double x)
#

[math/log1p.go]
#// #
Method :
  1. Argument Reduction: find k and f such that
                     1+x = 2**k * (1+f),
        where  sqrt(2)/2 < 1+f < sqrt(2) .
#
Method :
  1. Argument Reduction: find k and f such that
                     1+x = 2**k * (1+f),
        where  sqrt(2)/2 < 1+f < sqrt(2) .
#

[math/log1p.go]
#// #
     Note. If k=0, then f=x is exact. However, if k!=0, then f
     may not be representable exactly. In that case, a correction
     term is need. Let u=1+x rounded. Let c = (1+x)-u, then
     log(1+x) - log(u) ~ c/u. Thus, we proceed to compute log(u),
     and add back the correction term c/u.
     (Note: when x > 2**53, one can simply return log(x))
#
     Note. If k=0, then f=x is exact. However, if k!=0, then f
     may not be representable exactly. In that case, a correction
     term is need. Let u=1+x rounded. Let c = (1+x)-u, then
     log(1+x) - log(u) ~ c/u. Thus, we proceed to compute log(u),
     and add back the correction term c/u.
     (Note: when x > 2**53, one can simply return log(x))
#

[math/log1p.go]
#// #
  2. Approximation of log1p(f).
     Let s = f/(2+f) ; based on log(1+f) = log(1+s) - log(1-s)
              = 2s + 2/3 s**3 + 2/5 s**5 + .....,
              = 2s + s*R
     We use a special Reme algorithm on [0,0.1716] to generate
     a polynomial of degree 14 to approximate R The maximum error
     of this polynomial approximation is bounded by 2**-58.45. In
     other words,
                     2      4      6      8      10      12      14
         R(z) ~ Lp1*s +Lp2*s +Lp3*s +Lp4*s +Lp5*s  +Lp6*s  +Lp7*s
     (the values of Lp1 to Lp7 are listed in the program)
     and
         |      2          14          |     -58.45
         | Lp1*s +...+Lp7*s    -  R(z) | <= 2
         |                             |
     Note that 2s = f - s*f = f - hfsq + s*hfsq, where hfsq = f*f/2.
     In order to guarantee error in log below 1ulp, we compute log
     by
             log1p(f) = f - (hfsq - s*(hfsq+R)).
#
  2. Approximation of log1p(f).
     Let s = f/(2+f) ; based on log(1+f) = log(1+s) - log(1-s)
              = 2s + 2/3 s**3 + 2/5 s**5 + .....,
              = 2s + s*R
     We use a special Reme algorithm on [0,0.1716] to generate
     a polynomial of degree 14 to approximate R The maximum error
     of this polynomial approximation is bounded by 2**-58.45. In
     other words,
                     2      4      6      8      10      12      14
         R(z) ~ Lp1*s +Lp2*s +Lp3*s +Lp4*s +Lp5*s  +Lp6*s  +Lp7*s
     (the values of Lp1 to Lp7 are listed in the program)
     and
         |      2          14          |     -58.45
         | Lp1*s +...+Lp7*s    -  R(z) | <= 2
         |                             |
     Note that 2s = f - s*f = f - hfsq + s*hfsq, where hfsq = f*f/2.
     In order to guarantee error in log below 1ulp, we compute log
     by
             log1p(f) = f - (hfsq - s*(hfsq+R)).
#

[math/log1p.go]
#// #
  3. Finally, log1p(x) = k*ln2 + log1p(f).
                       = k*ln2_hi+(f-(hfsq-(s*(hfsq+R)+k*ln2_lo)))
     Here ln2 is split into two floating point number:
                  ln2_hi + ln2_lo,
     where n*ln2_hi is always exact for |n| < 2000.
#
  3. Finally, log1p(x) = k*ln2 + log1p(f).
                       = k*ln2_hi+(f-(hfsq-(s*(hfsq+R)+k*ln2_lo)))
     Here ln2 is split into two floating point number:
                  ln2_hi + ln2_lo,
     where n*ln2_hi is always exact for |n| < 2000.
#

[math/log1p.go]
#// #
Special cases:
     log1p(x) is NaN with signal if x < -1 (including -INF) ;
     log1p(+INF) is +INF; log1p(-1) is -INF with signal;
     log1p(NaN) is that NaN with no signal.
#
Special cases:
     log1p(x) is NaN with signal if x < -1 (including -INF) ;
     log1p(+INF) is +INF; log1p(-1) is -INF with signal;
     log1p(NaN) is that NaN with no signal.
#

[math/log1p.go]
#// #
Accuracy:
     according to an error analysis, the error is always less than
     1 ulp (unit in the last place).
#
Accuracy:
     according to an error analysis, the error is always less than
     1 ulp (unit in the last place).
#

[math/log1p.go]
#// #
Constants:
The hexadecimal values are the intended ones for the following
constants. The decimal values may be used, provided that the
compiler will convert from decimal to binary accurately enough
to produce the hexadecimal values shown.
#
Constants:
The hexadecimal values are the intended ones for the following
constants. The decimal values may be used, provided that the
compiler will convert from decimal to binary accurately enough
to produce the hexadecimal values shown.
#

[math/log1p.go]
#// #
Note: Assuming log() return accurate answer, the following
      algorithm can be used to compute log1p(x) to within a few ULP:
#
Note: Assuming log() return accurate answer, the following
      algorithm can be used to compute log1p(x) to within a few ULP:
#

[math/log1p.go]
#// #
             u = 1+x;
             if(u==1.0) return x ; else
                        return log(u)*(x/(u-1.0));
#
             u = 1+x;
             if(u==1.0) return x ; else
                        return log(u)*(x/(u-1.0));
#

[math/log1p.go]
#// #
      See HP-15C Advanced Functions Handbook, p.193.
#
      See HP-15C Advanced Functions Handbook, p.193.
#

[math/log1p.go]
#		Sqrt2M1     = 4.142135623730950488017e-01  // #
Sqrt(2)-1 = 0x3fda827999fcef34
#
Sqrt(2)-1 = 0x3fda827999fcef34
#

[math/log1p.go]
#		Sqrt2HalfM1 = -2.928932188134524755992e-01 // #
Sqrt(2)/2-1 = 0xbfd2bec333018866
#
Sqrt(2)/2-1 = 0xbfd2bec333018866
#

[math/log1p.go]
#		Small       = 1.0 / (1 << 29)              // #
2**-29 = 0x3e20000000000000
#
2**-29 = 0x3e20000000000000
#

[math/log1p.go]
#		Tiny        = 1.0 / (1 << 54)              // #
2**-54
#
2**-54
#

[math/log1p.go]
#		Two53       = 1 << 53                      // #
2**53
#
2**53
#

[math/log1p.go]
#		Ln2Hi       = 6.93147180369123816490e-01   // #
3fe62e42fee00000
#
3fe62e42fee00000
#

[math/log1p.go]
#		Ln2Lo       = 1.90821492927058770002e-10   // #
3dea39ef35793c76
#
3dea39ef35793c76
#

[math/log1p.go]
#		Lp1         = 6.666666666666735130e-01     // #
3FE5555555555593
#
3FE5555555555593
#

[math/log1p.go]
#		Lp2         = 3.999999999940941908e-01     // #
3FD999999997FA04
#
3FD999999997FA04
#

[math/log1p.go]
#		Lp3         = 2.857142874366239149e-01     // #
3FD2492494229359
#
3FD2492494229359
#

[math/log1p.go]
#		Lp4         = 2.222219843214978396e-01     // #
3FCC71C51D8E78AF
#
3FCC71C51D8E78AF
#

[math/log1p.go]
#		Lp5         = 1.818357216161805012e-01     // #
3FC7466496CB03DE
#
3FC7466496CB03DE
#

[math/log1p.go]
#		Lp6         = 1.531383769920937332e-01     // #
3FC39A09D078C69F
#
3FC39A09D078C69F
#

[math/log1p.go]
#		Lp7         = 1.479819860511658591e-01     // #
3FC2F112DF3E5244
#
3FC2F112DF3E5244
#

[math/log1p.go]
#	if absx < Sqrt2M1 { // #
 |x| < Sqrt(2)-1
#
 |x| < Sqrt(2)-1
#

[math/log1p.go]
#		if absx < Small { // #
|x| < 2**-29
#
|x| < 2**-29
#

[math/log1p.go]
#			if absx < Tiny { // #
|x| < 2**-54
#
|x| < 2**-54
#

[math/log1p.go]
#		if x > Sqrt2HalfM1 { // #
Sqrt(2)/2-1 < x
#
Sqrt(2)/2-1 < x
#

[math/log1p.go]
#			// #
(Sqrt(2)/2-1) < x < (Sqrt(2)-1)
#
(Sqrt(2)/2-1) < x < (Sqrt(2)-1)
#

[math/log1p.go]
#		if absx < Two53 { // #
1<<53
#
1<<53
#

[math/log1p.go]
#		f = u - 1.0 // #
Sqrt(2)/2 < u < Sqrt(2)
#
Sqrt(2)/2 < u < Sqrt(2)
#

[math/log1p.go]
#	if iu == 0 { // #
|f| < 2**-20
#
|f| < 2**-20
#

