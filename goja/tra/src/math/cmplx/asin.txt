* 1

#// #
The original C code, the long comment, and the constants
below are from http://netlib.sandia.gov/cephes/c9x-complex/clog.c.
The go code is a simplified version of the original C.
#
以下のオリジナルの C コード，長いコメント，そして定数は http://netlib.sandia.gov/cephes/c9x-complex/clog.c からのものです。
go コードはオリジナルの C の簡易版です。
#

#// #
The readme file at http://netlib.sandia.gov/cephes/ says:
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#
http://netlib.sandia.gov/cephes/ にある readme ファイルには，次のように書かれています。
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#

#// #
  The two known misprints in the book are repaired here in the
source listings for the gamma function and the incomplete beta
integral.
#
  この本の 2 つの既知の誤植は，ここではガンマ関数と不完全なベータ積分のソースリストで修復されています。
#

#// #
Complex circular arc sine
#
複素円弧サイン
#

#// #
DESCRIPTION:
#
説明 :
#

#// #
Inverse complex sine:
#
逆複素正弦:
#

#// #
ACCURACY:
#
正確さ :
#

#// #
                     Relative error:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10     10100       2.1e-15     3.4e-16
   IEEE      -10,+10     30000       2.2e-14     2.7e-15
Larger relative error can be observed for z near zero.
Also tested by csin(casin(z)) = z.
#
                     相対誤差:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10     10100       2.1e-15     3.4e-16
   IEEE      -10,+10     30000       2.2e-14     2.7e-15
ゼロに近い z の場合，相対誤差が大きくなることがあります。
csin(casin(z)) = z でもテストされています。
#

#// #
Asin returns the inverse sine of x.
#
Asin は x の逆正弦を返します。
#

#// #
Asinh returns the inverse hyperbolic sine of x.
#
Asinh は x の逆双曲線サインを返します。
#

#// #
Complex circular arc cosine
#
複素円弧コサイン
#

#// #
                     Relative error:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10      5200      1.6e-15      2.8e-16
   IEEE      -10,+10     30000      1.8e-14      2.2e-15
#
                     相対誤差:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10      5200      1.6e-15      2.8e-16
   IEEE      -10,+10     30000      1.8e-14      2.2e-15
#

#// #
Acos returns the inverse cosine of x.
#
Acos は x の逆余弦を返します。
#

#// #
Acosh returns the inverse hyperbolic cosine of x.
#
Acosh は x の逆双曲線余弦を返します。
#

#// #
Complex circular arc tangent
#
複素円弧タンジェント
#

#// #
Where k is an arbitrary integer.
#
ここで， k は任意の整数です。
#

#// #
                     Relative error:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10      5900       1.3e-16     7.8e-18
   IEEE      -10,+10     30000       2.3e-15     8.5e-17
The check catan( ctan(z) )  =  z, with |x| and |y| < PI/2,
had peak relative error 1.5e-16, rms relative error
2.9e-17.  See also clog().
#
                     相対誤差:
arithmetic   domain     # trials      peak         rms
   DEC       -10,+10      5900       1.3e-16     7.8e-18
   IEEE      -10,+10     30000       2.3e-15     8.5e-17
The check catan( ctan(z) )  =  z, with |x| and |y| < PI/2,
had peak relative error 1.5e-16, rms relative error
2.9e-17.  clog() もご覧ください。
#

#// #
Atan returns the inverse tangent of x.
#
Atan は x の逆正接を返します。
#

#// #
Atanh returns the inverse hyperbolic tangent of x.
#
Atanh は， x の逆双曲線正接を返します。
#

[math/cmplx/asin.go]
#// #
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#

[math/cmplx/asin.go]
#// #
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#

[math/cmplx/asin.go]
#// #
casin(z) = -i casinh(iz)
#
casin(z) = -i casinh(iz)
#

[math/cmplx/asin.go]
#	ct := complex(-imag(x), real(x)) // #
i * x
#
i * x
#

[math/cmplx/asin.go]
#	x1 := complex(1-real(xx), -imag(xx)) // #
1 - x*x
#
1 - x*x
#

[math/cmplx/asin.go]
#	x2 := Sqrt(x1)                       // #
x2 = sqrt(1 - x*x)
#
x2 = sqrt(1 - x*x)
#

[math/cmplx/asin.go]
#	return complex(imag(w), -real(w)) // #
-i * w
#
-i * w
#

[math/cmplx/asin.go]
#	x1 := complex(1+real(xx), imag(xx)) // #
1 + x*x
#
1 + x*x
#

[math/cmplx/asin.go]
#	return Log(x + Sqrt(x1))            // #
log(x + sqrt(1 + x*x))
#
log(x + sqrt(1 + x*x))
#

[math/cmplx/asin.go]
#// #
w = arccos z  =  PI/2 - arcsin z.
#
w = arccos z  =  PI/2 - arcsin z.
#

[math/cmplx/asin.go]
#		return complex(-imag(w), real(w)) // #
i * w
#
i * w
#

[math/cmplx/asin.go]
#// #
If
    z = x + iy,
#
If
    z = x + iy,
#

[math/cmplx/asin.go]
#// #
then
         1       (    2x     )
Re w  =  - arctan(-----------)  +  k PI
         2       (     2    2)
                 (1 - x  - y )
#
then
         1       (    2x     )
Re w  =  - arctan(-----------)  +  k PI
         2       (     2    2)
                 (1 - x  - y )
#

[math/cmplx/asin.go]
#// #
              ( 2         2)
         1    (x  +  (y+1) )
Im w  =  - log(------------)
         4    ( 2         2)
              (x  +  (y-1) )
#
              ( 2         2)
         1    (x  +  (y+1) )
Im w  =  - log(------------)
         4    ( 2         2)
              (x  +  (y-1) )
#

[math/cmplx/asin.go]
#// #
catan(z) = -i catanh(iz).
#
catan(z) = -i catanh(iz).
#

[math/cmplx/asin.go]
#	z := complex(-imag(x), real(x)) // #
z = i * x
#
z = i * x
#

[math/cmplx/asin.go]
#	return complex(imag(z), -real(z)) // #
z = -i * z
#
z = -i * z
#

