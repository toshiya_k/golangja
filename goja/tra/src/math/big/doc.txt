* 1

##
Package big implements arbitrary-precision arithmetic (big numbers).
The following numeric types are supported:
#
big パッケージは任意精度の計算 (big numbers) を実装します。
以下の数値型がサポートされています。
#

##
	Int    signed integers
	Rat    rational numbers
	Float  floating-point numbers
#
整数の符号付き整数ラット有理数浮動小数点数
#

##
The zero value for an Int, Rat, or Float correspond to 0. Thus, new
values can be declared in the usual ways and denote 0 without further
initialization:
#
Int, Rat ，または Float のゼロ値は 0 に対応します。
したがって，新しい値は通常の方法で宣言でき，それ以上の初期化なしで 0 を表します。
#

##
	var x Int        // &x is an *Int of value 0
	var r = &Rat{}   // r is a *Rat of value 0
	y := new(Float)  // y is a *Float of value 0
#
var x Int        // &x は，値 0 の *Int です。
var r = &Rat{}   // r は，値 0 の *Rat です。
==(Float)//y は，値 0 の *Float です。
#

##
Alternatively, new values can be allocated and initialized with factory
functions of the form:
#
あるいは，次の形式のファクトリ関数で新しい値を割り当てて初期化することができます。
#

##
For instance, NewInt(x) returns an *Int set to the value of the int64
argument x, NewRat(a, b) returns a *Rat set to the fraction a/b where
a and b are int64 values, and NewFloat(f) returns a *Float initialized
to the float64 argument f. More flexibility is provided with explicit
setters, for instance:
#
例えば， NewInt(x) は int64 引数 x の値に設定された *Int を返し， NewRat(a, b) は a と b が int64 値である分数 a/b に設定された *Rat を返します。
NewFloat(f) は， float64 引数 f に初期化された *Float を返します。
より明確な設定は明示的な設定子によって渡されます。
例えば :
#

##
Setters, numeric operations and predicates are represented as methods of
the form:
#
セッター，数値演算，および述語は，次の形式のメソッドとして表されます。
#

##
with T one of Int, Rat, or Float. For unary and binary operations, the
result is the receiver (usually named z in that case; see below); if it
is one of the operands x or y it may be safely overwritten (and its memory
reused).
#
T ， Int ， Rat ，または Float のいずれか。
単項演算と二項演算の場合，結果はレシーバになります (その場合は通常 z という名前です。
下記参照) 。
それがオペランド x または y の 1 つであるなら，それは安全に上書きされるかもしれません (そしてそのメモリは再利用されます) 。
#

##
Arithmetic expressions are typically written as a sequence of individual
method calls, with each call corresponding to an operation. The receiver
denotes the result and the method arguments are the operation's operands.
For instance, given three *Int values a, b and c, the invocation
#
算術式は通常，個々のメソッド呼び出しのシーケンスとして記述され，各呼び出しは操作に対応します。
レシーバは結果を表し，メソッドの引数は操作のオペランドです。
例えば， 3 つの *Int 値 a, b ，および c を指定すると，呼び出しは次のようになります。
#

##
computes the sum a + b and stores the result in c, overwriting whatever
value was held in c before. Unless specified otherwise, operations permit
aliasing of parameters, so it is perfectly ok to write
#
a + b の合計を計算し，その結果を c に格納し，それまで c に保持されていた値を上書きします。
他に指定されない限り，操作はパラメータのエイリアシングを許します，従って書くことは全く問題ありません
#

##
to accumulate values x in a sum.
#
値 x を合計する。
#

##
(By always passing in a result value via the receiver, memory use can be
much better controlled. Instead of having to allocate new memory for each
result, an operation can reuse the space allocated for the result value,
and overwrite that value with the new result in the process.)
#
(レシーバを介して結果値を常に渡すことで，メモリ使用量をより適切に制御できます。
結果ごとに新しいメモリを割り当てる代わりに，操作で結果値に割り当てられた領域を再利用し，新しい値で上書きできます。
プロセスになります。)
#

##
Notational convention: Incoming method parameters (including the receiver)
are named consistently in the API to clarify their use. Incoming operands
are usually named x, y, a, b, and so on, but never z. A parameter specifying
the result is named z (typically the receiver).
#
表記規則 : 着信メソッドのパラメータ (受信側を含む) は，その使用を明確にするために API 内で一貫して名前が付けられています。
入力オペランドは通常 x, y, a, b などと名前が付けられますが， z とはされません。
結果を指定するパラメーターは z (通常は受信側) という名前です。
#

##
For instance, the arguments for (*Int).Add are named x and y, and because
the receiver specifies the result destination, it is called z:
#
たとえば， (*Int).Add の引数は x および y という名前で，受信側は結果の宛先を指定しているため， z と呼ばれます。
#

##
Methods of this form typically return the incoming receiver as well, to
enable simple call chaining.
#
この形式のメソッドは通常，単純なコールチェーニングを可能にするために着信レシーバも返します。
#

##
Methods which don't require a result value to be passed in (for instance,
Int.Sign), simply return the result. In this case, the receiver is typically
the first operand, named x:
#
結果値を渡す必要がないメソッド (たとえば Int.Sign) は，単に結果を返します。
この場合，レシーバは通常， x という名前の最初のオペランドです。
#

##
Various methods support conversions between strings and corresponding
numeric values, and vice versa: *Int, *Rat, and *Float values implement
the Stringer interface for a (default) string representation of the value,
but also provide SetString methods to initialize a value from a string in
a variety of supported formats (see the respective SetString documentation).
#
さまざまなメソッドが文字列とそれに対応する数値の間の変換をサポートしています。
*Int, *Rat ，および *Float 値は，値の (デフォルト) 文字列表現用の Stringer インターフェースを実装します。
サポートされているさまざまな形式の文字列 (それぞれの SetString のドキュメントを参照) 。
#

##
Finally, *Int, *Rat, and *Float satisfy the fmt package's Scanner interface
for scanning and (except for *Rat) the Formatter interface for formatted
printing.
#
最後に， *Int, *Rat, および *Float は，スキャン用の fmt パッケージのスキャナーインターフェースと， (*Rat を除く) フォーマット済み表示用のフォーマッターインターフェースを満たします。
#

[math/big/doc.go]
##
	func NewT(v V) *T
#
	func NewT(v V) *T
#

[math/big/doc.go]
##
	var z1 Int
	z1.SetUint64(123)                 // z1 := 123
	z2 := new(Rat).SetFloat64(1.25)   // z2 := 5/4
	z3 := new(Float).SetInt(z1)       // z3 := 123.0
#
	var z1 Int
	z1.SetUint64(123)                 // z1 := 123
	z2 := new(Rat).SetFloat64(1.25)   // z2 := 5/4
	z3 := new(Float).SetInt(z1)       // z3 := 123.0
#

[math/big/doc.go]
##
	func (z *T) SetV(v V) *T          // z = v
	func (z *T) Unary(x *T) *T        // z = unary x
	func (z *T) Binary(x, y *T) *T    // z = x binary y
	func (x *T) Pred() P              // p = pred(x)
#
	func (z *T) SetV(v V) *T          // z = v
	func (z *T) Unary(x *T) *T        // z = unary x
	func (z *T) Binary(x, y *T) *T    // z = x binary y
	func (x *T) Pred() P              // p = pred(x)
#

[math/big/doc.go]
##
	c.Add(a, b)
#
	c.Add(a, b)
#

[math/big/doc.go]
##
	sum.Add(sum, x)
#
	sum.Add(sum, x)
#

[math/big/doc.go]
##
	func (z *Int) Add(x, y *Int) *Int
#
	func (z *Int) Add(x, y *Int) *Int
#

[math/big/doc.go]
##
	func (x *Int) Sign() int
#
	func (x *Int) Sign() int
#

