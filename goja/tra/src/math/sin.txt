* 1

#	#
Floating-point sine and cosine.
#
浮動小数点のサインとコサイン。
#

#// #
The original C code, the long comment, and the constants
below were from http://netlib.sandia.gov/cephes/cmath/sin.c,
available from http://www.netlib.org/cephes/cmath.tgz.
The go code is a simplified version of the original C.
#
以下のオリジナルの C コード，長いコメント，および定数は， http://wwwliblib.org/cephes/cmath.tgz から入手可能な http://netlib.sandia.gov/cephes/cmath/sin.c からのものです。
go コードはオリジナルの C の簡易版です。
#

#// #
sin coefficients
#
sin 係数
#

#// #
Cos returns the cosine of the radian argument x.
#
Cos は，ラジアン引数 x の余弦 (コサイン) を返します。
#

#// #
Special cases are:
#
特別な場合は :
#

#		PI4A = 7.85398125648498535156E-1  // #
0x3fe921fb40000000, Pi/4 split into three parts
#
0x3fe921fb40000000, Pi/4 を 3 つに分割
#

#	// #
special cases
#
特殊なケース
#

#	// #
make argument positive
#
argument を正にする
#

#		j = uint64(x * (4 / Pi)) // #
integer part of x/(Pi/4), as integer for tests on the phase angle
#
x/(Pi/4) の整数部，位相角の検定用の整数として
#

#		y = float64(j)           // #
integer part of x/(Pi/4), as float
#
x/(Pi/4) の整数部， float として
#

#		// #
map zeros to origin
#
原点にゼロを写像する
#

#		j &= 7                               // #
octant modulo 2Pi radians (360 degrees)
#
オクタントモジュロ 2Pi ラジアン (360 度)
#

#		z = ((x - y*PI4A) - y*PI4B) - y*PI4C // #
Extended precision modular arithmetic
#
拡張精密モジュラ算術
#

#// #
Sin returns the sine of the radian argument x.
#
Sin は，ラジアン引数 x の正弦 (サイン) を返します。
#

#	// #
make argument positive but save the sign
#
引数を正にするが符号を保存する
#

#	// #
reflect in x axis
#
X 軸に反映
#

[math/sin.go]
#// #
     sin.c
#
     sin.c
#

[math/sin.go]
#// #
     Circular sine
#
     Circular sine
#

[math/sin.go]
#// #
SYNOPSIS:
#
SYNOPSIS:
#

[math/sin.go]
#// #
double x, y, sin();
y = sin( x );
#
double x, y, sin();
y = sin( x );
#

[math/sin.go]
#// #
DESCRIPTION:
#
DESCRIPTION:
#

[math/sin.go]
#// #
Range reduction is into intervals of pi/4.  The reduction error is nearly
eliminated by contriving an extended precision modular arithmetic.
#
Range reduction is into intervals of pi/4.  The reduction error is nearly
eliminated by contriving an extended precision modular arithmetic.
#

[math/sin.go]
#// #
Two polynomial approximating functions are employed.
Between 0 and pi/4 the sine is approximated by
     x  +  x**3 P(x**2).
Between pi/4 and pi/2 the cosine is represented as
     1  -  x**2 Q(x**2).
#
Two polynomial approximating functions are employed.
Between 0 and pi/4 the sine is approximated by
     x  +  x**3 P(x**2).
Between pi/4 and pi/2 the cosine is represented as
     1  -  x**2 Q(x**2).
#

[math/sin.go]
#// #
ACCURACY:
#
ACCURACY:
#

[math/sin.go]
#// #
                     Relative error:
arithmetic   domain      # trials      peak         rms
   DEC       0, 10       150000       3.0e-17     7.8e-18
   IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
#
                     Relative error:
arithmetic   domain      # trials      peak         rms
   DEC       0, 10       150000       3.0e-17     7.8e-18
   IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
#

[math/sin.go]
#// #
Partial loss of accuracy begins to occur at x = 2**30 = 1.074e9.  The loss
is not gradual, but jumps suddenly to about 1 part in 10e7.  Results may
be meaningless for x > 2**49 = 5.6e14.
#
Partial loss of accuracy begins to occur at x = 2**30 = 1.074e9.  The loss
is not gradual, but jumps suddenly to about 1 part in 10e7.  Results may
be meaningless for x > 2**49 = 5.6e14.
#

[math/sin.go]
#// #
     cos.c
#
     cos.c
#

[math/sin.go]
#// #
     Circular cosine
#
     Circular cosine
#

[math/sin.go]
#// #
double x, y, cos();
y = cos( x );
#
double x, y, cos();
y = cos( x );
#

[math/sin.go]
#// #
Two polynomial approximating functions are employed.
Between 0 and pi/4 the cosine is approximated by
     1  -  x**2 Q(x**2).
Between pi/4 and pi/2 the sine is represented as
     x  +  x**3 P(x**2).
#
Two polynomial approximating functions are employed.
Between 0 and pi/4 the cosine is approximated by
     1  -  x**2 Q(x**2).
Between pi/4 and pi/2 the sine is represented as
     x  +  x**3 P(x**2).
#

[math/sin.go]
#// #
                     Relative error:
arithmetic   domain      # trials      peak         rms
   IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
   DEC        0,+1.07e9   17000       3.0e-17     7.2e-18
#
                     Relative error:
arithmetic   domain      # trials      peak         rms
   IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
   DEC        0,+1.07e9   17000       3.0e-17     7.2e-18
#

[math/sin.go]
#// #
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#

[math/sin.go]
#// #
The readme file at http://netlib.sandia.gov/cephes/ says:
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#
The readme file at http://netlib.sandia.gov/cephes/ says:
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#

[math/sin.go]
#// #
  The two known misprints in the book are repaired here in the
source listings for the gamma function and the incomplete beta
integral.
#
  The two known misprints in the book are repaired here in the
source listings for the gamma function and the incomplete beta
integral.
#

[math/sin.go]
#// #
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#

[math/sin.go]
#	1.58962301576546568060e-10, // #
0x3de5d8fd1fd19ccd
#
0x3de5d8fd1fd19ccd
#

[math/sin.go]
#	-2.50507477628578072866e-8, // #
0xbe5ae5e5a9291f5d
#
0xbe5ae5e5a9291f5d
#

[math/sin.go]
#	2.75573136213857245213e-6,  // #
0x3ec71de3567d48a1
#
0x3ec71de3567d48a1
#

[math/sin.go]
#	-1.98412698295895385996e-4, // #
0xbf2a01a019bfdf03
#
0xbf2a01a019bfdf03
#

[math/sin.go]
#	8.33333333332211858878e-3,  // #
0x3f8111111110f7d0
#
0x3f8111111110f7d0
#

[math/sin.go]
#	-1.66666666666666307295e-1, // #
0xbfc5555555555548
#
0xbfc5555555555548
#

[math/sin.go]
#// #
cos coefficients
#
cos coefficients
#

[math/sin.go]
#	-1.13585365213876817300e-11, // #
0xbda8fa49a0861a9b
#
0xbda8fa49a0861a9b
#

[math/sin.go]
#	2.08757008419747316778e-9,   // #
0x3e21ee9d7b4e3f05
#
0x3e21ee9d7b4e3f05
#

[math/sin.go]
#	-2.75573141792967388112e-7,  // #
0xbe927e4f7eac4bc6
#
0xbe927e4f7eac4bc6
#

[math/sin.go]
#	2.48015872888517045348e-5,   // #
0x3efa01a019c844f5
#
0x3efa01a019c844f5
#

[math/sin.go]
#	-1.38888888888730564116e-3,  // #
0xbf56c16c16c14f91
#
0xbf56c16c16c14f91
#

[math/sin.go]
#	4.16666666666665929218e-2,   // #
0x3fa555555555554b
#
0x3fa555555555554b
#

[math/sin.go]
#		PI4A = 7.85398125648498535156e-1  // #
0x3fe921fb40000000, Pi/4 split into three parts
#
0x3fe921fb40000000, Pi/4 split into three parts
#

[math/sin.go]
#		PI4B = 3.77489470793079817668e-8  // #
0x3e64442d00000000,
#
0x3e64442d00000000,
#

[math/sin.go]
#		PI4C = 2.69515142907905952645e-15 // #
0x3ce8469898cc5170,
#
0x3ce8469898cc5170,
#

