* 1

#// #
The original C code, the long comment, and the constants
below are from http://netlib.sandia.gov/cephes/cprob/gamma.c.
The go code is a simplified version of the original C.
#
以下のオリジナルの C コード，長いコメント，そして定数は http://netlib.sandia.gov/cephes/cprob/gamma.c からのものです。
go コードはオリジナルの C の簡易版です。
#

#// #
Gamma function computed by Stirling's formula.
The pair of results must be multiplied together to get the actual answer.
The multiplication is left to the caller so that, if careful, the caller can avoid
infinity for 172 <= x <= 180.
The polynomial is valid for 33 <= x <= 172; larger values are only used
in reciprocal and produce denormalized floats. The lower precision there
masks any imprecision in the polynomial.
#
スターリングの公式によって計算されたガンマ関数。
実際の答えを得るには，結果のペアを掛け合わせる必要があります。
注意があれば，呼び出し元が 172 <= x <= 180 の場合に無限大を回避できるように，乗算は呼び出し元に任されます。
多項式は 33 <= x <= 172 に対して有効です。
大きい値は逆数でのみ使用され，非正規化フロートを生成します。
精度が低いと，多項式の不正確さが隠されます。
#

#	if x > MaxStirling { // #
avoid Pow() overflow
#
Pow() オーバーフローを回避
#

#// #
Gamma returns the Gamma function of x.
#
Gamma は x のガンマ関数を返します。
#

#// #
Special cases are:
#
特別な場合は :
#

#	// #
special cases
#
特殊なケース
#

#		// #
Note: x is negative but (checked above) not a negative integer,
so x must be small enough to be in range for conversion to int64.
If |x| were >= 2⁶³ it would have to be an integer.
#
Note: x は負の値ですが (上で確認したように) 負の整数ではありません。
したがって， x は int64 に変換するのに十分な範囲内でなければなりません。
|x| の場合 263 以上であれば整数でなければならないでしょう。
#

#	// #
Reduce argument
#
argument を減らす
#

[math/gamma.go]
#// #
     tgamma.c
#
     tgamma.c
#

[math/gamma.go]
#// #
     Gamma function
#
     Gamma function
#

[math/gamma.go]
#// #
SYNOPSIS:
#
SYNOPSIS:
#

[math/gamma.go]
#// #
double x, y, tgamma();
extern int signgam;
#
double x, y, tgamma();
extern int signgam;
#

[math/gamma.go]
#// #
y = tgamma( x );
#
y = tgamma( x );
#

[math/gamma.go]
#// #
DESCRIPTION:
#
DESCRIPTION:
#

[math/gamma.go]
#// #
Returns gamma function of the argument. The result is
correctly signed, and the sign (+1 or -1) is also
returned in a global (extern) variable named signgam.
This variable is also filled in by the logarithmic gamma
function lgamma().
#
Returns gamma function of the argument. The result is
correctly signed, and the sign (+1 or -1) is also
returned in a global (extern) variable named signgam.
This variable is also filled in by the logarithmic gamma
function lgamma().
#

[math/gamma.go]
#// #
Arguments |x| <= 34 are reduced by recurrence and the function
approximated by a rational function of degree 6/7 in the
interval (2,3).  Large arguments are handled by Stirling's
formula. Large negative arguments are made positive using
a reflection formula.
#
Arguments |x| <= 34 are reduced by recurrence and the function
approximated by a rational function of degree 6/7 in the
interval (2,3).  Large arguments are handled by Stirling's
formula. Large negative arguments are made positive using
a reflection formula.
#

[math/gamma.go]
#// #
ACCURACY:
#
ACCURACY:
#

[math/gamma.go]
#// #
                     Relative error:
arithmetic   domain     # trials      peak         rms
   DEC      -34, 34      10000       1.3e-16     2.5e-17
   IEEE    -170,-33      20000       2.3e-15     3.3e-16
   IEEE     -33,  33     20000       9.4e-16     2.2e-16
   IEEE      33, 171.6   20000       2.3e-15     3.2e-16
#
                     Relative error:
arithmetic   domain     # trials      peak         rms
   DEC      -34, 34      10000       1.3e-16     2.5e-17
   IEEE    -170,-33      20000       2.3e-15     3.3e-16
   IEEE     -33,  33     20000       9.4e-16     2.2e-16
   IEEE      33, 171.6   20000       2.3e-15     3.2e-16
#

[math/gamma.go]
#// #
Error for arguments outside the test range will be larger
owing to error amplification by the exponential function.
#
Error for arguments outside the test range will be larger
owing to error amplification by the exponential function.
#

[math/gamma.go]
#// #
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
#

[math/gamma.go]
#// #
The readme file at http://netlib.sandia.gov/cephes/ says:
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#
The readme file at http://netlib.sandia.gov/cephes/ says:
   Some software in this archive may be from the book _Methods and
Programs for Mathematical Functions_ (Prentice-Hall or Simon & Schuster
International, 1989) or from the Cephes Mathematical Library, a
commercial product. In either event, it is copyrighted by the author.
What you see here may be used freely but it comes with no support or
guarantee.
#

[math/gamma.go]
#// #
  The two known misprints in the book are repaired here in the
source listings for the gamma function and the incomplete beta
integral.
#
  The two known misprints in the book are repaired here in the
source listings for the gamma function and the incomplete beta
integral.
#

[math/gamma.go]
#// #
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#
  Stephen L. Moshier
  moshier@na-net.ornl.gov
#

[math/gamma.go]
#//	#
Gamma(+Inf) = +Inf
Gamma(+0) = +Inf
Gamma(-0) = -Inf
Gamma(x) = NaN for integer x < 0
Gamma(-Inf) = NaN
Gamma(NaN) = NaN
#
Gamma(+Inf) = +Inf
Gamma(+0) = +Inf
Gamma(-0) = -Inf
Gamma(x) = NaN for integer x < 0
Gamma(-Inf) = NaN
Gamma(NaN) = NaN
#

[math/gamma.go]
#	const Euler = 0.57721566490153286060651209008240243104215933593992 // #
A001620
#
A001620
#

