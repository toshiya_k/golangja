* 1

##
 * Normal distribution
 *
 * See "The Ziggurat Method for Generating Random Variables"
 * (Marsaglia & Tsang, 2000)
 * http://www.jstatsoft.org/v05/i08/paper [pdf]
#
 * 正規分布
 *
 * "The Ziggurat Method for Generating Random Variables"
 * (Marsaglia & Tsang, 2000) をご覧ください。
 * http://www.jstatsoft.org/v05/i08/paper [pdf]
#

#// #
NormFloat64 returns a normally distributed float64 in
the range -math.MaxFloat64 through +math.MaxFloat64 inclusive,
with standard normal distribution (mean = 0, stddev = 1).
To produce a different normal distribution, callers can
adjust the output using:
#
NormFloat64 は，標準正規分布 (mean = 0, stddev = 1) で， -math.MaxFloat64 から +math.MaxFloat64 までの範囲の正規分布 float64 を返します。
別の正規分布を生成するために，呼び出し側は次のように出力を調整できます。
#

#		j := int32(r.Uint32()) // #
Possibly negative
#
おそらく負
#

#			// #
This case should be hit better than 99% of the time.
#
このケースは 99%以上の頻度となるはずです。
#

#			// #
This extra work is only required for the base strip.
#
この余分な作業はベースストリップにのみ必要です。
#

[math/rand/normal.go]
#// #
 sample = NormFloat64() * desiredStdDev + desiredMean
#
 sample = NormFloat64() * desiredStdDev + desiredMean
#

