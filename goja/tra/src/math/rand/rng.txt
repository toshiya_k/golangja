* 1

##
 * Uniform distribution
 *
 * algorithm by
 * DP Mitchell and JA Reeds
#
 * 一様分布
 *
 * DP ミッチェルと JA リードによる
 * アルゴリズム
#

#	// #
rngCooked used for seeding. See gen_cooked.go for details.
#
rngCooked はシードに使用されます。
詳細は gen_cooked.go を見てください。
#

#	tap  int           // #
index into vec
#
vec へのインデックス
#

#	feed int           // #
index into vec
#
vec へのインデックス
#

#	vec  [rngLen]int64 // #
current feedback register
#
現在のフィードバックレジスタ
#

#// #
Seed uses the provided seed value to initialize the generator to a deterministic state.
#
Seed は渡されたシード値を使用してジェネレータを決定論的状態に初期化します。
#

#// #
Int63 returns a non-negative pseudo-random 63-bit integer as an int64.
#
Int63 は，負でない疑似乱数 63 ビット整数を int64 で返します。
#

#// #
Uint64 returns a non-negative pseudo-random 64-bit integer as an uint64.
#
Uint64 は，負でない疑似乱数 64 ビット整数を uint64 で返します。
#

[math/rand/rng.go]
#// #
seed rng x[n+1] = 48271 * x[n] mod (2**31 - 1)
#
seed rng x[n+1] = 48271 * x[n] mod (2**31 - 1)
#

