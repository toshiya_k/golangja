* 1

#// #
groupRetry retries getGroupList with an increasingly large size for n. The
result is stored in gids.
#
groupRetryは，getGroupListをnを大きくして再試行します。結果はGIDに格納されます。
#

#			// #
n is set correctly
#
nが正しく設定されている
#

[os/user/getgrouplist_darwin.go]
##
#
#

#include <stdlib.h>#
#
#

#include <sys/types.h>#
#
#

[os/user/getgrouplist_darwin.go]
##
static int mygetgrouplist(const char* user, gid_t group, gid_t* groups, int* ngroups) {
	int* buf = malloc(*ngroups * sizeof(int));
	int rv = getgrouplist(user, (int) group, buf, ngroups);
	int i;
	if (rv == 0) {
		for (i = 0; i < *ngroups; i++) {
			groups[i] = (gid_t) buf[i];
		}
	}
	free(buf);
	return rv;
}
#
static int mygetgrouplist(const char* user, gid_t group, gid_t* groups, int* ngroups) {
	int* buf = malloc(*ngroups * sizeof(int));
	int rv = getgrouplist(user, (int) group, buf, ngroups);
	int i;
	if (rv == 0) {
		for (i = 0; i < *ngroups; i++) {
			groups[i] = (gid_t) buf[i];
		}
	}
	free(buf);
	return rv;
}
#

[os/user/getgrouplist_darwin.go]
##
＃include <unistd.h>
＃include <sys/types.h>
＃include <stdlib.h>
#
＃include <unistd.h>
＃include <sys/types.h>
＃include <stdlib.h>
#

