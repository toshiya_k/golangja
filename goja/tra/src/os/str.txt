* 1

#// #
Simple conversions to avoid depending on strconv.
#
strconv に依存しないようにする簡単な変換。
#

#// #
Convert integer to decimal string
#
整数を 10 進数文字列に変換
#

#// #
Convert unsigned integer to decimal string
#
符号なし整数を 10 進数ストリングに変換
#

#	if val == 0 { // #
avoid string allocation
#
文字列割り当てを避ける
#

#	var buf [20]byte // #
big enough for 64bit value base 10
#
64 ビット 10 進数に十分な大きさ
#

[os/str.go]
#	// #
val < 10
#
val < 10
#

