* 1

#// #
Auxiliary information if the File describes a directory
#
ファイルがディレクトリを記述している場合の補助情報
#

#	buf  []byte // #
buffer for directory I/O
#
ディレクトリ入出力用バッファ
#

#	nbuf int    // #
length of buf; return value from Getdirentries
#
buf の長さ Getdirentries からの戻り値
#

#	bufp int    // #
location of next record in buf.
#
buf 内の次のレコードの位置
#

#	// #
More than 5760 to work around https://golang.org/issue/24015.
#
https://golang.org/issue/24015 を回避するため， 5760 以上。
#

#	// #
If this file has no dirinfo, create one.
#
このファイルに dirinfo がない場合は作成します。
#

#		// #
The buffer must be at least a block long.
#
バッファは少なくとも 1 ブロック長でなければなりません。
#

#	names = make([]string, 0, size) // #
Empty with room to grow.
#
追加の余地をもって。
#

#		// #
Refill the buffer if necessary
#
必要ならばバッファを補充します
#

#		// #
Drain the buffer
#
バッファを排水する
#

