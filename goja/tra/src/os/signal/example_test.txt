* 1

#	// #
Set up channel on which to send signal notifications.
We must use a buffered channel or risk missing the signal
if we're not ready to receive when the signal is sent.
#
シグナル通知を送信するチャンネルを設定します。
シグナルが送信されたときに受信する準備が整っていない場合は，バッファチャンネルを使用するか，シグナルを見逃す危険性があります。
#

#	// #
Block until a signal is received.
#
シグナルが受信されるまでブロックします。
#

#	// #
Passing no signals to Notify means that
all signals will be sent to the channel.
#
Notify にシグナルを渡さないと，すべてのシグナルがそのチャンネルに送信されます。
#

#	// #
Block until any signal is received.
#
シグナルが受信されるまでブロックします。
#

