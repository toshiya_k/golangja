* 1

##
Package signal implements access to incoming signals.
#
signal パッケージは，入力シグナルへのアクセスを実装します。
#

##
Signals are primarily used on Unix-like systems. For the use of this
package on Windows and Plan 9, see below.
#
シグナルは主に Unix ライクなシステムで使われています。
Windows および Plan 9 でこのパッケージを使用する場合は，以下を参照してください。
#

##
Types of signals
#
シグナルの種類
#

##
The signals SIGKILL and SIGSTOP may not be caught by a program, and
therefore cannot be affected by this package.
#
シグナル SIGKILL と SIGSTOP はプログラムによって捕捉されない可能性があるため，このパッケージの影響を受けることはありません。
#

##
Synchronous signals are signals triggered by errors in program
execution: SIGBUS, SIGFPE, and SIGSEGV. These are only considered
synchronous when caused by program execution, not when sent using
os.Process.Kill or the kill program or some similar mechanism. In
general, except as discussed below, Go programs will convert a
synchronous signal into a run-time panic.
#
同期シグナルは，プログラム実行中のエラー (SIGBUS ， SIGFPE ，および SIGSEGV) によってトリガーされるシグナルです。
これらはプログラムの実行によって引き起こされた場合にのみ同期と見なされ， os.Process.Kill や kill プログラム，あるいは同様のメカニズムを使って送られた場合には同期されません。
一般に，以下で説明する場合を除き， Go プログラムは同期シグナルをランタイムパニックに変換します。
#

##
The remaining signals are asynchronous signals. They are not
triggered by program errors, but are instead sent from the kernel or
from some other program.
#
残りのシグナルは非同期シグナルです。
それらはプログラムエラーによって引き起こされるのではなく，代わりにカーネルまたは他のプログラムから送られます。
#

##
Of the asynchronous signals, the SIGHUP signal is sent when a program
loses its controlling terminal. The SIGINT signal is sent when the
user at the controlling terminal presses the interrupt character,
which by default is ^C (Control-C). The SIGQUIT signal is sent when
the user at the controlling terminal presses the quit character, which
by default is ^\ (Control-Backslash). In general you can cause a
program to simply exit by pressing ^C, and you can cause it to exit
with a stack dump by pressing ^\.
#
非同期シグナルのうち，プログラムが制御端末を失うと SIGHUP シグナルが送信されます。
SIGINT シグナルは，制御端末のユーザーが割り込み文字 (デフォルトでは ^C(Control-C)) を押すと送信されます。
SIGQUIT シグナルは，制御端末のユーザーが終了文字を押すと送信されます。
デフォルトでは ^\(Control-Backslash) です。
一般的には， ^C を押すことでプログラムを終了させることができ， ^\ を押すとスタックダンプで終了させることができます。
#

##
Default behavior of signals in Go programs
#
Go プログラムにおけるシグナルのデフォルトの振る舞い
#

##
By default, a synchronous signal is converted into a run-time panic. A
SIGHUP, SIGINT, or SIGTERM signal causes the program to exit. A
SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGSTKFLT, SIGEMT, or SIGSYS signal
causes the program to exit with a stack dump. A SIGTSTP, SIGTTIN, or
SIGTTOU signal gets the system default behavior (these signals are
used by the shell for job control). The SIGPROF signal is handled
directly by the Go runtime to implement runtime.CPUProfile. Other
signals will be caught but no action will be taken.
#
デフォルトでは，同期シグナルはランタイムパニックに変換されます。
SIGHUP, SIGINT ，または SIGTERM シグナルはプログラムを終了させます。
SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGSTKFLT, SIGEMT ，または SIGSYS シグナルは，プログラムをスタックダンプで終了させます。
SIGTSTP, SIGTTIN ，または SIGTTOU シグナルは，システムのデフォルトの動作になります (これらのシグナルは，ジョブ制御のためにシェルによって使用されます) 。
SIGPROF シグナルは， runtime.CPUProfile を実装するために Go ランタイムによって直接処理されます。
他のシグナルは捕捉されますが，アクションはとられません。
#

##
If the Go program is started with either SIGHUP or SIGINT ignored
(signal handler set to SIG_IGN), they will remain ignored.
#
SIGHUP または SIGINT のいずれかを無視して (シグナルハンドラを SIG_IGN に設定して)Go プログラムを起動した場合，それらは無視されたままになります。
#

##
If the Go program is started with a non-empty signal mask, that will
generally be honored. However, some signals are explicitly unblocked:
the synchronous signals, SIGILL, SIGTRAP, SIGSTKFLT, SIGCHLD, SIGPROF,
and, on GNU/Linux, signals 32 (SIGCANCEL) and 33 (SIGSETXID)
(SIGCANCEL and SIGSETXID are used internally by glibc). Subprocesses
started by os.Exec, or by the os/exec package, will inherit the
modified signal mask.
#
Go プログラムが空でないシグナルマスクで起動された場合，それは一般的に尊重されます。
しかし，シグナルの中には明示的にブロック解除されているものがあります : 同期シグナル， SIGILL, SIGTRAP, SIGSTKFLT, SIGCHLD, SIGPROF ，および GNU/Linux ではシグナル 32 (SIGCANCEL) および 33(SIGSETXID)(SIGCANCEL および SIGSETXID は glibc によって内部的に使用されます) 。
os.Exec または os/exec パッケージによって開始されたサブプロセスは，変更されたシグナルマスクを継承します。
#

##
Changing the behavior of signals in Go programs
#
Go プログラムでのシグナルの振る舞いを変える
#

##
The functions in this package allow a program to change the way Go
programs handle signals.
#
このパッケージの関数により，プログラムは Go プログラムがシグナルを処理する方法を変更することができます。
#

##
Notify disables the default behavior for a given set of asynchronous
signals and instead delivers them over one or more registered
channels. Specifically, it applies to the signals SIGHUP, SIGINT,
SIGQUIT, SIGABRT, and SIGTERM. It also applies to the job control
signals SIGTSTP, SIGTTIN, and SIGTTOU, in which case the system
default behavior does not occur. It also applies to some signals that
otherwise cause no action: SIGUSR1, SIGUSR2, SIGPIPE, SIGALRM,
SIGCHLD, SIGCONT, SIGURG, SIGXCPU, SIGXFSZ, SIGVTALRM, SIGWINCH,
SIGIO, SIGPWR, SIGSYS, SIGINFO, SIGTHR, SIGWAITING, SIGLWP, SIGFREEZE,
SIGTHAW, SIGLOST, SIGXRES, SIGJVM1, SIGJVM2, and any real time signals
used on the system. Note that not all of these signals are available
on all systems.
#
Notify は，非同期シグナルの特定のセットに対するデフォルトの動作を無効にし，代わりにそれらを 1 つ以上の登録済みチャンネルを介して配信します。
具体的には， SIGHUP ， SIGINT ， SIGQUIT ， SIGABRT ，および SIGTERM の各シグナルに適用されます。
ジョブ制御シグナル SIGTSTP, SIGTTIN ，および SIGTTOU にも適用されます。
この場合，システムのデフォルトの動作は発生しません。
それ以外ではアクションを引き起こさないシグナルにも適用されます。
SIGTHAW, SIGLOST, SIGXRES, SIGJVM1, SIGJVM2 ，およびシステムで使用されるリアルタイムシグナル。
これらのシグナルすべてがすべてのシステムで利用できるわけではないことに注意してください。
#

##
If the program was started with SIGHUP or SIGINT ignored, and Notify
is called for either signal, a signal handler will be installed for
that signal and it will no longer be ignored. If, later, Reset or
Ignore is called for that signal, or Stop is called on all channels
passed to Notify for that signal, the signal will once again be
ignored. Reset will restore the system default behavior for the
signal, while Ignore will cause the system to ignore the signal
entirely.
#
プログラムが SIGHUP または SIGINT を無視して開始され，いずれかのシグナルに対して Notify が呼び出された場合，そのシグナルに対してシグナルハンドラがインストールされ，無視されることはなくなります。
後でそのシグナルに対して Reset または Ignore が呼び出されるか，そのシグナルに対して Notify に渡されたすべてのチャンネルで Stop が呼び出されると，そのシグナルはもう一度無視されます。
Reset でシグナルのシステムデフォルトの動作に戻りますが，無視するとシステムはシグナルを完全に無視します。
#

##
If the program is started with a non-empty signal mask, some signals
will be explicitly unblocked as described above. If Notify is called
for a blocked signal, it will be unblocked. If, later, Reset is
called for that signal, or Stop is called on all channels passed to
Notify for that signal, the signal will once again be blocked.
#
プログラムが空でないシグナルマスクで開始された場合，いくつかのシグナルは上記のように明示的にブロック解除されます。
Notify がブロックされたシグナルに対して呼び出された場合，そのシグナルはブロック解除されます。
後でそのシグナルに対して Reset が呼び出された場合，またはそのシグナルについて Notify に渡されたすべてのチャンネルで Stop が呼び出された場合，シグナルは再びブロックされます。
#

##
SIGPIPE
#
SIGPIPE
#

##
When a Go program writes to a broken pipe, the kernel will raise a
SIGPIPE signal.
#
Go プログラムが壊れたパイプに書き込むと，カーネルは SIGPIPE シグナルを送出します。
#

##
If the program has not called Notify to receive SIGPIPE signals, then
the behavior depends on the file descriptor number. A write to a
broken pipe on file descriptors 1 or 2 (standard output or standard
error) will cause the program to exit with a SIGPIPE signal. A write
to a broken pipe on some other file descriptor will take no action on
the SIGPIPE signal, and the write will fail with an EPIPE error.
#
プログラムが SIGPIPE シグナルを受信するために Notify を呼び出さなかった場合，動作はファイル記述子番号によって異なります。
ファイル記述子 1 または 2 (標準出力または標準エラー) の壊れたパイプへの書き込みは，プログラムを SIGPIPE シグナルで終了させます。
他のファイル記述子の壊れたパイプへの書き込みは SIGPIPE シグナルに対して何のアクションも取らず，書き込みは EPIPE エラーで失敗します。
#

##
If the program has called Notify to receive SIGPIPE signals, the file
descriptor number does not matter. The SIGPIPE signal will be
delivered to the Notify channel, and the write will fail with an EPIPE
error.
#
プログラムが SIGPIPE シグナルを受信するために Notify を呼び出した場合は，ファイル記述子番号は関係ありません。
SIGPIPE シグナルは Notify チャンネルに配信され，書き込みは EPIPE エラーで失敗します。
#

##
This means that, by default, command line programs will behave like
typical Unix command line programs, while other programs will not
crash with SIGPIPE when writing to a closed network connection.
#
つまり，デフォルトでは，コマンドラインプログラムは一般的な Unix のコマンドラインプログラムのように動作しますが，他のプログラムは閉じたネットワーク接続に書き込んでも SIGPIPE でクラッシュしません。
#

##
Go programs that use cgo or SWIG
#
cgo や SWIG を使ったプログラムに行く
#

##
In a Go program that includes non-Go code, typically C/C++ code
accessed using cgo or SWIG, Go's startup code normally runs first. It
configures the signal handlers as expected by the Go runtime, before
the non-Go startup code runs. If the non-Go startup code wishes to
install its own signal handlers, it must take certain steps to keep Go
working well. This section documents those steps and the overall
effect changes to signal handler settings by the non-Go code can have
on Go programs. In rare cases, the non-Go code may run before the Go
code, in which case the next section also applies.
#
Go 以外のコード (通常は cgo または SWIG を使用してアクセスされる C/C++ コード) を含む Go プログラムでは，通常， Go のスタートアップコードが最初に実行されます。
Go 以外のスタートアップコードが実行される前に， Go ランタイムが期待するとおりにシグナルハンドラを設定します。
Go 以外のスタートアップコードが独自のシグナルハンドラをインストールしたい場合は， Go をうまく機能させるために一定の手順を踏む必要があります。
このセクションでは，それらの手順と， Go 以外のコードによるシグナルハンドラの設定に対する全体的な効果の変更が Go プログラムに与える可能性があることについて説明します。
まれに，非 Go コードが Go コードの前に実行されることがあります。
その場合は，次のセクションも適用されます。
#

##
If the non-Go code called by the Go program does not change any signal
handlers or masks, then the behavior is the same as for a pure Go
program.
#
Go プログラムによって呼び出された Go 以外のコードがシグナルハンドラやマスクを変更しない場合，その動作は純粋な Go プログラムの場合と同じです。
#

##
If the non-Go code installs any signal handlers, it must use the
SA_ONSTACK flag with sigaction. Failing to do so is likely to cause
the program to crash if the signal is received. Go programs routinely
run with a limited stack, and therefore set up an alternate signal
stack. Also, the Go standard library expects that any signal handlers
will use the SA_RESTART flag. Failing to do so may cause some library
calls to return "interrupted system call" errors.
#
Go 以外のコードがシグナルハンドラをインストールする場合は， sigaction とともに SA_ONSTACK フラグを使用する必要があります。
そうしないと，シグナルを受信した場合にプログラムがクラッシュする可能性があります。
Go プログラムは通常，限られたスタックで実行されるため，代替シグナルスタックを設定します。
また， Go 標準ライブラリは，すべてのシグナルハンドラが SA_RESTART フラグを使用することを期待しています。
これを怠ると，一部のライブラリ呼び出しが "割り込みシステムコール" エラーを返す可能性があります。
#

##
If the non-Go code installs a signal handler for any of the
synchronous signals (SIGBUS, SIGFPE, SIGSEGV), then it should record
the existing Go signal handler. If those signals occur while
executing Go code, it should invoke the Go signal handler (whether the
signal occurs while executing Go code can be determined by looking at
the PC passed to the signal handler). Otherwise some Go run-time
panics will not occur as expected.
#
非 Go コードが同期シグナル (SIGBUS, SIGFPE, SIGSEGV) のいずれかに対してシグナルハンドラーをインストールする場合は，既存の Go シグナルハンドラーを記録する必要があります。
Go コードの実行中にこれらのシグナルが発生した場合は， Go シグナルハンドラを呼び出す必要があります (Go コードの実行中にシグナルが発生したかどうかは，シグナルハンドラに渡された PC を見ればわかります) 。
そうでなければ， Go の実行時パニックが予想通りに発生しません。
#

##
If the non-Go code installs a signal handler for any of the
asynchronous signals, it may invoke the Go signal handler or not as it
chooses. Naturally, if it does not invoke the Go signal handler, the
Go behavior described above will not occur. This can be an issue with
the SIGPROF signal in particular.
#
非 Go コードが非同期シグナルのいずれかに対してシグナルハンドラーをインストールする場合， Go シグナルハンドラーを起動するか，または選択したとおりに起動しない可能性があります。
当然， Go シグナルハンドラが呼び出されない場合は，上記の Go 動作は発生しません。
これは特に SIGPROF シグナルの問題です。
#

##
The non-Go code should not change the signal mask on any threads
created by the Go runtime. If the non-Go code starts new threads of
its own, it may set the signal mask as it pleases.
#
Go 以外のコードは， Go ランタイムによって作成されたスレッドのシグナルマスクを変更してはいけません。
Go 以外のコードが独自の新しいスレッドを開始した場合は，シグナルマスクが適切に設定される可能性があります。
#

##
If the non-Go code starts a new thread, changes the signal mask, and
then invokes a Go function in that thread, the Go runtime will
automatically unblock certain signals: the synchronous signals,
SIGILL, SIGTRAP, SIGSTKFLT, SIGCHLD, SIGPROF, SIGCANCEL, and
SIGSETXID. When the Go function returns, the non-Go signal mask will
be restored.
#
非 Go コードが新しいスレッドを開始し，シグナルマスクを変更し，そのスレッドで Go 関数を呼び出すと， Go ランタイムは自動的に特定のシグナルのブロックを解除します。
同期シグナル， SIGILL, SIGTRAP, SIGSTKFLT, SIGCHLD, SIGPROF, SIGCANCEL ，および SIGSETXID 。
Go 関数が戻ると， Go 以外のシグナルマスクが復元されます。
#

##
If the Go signal handler is invoked on a non-Go thread not running Go
code, the handler generally forwards the signal to the non-Go code, as
follows. If the signal is SIGPROF, the Go handler does
nothing. Otherwise, the Go handler removes itself, unblocks the
signal, and raises it again, to invoke any non-Go handler or default
system handler. If the program does not exit, the Go handler then
reinstalls itself and continues execution of the program.
#
Go シグナルハンドラが Go コードを実行していない Go 以外のスレッドで呼び出された場合，ハンドラは通常，次のようにシグナルを Go 以外のコードに転送します。
シグナルが SIGPROF の場合， Go ハンドラは何もしません。
それ以外の場合は， Go ハンドラは自分自身を削除し，シグナルのブロックを解除して再度呼び出し， Go 以外のハンドラまたはデフォルトのシステムハンドラを呼び出します。
プログラムが終了しない場合は， Go ハンドラはそれ自体を再インストールしてプログラムの実行を続行します。
#

##
Non-Go programs that call Go code
#
Go コードを呼び出す Non-Go プログラム
#

##
When Go code is built with options like -buildmode=c-shared, it will
be run as part of an existing non-Go program. The non-Go code may
have already installed signal handlers when the Go code starts (that
may also happen in unusual cases when using cgo or SWIG; in that case,
the discussion here applies).  For -buildmode=c-archive the Go runtime
will initialize signals at global constructor time.  For
-buildmode=c-shared the Go runtime will initialize signals when the
shared library is loaded.
#
Go コードが -buildmode=c-shared のようなオプションで構築されている場合，それは既存の Go 以外のプログラムの一部として実行されます。
Go 以外のコードは， Go コードの開始時にすでにシグナルハンドラをインストールしている可能性があります。
(これは， cgo または SWIG を使用している場合の異常な場合にも発生する可能性があります。)
-buildmode=c- アーカイブの場合， Go ランタイムはグローバルコンストラクタ時にシグナルを初期化します。
-buildmode=c-shared の場合，共有ライブラリがロードされると Go ランタイムはシグナルを初期化します。
#

##
If the Go runtime sees an existing signal handler for the SIGCANCEL or
SIGSETXID signals (which are used only on GNU/Linux), it will turn on
the SA_ONSTACK flag and otherwise keep the signal handler.
#
Go ランタイムが SIGCANCEL または SIGSETXID シグナル用の既存のシグナルハンドラ (GNU/Linux でのみ使用される) を見た場合， SA_ONSTACK フラグをオンにし，それ以外の場合はシグナルハンドラを保持します。
#

##
For the synchronous signals and SIGPIPE, the Go runtime will install a
signal handler. It will save any existing signal handler. If a
synchronous signal arrives while executing non-Go code, the Go runtime
will invoke the existing signal handler instead of the Go signal
handler.
#
同期シグナルと SIGPIPE の場合， Go ランタイムはシグナルハンドラをインストールします。
既存のシグナルハンドラを保存します。
Go 以外のコードの実行中に同期シグナルが到着した場合， Go ランタイムは Go シグナルハンドラの代わりに既存のシグナルハンドラを呼び出します。
#

##
Go code built with -buildmode=c-archive or -buildmode=c-shared will
not install any other signal handlers by default. If there is an
existing signal handler, the Go runtime will turn on the SA_ONSTACK
flag and otherwise keep the signal handler. If Notify is called for an
asynchronous signal, a Go signal handler will be installed for that
signal. If, later, Reset is called for that signal, the original
handling for that signal will be reinstalled, restoring the non-Go
signal handler if any.
#
-buildmode=c-archive または -buildmode=c-shared でビルドされた go コードは，デフォルトでは他のシグナルハンドラをインストールしません。
既存のシグナルハンドラがある場合， Go ランタイムは SA_ONSTACK フラグをオンにし，それ以外の場合はシグナルハンドラを保持します。
非同期シグナルに対して Notify が呼び出された場合，そのシグナルに対して Go シグナルハンドラがインストールされます。
後でそのシグナルに対して Reset が呼び出されると，そのシグナルの元の処理が再インストールされ，存在する場合は Go 以外のシグナルハンドラが復元されます。
#

##
Go code built without -buildmode=c-archive or -buildmode=c-shared will
install a signal handler for the asynchronous signals listed above,
and save any existing signal handler. If a signal is delivered to a
non-Go thread, it will act as described above, except that if there is
an existing non-Go signal handler, that handler will be installed
before raising the signal.
#
-buildmode=c-archive または -buildmode=c-shared なしでビルドされた go コードは，上記の非同期シグナル用のシグナルハンドラをインストールし，既存のシグナルハンドラを保存します。
シグナルが Go 以外のスレッドに配信された場合，既存の Go 以外のシグナルハンドラがある場合は，そのシグナルが発生する前にそのハンドラがインストールされます。
#

##
Windows
#
Windows
#

##
On Windows a ^C (Control-C) or ^BREAK (Control-Break) normally cause
the program to exit. If Notify is called for os.Interrupt, ^C or ^BREAK
will cause os.Interrupt to be sent on the channel, and the program will
not exit. If Reset is called, or Stop is called on all channels passed
to Notify, then the default behavior will be restored.
#
Windows では，通常 ^C (Control-C) または ^BREAK (Control-Break) を押すとプログラムが終了します。
Notify が os.Interrupt に対して呼び出された場合， ^C または ^BREAK は os.Interrupt をそのチャンネルで送信させ，プログラムは終了しません。
Reset が呼び出された場合，または Notify に渡されたすべてのチャンネルで Stop が呼び出された場合は，デフォルトの動作に戻ります。
#

##
Plan 9
#
Plan 9
#

##
On Plan 9, signals have type syscall.Note, which is a string. Calling
Notify with a syscall.Note will cause that value to be sent on the
channel when that string is posted as a note.
#
Plan 9 では，シグナルの型は syscall.Note です。
これは文字列です。
syscall.Note で Notify を呼び出すと，その文字列がメモとして投稿されたときにその値がチャンネルに送信されます。
#

