* 1

#// #
fixLongPath is a noop on non-Windows platforms.
#
fixLongPath は， Windows 以外のプラットフォームでは無効です。
#

#		// #
There are two independent errors this function can return:
one for a bad oldname, and one for a bad newname.
At this point we've determined the newname is bad.
But just in case oldname is also bad, prioritize returning
the oldname error because that's what we did historically.
#
この関数から返される可能性がある 2 つの独立したエラーがあります。
1 つは不正な oldname に対するもので，もう 1 つは不正な newname に対するものです。
この時点で， newname は悪いと判断しました。
しかし， oldname も悪い場合は， oldname エラーを返すことを優先してください。
#

#// #
file is the real representation of *File.
The extra level of indirection ensures that no clients of os
can overwrite this data, which could cause the finalizer
to close the wrong file descriptor.
#
file は *File の実際の表現です。
追加レベルの間接指定は， os のどのクライアントもこのデータを上書きできないことを保証します。
これにより，ファイナライザーが誤ったファイル記述子を閉じることがあります。
#

#	dirinfo     *dirInfo // #
nil unless directory being read
#
ディレクトリが読み込まれていない限り nil
#

#	nonblock    bool     // #
whether we set nonblocking mode
#
ノンブロッキングモードに設定したかどうか
#

#	stdoutOrErr bool     // #
whether this is stdout or stderr
#
これが標準出力か標準エラー出力か
#

#// #
Fd returns the integer Unix file descriptor referencing the open file.
The file descriptor is valid only until f.Close is called or f is garbage collected.
On Unix systems this will cause the SetDeadline methods to stop working.
#
Fd は，開いているファイルを参照する整数の Unix ファイル記述子を返します。
ファイル記述子は， f.Close が呼び出されるか， f がガベージコレクションされるまでの間だけ有効です。
Unix システムでは，これにより SetDeadline メソッドが機能しなくなります。
#

#	// #
If we put the file descriptor into nonblocking mode,
then set it to blocking mode before we return it,
because historically we have always returned a descriptor
opened in blocking mode. The File will continue to work,
but any blocking operation will tie up a thread.
#
ファイル記述子をノンブロッキングモードにした場合は，それを返す前にブロックモードに設定します。
これまでは，ブロックモードで開いた記述子を常に返していました。
ファイルは引き続き機能しますが，ブロック操作を行うとスレッドが拘束されます。
#

#// #
NewFile returns a new File with the given file descriptor and
name. The returned value will be nil if fd is not a valid file
descriptor. On Unix systems, if the file descriptor is in
non-blocking mode, NewFile will attempt to return a pollable File
(one for which the SetDeadline methods work).
#
NewFile は与えられたファイルディスクリプタと名前で新しい File を返します。
fd が有効なファイル記述子ではない場合，戻り値は nil になります。
Unix システムでは，ファイルディスクリプタがノンブロッキングモードの場合， NewFile はポーリング可能な File(SetDeadline メソッドが機能するファイル) を返そうとします。
#

#// #
newFileKind describes the kind of file to newFile.
#
newFileKind は， newFile にファイルの種類を記述します。
#

#// #
newFile is like NewFile, but if called from OpenFile or Pipe
(as passed in the kind parameter) it tries to add the file to
the runtime poller.
#
newFile は NewFile に似ていますが， OpenFile または Pipe(kind パラメータで渡される) から呼び出された場合は，ランタイムポーラーにファイルを追加しようとします。
#

#	// #
If the caller passed a non-blocking filedes (kindNonBlock),
we assume they know what they are doing so we allow it to be
used with kqueue.
#
呼び出し側がノンブロッキングファイル des (kindNonBlock) を渡した場合，我々は彼らが彼らが何をしているのか知っていると仮定しますので， kqueue でそれを使用することができます。
#

#			// #
On FreeBSD before 10.4 it used to crash the
system unpredictably while running all.bash.
When we stop supporting FreeBSD 10 we can merge
this into the dragonfly/netbsd/openbsd case.
Issue 27619.
#
10.4 より前の FreeBSD では， all.bash の実行中にシステムを予期せずクラッシュさせていました。
FreeBSD 10 のサポートをやめると，これを dragonfly/netbsd/openbsd のケースにマージすることができます。
Issue 27619 。
#

#			// #
Don't try to use kqueue with regular files on *BSDs.
On FreeBSD a regular file is always
reported as ready for writing.
On Dragonfly, NetBSD and OpenBSD the fd is signaled
only once as ready (both read and write).
Issue 19093.
#
*BSD 上の通常のファイルで kqueue を使用しないでください。
FreeBSD では，通常のファイルは常に書く準備ができていると報告されます。
Dragonfly, NetBSD ，および OpenBSD では， fd は準備ができたときに 1 回だけ通知されます (読み書き両方) 。
Issue 19093
#

#			// #
In addition to the behavior described above for regular files,
on Darwin, kqueue does not work properly with fifos:
closing the last writer does not cause a kqueue event
for any readers. See issue ＃24164.
#
通常のファイルに関して上記で説明した動作に加えて， Darwin では， kqueue は fifos では正しく動作しません。
最後のライターを閉じても，どのリーダーに対しても kqueue イベントは発生しません。
issue ＃24164 を参照してください。
#

#		// #
An error here indicates a failure to register
with the netpoll system. That can happen for
a file descriptor that is not supported by
epoll/kqueue; for example, disk files on
GNU/Linux systems. We assume that any real error
will show up in later I/O.
#
ここでのエラーは，ネットポールシステムに登録できなかったことを示します。
これは epoll/kqueue でサポートされていないファイル記述子に対して起こります。
たとえば， GNU/Linux システム上のディスクファイルです。
実際のエラーは後の I/O に現れると仮定します。
#

#		// #
We successfully registered with netpoll, so put
the file into nonblocking mode.
#
netpoll に正常に登録されたので，ファイルを非ブロックモードにします。
#

#// #
epipecheck raises SIGPIPE if we get an EPIPE error on standard
output or standard error. See the SIGPIPE docs in os/signal, and
issue 11845.
#
epipecheck は，標準出力または標準エラー出力に EPIPE エラーが発生した場合， SIGPIPE を送出します。
os/signal にある SIGPIPE のドキュメントと， 11845 を発行してください。
#

#// #
DevNull is the name of the operating system's ``null device.''
On Unix-like systems, it is "/dev/null"; on Windows, "NUL".
#
DevNull はオペレーティングシステムの ``null デバイス'' の名前です。
Unix 系システムでは "/dev/null" です。
Windows では "NUL" です。
#

#// #
openFileNolog is the Unix implementation of OpenFile.
Changes here should be reflected in openFdAt, if relevant.
#
openFileNolog は OpenFile の Unix による実装です。
関連がある場合，ここでの変更は openFdAt に反映されるべきです。
#

#		// #
On OS X, sigaction(2) doesn't guarantee that SA_RESTART will cause
open(2) to be restarted for regular files. This is easy to reproduce on
fuse file systems (see https://golang.org/issue/11180).
#
OS X では， sigaction(2) は SA_RESTART によって open(2) が通常のファイルに対して再起動されることを保証しません。
これは，ヒューズファイルシステムで簡単に再現できます (https://golang.org/issue/11180 を参照) 。
#

#	// #
open(2) itself won't handle the sticky bit on *BSD and Solaris
#
open(2) 自体は *BSD と Solaris のスティッキービットを処理しません
#

#	// #
There's a race here with fork/exec, which we are
content to live with. See ../syscall/exec_unix.go.
#
ここには fork/exec との競合がありますが，これは私たちが共存できるコンテンツです。
../syscall/exec_unix.go を参照してください。
#

#// #
Close closes the File, rendering it unusable for I/O.
On files that support SetDeadline, any pending I/O operations will
be canceled and return immediately with an error.
#
Close は，ファイルを閉じ，ファイルは I/O に使用できなくなります。
SetDeadline をサポートしているファイルでは，保留中の I/O 操作はキャンセルされ，すぐにエラーを返します。
#

#	// #
no need for a finalizer anymore
#
ファイナライザはもう必要ありません
#

#// #
read reads up to len(b) bytes from the File.
It returns the number of bytes read and an error, if any.
#
read は File から len(b) バイトまで読み込みます。
読み込んだバイト数とエラーがあればそれを返します。
#

#// #
pread reads len(b) bytes from the File starting at byte offset off.
It returns the number of bytes read and the error, if any.
EOF is signaled by a zero count with err set to nil.
#
pread は，ファイルからバイトオフセット off から len(b) バイトを読み込みます。
読み込まれたバイト数と，もしあればエラーを返します。
EOF は， err が nil に設定されたゼロカウントによって通知されます。
#

#// #
write writes len(b) bytes to the File.
It returns the number of bytes written and an error, if any.
#
write は，ファイルに len(b) バイトを書き込みます。
書き込まれたバイト数と，もしあればエラーを返します。
#

#// #
pwrite writes len(b) bytes to the File starting at byte offset off.
It returns the number of bytes written and an error, if any.
#
pwrite は，バイトオフセット off から始めて， len(b) バイトを File に書き込みます。
書き込まれたバイト数と，もしあればエラーを返します。
#

#// #
seek sets the offset for the next Read or Write on file to offset, interpreted
according to whence: 0 means relative to the origin of the file, 1 means
relative to the current offset, and 2 means relative to the end.
It returns the new offset and an error, if any.
#
seek は，ファイルの起点に対する相対位置， 1 が現在のオフセットに対する相対位置， 2 が末尾に対する相対位置を意味します。
もしあれば，新しいオフセットとエラーを返します。
#

#// #
Truncate changes the size of the named file.
If the file is a symbolic link, it changes the size of the link's target.
If there is an error, it will be of type *PathError.
#
Truncate は，指定されたファイルのサイズを変更します。
ファイルがシンボリックリンクの場合は，リンクのターゲットのサイズを変更します。
エラーがある場合は， *PathError 型になります。
#

#// #
Remove removes the named file or (empty) directory.
If there is an error, it will be of type *PathError.
#
Remove は，指定したファイルまたは (空の) ディレクトリを削除します。
エラーがある場合は， *PathError 型になります。
#

#	// #
System call interface forces us to know
whether name is a file or directory.
Try both: it is cheaper on average than
doing a Stat plus the right one.
#
システムコールインターフェースにより， name がファイルかディレクトリかを知る必要があります。
両方試します: Stat プラス正しいものを行うよりも平均してコストが低いです。
#

#	// #
Both failed: figure out which error to return.
OS X and Linux differ on whether unlink(dir)
returns EISDIR, so can't use that. However,
both agree that rmdir(file) returns ENOTDIR,
so we can use that to decide which error is real.
Rmdir might also return ENOTDIR if given a bad
file path, like /etc/passwd/foo, but in that case,
both errors will be ENOTDIR, so it's okay to
use the error from unlink.
#
どちらも失敗しました。
どのエラーを返すかを判断します。
OS X と Linux では， unlink(dir) が EISDIR を返すかどうかが異なるため，それを使用できません。
しかし，どちらも rmdir(ファイル) が ENOTDIR を返すことに同意しているので，どちらのエラーが本当かを判断するためにそれを使用できます。
/etc/passwd/foo のように不正なファイルパスを指定した場合， Rmdir は ENOTDIR も返す可能性がありますが，その場合は両方のエラーが ENOTDIR になるため， unlink からのエラーを使用してもかまいません。
#

#// #
Link creates newname as a hard link to the oldname file.
If there is an error, it will be of type *LinkError.
#
Link は oldname ファイルへのハードリンクとして newname を作成します。
エラーがある場合は， *LinkError 型になります。
#

#// #
Symlink creates newname as a symbolic link to oldname.
If there is an error, it will be of type *LinkError.
#
Symlink は newname を oldname へのシンボリックリンクとして作成します。
エラーがある場合は， *LinkError 型になります。
#

#			// #
File disappeared between readdir + stat.
Just treat it as if it didn't exist.
#
readdir + stat の間にファイルが消えました。
それが存在していないかのようにそれを扱うだけです。
#

#		// #
Per File.Readdir, the slice must be non-empty or err
must be non-nil if n > 0.
#
File.Readdir によると， n > 0 の場合，スライスは空でないか， err は非 nil である必要があります。
#

[os/file_unix.go]
#	appendMode  bool     // #
whether file is opened for appending
#
ファイルが追加のために開かれているかどうか
#

[os/file_unix.go]
#// #
Readlink returns the destination of the named symbolic link.
If there is an error, it will be of type *PathError.
#
Readlink は，指定されたシンボリックリンクのリンク先を返します。エラーがある場合，型は *PathError になります。
#

[os/file_unix.go]
#		// #
buffer too small
#
バッファが小さすぎる
#

