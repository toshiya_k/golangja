* 1

#// #
supportsCloseOnExec reports whether the platform supports the
O_CLOEXEC flag.
On Darwin, the O_CLOEXEC flag was introduced in OS X 10.7 (Darwin 11.0.0).
See https://support.apple.com/kb/HT1633.
On FreeBSD, the O_CLOEXEC flag was introduced in version 8.3.
#
supportsCloseOnExec は，プラットフォームが O_CLOEXEC フラグをサポートしているかどうかを報告します。
ダーウィンでは， O_CLOEXEC フラグは OS X 10.7 (Darwin 11.0.0) で導入されました。
https://support.apple.com/kb/HT1633 を参照してください。
FreeBSD では， O_CLOEXEC フラグはバージョン 8.3 で導入されました。
#

