* 1

[database/sql/ctxutil.go]
#		// #
Check the transaction level. If the transaction level is non-default
then return an error here as the BeginTx driver value is not supported.
#
トランザクションレベルを確認します。トランザクションレベルがデフォルト以外の場合， BeginTx ドライバーの値はサポートされないため，ここでエラーを返します。
#

[database/sql/ctxutil.go]
#		// #
If a read-only transaction is requested return an error as the
BeginTx driver value is not supported.
#
読み取り専用トランザクションがリクエストされた場合， BeginTx ドライバー値がサポートされていないため，エラーを返します。
#

