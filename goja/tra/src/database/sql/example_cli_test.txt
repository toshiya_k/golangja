* 1

##
var pool *sql.DB // Database connection pool.
#
var pool *sql.DB // データベースコネクションプール
#

#	// #
Opening a driver typically will not attempt to connect to the database.
#
通常，ドライバを開いてもデータベースへの接続は試みられません。
#

#		// #
This will not be a connection error, but a DSN parse error or
another initialization error.
#
これは接続エラーではなく，
DSN 解析エラーまたは別の初期化エラーです。
#

#// #
Ping the database to verify DSN provided by the user is valid and the
server accessible. If the ping fails exit the program with an error.
#
データベースに ping を実行し，ユーザーが提供した DSN が有効でセーバーにアクセスできるかを検証します。
ping が失敗した場合は，エラーでプログラムを終了します。
#

#// #
Query the database for the information requested and prints the results.
If the query fails exit the program with an error.
#
データベースにクエリを出し，結果を表示します。
クエリが失敗した場合は，エラーを出してプログラムを終了します。
#

