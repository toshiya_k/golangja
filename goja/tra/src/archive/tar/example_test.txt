* 1

#	// #
Create and add some files to the archive.
#
いくつかのファイルを作成してアーカイブに追加します。
#

#	// #
Open and iterate through the files in the archive.
#
アーカイブ内のファイルを開いて繰り返します。
#

#			break // #
End of archive
#
アーカイブの終わり
#

#	// #
Output:
Contents of readme.txt:
This archive contains some text files.
Contents of gopher.txt:
Gopher names:
George
Geoffrey
Gonzo
Contents of todo.txt:
Get animal handling license.
#
Output:
Contents of readme.txt:
This archive contains some text files.
Contents of gopher.txt:
Gopher names:
George
Geoffrey
Gonzo
Contents of todo.txt:
Get animal handling license.
#

