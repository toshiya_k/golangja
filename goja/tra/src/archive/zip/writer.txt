* 1

#// #
Writer implements a zip file writer.
#
Writer は zip ファイルライターを実装します。
#

#	// #
testHookCloseSizeOffset if non-nil is called with the size
of offset of the central directory at Close.
#
クローズ時に中央ディレクトリのオフセットのサイズを指定して nil 以外が呼び出された場合は testHookCloseSizeOffset 。
#

#// #
NewWriter returns a new Writer writing a zip file to w.
#
NewWriter は， zip ファイルを w に書き込む新しい Writer を返します。
#

#// #
SetOffset sets the offset of the beginning of the zip data within the
underlying writer. It should be used when the zip data is appended to an
existing file, such as a binary executable.
It must be called before any data is written.
#
SetOffset (オフセットを設定) は，基になるライター内の zip データの始まりのオフセットを設定します。
zip データをバイナリ実行可能ファイルなどの既存のファイルに追加するときに使用する必要があります。
データが書き込まれる前に呼び出す必要があります。
#

#// #
Flush flushes any buffered data to the underlying writer.
Calling Flush is not normally necessary; calling Close is sufficient.
#
Flush (フラッシュ) は，バッファリングされているデータを内部のライターにフラッシュします。
Flush を呼び出すことは通常必要ありません。 Close を呼び出すだけで十分です。
#

#// #
SetComment sets the end-of-central-directory comment field.
It can only be called before Close.
#
SetComment (コメントを設定) は，end-of-central-directory コメントフィールドを設定します。
Close を呼び出す前にのみ呼び出すことができます。
#

#// #
Close finishes writing the zip file by writing the central directory.
It does not close the underlying writer.
#
Close は，central directory (中央ディレクトリ) を書き込んで zip ファイルの書き込みを終了します。
内部のライターは閉じません。
#

#	// #
write central directory
#
中央ディレクトリを書き込む
#

#			// #
the file needs a zip64 header. store maxint in both
32 bit size fields (and offset later) to signal that the
zip64 extra header should be used.
#
ファイルには zip64 ヘッダーが必要です。
両方の 32 ビットサイズフィールドに maxint を格納し (後でオフセットします) ， zip64 エクストラヘッダーを使用する必要があることを通知します。
#

#			b.uint32(uint32max) // #
compressed size
uncompressed size
#
圧縮サイズ非圧縮サイズ
#

#			// #
append a zip64 extra block to Extra
#
Extra に zip64 エクストラブロックを追加
#

#		b = b[4:] // #
skip disk number start and internal file attr (2x uint16)
#
ディスク番号の先頭と内部ファイルの attr をスキップする (2x uint16)
#

#		// #
zip64 end of central directory record
#
zip64 中央ディレクトリレコードの終わり
#

#		b.uint64(directory64EndLen - 12) // #
length minus signature (uint32) and length fields (uint64)
#
長さ - 署名 (uint32) および長さフィールド (uint64)
#

#		b.uint16(zipVersion45)           // #
version made by
version needed to extract
#
抽出に必要なバージョンで作られたバージョン
#

#		b.uint32(0)                      // #
number of this disk
number of the disk with the start of the central directory
#
このディスクの番号セントラルディレクトリの先頭を持つディスクの番号
#

#		b.uint64(records)                // #
total number of entries in the central directory on this disk
total number of entries in the central directory
#
このディスクの中央ディレクトリにあるエントリの総数中央ディレクトリにあるエントリの総数
#

#		b.uint64(size)                   // #
size of the central directory
#
中央ディレクトリのサイズ
#

#		b.uint64(offset)                 // #
offset of start of central directory with respect to the starting disk number
#
開始ディスク番号を基準とした中央ディレクトリの開始オフセット
#

#		// #
zip64 end of central directory locator
#
zip64 中央ディレクトリロケータの終わり
#

#		b.uint32(0)           // #
number of the disk with the start of the zip64 end of central directory
#
zip64 の先頭が中央ディレクトリの末尾にあるディスクの番号
#

#		b.uint64(uint64(end)) // #
relative offset of the zip64 end of central directory record
#
zip64 の中央ディレクトリレコードの末尾の相対オフセット
#

#		b.uint32(1)           // #
total number of disks
#
ディスクの総数
#

#		// #
store max values in the regular end record to signal
that the zip64 values should be used instead
#
通常の最後のレコードに最大値を格納して，代わりに zip64 値を使用するように指示します。
#

#	// #
write end record
#
書き込み終了レコード
#

#	b = b[4:]                        // #
skip over disk number and first disk number (2x uint16)
#
ディスク番号と最初のディスク番号をスキップする (2x uint16)
#

#	b.uint16(uint16(records))        // #
number of entries this disk
number of entries total
#
このディスクエントリ数の合計エントリ数
#

#	b.uint32(uint32(size))           // #
size of directory
#
ディレクトリサイズ
#

#	b.uint32(uint32(offset))         // #
start of directory
#
ディレクトリの先頭
#

#	b.uint16(uint16(len(w.comment))) // #
byte size of EOCD comment
#
EOCD コメントのバイトサイズ
#

#// #
Create adds a file to the zip file using the provided name.
It returns a Writer to which the file contents should be written.
The file contents will be compressed using the Deflate method.
The name must be a relative path: it must not start with a drive
letter (e.g. C:) or leading slash, and only forward slashes are
allowed. To create a directory instead of a file, add a trailing
slash to the name.
The file's contents must be written to the io.Writer before the next
call to Create, CreateHeader, or Close.
#
Create は，zip ファイルに name ファイルを追加します。
ファイルの内容を書き込むべき Writer を返します。
ファイルの内容は Deflate メソッドを使用して圧縮されます。
name は相対パスである必要があります。
ドライブ文字 (例 :C:) または先頭のスラッシュで始まってはいけません。
スラッシュのみが許可されます。
ファイルではなくディレクトリを作成するには，名前の末尾にスラッシュを追加します。
次に Create, CreateHeader, または Close を呼び出す前に，ファイルの内容を io.Writer に書き込む必要があります。
#

#// #
detectUTF8 reports whether s is a valid UTF-8 string, and whether the string
must be considered UTF-8 encoding (i.e., not compatible with CP-437, ASCII,
or any other common encoding).
#
detectUTF8 は， s が有効な UTF-8 文字列かどうか，およびその文字列を UTF-8 エンコーディングと見なす必要があるかどうか (つまり， CP-437, ASCII ，またはその他の一般的なエンコーディングとは互換性がない) を報告します。
#

#		// #
Officially, ZIP uses CP-437, but many readers use the system's
local character encoding. Most encoding are compatible with a large
subset of CP-437, which itself is ASCII-like.
#
正式には， ZIP は CP-437 を使用しますが，多くのリーダーはシステムのローカル文字エンコーディングを使用します。
ほとんどのエンコーディングは CP-437 の大きなサブセットと互換性があり，それ自体は ASCII に似ています。
#

#		// #
Forbid 0x7e and 0x5c since EUC-KR and Shift-JIS replace those
characters with localized currency and overline characters.
#
EUC-KR と Shift-JIS がそれらの文字をローカライズされた通貨と上線文字に置き換えるので， 0x7e と 0x5c を禁止します。
#

#// #
CreateHeader adds a file to the zip archive using the provided FileHeader
for the file metadata. Writer takes ownership of fh and may mutate
its fields. The caller must not modify fh after calling CreateHeader.
#
CreateHeader は，ファイルメタデータ用の引数の FileHeader を使用して zip アーカイブにファイルを追加します。
ライターは fh の所有権を取得し，そのフィールドを変更するかもしれません。
呼び出し側は， CreateHeader を呼び出した後に fh を変更してはいけません。
#

#// #
This returns a Writer to which the file contents should be written.
The file's contents must be written to the io.Writer before the next
call to Create, CreateHeader, or Close.
#
ファイルの内容を書き込むための Writer を返します。
次に Create, CreateHeader, または Close を呼び出す前に，ファイルの内容を io.Writer に書き込む必要があります。
#

#		// #
See https://golang.org/issue/11144 confusion.
#
https://golang.org/issue/11144 confusion を参照してください。
#

#	// #
The ZIP format has a sad state of affairs regarding character encoding.
Officially, the name and comment fields are supposed to be encoded
in CP-437 (which is mostly compatible with ASCII), unless the UTF-8
flag bit is set. However, there are several problems:
#
ZIP 形式には，文字エンコードに関して悲しい問題があります。
正式には，名前とコメントのフィールドは， UTF-8 フラグビットが設定されていない限り， CP-437 (これはほとんど ASCII と互換性があります) でエンコードされることになっています。
ただし，いくつか問題があります。
#

#	// #
In order to avoid breaking readers without UTF-8 support,
we avoid setting the UTF-8 flag if the strings are CP-437 compatible.
However, if the strings require multibyte UTF-8 encoding and is a
valid UTF-8 string, then we set the UTF-8 bit.
#
UTF-8 をサポートしないでリーダーが壊れるのを避けるために，文字列が CP-437 互換の場合は UTF-8 フラグを設定しないでください。
ただし，文字列がマルチバイトの UTF-8 エンコーディングを必要とし，それが有効な UTF-8 文字列である場合は， UTF-8 ビットを設定します。
#

#	// #
For the case, where the user explicitly wants to specify the encoding
as UTF-8, they will need to set the flag bit themselves.
#
ユーザーがエンコーディングを UTF-8 として明示的に指定したい場合は，フラグビットを自分で設定する必要があります。
#

#	fh.CreatorVersion = fh.CreatorVersion&0xff00 | zipVersion20 // #
preserve compatibility byte
#
互換性バイトを保持
#

#	// #
If Modified is set, this takes precedence over MS-DOS timestamp fields.
#
Modified が設定されている場合，これは MS-DOS タイムスタンプフィールドよりも優先されます。
#

#		// #
Contrary to the FileHeader.SetModTime method, we intentionally
do not convert to UTC, because we assume the user intends to encode
the date using the specified timezone. A user may want this control
because many legacy ZIP readers interpret the timestamp according
to the local timezone.
#
FileHeader.SetModTime メソッドとは反対に，ユーザーは指定されたタイムゾーンを使用して日付をエンコードすることを想定しているため，意図的に UTC に変換しません。
多くの従来の ZIP リーダーは現地のタイムゾーンに従ってタイムスタンプを解釈するため，ユーザーはこの制御を希望する場合があります。
#

#		// #
The timezone is only non-UTC if a user directly sets the Modified
field directly themselves. All other approaches sets UTC.
#
ユーザーが直接 Modified フィールドを直接設定した場合，タイムゾーンは UTC 以外になります。
他のすべてのアプローチは UTC を設定します。
#

#		// #
Use "extended timestamp" format since this is what Info-ZIP uses.
Nearly every major ZIP implementation uses a different format,
but at least most seem to be able to understand the other formats.
#
これが Info-ZIP が使用しているものであるため， " 拡張タイムスタンプ " フォーマットを使用してください。
ほとんどすべての主要な ZIP 実装は異なるフォーマットを使用しますが，少なくともほとんどが他のフォーマットを理解できるように思われます。
#

#		// #
This format happens to be identical for both local and central header
if modification time is the only timestamp being encoded.
#
変更時刻がエンコードされている唯一のタイムスタンプである場合，このフォーマットはローカルヘッダと中央ヘッダの両方で同じになることがあります。
#

#		var mbuf [9]byte // #
2*SizeOf(uint16) + SizeOf(uint8) + SizeOf(uint32)
#
2*SizeOf(uint16) + SizeOf(uint8) + SizeOf(uint32)
#

#		eb.uint16(5)  // #
Size: SizeOf(uint8) + SizeOf(uint32)
#
サイズ : SizeOf(uint8) + SizeOf(uint32)
#

#		eb.uint8(1)   // #
Flags: ModTime
#
フラグ : ModTime
#

#		eb.uint32(mt) // #
ModTime
#
ModTime
#

#		// #
Set the compression method to Store to ensure data length is truly zero,
which the writeHeader method always encodes for the size fields.
This is necessary as most compression formats have non-zero lengths
even when compressing an empty string.
#
データの長さが本当にゼロになるように圧縮方法を Store に設定します。
writeHeader メソッドは常にサイズフィールドをエンコードします。
空の文字列を圧縮する場合でも，ほとんどの圧縮形式で長さがゼロ以外になるため，これが必要です。
#

#		fh.Flags &^= 0x8 // #
we will not write a data descriptor
#
データ記述子は書きません
#

#		// #
Explicitly clear sizes as they have no meaning for directories.
#
ディレクトリには意味がないため，サイズは明示的に明確にしてください。
#

#		fh.Flags |= 0x8 // #
we will write a data descriptor
#
データ記述子を書きます
#

#	// #
If we're creating a directory, fw is nil.
#
ディレクトリを作成している場合， fw は nil です。
#

#	b.uint32(0) // #
since we are writing a data descriptor crc32,
compressed size,
and uncompressed size should be zero
#
我々はデータ記述子 crc32 を書いているので，圧縮サイズと非圧縮サイズはゼロであるべきです
#

#// #
RegisterCompressor registers or overrides a custom compressor for a specific
method ID. If a compressor for a given method is not found, Writer will
default to looking up the compressor at the package level.
#
RegisterCompressor は，method ID のカスタムコンプレッサを登録または上書きします。
method のコンプレッサが見つからない場合， Writer はパッケージレベルでコンプレッサを検索します。
#

#	// #
update FileHeader
#
FileHeader を更新する
#

#		fh.ReaderVersion = zipVersion45 // #
requires 4.5 - File uses ZIP64 format extensions
#
4.5 が必要です - ファイルは ZIP64 フォーマット拡張子を使用します
#

#	// #
Write data descriptor. This is more complicated than one would
think, see e.g. comments in zipfile.c:putextended() and
http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7073588.
The approach here is to write 8 byte sizes if needed without
adding a zip64 extra in the local header (too late anyway).
#
データディスクリプタを書き込みます。
これは想像以上に複雑です。
zipfile.c:putextended() 内のコメント および http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7073588 を参照してください。
ここでのアプローチは，必要ならばローカルヘッダに zip64 を追加せずに 8 バイトサイズを書き込むことです (いずれにせよ遅すぎます) 。
#

#	b.uint32(dataDescriptorSignature) // #
de-facto standard, required by OS X
#
OS X で必須のデファクトスタンダード
#

#			var buf [28]byte // #
2x uint16 + 3x uint64
#
2x uint16 + 3x uint64
#

#			eb.uint16(24) // #
size = 3x uint64
#
size = 3x uint64
#

#	//	#
* Many ZIP readers still do not support UTF-8.
* If the UTF-8 flag is cleared, several readers simply interpret the
name and comment fields as whatever the local system encoding is.
#
* 多くの ZIP リーダーはまだ UTF-8 をサポートしていません。
* UTF-8 フラグがクリアされている場合，いくつかのリーダーは，ローカルシステムのエンコーディングが何であれ，名前とコメントフィールドを単に解釈します。
#


