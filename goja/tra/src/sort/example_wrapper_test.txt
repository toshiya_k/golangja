* 1

#// #
ByName implements sort.Interface by providing Less and using the Len and
Swap methods of the embedded Organs value.
#
ByName は，自分の　Less メソッドと，埋め込まれた Organs の Len と Swap メソッドとを用いて
sort.Interface を実装します。
#

#// #
ByWeight implements sort.Interface by providing Less and using the Len and
Swap methods of the embedded Organs value.
#
ByWeight は，自分の　Less メソッドと，埋め込まれた Organs の Len と Swap メソッドとを用いて
sort.Interface を実装します。
#

[sort/example_wrapper_test.go]
#	// #
Output:
Organs by weight:
prostate (62g)
pancreas (131g)
spleen   (162g)
heart    (290g)
brain    (1340g)
liver    (1494g)
Organs by name:
brain    (1340g)
heart    (290g)
liver    (1494g)
pancreas (131g)
prostate (62g)
spleen   (162g)
#
Output:
Organs by weight:
prostate (62g)
pancreas (131g)
spleen   (162g)
heart    (290g)
brain    (1340g)
liver    (1494g)
Organs by name:
brain    (1340g)
heart    (290g)
liver    (1494g)
pancreas (131g)
prostate (62g)
spleen   (162g)
#

