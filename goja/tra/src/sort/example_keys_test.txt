* 1

#// #
A couple of type definitions to make the units clear.
#
単位をわかりやすくするための型定義
#

#// #
A Planet defines the properties of a solar system object.
#
Planet は，太陽系の惑星の特徴を定義します。
#

#// #
By is the type of a "less" function that defines the ordering of its Planet arguments.
#
By は，Planet の順序を定義する "less" 関数の型です。
#

#// #
Sort is a method on the function type, By, that sorts the argument slice according to the function.
#
Sort は，関数型 By のメソッドで，関数に従ってスライスをソートします。
#

#		#
by:      by, // The Sort method's receiver is the function (closure) that defines the sort order.
#
by:      by, // Sort メソッドのレシーバはソート順を決める関数(クロージャ)です。
#

#// #
planetSorter joins a By function and a slice of Planets to be sorted.
#
planetSorter は，ソートするための by 関数と Planet のスライスを組み合わせです。
#

#	#
by      func(p1, p2 *Planet) bool // Closure used in the Less method.
#
by      func(p1, p2 *Planet) bool // Less 関数で用いるクロージャ
#

#// #
Len is part of sort.Interface.
#
Len は sort.Interface で必要な関数です。
#

#// #
Swap is part of sort.Interface.
#
Swap は sort.Interface で必要な関数です。
#

#// #
Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
#
Less は sort.Interface で必要な関数です。 by クロージャを呼び出します。
#

#// #
ExampleSortKeys demonstrates a technique for sorting a struct type using programmable sort criteria.
#
ExampleSortKeys では，ソート方法を指定してソートするテクニックを紹介します。
#

#	// #
Closures that order the Planet structure.
#
Planet 構造体をソートするクロージャ。
#

#	// #
Sort the planets by the various criteria.
#
様々な方法で Planet をソートします。
#

[sort/example_keys_test.go]
#	// #
Output: By name: [{Earth 1 1} {Mars 0.107 1.5} {Mercury 0.055 0.4} {Venus 0.815 0.7}]
By mass: [{Mercury 0.055 0.4} {Mars 0.107 1.5} {Venus 0.815 0.7} {Earth 1 1}]
By distance: [{Mercury 0.055 0.4} {Venus 0.815 0.7} {Earth 1 1} {Mars 0.107 1.5}]
By decreasing distance: [{Mars 0.107 1.5} {Earth 1 1} {Venus 0.815 0.7} {Mercury 0.055 0.4}]
#
Output: By name: [{Earth 1 1} {Mars 0.107 1.5} {Mercury 0.055 0.4} {Venus 0.815 0.7}]
By mass: [{Mercury 0.055 0.4} {Mars 0.107 1.5} {Venus 0.815 0.7} {Earth 1 1}]
By distance: [{Mercury 0.055 0.4} {Venus 0.815 0.7} {Earth 1 1} {Mars 0.107 1.5}]
By decreasing distance: [{Mars 0.107 1.5} {Earth 1 1} {Venus 0.815 0.7} {Mercury 0.055 0.4}]
#

