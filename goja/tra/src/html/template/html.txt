* 1

#// #
htmlNospaceEscaper escapes for inclusion in unquoted attribute values.
#
htmlNospaceEscaper は，引用符で囲まれていない属性値に含めるためにエスケープします。
#

#// #
attrEscaper escapes for inclusion in quoted attribute values.
#
attrEscaper は，引用符で囲まれた属性値に含めるためにエスケープします。
#

#// #
rcdataEscaper escapes for inclusion in an RCDATA element body.
#
rcdataEscaper は， RCDATA 要素本体に含めるためにエスケープします。
#

#// #
htmlEscaper escapes for inclusion in HTML text.
#
htmlEscaper は HTML テキストに含めるためにエスケープします。
#

#// #
htmlReplacementTable contains the runes that need to be escaped
inside a quoted attribute value or in a text node.
#
htmlReplacementTable には，引用符で囲まれた属性値内またはテキストノード内でエスケープする必要があるルーン文字が含まれています。
#

#	// #
U+0000 NULL Parse error. Append a U+FFFD REPLACEMENT
CHARACTER character to the current attribute's value.
"
and similarly
#
U+0000 NULL 解析エラー。
現在の属性の値に U+FFFD REPLACEMENT CHARACTER 文字を追加します。
"
同様に，
#

#// #
htmlNormReplacementTable is like htmlReplacementTable but without '&' to
avoid over-encoding existing entities.
#
htmlNormReplacementTable は htmlReplacementTable に似ていますが，既存のエンティティのオーバーエンコードを避けるために&を付けません。
#

#// #
htmlNospaceReplacementTable contains the runes that need to be escaped
inside an unquoted attribute value.
The set of runes escaped is the union of the HTML specials and
those determined by running the JS below in browsers:
#
htmlNospaceReplacementTable には，引用符で囲まれていない属性値内でエスケープする必要があるルーン文字が含まれています。
エスケープされたルーン文字のセットは， HTML スペシャルと，以下の JS をブラウザで実行することによって決定されたルーンの和です。
#

#	// #
A parse error in the attribute value (unquoted) and
before attribute value states.
Treated as a quoting character by IE.
#
属性値 (引用符なし) および属性値状態の前の解析エラー。
IE では引用符として扱われます。
#

#// #
htmlNospaceNormReplacementTable is like htmlNospaceReplacementTable but
without '&' to avoid over-encoding existing entities.
#
htmlNospaceNormReplacementTable は htmlNospaceReplacementTable に似ていますが，既存のエンティティのオーバーエンコードを避けるために '&' を付けません。
#

#// #
htmlReplacer returns s with runes replaced according to replacementTable
and when badRunes is true, certain bad runes are allowed through unescaped.
#
htmlReplacer は replacementTable に従って置き換えられたルーン文字で s を返し， badRunes が true の場合，特定の悪いルーン文字はエスケープされずに許可されます。
#

#		// #
Cannot use 'for range s' because we need to preserve the width
of the runes in the input. If we see a decoding error, the input
width will not be utf8.Runelen(r) and we will overrun the buffer.
#
入力のルーンの幅を保持する必要があるため， 'for range s' を使用できません。
デコードエラーが発生した場合，入力幅は utf8.Runelen(r) にはならず，バッファがオーバーランします。
#

#			// #
No-op.
IE does not allow these ranges in unquoted attrs.
#
何もしない。
IE は引用符なしの属性でこれらの範囲を許可しません。
#

#// #
stripTags takes a snippet of HTML and returns only the text content.
For example, `<b>&iexcl;Hi!</b> <script>...</script>` -> `&iexcl;Hi! `.
#
stripTags は HTML のスニペットを受け取り，テキストコンテンツのみを返します。
たとえば， `<b>&iexcl;Hi!</b> <script>...</script>` -> `&iexcl;Hi! ` 。
#

#	// #
Using the transition funcs helps us avoid mangling
`<div title="1>2">` or `I <3 Ponies!`.
#
トランジション関数を使うことで， `<div title="1>2">` や `I <3 Ponies!` を壊すのを避けることができます。
#

#			// #
Use RCDATA instead of parsing into JS or CSS styles.
#
JS または CSS スタイルに解析する代わりに RCDATA を使用する。
#

#				// #
Emit text up to the start of the tag or comment.
#
タグまたはコメントの先頭までテキストを出力します。
#

#			// #
Consume any quote.
#
引用符を使用します。
#

#// #
htmlNameFilter accepts valid parts of an HTML attribute or tag name or
a known-safe HTML attribute.
#
htmlNameFilter は， HTML 属性，タグ名，または既知の安全な HTML 属性の有効な部分を受け入れます。
#

#		// #
Avoid violation of structure preservation.
<input checked {{.K}}={{.V}}>.
Without this, if .K is empty then .V is the value of
checked, but otherwise .V is the value of the attribute
named .K.
#
構造保存の違反を避けます。
<input checked {{.K}}={{.V}}>.
これがないと， .K が空の場合は .V が checked の値になり，それ以外の場合は .V は .K という名前の属性の値になります。
#

#		// #
TODO: Split attr and element name part filters so we can whitelist
attributes.
#
TODO: 属性をホワイトリストに登録できるように， attr と要素名の部分フィルタを分割します。
#

#// #
commentEscaper returns the empty string regardless of input.
Comment content does not correspond to any parsed structure or
human-readable content, so the simplest and most secure policy is to drop
content interpolated into comments.
This approach is equally valid whether or not static comment content is
removed from the template.
#
commentEscaper は，入力に関係なく空の文字列を返します。
コメントの内容は，解析された構造や人間が読める内容には対応していません。
そのため，最も簡単で安全なポリシーは，コメントに補間された内容を削除することです。
この方法は，静的なコメントの内容がテンプレートから削除されているかどうかにかかわらず同様に有効です。
#

