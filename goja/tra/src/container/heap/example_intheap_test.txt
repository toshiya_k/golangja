* 1

#// #
This example demonstrates an integer heap built using the heap interface.
#
この例は，ヒープインターフェースを利用して，整数ヒープを構築する例を示しています。
#

#// #
An IntHeap is a min-heap of ints.
#
IntHeap は，整数の最小ヒープです。
#

#	// #
Push and Pop use pointer receivers because they modify the slice's length,
not just its contents.
#
Push と Pop はポインタレシーバを使っています。
なぜなら，スライスの中身だけでなく，スライスの長さも変更するからです。
#

#// #
This example inserts several ints into an IntHeap, checks the minimum,
and removes them in order of priority.
#
この例では IntHeap にいくつかの整数を挿入しています。そして，最小値をチェックしてから，
優先度順に取り除いています。
#

[container/heap/example_intheap_test.go]
#	// #
Output:
minimum: 1
1 2 3 5
#
Output:
minimum: 1
1 2 3 5
#


