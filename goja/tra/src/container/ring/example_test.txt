* 1

#	// #
Create a new ring of size 4
#
サイズ 4 の新しいリングを作る
#

#	// #
Print out its length
#
長さを表示する
#

#	// #
Create a new ring of size 5
#
サイズ 5 の新しいリングを作る
#

#	// #
Get the length of the ring
#
リングの長さを取得する
#

#	// #
Initialize the ring with some integer values
#
いくつかの整数値でリングを初期化する
#

#	// #
Iterate through the ring and print its contents
#
リングを反復処理してその内容を表示する
#

#	// #
Iterate through the ring backwards and print its contents
#
リングを逆方向に反復処理して内容を表示する
#

#	// #
Move the pointer forward by three steps
#
ポインタを 3 ステップ先に進める
#

#	// #
Create two rings, r and s, of size 2
#
サイズ 2 の 2 つのリング r と s を作成する
#

#	// #
Initialize r with 0s
#
r を 0 で初期化する
#

#	// #
Initialize s with 1s
#
s を 1 で初期化する
#

#	// #
Link ring r and ring s
#
リンクリング r とリング s
#

#	// #
Iterate through the combined ring and print its contents
#
結合されたリングを反復処理してその内容を表示する
#

#	// #
Create a new ring of size 6
#
サイズ 6 の新しいリングを作る
#

#	// #
Unlink three elements from r, starting from r.Next()
#
r から 3 つの要素を取り除きます。
r.Next() から開始します。
#

#	// #
Iterate through the remaining ring and print its contents
#
残りのリングを反復処理てその内容を表示する
#

[container/ring/example_test.go]
#	// #
Output:
4
#
Output:
4
#

[container/ring/example_test.go]
#	// #
Output:
0
1
2
3
4
#
Output:
0
1
2
3
4
#

[container/ring/example_test.go]
#	// #
Output:
4
3
2
1
0
#
Output:
4
3
2
1
0
#

[container/ring/example_test.go]
#	// #
Output:
0
1
2
3
4
#
Output:
0
1
2
3
4
#

[container/ring/example_test.go]
#	// #
Output:
3
4
0
1
2
#
Output:
3
4
0
1
2
#

[container/ring/example_test.go]
#	// #
Output:
0
0
1
1
#
Output:
0
0
1
1
#

[container/ring/example_test.go]
#	// #
Output:
0
4
5
#
Output:
0
4
5
#


