* 1

#// #
Package list implements a doubly linked list.
#
list パッケージは双方向連結リストを実装します。
#

#// #
To iterate over a list (where l is a *List):
#
l を *List として，リストを反復処理するには，
#

#//		// #
do something with e.Value
#
e.Value で何かをする
#

#// #
Element is an element of a linked list.
#
Element は連結リストの要素です。
#

#	// #
Next and previous pointers in the doubly-linked list of elements.
To simplify the implementation, internally a list l is implemented
as a ring, such that &l.root is both the next element of the last
list element (l.Back()) and the previous element of the first list
element (l.Front()).
#
双方向連結リストの要素リスト内の前後のポインタ。
実装を簡単にするために，リスト l は内部的にリングとして実装されます。
&l.root が最後のリスト要素の次の要素 (l.Back()) と最初のリスト要素の前の要素 (l.Front) の両方となります。
#

#	// #
The list to which this element belongs.
#
この要素が属するリスト。
#

#	// #
The value stored with this element.
#
この要素に格納されている値。
#

#// #
Next returns the next list element or nil.
#
Next は次のリスト要素または nil を返します。
#

#// #
Prev returns the previous list element or nil.
#
Prev は前のリスト要素または nil を返します。
#

#// #
List represents a doubly linked list.
The zero value for List is an empty list ready to use.
#
リストは双方向連結リストを表します。
List のゼロ値は，使用可能な空のリストです。
#

#	root Element // #
sentinel list element, only &root, root.prev, and root.next are used
#
センチネルリスト要素。 &root, root.prev, および root.next のみが使用されます。
#

#	len  int     // #
current list length excluding (this) sentinel element
#
(この) センチネル要素を除く現在のリストの長さ
#

#// #
Init initializes or clears list l.
#
Init はリスト l を初期化またはクリアします。
#

#// #
New returns an initialized list.
#
New は初期化リストを返します。
#

#// #
Len returns the number of elements of list l.
The complexity is O(1).
#
Len はリスト l の要素数を返します。
複雑度は O(1) です。
#

#// #
Front returns the first element of list l or nil if the list is empty.
#
Front はリスト l の最初の要素を返します。
リストが空の場合は nil を返します。
#

#// #
Back returns the last element of list l or nil if the list is empty.
#
リストの最後の要素を返します。
リストが空の場合は， nil を返します。
#

#// #
lazyInit lazily initializes a zero List value.
#
lazyInit は，ゼロの List 値を遅延初期化します。
#

#// #
insert inserts e after at, increments l.len, and returns e.
#
insert は at の後に e を挿入し， l.len をインクリメントして e を返します。
#

#// #
insertValue is a convenience wrapper for insert(&Element{Value: v}, at).
#
insertValue は， insert(&Element{Value: v}, at) の便利なラッパーです。
#

#// #
remove removes e from its list, decrements l.len, and returns e.
#
リストから e を削除し， l.len をデクリメントして e を返します。
#

#	e.next = nil // #
avoid memory leaks
#
メモリリークを回避する
#

#	e.prev = nil // #
avoid memory leaks
#
メモリリークを回避する
#

#// #
move moves e to next to at and returns e.
#
move は e を at の隣に移動し， e を返します。
#

#// #
Remove removes e from l if e is an element of list l.
It returns the element value e.Value.
The element must not be nil.
#
Remove は，e がリスト l の要素である場合，l から e を削除します。
要素値 e.Value を返します。
要素は nil であってはいけません。
#

#		// #
if e.list == l, l must have been initialized when e was inserted
in l or l == nil (e is a zero Element) and l.remove will crash
#
e.list == l の場合， e が l または l == nil (e はゼロ要素)
に挿入されたときに l が初期化されている必要があり， l.remove がクラッシュします。
#

#// #
PushFront inserts a new element e with value v at the front of list l and returns e.
#
PushFront は，リスト l の先頭に値 v の新しい要素 e を挿入し， e を返します。
#

#// #
PushBack inserts a new element e with value v at the back of list l and returns e.
#
PushBack は，リスト l の最後に値 v の新しい要素 e を挿入し， e を返します。
#

#// #
InsertBefore inserts a new element e with value v immediately before mark and returns e.
If mark is not an element of l, the list is not modified.
The mark must not be nil.
#
InsertBefore は， mark の直前に値 v の新しい要素 e を挿入し， e を返します。
mark が l の要素ではない場合，リストは変更されません。
mark は nil であってはいけません。
#

#	// #
see comment in List.Remove about initialization of l
#
l の初期化については
List.Remove のコメントを参照してください。
#

#// #
InsertAfter inserts a new element e with value v immediately after mark and returns e.
If mark is not an element of l, the list is not modified.
The mark must not be nil.
#
InsertAfter は， mark の直後に値 v の新しい要素 e を挿入し， e を返します。
mark が l の要素ではない場合，リストは変更されません。
mark は nil であってはいけません。
#

#// #
MoveToFront moves element e to the front of list l.
If e is not an element of l, the list is not modified.
The element must not be nil.
#
MoveToFront は要素 e をリスト l の先頭に移動します。
e が l の要素ではない場合，リストは変更されません。
要素は nil であってはいけません。
#

#// #
MoveToBack moves element e to the back of list l.
If e is not an element of l, the list is not modified.
The element must not be nil.
#
MoveToBack は，要素 e をリスト l の最後に移動します。
e が l の要素ではない場合，リストは変更されません。
要素は nil であってはいけません。
#

#// #
MoveBefore moves element e to its new position before mark.
If e or mark is not an element of l, or e == mark, the list is not modified.
The element and mark must not be nil.
#
MoveBefore は要素 e を mark の前の新しい位置に移動します。
e または mark が l の要素ではない場合，または e == mark の場合，リストは変更されません。
要素と mark は nil であってはいけません。
#

#// #
MoveAfter moves element e to its new position after mark.
If e or mark is not an element of l, or e == mark, the list is not modified.
The element and mark must not be nil.
#
MoveAfter は，要素 e を mark の後の新しい位置に移動します。
e または mark が l の要素ではない場合，または e == mark の場合，リストは変更されません。
要素と mark は nil であってはいけません。
#

#// #
PushBackList inserts a copy of an other list at the back of list l.
The lists l and other may be the same. They must not be nil.
#
PushBackList は他のリストのコピーをリスト l の後ろに挿入します。
リスト l と他のリストは同じでもかまいません。
これらのリストは nil であってはいけません。
#

#// #
PushFrontList inserts a copy of an other list at the front of list l.
The lists l and other may be the same. They must not be nil.
#
PushFrontList は他のリストのコピーをリスト l の前に挿入します。
リスト l と他のリストは同じでもかまいません。
これらのリストは nil であってはいけません。
#

[container/list/list.go]
#//	#
for e := l.Front(); e != nil; e = e.Next() {
#
for e := l.Front(); e != nil; e = e.Next() {
#

[container/list/list.go]
#//	#
}
#
}
#


