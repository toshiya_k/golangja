* 1

#// #
A Builder is used to efficiently build a string using Write methods.
It minimizes memory copying. The zero value is ready to use.
Do not copy a non-zero Builder.
#
Builder は Write 関数を使って効率的に文字列を作成します。
メモリコピーを最小限にします。ゼロ値で使えます。
ゼロ値以外の Builder をコピーしないでください。
#

#// #
String returns the accumulated string.
#
String は作成された文字列を返します。
#

#// #
Len returns the number of accumulated bytes; b.Len() == len(b.String()).
#
Len は作成されたバイトの数を返します。b.Len() == len(b.String())
#

#// #
Cap returns the capacity of the builder's underlying byte slice. It is the
total space allocated for the string being built and includes any bytes
already written.
#
Cap は Builder 中のバイトスライスの容量を返します。
すでに書き込まれたバイトを含む，作成中の文字列のために割り当てられたスペースの容量となります。
#

#// #
Reset resets the Builder to be empty.
#
Reset は Builder を空にします。
#

#// #
Grow grows b's capacity, if necessary, to guarantee space for
another n bytes. After Grow(n), at least n bytes can be written to b
without another allocation. If n is negative, Grow panics.
#
Grow は追加 n バイトの容量を保証し，必要に応じて b の容量を増やします。
Grow(n) の後，再度のアロケーションなしに， b に少なくとも n バイトを書き込むことができます。
n が負の場合， Grow はパニックします。
#

#// #
Write appends the contents of p to b's buffer.
Write always returns len(p), nil.
#
Write は p の内容をバッファ b に追加します。
Write はいつも len(p), nil を返します。
#

#// #
WriteByte appends the byte c to b's buffer.
The returned error is always nil.
#
WriteByte はバイト c をバッファ b に追加します。
返り値のエラーは常に nil です。
#

#// #
WriteRune appends the UTF-8 encoding of Unicode code point r to b's buffer.
It returns the length of r and a nil error.
#
WriteRune は Unicode コードポイント r の UTF-8 エンコードをバッファ b に追加します。
r の長さと nil エラーを返します。
#

#// #
WriteString appends the contents of s to b's buffer.
It returns the length of s and a nil error.
#
WriteString は s の内容を バッファ b に追加します。
s の長さと nil エラーを返します。
#

[strings/builder.go]
#	addr *Builder // #
of receiver, to detect copies by value
#
値によってコピーを検出するための受信者の
#

[strings/builder.go]
#// #
noescape hides a pointer from escape analysis.  noescape is
the identity function but escape analysis doesn't think the
output depends on the input. noescape is inlined and currently
compiles down to zero instructions.
USE CAREFULLY!
This was copied from the runtime; see issues 23382 and 7921.
#
noescape は，エスケープ分析からポインターを隠します。 noescape は恒等関数ですが，エスケープ分析は出力が入力に依存するとは考えません。 noescape はインライン化され，現在ゼロ命令までコンパイルされます。慎重に使用する ! これはランタイムからコピーされました。問題 23382 および 7921 を参照する。
#

[strings/builder.go]
#		// #
This hack works around a failing of Go's escape analysis
that was causing b to escape and be heap allocated.
See issue 23382.
TODO: once issue 7921 is fixed, this should be reverted to
just "b.addr = b".
#
このハックは， Go のエスケープ分析が失敗し， b がエスケープされてヒープが割り当てられる原因となります。問題 23382 を参照する。 TODO: 問題 7921 が修正されたら，これを "b.addr = b" に戻す必要があります。
#

[strings/builder.go]
#// #
grow copies the buffer to a new, larger buffer so that there are at least n
bytes of capacity beyond len(b.buf).
#
grow は， len(b.buf) を超える容量が少なくとも n バイトになるように，バッファーを新しい大きなバッファーにコピーします。
#

