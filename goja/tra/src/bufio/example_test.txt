* 1

#	#
w.Flush() // Don't forget to flush!
#
w.Flush() // Flush を呼び出すのを忘れないように!
#

#// #
The simplest use of a Scanner, to read standard input as a set of lines.
#
もっともシンプルな Scanner の使い方。標準入力から，毎行読み込みます。
#

#		#
fmt.Println(scanner.Text()) // Println will add back the final '\n'
#
fmt.Println(scanner.Text()) // Println で行末の '\n' を元に戻す
#

#// #
Return the most recent call to Scan as a []byte.
#
Scan の最新の呼び出しを []byte として返します。
#

#	// #
An artificial input source.
#
人工的な入力
#

#	// #
Set the split function for the scanning operation.
#
スキャン操作用の分割関数を設定する。
#

#	// #
Count the words.
#
単語を数える。
#

#// #
Use a Scanner to implement a simple word-count utility by scanning the
input as a sequence of space-delimited tokens.
#
Scanner を使って，空白で区切られた単語数を数えます。

#

#// #
Use a Scanner with a custom split function (built by wrapping ScanWords) to validate
32-bit decimal input.
#
ScanWords をラップした独自の分割関数を使った Scanner を使い，
32ビット小数入力が正しいかを判定する。
#

#	// #
Create a custom split function by wrapping the existing ScanWords function.
#
ScanWords 関数をラップして，独自の分割関数を作る。
#

#	// #
Validate the input
#
入力が正しいかチェックする
#

#// #
Use a Scanner with a custom split function to parse a comma-separated
list with an empty final value.
#
独自の分割関数を持つ Scanner を使って，カンマで区切られたリストを
パースする。
#

#	// #
Comma-separated list; last entry is empty.
#
カンマで区切られたリスト。最後は空文字。
#

#	// #
Define a split function that separates on commas.
#
カンマで区切る分割関数を定義する。
#

#		// #
There is one final token to be delivered, which may be the empty string.
Returning bufio.ErrFinalToken here tells Scan there are no more tokens after this
but does not trigger an error to be returned from Scan itself.
#
最後のトークン。空文字かもしれない。
bufio.ErrFinalToken を返すことによって，これ以上トークンが存在しないことを示す。
しかし，Scan 自身からエラーを返すことにはならない。
#

#	// #
Scan.
#
スキャンする。
#

[bufio/example_test.go]
#	// #
Output: Hello, world!
#
Output: Hello, world!
#

[bufio/example_test.go]
#	// #
Output:
true
#
Output:
true
#

[bufio/example_test.go]
#	// #
Output: 15
#
Output: 15
#

[bufio/example_test.go]
#	// #
Output:
1234
5678
Invalid input: strconv.ParseInt: parsing "1234567901234567890": value out of range
#
Output:
1234
5678
Invalid input: strconv.ParseInt: parsing "1234567901234567890": value out of range
#

[bufio/example_test.go]
#	// #
Output: "1" "2" "3" "4" ""
#
Output: "1" "2" "3" "4" ""
#


