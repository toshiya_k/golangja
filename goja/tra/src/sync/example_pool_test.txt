* 1

#		// #
The Pool's New function should generally only return pointer
types, since a pointer can be put into the return interface
value without an allocation:
#
Pool の New 関数は一般にポインタ型のみを返すべきです，なぜならポインタは割り当てなしで戻りインターフェース値に入れることができるからです。
#

#// #
timeNow is a fake version of time.Now for tests.
#
timeNow は time のテスト用偽バージョンです。
#

#	// #
Replace this with time.Now() in a real logger.
#
これを実際のロガーの time.Now() に置き換えます。
#

[sync/example_pool_test.go]
#	// #
Output: 2006-01-02T15:04:05Z path=/search?q=flowers
#
出力 : 2006-01-02T15:04:05Z パス =/search?q=flowers
#

