* 1

#// #
YCbCrSubsampleRatio is the chroma subsample ratio used in a YCbCr image.
#
YCbCrSubsampleRatio は， YCbCr 画像で使用される彩度サブサンプル比です。
#

#// #
YCbCr is an in-memory image of Y'CbCr colors. There is one Y sample per
pixel, but each Cb and Cr sample can span one or more pixels.
YStride is the Y slice index delta between vertically adjacent pixels.
CStride is the Cb and Cr slice index delta between vertically adjacent pixels
that map to separate chroma samples.
It is not an absolute requirement, but YStride and len(Y) are typically
multiples of 8, and:
#
YCbCr は， Y'CbCr カラーのメモリ内画像です。
ピクセルごとに 1 つの Y サンプルがありますが，各 Cb および Cr サンプルは 1 つ以上のピクセルにまたがることができます。
YStride は，垂直方向に隣接するピクセル間の Y スライスインデックスのデルタです。
CStride は，別々のクロマサンプルにマッピングされる垂直方向に隣接するピクセル間の Cb および Cr スライスインデックスのデルタです。
これは絶対的な要件ではありませんが， YStride と len(Y) は通常 8 の倍数です。
#

#// #
YOffset returns the index of the first element of Y that corresponds to
the pixel at (x, y).
#
YOffset は， (x, y) のピクセルに対応する Y の最初の要素のインデックスを返します。
#

#// #
COffset returns the index of the first element of Cb or Cr that corresponds
to the pixel at (x, y).
#
COffset は， (x, y) のピクセルに対応する Cb または Cr の最初の要素のインデックスを返します。
#

#	// #
Default to 4:4:4 subsampling.
#
デフォルトは 4:4:4 サブサンプリングです。
#

#// #
SubImage returns an image representing the portion of the image p visible
through r. The returned value shares pixels with the original image.
#
SubImage は， r を通して見える p の部分を表す画像を返します。
戻り値は，元の画像とピクセルを共有します。
#

#	// #
If r1 and r2 are Rectangles, r1.Intersect(r2) is not guaranteed to be inside
either r1 or r2 if the intersection is empty. Without explicitly checking for
this, the Pix[i:] expression below can panic.
#
r1 と r2 が Rectangle の場合，交差点が空の場合， r1.Intersect(r2) が r1 と r2 のどちらの内側にあることは保証されません。
これを明示的にチェックしないと，以下の Pix[i:] 式がパニックになる可能性があります。
#

#// #
NewYCbCr returns a new YCbCr image with the given bounds and subsample
ratio.
#
NewYCbCr は，与えられた範囲とサブサンプルの比率で新しい YCbCr 画像を返します。
#

#// #
NYCbCrA is an in-memory image of non-alpha-premultiplied Y'CbCr-with-alpha
colors. A and AStride are analogous to the Y and YStride fields of the
embedded YCbCr.
#
NYCbCrA は，アルファが乗算されていないアルファ付きの Y'CbCr カラーのメモリ内画像です。
A と AStride は，埋め込まれた YCbCr の Y と YStride フィールドに似ています。
#

#// #
AOffset returns the index of the first element of A that corresponds to the
pixel at (x, y).
#
AOffset は， (x, y) のピクセルに対応する A の最初の要素のインデックスを返します。
#

#// #
Opaque scans the entire image and reports whether it is fully opaque.
#
Opaque は画像全体をスキャンし，完全に不透明かどうかを報告します。
#

#// #
NewNYCbCrA returns a new NYCbCrA image with the given bounds and subsample
ratio.
#
NewNYCbCrA は，与えられた範囲とサブサンプルの比率で新しい NYCbCrA 画像を返します。
#

[image/ycbcr.go]
#//	#
For 4:4:4, CStride == YStride/1 && len(Cb) == len(Cr) == len(Y)/1.
For 4:2:2, CStride == YStride/2 && len(Cb) == len(Cr) == len(Y)/2.
For 4:2:0, CStride == YStride/2 && len(Cb) == len(Cr) == len(Y)/4.
For 4:4:0, CStride == YStride/1 && len(Cb) == len(Cr) == len(Y)/2.
For 4:1:1, CStride == YStride/4 && len(Cb) == len(Cr) == len(Y)/4.
For 4:1:0, CStride == YStride/4 && len(Cb) == len(Cr) == len(Y)/8.
#
For 4:4:4, CStride == YStride/1 && len(Cb) == len(Cr) == len(Y)/1.
For 4:2:2, CStride == YStride/2 && len(Cb) == len(Cr) == len(Y)/2.
For 4:2:0, CStride == YStride/2 && len(Cb) == len(Cr) == len(Y)/4.
For 4:4:0, CStride == YStride/1 && len(Cb) == len(Cr) == len(Y)/2.
For 4:1:1, CStride == YStride/4 && len(Cb) == len(Cr) == len(Y)/4.
For 4:1:0, CStride == YStride/4 && len(Cb) == len(Cr) == len(Y)/8.
#

