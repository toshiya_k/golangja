* 1

#// #
Package jpeg implements a JPEG image decoder and encoder.
#
jpeg パッケージは JPEG 画像デコーダとエンコーダを実装します。
#

#// #
JPEG is defined in ITU-T T.81: https://www.w3.org/Graphics/JPEG/itu-t81.pdf.
#
JPEG は ITU-T T.81: https://www.w3.org/Graphics/JPEG/itu-t81.pdf で定義されています。
#

#// #
TODO(nigeltao): fix up the doc comment style so that sentences start with
the name of the type or function that they annotate.
#
TODO(nigeltao): 文が注釈を付ける型または関数の名前で始まるように， doc のコメントスタイルを修正しました。
#

#// #
A FormatError reports that the input is not a valid JPEG.
#
FormatError は，入力が有効な JPEG ではないことを報告します。
#

#// #
An UnsupportedError reports that the input uses a valid but unimplemented JPEG feature.
#
UnsupportedError は，入力が有効だが未実装の JPEG 機能を使用していることを報告します。
#

#// #
Component specification, specified in section B.2.2.
#
component 定義，セクション B.2.2
#

#	h  int   // #
Horizontal sampling factor.
#
水平方向のサンプリング係数
#

#	v  int   // #
Vertical sampling factor.
#
垂直方向のサンプリング係数
#

#	c  uint8 // #
Component identifier.
#
コンポーネント識別子
#

#	tq uint8 // #
Quantization table destination selector.
#
量子化テーブル宛先セレクタ。
#

#	sof0Marker = 0xc0 // #
Start Of Frame (Baseline Sequential).
#
フレームの開始 (ベースライン順次) 。
#

#	sof1Marker = 0xc1 // #
Start Of Frame (Extended Sequential).
#
フレームの始まり (拡張順次) 。
#

#	sof2Marker = 0xc2 // #
Start Of Frame (Progressive).
#
フレームの始まり (プログレッシブ) 。
#

#	dhtMarker  = 0xc4 // #
Define Huffman Table.
#
ハフマンテーブルを定義します。
#

#	rst0Marker = 0xd0 // #
ReSTart (0).
#
再起動 (0)
#

#	rst7Marker = 0xd7 // #
ReSTart (7).
#
再起動 (7) 。
#

#	soiMarker  = 0xd8 // #
Start Of Image.
#
画像の始まり
#

#	eoiMarker  = 0xd9 // #
End Of Image.
#
画像の終わり
#

#	sosMarker  = 0xda // #
Start Of Scan.
#
スキャン開始。
#

#	dqtMarker  = 0xdb // #
Define Quantization Table.
#
量子化テーブルを定義します。
#

#	driMarker  = 0xdd // #
Define Restart Interval.
#
再起動間隔を定義します。
#

#	comMarker  = 0xfe // #
COMment.
#
コメント。
#

#	// #
"APPlication specific" markers aren't part of the JPEG spec per se,
but in practice, their use is described at
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html
#
" アプリケーション固有の " マーカー自体は JPEG の仕様の一部ではありませんが，実際には，その使用方法は https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html に記載されています。
#

#// #
See https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe
#
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe を参照してください。
#

#// #
unzig maps from the zig-zag ordering to the natural ordering. For example,
unzig[3] is the column and row of the fourth element in zig-zag order. The
value is 16, which means first column (16%8 == 0) and third row (16/8 == 2).
#
unzig は，ジグザグ順序から自然順序へとマップします。
たとえば， unzig[3] は， 4 番目の要素のジグザグ順の列と行です。
値は 16 です。
これは， 1 列目 (16%8 == 0) と 3 行目 (16/8 == 2) を意味します。
#

#// #
Deprecated: Reader is not used by the image/jpeg package and should
not be used by others. It is kept for compatibility.
#
非推奨: Reader は image/jpeg パッケージでは使用されていません。他のパッケージからも使用しないでください。
互換性のために残されています。
#

#// #
bits holds the unprocessed bits that have been taken from the byte-stream.
The n least significant bits of a form the unread bits, to be read in MSB to
LSB order.
#
bits は，バイトストリームから取得された未処理のビットを保持します。
a の最下位 n ビットは， MSB から LSB の順に読み出される未読ビットです。
#

#	a uint32 // #
accumulator.
#
アキュムレータ。
#

#	m uint32 // #
mask. m==1<<(n-1) when n>0, with m==0 when n==0.
#
マスク。 n>0のときm==1<<(n−1) ，n==0のときm==0である。
#

#	n int32  // #
the number of unread bits in a.
#
a の未読ビット数
#

#	// #
bytes is a byte buffer, similar to a bufio.Reader, except that it
has to be able to unread more than 1 byte, due to byte stuffing.
Byte stuffing is specified in section F.1.2.3.
#
bytes は， bufio.Reader に似たバイトバッファです。
ただし，バイトスタッフィングのため， 1 バイト以上を読み取れないようにする必要があります。
バイトスタッフィングはセクション F.1.2.3 で指定されています。
#

#		// #
buf[i:j] are the buffered bytes read from the underlying
io.Reader that haven't yet been passed further on.
#
buf[i:j] は，内部の io.Reader から読み取られたバッファリングされたバイトのうち，まだ渡されていないものです。
#

#		// #
nUnreadable is the number of bytes to back up i after
overshooting. It can be 0, 1 or 2.
#
nUnnreadable は，オーバーシュート後に i をバックアップするためのバイト数です。
0, 1 ，または 2 です。
#

#	ri    int // #
Restart Interval.
#
間隔を再起動します。
#

#	// #
As per section 4.5, there are four modes of operation (selected by the
SOF? markers): sequential DCT, progressive DCT, lossless and
hierarchical, although this implementation does not support the latter
two non-DCT modes. Sequential DCT is further split into baseline and
extended, as per section 4.11.
#
セクション4.5によると，4つの動作モード (SOFマーカによって選択される) がある。
順次DCT，プログレッシブDCT，ロスレスおよび階層的であるが，この実施は後者の2つの非DCTモードをサポートしない。
逐次 DCT は，セクション 4.11 に従って，さらにベースラインと拡張に分けられます。
#

#	eobRun              uint16 // #
End-of-Band run, specified in section G.1.2.2.
#
セクション G.1.2.2 で指定されている，エンドオブバンドラン。
#

#	progCoeffs [maxComponents][]block // #
Saved state between progressive-mode scans.
#
プログレッシブモードスキャン間で保存された状態。
#

#	quant      [maxTq + 1]block // #
Quantization tables, in zig-zag order.
#
ジグザグ順の量子化テーブル。
#

#// #
fill fills up the d.bytes.buf buffer from the underlying io.Reader. It
should only be called when there are no unread bytes in d.bytes.
#
fill は，内部の io.Reader から d.bytes.buf バッファをいっぱいにします。
d.bytes に未読バイトがない場合にのみ呼び出されるべきです。
#

#	// #
Move the last 2 bytes to the start of the buffer, in case we need
to call unreadByteStuffedByte.
#
unreadByteStuffedByte を呼び出す必要がある場合に備えて，最後の 2 バイトをバッファの先頭に移動します。
#

#	// #
Fill in the rest of the buffer.
#
バッファの残りを埋めます。
#

#// #
unreadByteStuffedByte undoes the most recent readByteStuffedByte call,
giving a byte of data back from d.bits to d.bytes. The Huffman look-up table
requires at least 8 bits for look-up, which means that Huffman decoding can
sometimes overshoot and read one or two too many bytes. Two-byte overshoot
can happen when expecting to read a 0xff 0x00 byte-stuffed byte.
#
unreadByteStuffedByte は最新の readByteStuffedByte 呼び出しを取り消し， 1 バイトのデータを d.bits から d.bytes に戻します。
ハフマンルックアップテーブルは，ルックアップのために少なくとも 8 ビットを必要とします。
これは，ハフマン復号化が時々オーバーシュートして 1 つか 2 つの非常に多くのバイトを読むことができることを意味します。
2 バイトのオーバーシュートは， 0xff 0x00 バイト詰めバイトを読み取ることを期待しているときに発生する可能性があります。
#

#// #
readByte returns the next byte, whether buffered or not buffered. It does
not care about byte stuffing.
#
readByte は，バッファされているかどうかにかかわらず，次のバイトを返します。
バイトスタッフィングについては関係ありません。
#

#// #
errMissingFF00 means that readByteStuffedByte encountered an 0xff byte (a
marker byte) that wasn't the expected byte-stuffed sequence 0xff, 0x00.
#
errMissingFF00 は， readByteStuffedByte が予期されるバイト詰めシーケンス 0xff, 0x00 ではない 0xff バイト (マーカーバイト) を検出したことを意味します。
#

#// #
readByteStuffedByte is like readByte but is for byte-stuffed Huffman data.
#
readByteStuffedByte は readByte に似ていますが，バイト詰めハフマンデータ用です。
#

#	// #
Take the fast path if d.bytes.buf contains at least two bytes.
#
d.bytes.buf に少なくとも 2 バイトが含まれている場合は，高速パスを使用してください。
#

#// #
readFull reads exactly len(p) bytes into p. It does not care about byte
stuffing.
#
readFull は正確に len(p) バイトを p に読み込みます。
バイトスタッフィングについては関係ありません。
#

#	// #
Unread the overshot bytes, if any.
#
もしあれば，オーバーショットバイトを未読にします。
#

#// #
ignore ignores the next n bytes.
#
ignore は次の n バイトを無視します。
#

#// #
Specified in section B.2.2.
#
セクション B.2.2 で指定されています。
#

#	case 6 + 3*1: // #
Grayscale image.
#
グレースケール画像
#

#	case 6 + 3*3: // #
YCbCr or RGB image.
#
YCbCr または RGB 画像
#

#	case 6 + 3*4: // #
YCbCrK or CMYK image.
#
YCbCrK または CMYK 画像
#

#	// #
We only support 8-bit precision.
#
8 ビット精度のみをサポートします。
#

#		// #
Section B.2.2 states that "the value of C_i shall be different from
the values of C_1 through C_(i-1)".
#
B.2.2 節では， "C_i の値は C_1 から C_(i-1) の値とは異なるものとする " と述べています。
#

#			// #
If a JPEG image has only one component, section A.2 says "this data
is non-interleaved by definition" and section A.2.2 says "[in this
case...] the order of data units within a scan shall be left-to-right
and top-to-bottom... regardless of the values of H_1 and V_1". Section
4.8.2 also says "[for non-interleaved data], the MCU is defined to be
one data unit". Similarly, section A.1.1 explains that it is the ratio
of H_i to max_j(H_j) that matters, and similarly for V. For grayscale
images, H_1 is the maximum H_j for all components j, so that ratio is
always 1. The component's (h, v) is effectively always (1, 1): even if
the nominal (h, v) is (2, 1), a 20x5 image is encoded in three 8x8
MCUs, not two 16x8 MCUs.
#
JPEG 画像が 1 つの成分しか持たない場合，セクション A.2 は " このデータは定義によってインターリーブされていない " と述べ，セクション A.2.2 は "[この場合 ...] スキャン内のデータユニットの順序はそのままにする "H_1 と V_1 の値に関係なく，右から上，下から ... 。
セクション 4.8.2 はまた "[非インタリーブデータの場合] ， MCU は 1 つのデータユニットとして定義されています " とも述べています。
同様に，セクション A.1.1 では， max_j に対する H_i の比率 (H_j) が重要であることが説明されています。
グレースケール画像の場合， H_1 はすべてのコンポーネント j の最大 H_j なので，比率は常に 1 になります。
(h, v) は事実上常に (1, 1) である。
公称 (h, v) が (2, 1) であっても，20 ×5画像は2つの16 ×8 MCUではなく3つの8 ×8 MCUで符号化される。
#

#			// #
For YCbCr images, we only support 4:4:4, 4:4:0, 4:2:2, 4:2:0,
4:1:1 or 4:1:0 chroma subsampling ratios. This implies that the
(h, v) values for the Y component are either (1, 1), (1, 2),
(2, 1), (2, 2), (4, 1) or (4, 2), and the Y component's values
must be a multiple of the Cb and Cr component's values. We also
assume that the two chroma components have the same subsampling
ratio.
#
YCbCr 画像の場合， 4:4:4 ， 4:4:0 ， 4:2:2 ， 4:2:0 ， 4:1:1 または 4:1:0 の彩度サブサンプリング比のみがサポートされます。
これは， Y 成分の (h, v) 値が (1 ， 1) ， (1 ， 2) ， (2 ， 1) ， (2 ， 2) ， (4 ， 1) ，または (4, 2) のいずれかであることを意味します。
また， Y コンポーネントの値は Cb および Cr コンポーネントの値の倍数でなければなりません。
また， 2 つの色差成分が同じサブサンプリング比を持つと仮定します。
#

#				// #
We have already verified, above, that h and v are both
either 1, 2 or 4, so invalid (h, v) combinations are those
with v == 4.
#
h と v はどちらも 1 ， 2 ， 4 のいずれかであることを上で検証済みですので，無効な (h, v) の組み合わせは v == 4 の組み合わせです。
#

#			// #
For 4-component images (either CMYK or YCbCrK), we only support two
hv vectors: [0x11 0x11 0x11 0x11] and [0x22 0x11 0x11 0x22].
Theoretically, 4-component JPEG images could mix and match hv values
but in practice, those two combinations are the only ones in use,
and it simplifies the applyBlack code below if we can assume that:
#
4 成分画像 (CMYK または YCbCrK) の場合， 2 つの hv ベクトル [0x11 0x11 0x11 0x11] と [0x22 0x11 0x11 0x22] のみがサポートされます。
理論的には， 4 成分 JPEG 画像は hv 値を混ぜ合わせることができますが，実際には，これら 2 つの組み合わせが唯一使用されるものであり，以下のことを前提とすれば下記の applyBlack コードを単純化します。
#

#			//	#
- for CMYK, the C and K channels have full samples, and if the M
  and Y channels subsample, they subsample both horizontally and
  vertically.
- for YCbCrK, the Y and K channels have full samples.
#
-CMYK の場合， C チャンネルと K チャンネルにはフルサンプルがあり， M チャンネルと Y チャンネルにサブサンプルがある場合は，水平方向と垂直方向の両方にサブサンプルされます。
-YCbCrK の場合， Y チャンネルと K チャンネルにはフルサンプルがあります。
#

#// #
Specified in section B.2.4.1.
#
セクション B.2.4.1 で指定されています。
#

#// #
Specified in section B.2.4.4.
#
セクション B.2.4.4 で指定されています。
#

#// #
decode reads a JPEG image from r and returns it as an image.Image.
#
decode は r から JPEG 画像を読み込み，それを image.Image として返します。
#

#	// #
Check for the Start Of Image marker.
#
画像の開始マーカーを確認します。
#

#	// #
Process the remaining segments until the End Of Image marker.
#
残りのセグメントを End Of Image マーカーまで処理します。
#

#			// #
Strictly speaking, this is a format error. However, libjpeg is
liberal in what it accepts. As of version 9, next_marker in
jdmarker.c treats this as a warning (JWRN_EXTRANEOUS_DATA) and
continues to decode the stream. Even before next_marker sees
extraneous data, jpeg_fill_bit_buffer in jdhuff.c reads as many
bytes as it can, possibly past the end of a scan's data. It
effectively puts back any markers that it overscanned (e.g. an
"\xff\xd9" EOI marker), but it does not put back non-marker data,
and thus it can silently ignore a small number of extraneous
non-marker bytes before next_marker has a chance to see them (and
print a warning).
#
厳密に言えば，これはフォーマットエラーです。
しかしながら， libjpeg はそれが受け入れるものにおいては寛大です。
バージョン 9 以降， jdmarker.c の next_marker はこれを警告 (JWRN_EXTRANEOUS_DATA) として扱い，ストリームのデコードを続けます。
next_marker が無関係なデータを見る前であっても， jdhuff.c の jpeg_fill_bit_buffer はできる限り多くのバイトを読み込みます。
おそらくスキャンのデータの終わりを超えています。
それはオーバースキャンしたマーカーを効果的に元に戻します (例えば "\xff\xd9" EOI マーカー) ，しかしマーカー以外のデータを元に戻すことはしません。
したがって next_marker の前の少数の余分なマーカー以外のバイトを静かに無視できます。
それらを見る (そして警告を表示する) 機会があります。
#

#			// #
We are therefore also liberal in what we accept. Extraneous data
is silently ignored.
#
それゆえ私達は私達が受け入れるものにも寛容です。
余分なデータは黙って無視されます。
#

#			// #
This is similar to, but not exactly the same as, the restart
mechanism within a scan (the RST[0-7] markers).
#
これは，スキャン内の再起動メカニズム (RST[0-7] マーカー) と似ていますが，まったく同じではありません。
#

#			// #
Note that extraneous 0xff bytes in e.g. SOS data are escaped as
"\xff\x00", and so are detected a little further down below.
#
余分な 0xff バイトは， SOS データは "\xff\x00" としてエスケープされているので，さらに少し下に検出されます。
#

#			// #
Treat "\xff\x00" as extraneous data.
#
"\xff\x00" を無関係なデータとして扱います。
#

#			// #
Section B.1.1.2 says, "Any marker may optionally be preceded by any
number of fill bytes, which are bytes assigned code X'FF'".
#
B.1.1.2 節では， " 任意のマーカーの前に任意の数のフィルバイトを付けることができます。
フィルバイトにはコード X'FF' が割り当てられています。
"
#

#		if marker == eoiMarker { // #
End Of Image.
#
画像の終わり
#

#			// #
Figures B.2 and B.16 of the specification suggest that restart markers should
only occur between Entropy Coded Segments and not after the final ECS.
However, some encoders may generate incorrect JPEGs with a final restart
marker. That restart marker will be seen here instead of inside the processSOS
method, and is ignored as a harmless error. Restart markers have no extra data,
so we check for this before we read the 16-bit length of the segment.
#
仕様書の図 B.2 及び B.16 は，再開マーカはエントロピ符号化セグメント間でのみ発生し，最終 ECS 後では発生しないことを示唆している。
ただし，エンコーダによっては，最終的なリスタートマーカーを使用して誤った JPEG を生成することがあります。
このリスタートマーカーは， processSOS メソッド内ではなく，ここで表示され，無害なエラーとして無視されます。
リスタートマーカーには余分なデータがないため， 16 ビット長のセグメントを読み取る前にこれを確認します。
#

#		// #
Read the 16-bit length of the segment. The value includes the 2 bytes for the
length itself, so we subtract 2 to get the number of remaining bytes.
#
セグメントの 16 ビット長を読みます。
値には長さ自体の 2 バイトが含まれているので，残りのバイト数を取得するために 2 を引きます。
#

#			} else if marker < 0xc0 { // #
See Table B.1 "Marker code assignments".
#
表 B.1 " マーカーコードの割り当て " を参照してください。
#

#// #
applyBlack combines d.img3 and d.blackPix into a CMYK image. The formula
used depends on whether the JPEG image is stored as CMYK or YCbCrK,
indicated by the APP14 (Adobe) metadata.
#
applyBlack は， d.img3 と d.blackPix を組み合わせて CMYK 画像にします。
使用される式は， APP14 (Adobe) メタデータで示されるように， JPEG イメージが CMYK または YCbCrK のどちらとして格納されているかによって異なります。
#

#// #
Adobe CMYK JPEG images are inverted, where 255 means no ink instead of full
ink, so we apply "v = 255 - v" at various points. Note that a double
inversion is a no-op, so inversions might be implicit in the code below.
#
Adobe CMYK JPEG 画像は反転されます。
255 はフルインクではなくインクがないことを意味するため，さまざまな箇所で "v = 255 - v" を適用します。
二重反転は何もしないことに注意してください。
そのため，以下のコードでは反転が暗黙的に含まれる可能性があります。
#

#	// #
If the 4-component JPEG image isn't explicitly marked as "Unknown (RGB
or CMYK)" as per
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe
we assume that it is YCbCrK. This matches libjpeg's jdapimin.c.
#
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe によるように， 4 成分 JPEG 画像が " 不明 (RGB または CMYK)" と明示的にマークされていない場合それが YCbCrK であると仮定します。
これは libjpeg の jdapimin.c と一致します。
#

#		// #
Convert the YCbCr part of the YCbCrK to RGB, invert the RGB to get
CMY, and patch in the original K. The RGB to CMY inversion cancels
out the 'Adobe inversion' described in the applyBlack doc comment
above, so in practice, only the fourth channel (black) is inverted.
#
YCbCrK の YCbCr 部分を RGB に変換し， RGB を反転して CMY を取得し，元の K にパッチします。
RGB から CMY への反転は，上記の applyBlack ドキュメントのコメントで説明されている "Adobe の反転 " を相殺します。
4 番目のチャンネル (黒) が反転します。
#

#	// #
The first three channels (cyan, magenta, yellow) of the CMYK
were decoded into d.img3, but each channel was decoded into a separate
[]byte slice, and some channels may be subsampled. We interleave the
separate channels into an image.CMYK's single []byte slice containing 4
contiguous bytes per pixel.
#
CMYKの最初の 3 つのチャンネル (シアン，マゼンタ，イエロー) は，d.img3 に復号化されたが，各チャンネルは別々の[]バイトスライスに復号化され，いくつかのチャンネルはサブサンプリングされてもよい。
別々のチャンネルを image.CMYK の 1 ピクセルあたり 4 バイトの連続したバイトを含む 1 つの []byte スライスにインターリーブします。
#

#		// #
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe
says that 0 means Unknown (and in practice RGB) and 1 means YCbCr.
#
https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/JPEG.html#Adobe は， 0 は不明 (実際には RGB) を意味し， 1 は YCbCr を意味すると言っています。
#

#// #
Decode reads a JPEG image from r and returns it as an image.Image.
#
Decode は， r から JPEG 画像を読み取り，image.Image として返します。
#

#// #
DecodeConfig returns the color model and dimensions of a JPEG image without
decoding the entire image.
#
DecodeConfig は，画像全体をデコードせずに JPEG 画像のカラーモデルと寸法を返します。
#

[image/jpeg/reader.go]
#			case 0: // #
Y.
#
Y.
#

[image/jpeg/reader.go]
#			case 1: // #
Cb.
#
Cb.
#

[image/jpeg/reader.go]
#			case 2: // #
Cr.
#
Cr.
#

