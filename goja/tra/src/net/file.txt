* 1

#// #
BUG(mikio): On JS, NaCl and Windows, the FileConn, FileListener and
FilePacketConn functions are not implemented.
#
BUG(mikio): JS ， NaCl ， Windows では， FileConn ， FileListener ， FilePacketConn 関数は実装されていません。
#

#// #
FileConn returns a copy of the network connection corresponding to
the open file f.
It is the caller's responsibility to close f when finished.
Closing c does not affect f, and closing f does not affect c.
#
FileConn は，開いているファイル f に対応するネットワーク接続のコピーを返します。
終了したら f を閉じるのは呼び出し側の責任です。
c を閉じても f には影響しません。
f を閉じても c には影響しません。
#

#// #
FileListener returns a copy of the network listener corresponding
to the open file f.
It is the caller's responsibility to close ln when finished.
Closing ln does not affect f, and closing f does not affect ln.
#
FileListener は，開いているファイル f に対応するネットワークリスナのコピーを返します。
終了したら ln を閉じるのは呼び出し側の責任です。
ln を閉じても f には影響しません。
f を閉じても ln には影響しません。
#

#// #
FilePacketConn returns a copy of the packet network connection
corresponding to the open file f.
It is the caller's responsibility to close f when finished.
Closing c does not affect f, and closing f does not affect c.
#
FilePacketConn は，開いているファイル f に対応するパケットネットワーク接続のコピーを返します。
終了したら f を閉じるのは呼び出し側の責任です。
c を閉じても f には影響しません。
f を閉じても c には影響しません。
#

