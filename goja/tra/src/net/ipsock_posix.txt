* 1

#// #
Probe probes IPv4, IPv6 and IPv4-mapped IPv6 communication
capabilities which are controlled by the IPV6_V6ONLY socket option
and kernel configuration.
#
Probe は， IPV6_V6ONLY ソケットオプションおよびカーネル構成によって制御される IPv4, IPv6 ，および IPv4 にマップされた IPv6 通信機能をプローブします。
#

#// #
Should we try to use the IPv4 socket interface if we're only
dealing with IPv4 sockets? As long as the host system understands
IPv4-mapped IPv6, it's okay to pass IPv4-mapeed IPv6 addresses to
the IPv6 interface. That simplifies our code and is most
general. Unfortunately, we need to run on kernels built without
IPv6 support too. So probe the kernel to figure it out.
#
IPv4 ソケットだけを扱っているのなら， IPv4 ソケットインターフェースを使うべきでしょうか。
ホストシステムが IPv4 マップの IPv6 を理解している限り， IPv4 対応の IPv6 アドレスを IPv6 インターフェースに渡しても問題ありません。
これはコードを単純化し，最も一般的なものです。
残念ながら， IPv6 もサポートしていないカーネルでも動作する必要があります。
それでカーネルをプローブします。
#

#		// #
IPv6 communication capability
#
IPv6 通信機能
#

#		// #
IPv4-mapped IPv6 address communication capability
#
IPv4 マップ IPv6 アドレス通信機能
#

#		// #
The latest DragonFly BSD and OpenBSD kernels don't
support IPV6_V6ONLY=0. They always return an error
and we don't need to probe the capability.
#
最新の DragonFly BSD および OpenBSD カーネルは IPV6_V6ONLY=0 をサポートしていません。
それらは常にエラーを返すので，機能を検証する必要はありません。
#

#// #
favoriteAddrFamily returns the appropriate address family for the
given network, laddr, raddr and mode.
#
favoriteAddrFamily は，指定されたネットワーク (laddr ， raddr ，および mode) に適したアドレスファミリを返します。
#

#// #
If mode indicates "listen" and laddr is a wildcard, we assume that
the user wants to make a passive-open connection with a wildcard
address family, both AF_INET and AF_INET6, and a wildcard address
like the following:
#
mode が "listen" を示し， laddr がワイルドカードである場合，ユーザーがワイルドカードアドレスファミリー， AF_INET と AF_INET6 の両方，および次のようなワイルドカードアドレスを使用してパッシブオープン接続を行いたいと仮定します。
#

#//	- #
A listen for a wildcard communication domain, "tcp" or
"udp", with a wildcard address: If the platform supports
both IPv6 and IPv4-mapped IPv6 communication capabilities,
or does not support IPv4, we use a dual stack, AF_INET6 and
IPV6_V6ONLY=0, wildcard address listen. The dual stack
wildcard address listen may fall back to an IPv6-only,
AF_INET6 and IPV6_V6ONLY=1, wildcard address listen.
Otherwise we prefer an IPv4-only, AF_INET, wildcard address
listen.
#
ワイルドカードアドレスを使用したワイルドカード通信ドメイン "tcp" または "udp" の待機 : プラットフォームが IPv6 と IPv4 でマップされた IPv6 通信機能の両方をサポートする場合，または IPv4 をサポートしない場合は，デュアルスタック AF_INET6 と IPV6_V6ONLY=0 ，ワイルドカードアドレスの待機デュアルスタックのワイルドカードアドレスリスンは， IPv6 のみの AF_INET6 および IPV6_V6ONLY=1 のワイルドカードアドレスリスンにフォールバックすることがあります。
それ以外の場合は， IPv4 専用の AF_INET ，ワイルドカードアドレスのリスンを選択します。
#

#//	- #
A listen for a wildcard communication domain, "tcp" or
"udp", with an IPv4 wildcard address: same as above.
#
ワイルドカード通信ドメイン， "tcp" または "udp" を， IPv4 ワイルドカードアドレスを使用して待機します。
上記と同じです。
#

#//	- #
A listen for a wildcard communication domain, "tcp" or
"udp", with an IPv6 wildcard address: same as above.
#
ワイルドカード通信ドメイン "tcp" または "udp" を， IPv6 ワイルドカードアドレスを使用して待機します。
上記と同じです。
#

#//	- #
A listen for an IPv4 communication domain, "tcp4" or "udp4",
with an IPv4 wildcard address: We use an IPv4-only, AF_INET,
wildcard address listen.
#
IPv4 ワイルドカードアドレスを使用して， IPv4 通信ドメイン "tcp4" または "udp4" を待機する :IPv4 専用の AF_INET ，ワイルドカードアドレス待機を使用します。
#

#//	- #
A listen for an IPv6 communication domain, "tcp6" or "udp6",
with an IPv6 wildcard address: We use an IPv6-only, AF_INET6
and IPV6_V6ONLY=1, wildcard address listen.
#
IPv6 ワイルドカードアドレスを使用して， IPv6 通信ドメイン "tcp6" または "udp6" を待機する :IPv6 のみの AF_INET6 および IPV6_V6ONLY=1 のワイルドカードアドレス待機を使用します。
#

#// #
Otherwise guess: If the addresses are IPv4 then returns AF_INET,
or else returns AF_INET6. It also returns a boolean value what
designates IPV6_V6ONLY option.
#
そうでなければ推測 : もしアドレスが IPv4 であれば AF_INET を返し，そうでなければ AF_INET6 を返します。
IPV6_V6ONLY オプションを指定するブール値も返します。
#

#// #
Note that the latest DragonFly BSD and OpenBSD kernels allow
neither "net.inet6.ip6.v6only=1" change nor IPPROTO_IPV6 level
IPV6_V6ONLY socket option setting.
#
最新の DragonFly BSD および OpenBSD カーネルでは， "net.inet6.ip6.v6only=1" の変更も IPPROTO_IPV6 レベルの IPV6_V6ONLY ソケットオプション設定もできません。
#

#		// #
In general, an IP wildcard address, which is either
"0.0.0.0" or "::", means the entire IP addressing
space. For some historical reason, it is used to
specify "any available address" on some operations
of IP node.
#
一般に， "0.0.0.0" または "::" のいずれかである IP ワイルドカードアドレスは， IP アドレス空間全体を意味します。
歴史的な理由で， IP ノードのいくつかの操作で "any available address" を指定するために使用されています。
#

#		// #
When the IP node supports IPv4-mapped IPv6 address,
we allow an listener to listen to the wildcard
address of both IP addressing spaces by specifying
IPv6 wildcard address.
#
IP ノードが IPv4 にマップされた IPv6 アドレスをサポートしている場合は， IPv6 ワイルドカードアドレスを指定することによって，リスナーが両方の IP アドレス指定スペースのワイルドカードアドレスをリスンできるようにします。
#

#		// #
We accept any IPv6 address including IPv4-mapped
IPv6 address.
#
私たちは， IPv4 にマップされた IPv6 アドレスを含むあらゆる IPv6 アドレスを受け入れます。
#

[net/ipsock_posix.go]
#//	#
- A listen for a wildcard communication domain, "tcp" or
  "udp", with a wildcard address: If the platform supports
  both IPv6 and IPv4-mapped IPv6 communication capabilities,
  or does not support IPv4, we use a dual stack, AF_INET6 and
  IPV6_V6ONLY=0, wildcard address listen. The dual stack
  wildcard address listen may fall back to an IPv6-only,
  AF_INET6 and IPV6_V6ONLY=1, wildcard address listen.
  Otherwise we prefer an IPv4-only, AF_INET, wildcard address
  listen.
#
- ワイルドカードアドレスを使用したワイルドカード通信ドメイン "tcp" または "udp" のリッスン : プラットフォームが IPv6 と IPv4-mapped IPv6 の両方の通信機能をサポートする場合，または IPv4 をサポートしない場合，デュアルスタック AF_INET6 および IPV6_V6ONLY=0 ，ワイルドカードアドレスリッスン。デュアルスタックワイルドカードアドレスリッスンは， IPv6 のみ， AF_INET6 および IPV6_V6ONLY=1 ，ワイルドカードアドレスリッスンにフォールバックする場合があります。それ以外の場合は， IPv4 のみ， AF_INET ，ワイルドカードアドレスリッスンを選択します。
#

[net/ipsock_posix.go]
#//	#
- A listen for a wildcard communication domain, "tcp" or
  "udp", with an IPv4 wildcard address: same as above.
#
- ワイルドカード通信ドメイン "tcp" または "udp" をリッスンします。 IPv4 ワイルドカードアドレス : 上記と同じです。
#

[net/ipsock_posix.go]
#//	#
- A listen for a wildcard communication domain, "tcp" or
  "udp", with an IPv6 wildcard address: same as above.
#
- ワイルドカード通信ドメイン "tcp" または "udp" をリッスンします。 IPv6 ワイルドカードアドレス : 上記と同じです。
#

[net/ipsock_posix.go]
#//	#
- A listen for an IPv4 communication domain, "tcp4" or "udp4",
  with an IPv4 wildcard address: We use an IPv4-only, AF_INET,
  wildcard address listen.
#
- IPv4 ワイルドカードアドレスを使用した IPv4 通信ドメイン "tcp4" または "udp4" のリッスン :IPv4 のみ， AF_INET ，ワイルドカードアドレスリッスンを使用します。
#

[net/ipsock_posix.go]
#//	#
- A listen for an IPv6 communication domain, "tcp6" or "udp6",
  with an IPv6 wildcard address: We use an IPv6-only, AF_INET6
  and IPV6_V6ONLY=1, wildcard address listen.
#
- IPv6 ワイルドカードアドレスを使用した IPv6 通信ドメイン "tcp6" または "udp6" のリッスン :IPv6 のみ， AF_INET6 および IPV6_V6ONLY=1 ，ワイルドカードアドレスリッスンを使用します。
#

