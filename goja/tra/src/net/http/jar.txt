* 1

#// #
A CookieJar manages storage and use of cookies in HTTP requests.
#
CookieJar は， HTTP リクエストでの Cookie の保存と使用を管理します。
#

#// #
Implementations of CookieJar must be safe for concurrent use by multiple
goroutines.
#
CookieJar の実装は，複数のゴルーチンによる平行使用に対して安全でなければなりません。
#

#// #
The net/http/cookiejar package provides a CookieJar implementation.
#
net/http/cookiejar パッケージは CookieJar の実装を提供します。
#

#	// #
SetCookies handles the receipt of the cookies in a reply for the
given URL.  It may or may not choose to save the cookies, depending
on the jar's policy and implementation.
#
SetCookies は，指定された URL に対する応答で Cookie の受信を処理します。 jar のポリシーと実装に応じて， Cookie を保存する場合と保存しない場合があります。
#

#	// #
Cookies returns the cookies to send in a request for the given URL.
It is up to the implementation to honor the standard cookie use
restrictions such as in RFC 6265.
#
Cookies は，指定された URL に対するリクエストを送信するためにクッキーを返します。 RFC 6265 のような標準的なクッキーの使用制限を守るのは実装次第です。
#

