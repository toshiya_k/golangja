* 1

#// #
HTTP client. See RFC 7230 through 7235.
#
HTTP クライアント。 RFC 7230 から 7235 を参照してください。
#

#// #
This is the high-level Client interface.
The low-level implementation is in transport.go.
#
これは高レベルの Client インターフェースです。低レベルの実装は transport.go にあります。
#

#// #
A Client is an HTTP client. Its zero value (DefaultClient) is a
usable client that uses DefaultTransport.
#
Client は HTTP クライアントです。ゼロ値 (DefaultClient) は， DefaultTransport を使うクライアントで，使用可能です。
#

#// #
The Client's Transport typically has internal state (cached TCP
connections), so Clients should be reused instead of created as
needed. Clients are safe for concurrent use by multiple goroutines.
#
Client の Transport は通常，内部状態 (キャッシュされた TCP 接続) を持っているので，Client は必要に応じて作成されるのではなく再利用されるべきです。Client は複数のゴルーチンによる平行使用に対して安全です。
#

#// #
A Client is higher-level than a RoundTripper (such as Transport)
and additionally handles HTTP details such as cookies and
redirects.
#
Client は RoundTripper (Transport など) よりも上位レベルであり，さらに Cookie やリダイレクトなどの HTTP の詳細を処理します。
#

#// #
When following redirects, the Client will forward all headers set on the
initial Request except:
#
リダイレクトに従うと，Client は最初のリクエストで設定されたすべてのヘッダを転送します。
#

#// #
• when forwarding sensitive headers like "Authorization",
"WWW-Authenticate", and "Cookie" to untrusted targets.
These headers will be ignored when following a redirect to a domain
that is not a subdomain match or exact match of the initial domain.
For example, a redirect from "foo.com" to either "foo.com" or "sub.foo.com"
will forward the sensitive headers, but a redirect to "bar.com" will not.
#
• "Authorization", "WWW-Authenticate", "Cookie" などの機密性の高いヘッダーを信頼できないターゲットに転送する場合。これらのヘッダーは，サブドメインが一致しないあるいはイニシャルドメインが完全一致ではないドメインへのリダイレクトをたどるときに無視されます。たとえば， "foo.com" から "foo.com" または "sub.foo.com" へのリダイレクトは機密ヘッダーを転送しますが， "bar.com" へのリダイレクトは転送しません。
#

#// #
• when forwarding the "Cookie" header with a non-nil cookie Jar.
Since each redirect may mutate the state of the cookie jar,
a redirect may possibly alter a cookie set in the initial request.
When forwarding the "Cookie" header, any mutated cookies will be omitted,
with the expectation that the Jar will insert those mutated cookies
with the updated values (assuming the origin matches).
If Jar is nil, the initial cookies are forwarded without change.
#
• "Cookie" ヘッダーを nil 以外の cookie Jar で転送するとき。
各リダイレクトは cookie jar の状態を変化させる可能性があるので，
リダイレクトは最初のリクエストで設定された cookie を変更する可能性があります。
"Cookie" ヘッダを転送するとき，すべての変換されたクッキーは省略されます。
オリジンが一致すると仮定して，
Jar は変換されたクッキーを更新された値で挿入することを期待します。
Jar が nil の場合，最初のクッキーは変更されずに転送されます。
#

#	// #
Transport specifies the mechanism by which individual
HTTP requests are made.
If nil, DefaultTransport is used.
#
Transport は，個々の HTTP リクエストが行われるメカニズムを指定します。 nil の場合， DefaultTransport が使用されます。
#

#	// #
CheckRedirect specifies the policy for handling redirects.
If CheckRedirect is not nil, the client calls it before
following an HTTP redirect. The arguments req and via are
the upcoming request and the requests made already, oldest
first. If CheckRedirect returns an error, the Client's Get
method returns both the previous Response (with its Body
closed) and CheckRedirect's error (wrapped in a url.Error)
instead of issuing the Request req.
As a special case, if CheckRedirect returns ErrUseLastResponse,
then the most recent response is returned with its body
unclosed, along with a nil error.
#
CheckRedirect はリダイレクトを処理するためのポリシーを指定します。 CheckRedirect が nil 以外の場合，クライアントは HTTP リダイレクトをたどる前にそれを呼び出します。引数 req と via は，次のリクエストとすでに行われたリクエストのうち，最も古いものから順に並べられます。 CheckRedirect がエラーを返す場合， Client の Get メソッドは， Request req を発行する代わりに，前の Response(Body を閉じた状態) と CheckRedirect のエラー (url.Error にラップされた状態) の両方を返します。特別な場合として， CheckRedirect が ErrUseLastResponse を返した場合，最新の応答は本体を閉じずに nil エラーと共に返されます。
#

#	// #
If CheckRedirect is nil, the Client uses its default policy,
which is to stop after 10 consecutive requests.
#
CheckRedirect が nil の場合，クライアントはデフォルトのポリシーを使用します。これは 10 回の連続したリクエストの後に停止することです。
#

#	// #
Jar specifies the cookie jar.
#
Jar はクッキージャーを指定します。
#

#	// #
The Jar is used to insert relevant cookies into every
outbound Request and is updated with the cookie values
of every inbound Response. The Jar is consulted for every
redirect that the Client follows.
#
Jar はすべてのアウトバウンドリクエストに関連するクッキーを挿入するために使用され，すべてのインバウンドレスポンスのクッキー値で更新されます。 Jar は，クライアントが従うすべてのリダイレクトについて調べられます。
#

#	// #
If Jar is nil, cookies are only sent if they are explicitly
set on the Request.
#
Jar が nil の場合， Cookie はリクエストに明示的に設定されている場合にのみ送信されます。
#

#	// #
Timeout specifies a time limit for requests made by this
Client. The timeout includes connection time, any
redirects, and reading the response body. The timer remains
running after Get, Head, Post, or Do return and will
interrupt reading of the Response.Body.
#
Timeout は，このクライアントによるリクエストの制限時間を指定します。タイムアウトには，接続時間，リダイレクト，および応答ボディの読み取りが含まれます。 Get, Head, Post ，または Do が戻った後もタイマーは動作し続け， Response.Body の読み取りを中断します。
#

#	// #
A Timeout of zero means no timeout.
#
タイムアウトがゼロの場合は，タイムアウトしないことを意味します。
#

#	// #
The Client cancels requests to the underlying Transport
as if the Request's Context ended.
#
Client は，リクエストのコンテキストが終了したかのように，内部のトランスポートへのリクエストを取り消します。
#

#	// #
For compatibility, the Client will also use the deprecated
CancelRequest method on Transport if found. New
RoundTripper implementations should use the Request's Context
for cancellation instead of implementing CancelRequest.
#
互換性を保つために，クライアントはトランスポートで廃止された CancelRequest メソッドが見つかった場合も使用します。新しい RoundTripper の実装では， CancelRequest を実装する代わりに，リクエストのコンテキストをキャンセルに使用する必要があります。
#

#// #
DefaultClient is the default Client and is used by Get, Head, and Post.
#
DefaultClient はデフォルトの Client で， Get, Head, Post で使用されます。
#

#// #
RoundTripper is an interface representing the ability to execute a
single HTTP transaction, obtaining the Response for a given Request.
#
RoundTripper は，1 つの HTTP トランザクションを実行し，特定のリクエストに対するレスポンスを取得する機能を表すインターフェースです。
#

#// #
A RoundTripper must be safe for concurrent use by multiple
goroutines.
#
RoundTripper は，複数のゴルーチンによる平行使用に対して安全でなければなりません。
#

#	// #
RoundTrip executes a single HTTP transaction, returning
a Response for the provided Request.
#
RoundTrip は 1 つの HTTP トランザクションを実行し，渡されたリクエストに対する応答を返します。
#

#	// #
RoundTrip should not attempt to interpret the response. In
particular, RoundTrip must return err == nil if it obtained
a response, regardless of the response's HTTP status code.
A non-nil err should be reserved for failure to obtain a
response. Similarly, RoundTrip should not attempt to
handle higher-level protocol details such as redirects,
authentication, or cookies.
#
RoundTrip は応答を解釈しようとしてはいけません。特に， RoundTrip は，レスポンスの HTTP ステータスコードに関係なく，レスポンスを取得した場合は err == nil を返さなければなりません。応答を得られなかった場合に， nil 以外の err を使います。同様に， RoundTrip はリダイレクト，認証，または Cookie などの上位レベルのプロトコル詳細を処理しようとしてはいけません。
#

#	// #
RoundTrip should not modify the request, except for
consuming and closing the Request's Body. RoundTrip may
read fields of the request in a separate goroutine. Callers
should not mutate or reuse the request until the Response's
Body has been closed.
#
RoundTrip は，リクエストのボディを消費して閉じる以外は，リクエストを変更してはいけません。 RoundTrip は別のゴルーチンでリクエストのフィールドを読むかもしれません。呼び出し側は，応答の本体が閉じられるまで，リクエストを変更したり再利用したりしないでください。
#

#	// #
RoundTrip must always close the body, including on errors,
but depending on the implementation may do so in a separate
goroutine even after RoundTrip returns. This means that
callers wanting to reuse the body for subsequent requests
must arrange to wait for the Close call before doing so.
#
RoundTrip はエラーを含めて常に本体を閉じる必要がありますが，実装によっては RoundTrip が戻った後でも別の goroutine で閉じることがあります。これは，後続のリクエストのために本体を再利用することを望んでいる呼び出し側が，そうする前に Close 呼び出しを待つように手配しなければならないことを意味します。
#

#	// #
The Request's URL and Header fields must be initialized.
#
リクエストの URL とヘッダーフィールドは初期化されなければなりません。
#

#// #
refererForURL returns a referer without any authentication info or
an empty string if lastReq scheme is https and newReq scheme is http.
#
refererForURL は，認証情報なしで，または lastReq スキームが https で newReq スキームが http の場合は空の文字列なしでリファラーを返します。
#

#	// #
https://tools.ietf.org/html/rfc7231#section-5.5.2
  "Clients SHOULD NOT include a Referer header field in a
   (non-secure) HTTP request if the referring page was
   transferred with a secure protocol."
#
"https://tools.ietf.org/html/rfc7231#section-5.5.2"
  "参照元ページが安全なプロトコルで転送された場合，
  クライアントは (安全ではない) HTTP リクエストに Referer
  ヘッダフィールドを含めるべきではありません"
#

#		// #
This is not very efficient, but is the best we can
do without:
- introducing a new method on URL
- creating a race condition
- copying the URL struct manually, which would cause
  maintenance problems down the line
#
これはあまり効率的ではありませんが，以下を避ける最善の方法です。
- URL に新しいメソッドを導入する
- 競合状態を作成する
- URL 構造体を手動でコピーする，メンテナンスの問題が発生する
#

#// #
didTimeout is non-nil only if err != nil.
#
didTimeout は， err != nil の場合に限り， nil 以外になります。
#

#// #
send issues an HTTP request.
Caller should close resp.Body when done reading from it.
#
send は HTTP リクエストを発行します。呼び出し元は，読み取りが終わったら resp.Body を閉じる必要があります。
#

#	req := ireq // #
req is either the original request, or a modified fork
#
req は元のリクエストか，修正された fork のどちらかです。
#

#	// #
forkReq forks req into a shallow clone of ireq the first
time it's called.
#
forkReq は，最初に呼び出されたときに， reire の浅いクローンに req をフォークします。
#

#			*req = *ireq // #
shallow clone
#
浅いクローン
#

#	// #
Most the callers of send (Get, Post, et al) don't need
Headers, leaving it uninitialized. We guarantee to the
Transport that this has been initialized, though.
#
send (Get, Post など) の呼び出し元のほとんどは，ヘッダーを必要としないため，初期化されていません。ただし，これが初期化されたことをトランスポートに保証します。
#

#			// #
If we get a bad TLS record header, check to see if the
response looks like HTTP and give a more helpful error.
See golang.org/issue/11111.
#
不正な TLS レコードヘッダを受け取った場合は，応答が HTTP のように見えるかどうかを確認し，より有用なエラーを出します。 golang.org/issue/11111 を参照してください。
#

#// #
setRequestCancel sets the Cancel field of req, if deadline is
non-zero. The RoundTripper's type is used to determine whether the legacy
CancelRequest behavior should be used.
#
期限がゼロ以外の場合， setRequestCancel は req の Cancel フィールドを設定します。 RoundTripper の型は，従来の CancelRequest 動作を使用する必要があるかどうかを決定するために使用されます。
#

#// #
As background, there are three ways to cancel a request:
First was Transport.CancelRequest. (deprecated)
Second was Request.Cancel (this mechanism).
Third was Request.Context.
#
背景として，リクエストをキャンセルする方法は 3 つあります。
1 番目は Transport.CancelRequest (非推奨)。
2 番目は Request.Cancel (このメカニズム)。
3 番目は Request.Context。
#

#	initialReqCancel := req.Cancel // #
the user's original Request.Cancel, if any
#
ユーザーの元の Request.Cancel(存在する場合)
#

#		// #
The newer way (the second way in the func comment):
#
新しい方法 (func コメントの 2 番目の方法):
#

#		// #
The legacy compatibility way, used only
for RoundTripper implementations written
before Go 1.5 or Go 1.6.
#
Go 1.5 または Go 1.6 より前に作成された RoundTripper の実装にのみ使用される，従来の互換性の方法。
#

#			// #
Do nothing. The net/http package's transports
support the new Request.Cancel channel
#
何もしない。 net/http パッケージのトランスポートは新しい Request.Cancel チャンネルをサポートします
#

#// #
See 2 (end of page 4) https://www.ietf.org/rfc/rfc2617.txt
"To receive authorization, the client sends the userid and password,
separated by a single colon (":") character, within a base64
encoded string in the credentials."
It is not meant to be urlencoded.
#
https://www.ietf.org/rfc/rfc2617.txt の 2 (4 ページの終わり) を参照してください。
"オーソリゼーションを受けるには，クライアントは，
ユーザー ID とパスワードを 1 つのコロン (":") 文字で区切って，
base64 でエンコードされた文字列を認証情報に含めます。"
これは，url エンコードされることを意味しません。
#

#// #
Get issues a GET to the specified URL. If the response is one of
the following redirect codes, Get follows the redirect, up to a
maximum of 10 redirects:
#
Get は指定された URL に GET を発行します。
レスポンスが次のいずれかのリダイレクトコードである場合，
Get は最大 10 のリダイレクトまでリダイレクトに従います。
#

#// #
   301 (Moved Permanently)
   302 (Found)
   303 (See Other)
   307 (Temporary Redirect)
   308 (Permanent Redirect)
#
   301 (永続的に移動)
   302 (見つかった)
   303 (その他を参照)
   307 (一時的なリダイレクト)
   308 (永続的なリダイレクト)
#

#// #
An error is returned if there were too many redirects or if there
was an HTTP protocol error. A non-2xx response doesn't cause an
error. Any returned error will be of type *url.Error. The url.Error
value's Timeout method will report true if request timed out or was
canceled.
#
リダイレクトが多すぎる場合，または HTTP プロトコルエラーが発生した場合は，エラーが返されます。
2xx 以外のレスポンスではエラーは発生しません。
返されたエラーはすべて *url.Error 型になります。
リクエストがタイムアウトまたはキャンセルされた場合， url.Error 値の Timeout メソッドは true を返します。
#

#// #
When err is nil, resp always contains a non-nil resp.Body.
Caller should close resp.Body when done reading from it.
#
err が nil の場合， resp は常に nil 以外の resp.Body を含みます。呼び出し元は，読み取りが終わったら resp.Body を閉じる必要があります。
#

#// #
Get is a wrapper around DefaultClient.Get.
#
Get は DefaultClient.Get のラッパーです。
#

#// #
To make a request with custom headers, use NewRequest and
DefaultClient.Do.
#
カスタムヘッダーを使用してリクエストを行うには， NewRequest と DefaultClient.Do を使用します。
#

#// #
Get issues a GET to the specified URL. If the response is one of the
following redirect codes, Get follows the redirect after calling the
Client's CheckRedirect function:
#
Get は指定された URL に GET を発行します。レスポンスが次のいずれかのリダイレクトコードである場合， Get は Client の CheckRedirect 関数を呼び出した後にリダイレクトに従います。
#

#// #
An error is returned if the Client's CheckRedirect function fails
or if there was an HTTP protocol error. A non-2xx response doesn't
cause an error. Any returned error will be of type *url.Error. The
url.Error value's Timeout method will report true if request timed
out or was canceled.
#
Client の CheckRedirect 関数が失敗した場合，または HTTP プロトコルエラーが発生した場合はエラーが返されます。 2xx 以外のレスポンスではエラーは発生しません。返されたエラーはすべて *url.Error 型になります。リクエストがタイムアウトまたはキャンセルされた場合， url.Error 値の Timeout メソッドは true を報告します。
#

#// #
To make a request with custom headers, use NewRequest and Client.Do.
#
カスタムヘッダでリクエストをするには， NewRequest と Client.Do を使います。
#

#// #
ErrUseLastResponse can be returned by Client.CheckRedirect hooks to
control how redirects are processed. If returned, the next request
is not sent and the most recent response is returned with its body
unclosed.
#
ErrUseLastResponse は Client.CheckRedirect フックによって返され，リダイレクトの処理方法を制御します。返された場合，次のリクエストは送信されず，最新のレスポンスがボディを閉じずに返されます。
#

#// #
checkRedirect calls either the user's configured CheckRedirect
function, or the default.
#
checkRedirect は，ユーザーが設定した CheckRedirect 関数またはデフォルトのいずれかを呼び出します。
#

#// #
redirectBehavior describes what should happen when the
client encounters a 3xx status code from the server
#
redirectBehavior はクライアントがサーバーから 3xx ステータスコードに遭遇したときに何が起こるべきかを記述する
#

#		// #
RFC 2616 allowed automatic redirection only with GET and
HEAD requests. RFC 7231 lifts this restriction, but we still
restrict other methods to GET to maintain compatibility.
See Issue 18570.
#
RFC 2616 では， GET リクエストと HEAD リクエストでのみ自動リダイレクトが許可されていました。 RFC 7231 ではこの制限は解除されていますが，互換性を維持するために，他のメソッドを GET に制限しています。
Issue 18570 を参照してください。
#

#		// #
Treat 307 and 308 specially, since they're new in
Go 1.8, and they also require re-sending the request body.
#
これらは Go 1.8 で新しく追加されたものであり，リクエストボディを再送信する必要があるため， 307 と 308 を特別に扱います。
#

#			// #
308s have been observed in the wild being served
without Location headers. Since Go 1.7 and earlier
didn't follow these codes, just stop here instead
of returning an error.
See Issue 17773.
#
Location ヘッダなしで配信されている 308s が，実際には取得されています。 Go 1.7 以前はこれらのコードに従わなかったので，エラーを返す代わりにここで止めるだけです。 Issue 17773 を参照してください。
#

#			// #
We had a request body, and 307/308 require
re-sending it, but GetBody is not defined. So just
return this response to the user instead of an
error, like we did in Go 1.7 and earlier.
#
リクエストボディがあり， 307/308 ではそれを再送信する必要がありますが， GetBody は定義されていません。 Go 1.7 以前で行ったように，エラーではなくこのレスポンスをユーザーに返すだけです。
#

#// #
urlErrorOp returns the (*url.Error).Op value to use for the
provided (*Request).Method value.
#
urlErrorOp は，指定された (*Request).Method 値に使用する (*url.Error).Op 値を返します。
#

#// #
Do sends an HTTP request and returns an HTTP response, following
policy (such as redirects, cookies, auth) as configured on the
client.
#
Do は，クライアントで設定されているポリシー (リダイレクト， Cookie ，認証など) に従って， HTTP リクエストを送信し， HTTP レスポンスを返します。
#

#// #
An error is returned if caused by client policy (such as
CheckRedirect), or failure to speak HTTP (such as a network
connectivity problem). A non-2xx status code doesn't cause an
error.
#
クライアントポリシー (CheckRedirect など) ，または HTTP の通信に失敗した場合 (ネットワーク接続の問題など) が原因でエラーが返されます。 2xx 以外のステータスコードではエラーは発生しません。
#

#// #
If the returned error is nil, the Response will contain a non-nil
Body which the user is expected to close. If the Body is not both
read to EOF and closed, the Client's underlying RoundTripper
(typically Transport) may not be able to re-use a persistent TCP
connection to the server for a subsequent "keep-alive" request.
#
返されたエラーが nil である場合， Response はユーザが閉じることが期待されている nil 以外の Body を含みます。 Body が EOF まで読み込まれずかつ閉じられない場合，Client 内部の RoundTripper (通常は Transport) は，その後の "キープアライブ" 要求のためにサーバーへの持続的 TCP 接続を再利用できない可能性があります。
#

#// #
The request Body, if non-nil, will be closed by the underlying
Transport, even on errors.
#
リクエストボディが nil 以外の場合，エラーが発生した場合でも，内部の Transport によって閉じられます。
#

#// #
On error, any Response can be ignored. A non-nil Response with a
non-nil error only occurs when CheckRedirect fails, and even then
the returned Response.Body is already closed.
#
エラーが発生した場合，レスポンスは無視されます。 nil 以外のエラーを含む nil 以外の Response は， CheckRedirect が失敗した場合にのみ発生し，Response.Body はすでに閉じられています。
#

#// #
Generally Get, Post, or PostForm will be used instead of Do.
#
通常は， Do のかわりに Get, Post，または PostForm が使用されます。
#

#// #
If the server replies with a redirect, the Client first uses the
CheckRedirect function to determine whether the redirect should be
followed. If permitted, a 301, 302, or 303 redirect causes
subsequent requests to use HTTP method GET
(or HEAD if the original request was HEAD), with no body.
A 307 or 308 redirect preserves the original HTTP method and body,
provided that the Request.GetBody function is defined.
The NewRequest function automatically sets GetBody for common
standard library body types.
#
サーバーがリダイレクトで応答した場合， Client は最初に CheckRedirect 関数を使用してリダイレクトに従うべきかどうかを判断します。許可されている場合， 301, 302 ，または 303 のリダイレクトにより，後続のリクエストは HTTP メソッド GET (または元のリクエストが HEAD の場合は HEAD) をボディなしで使用します。 307 または 308 リダイレクトは， Request.GetBody 関数が定義されていれば，元の HTTP メソッドとボディを保持します。 NewRequest 関数は，一般的な標準ライブラリのボディタイプに GetBody を自動的に設定します。
#

#// #
Any returned error will be of type *url.Error. The url.Error
value's Timeout method will report true if request timed out or was
canceled.
#
返されたエラーはすべて *url.Error 型になります。リクエストがタイムアウトまたはキャンセルされた場合， url.Error 値の Timeout メソッドは true を報告します。
#

#		reqBodyClosed = false // #
have we closed the current req.Body?
#
現在の req.Body を閉じましたか ?
#

#		// #
Redirect behavior:
#
リダイレクト動作
#

#		// #
the body may have been closed already by c.send()
#
本体は c.send() によって既に閉じられている可能性があります。
#

#		// #
For all but the first request, create the next
request hop and replace req.
#
最初のリクエストを除くすべてについて，次のリクエストホップを作成して req を置き換えます。
#

#				// #
If the caller specified a custom Host header and the
redirect location is relative, preserve the Host header
through the redirect. See issue #22233.
#
呼び出し元がカスタムの Host ヘッダーを指定し，リダイレクトの場所が相対的な場合は，リダイレクトを通じて Host ヘッダーを保持します。 issue #22233 を参照してください。
#

#			// #
Copy original headers before setting the Referer,
in case the user set Referer on their first request.
If they really want to override, they can do it in
their CheckRedirect func.
#
ユーザーが最初のリクエストで Referer を設定した場合に備えて， Referer を設定する前に元のヘッダーをコピーしてください。本当にオーバーライドしたい場合は， CheckRedirect 関数でそれを実行できます。
#

#			// #
Add the Referer header from the most recent
request URL to the new one, if it's not https->http:
#
https->http でない場合は，最新のリクエスト URL から新しい URL に Referer ヘッダを追加します。
#

#			// #
Sentinel error to let users select the
previous response, without closing its
body. See Issue 10069.
#
ボディを閉じずに前の応答をユーザーに選択させる Sentinel エラー。 Issue 10069 を参照してください。
#

#			// #
Close the previous response's body. But
read at least some of the body so if it's
small the underlying TCP connection will be
re-used. No need to check for errors: if it
fails, the Transport won't reuse it anyway.
#
前の応答のボディを閉じます。しかし，少なくともボディの一部を読んで，小さければ内部の TCP 接続が再利用されるでしょう。エラーをチェックする必要はありません。失敗した場合，トランスポートはそれを再利用しません。
#

#				// #
Special case for Go 1 compatibility: return both the response
and an error if the CheckRedirect function failed.
See https://golang.org/issue/3795
The resp.Body has already been closed.
#
Go 1 互換性のための特別なケース :CheckRedirect 関数が失敗した場合，応答とエラーの両方を返します。
https://golang.org/issue/3795 を参照してください。
resp.Body はすでに閉じられています。
#

#			// #
c.send() always closes req.Body
#
c.send() は常に req.Body を閉じます
#

#					// #
TODO: early in cycle: s/Client.Timeout exceeded/timeout or context cancellation/
#
TODO: サイクルの初期: s/Client.Timeout を超えました/タイムアウトまたはコンテキストキャンセル/
#

#// #
makeHeadersCopier makes a function that copies headers from the
initial Request, ireq. For every redirect, this function must be called
so that it can copy headers into the upcoming Request.
#
makeHeadersCopier は，最初の Request からヘッダをコピーする関数， ireq を作成します。リダイレクトごとに，この関数を呼び出して，今後の Request にヘッダーをコピーできるようにする必要があります。
#

#	// #
The headers to copy are from the very initial request.
We use a closured callback to keep a reference to these original headers.
#
コピーするヘッダーは，最初のリクエストからのものです。これらの元のヘッダーへの参照を保持するために，クローズドコールバックを使用します。
#

#	preq := ireq // #
The previous request
#
前回のリクエスト
#

#		// #
If Jar is present and there was some initial cookies provided
via the request header, then we may need to alter the initial
cookies as we follow redirects since each redirect may end up
modifying a pre-existing cookie.
#
Jar が存在し，リクエストヘッダを介して渡された初期クッキーがある場合，各リダイレクトは既存のクッキーを変更することになる可能性があるため，リダイレクトに従って初期クッキーを変更する必要があるかもしれません。
#

#		// #
Since cookies already set in the request header do not contain
information about the original domain and path, the logic below
assumes any new set cookies override the original cookie
regardless of domain or path.
#
リクエストヘッダーにすでに設定されている Cookie には元のドメインとパスに関する情報が含まれていないため，以下のロジックでは，ドメインまたはパスに関係なく，新しい設定 Cookie が元の Cookie を上書きすると仮定します。
#

#		// #
See https://golang.org/issue/17494
#
https://golang.org/issue/17494 を参照してください。
#

#			resp := req.Response // #
The response that caused the upcoming redirect
#
今後のリダイレクトを引き起こした応答
#

#				sort.Strings(ss) // #
Ensure deterministic headers
#
確定的ヘッダを確保する
#

#		// #
Copy the initial request's Header values
(at least the safe ones).
#
初期リクエストのヘッダ値 (少なくとも安全なもの) をコピーします。
#

#		preq = req // #
Update previous Request with the current request
#
前のリクエストを現在のリクエストで更新する
#

#// #
Post issues a POST to the specified URL.
#
Post は指定された URL に POST を発行します。
#

#// #
Caller should close resp.Body when done reading from it.
#
呼び出し元は，読み取りが終わったら resp.Body を閉じる必要があります。
#

#// #
If the provided body is an io.Closer, it is closed after the
request.
#
渡されたボディが io.Closer の場合は，リクエスト後に閉じられます。
#

#// #
Post is a wrapper around DefaultClient.Post.
#
Post は DefaultClient.Post のラッパーです。
#

#// #
To set custom headers, use NewRequest and DefaultClient.Do.
#
カスタムヘッダーを設定するには， NewRequest と DefaultClient.Do を使用します。
#

#// #
See the Client.Do method documentation for details on how redirects
are handled.
#
リダイレクトの処理方法の詳細については， Client.Do メソッドのドキュメントを参照してください。
#

#// #
To set custom headers, use NewRequest and Client.Do.
#
カスタムヘッダーを設定するには， NewRequest と Client.Do を使用します。
#

#// #
PostForm issues a POST to the specified URL, with data's keys and
values URL-encoded as the request body.
#
PostForm は，URL エンコードされたデータのキーと値をリクエストボディとして，指定された URL に POST を発行します。
#

#// #
The Content-Type header is set to application/x-www-form-urlencoded.
To set other headers, use NewRequest and DefaultClient.Do.
#
Content-Type ヘッダーは application/x-www-form-urlencoded に設定されています。他のヘッダを設定するには， NewRequest と DefaultClient.Do を使います。
#

#// #
PostForm is a wrapper around DefaultClient.PostForm.
#
PostForm は DefaultClient.PostForm のラッパーです。
#

#// #
PostForm issues a POST to the specified URL,
with data's keys and values URL-encoded as the request body.
#
PostForm は， URL エンコードされたデータのキーと値をリクエストボディとして，指定された URL に POST を発行します。
#

#// #
The Content-Type header is set to application/x-www-form-urlencoded.
To set other headers, use NewRequest and Client.Do.
#
Content-Type ヘッダーは application/x-www-form-urlencoded に設定されています。他のヘッダを設定するには， NewRequest と Client.Do を使います。
#

#// #
Head issues a HEAD to the specified URL. If the response is one of
the following redirect codes, Head follows the redirect, up to a
maximum of 10 redirects:
#
Head は指定された URL に HEAD を発行します。レスポンスが以下のいずれかのリダイレクトコードである場合， Head は最大 10 のリダイレクトまでリダイレクトに従います。
#

#// #
Head is a wrapper around DefaultClient.Head
#
Head は DefaultClient.Head のラッパーです。
#

#// #
Head issues a HEAD to the specified URL. If the response is one of the
following redirect codes, Head follows the redirect after calling the
Client's CheckRedirect function:
#
Head は指定された URL に HEAD を発行します。レスポンスが以下のいずれかのリダイレクトコードである場合， Head は Client の CheckRedirect 関数を呼び出した後にリダイレクトに従います。
#

#// #
CloseIdleConnections closes any connections on its Transport which
were previously connected from previous requests but are now
sitting idle in a "keep-alive" state. It does not interrupt any
connections currently in use.
#
CloseIdleConnections は，以前のリクエストで以前接続され，現在は "キープアライブ" 状態でアイドル状態になっている， Transport 上の接続をすべて閉じます。現在使用中の接続を中断することはありません。
#

#// #
If the Client's Transport does not have a CloseIdleConnections method
then this method does nothing.
#
Client の Transport に CloseIdleConnections メソッドがない場合，このメソッドは何もしません。
#

#// #
cancelTimerBody is an io.ReadCloser that wraps rc with two features:
1) on Read error or close, the stop func is called.
2) On Read failure, if reqDidTimeout is true, the error is wrapped and
   marked as net.Error that hit its timeout.
#
cancelTimerBody は， rc を 2 つの機能でラップする io.ReadCloser です。
1) 読み取りエラーまたはクローズ時に，停止機能が呼び出されます。
2) 読み取​​り失敗時に reqDidTimeout が true の場合，エラーはラップされ，タイムアウトに達した net.Error としてマークされます。
#

#	stop          func() // #
stops the time.Timer waiting to cancel the request
#
time.Timer はリクエストをキャンセルするのを待ちます
#

#			// #
TODO: early in cycle: s/Client.Timeout exceeded/timeout or context cancellation/
#
TODO: サイクルの初期 : s/Client.Timeout を超えました/タイムアウトまたはコンテキストキャンセル/
#

#		// #
Permit sending auth/cookie headers from "foo.com"
to "sub.foo.com".
#
"foo.com" から "sub.foo.com" への auth/cookie ヘッダの送信を許可します。
#

#		// #
Note that we don't send all cookies to subdomains
automatically. This function is only used for
Cookies set explicitly on the initial outgoing
client request. Cookies automatically added via the
CookieJar mechanism continue to follow each
cookie's scope as set by Set-Cookie. But for
outgoing requests with the Cookie header set
directly, we don't know their scope, so we assume
it's for *.domain.com.
#
すべてのクッキーをサブドメインに自動的に送信するわけではありません。この機能は，最初の発信クライアントリクエストで明示的に設定された Cookie に対してのみ使用されます。 CookieJar メカニズムを介して自動的に追加された Cookie は， Set-Cookie によって設定された各 Cookie の有効範囲に従います。しかし， Cookie ヘッダーを直接設定した送信リクエストの場合，その範囲がわからないため， *.domain.com 用であると想定します。
#

#	// #
All other headers are copied:
#
他のすべてのヘッダーはコピーされます。
#

#// #
isDomainOrSubdomain reports whether sub is a subdomain (or exact
match) of the parent domain.
#
isDomainOrSubdomain は， sub が親ドメインのサブドメイン (または完全一致) かどうかを報告します。
#

#// #
Both domains must already be in canonical form.
#
両方のドメインはすでに標準形式になっている必要があります。
#

#	// #
If sub is "foo.example.com" and parent is "example.com",
that means sub must end in "."+parent.
Do it without allocating.
#
sub が "foo.example.com" で parent が "example.com" の場合， sub は "."+parent で終わらなければならないことを意味します。
割り当てずに実行してください。
#

