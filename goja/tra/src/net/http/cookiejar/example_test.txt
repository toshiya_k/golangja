* 1

#	// #
Start a server to give us cookies.
#
サーバーを起動してクッキーを入手します。
#

#	// #
All users of cookiejar should import "golang.org/x/net/publicsuffix"
#
cookiejar のすべてのユーザーは "golang.org/x/net/publicsuffix" をインポートするべきです
#

[net/http/cookiejar/example_test.go]
#	// #
Output:
After 1st request:
  Flavor: Chocolate Chip
After 2nd request:
  Flavor: Oatmeal Raisin
#
Output:
After 1st request:
  Flavor: Chocolate Chip
After 2nd request:
  Flavor: Oatmeal Raisin
#

