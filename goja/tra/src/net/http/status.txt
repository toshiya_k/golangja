* 1

#// #
HTTP status codes as registered with IANA.
See: https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
#
IANA に登録されている HTTP ステータスコード。
https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml を参照してください。
#

#// #
StatusText returns a text for the HTTP status code. It returns the empty
string if the code is unknown.
#
StatusText は， HTTP ステータスコードのテキストを返します。
コードが不明な場合は空の文字列を返します。
#

[net/http/status.go]
#	StatusNotModified       = 304 // #
RFC 7232, 4.1
#
RFC 7232, 4.1
#

[net/http/status.go]
#	StatusUseProxy          = 305 // #
RFC 7231, 6.4.5
#
RFC 7231, 6.4.5
#

[net/http/status.go]
#	_                       = 306 // #
RFC 7231, 6.4.6 (Unused)
#
RFC 7231, 6.4.6 (Unused)
#

[net/http/status.go]
#	StatusTemporaryRedirect = 307 // #
RFC 7231, 6.4.7
#
RFC 7231, 6.4.7
#

[net/http/status.go]
#	StatusPermanentRedirect = 308 // #
RFC 7538, 3
#
RFC 7538, 3
#

[net/http/status.go]
#	StatusBadRequest                   = 400 // #
RFC 7231, 6.5.1
#
RFC 7231, 6.5.1
#

[net/http/status.go]
#	StatusUnauthorized                 = 401 // #
RFC 7235, 3.1
#
RFC 7235, 3.1
#

[net/http/status.go]
#	StatusPaymentRequired              = 402 // #
RFC 7231, 6.5.2
#
RFC 7231, 6.5.2
#

[net/http/status.go]
#	StatusForbidden                    = 403 // #
RFC 7231, 6.5.3
#
RFC 7231, 6.5.3
#

[net/http/status.go]
#	StatusNotFound                     = 404 // #
RFC 7231, 6.5.4
#
RFC 7231, 6.5.4
#

[net/http/status.go]
#	StatusMethodNotAllowed             = 405 // #
RFC 7231, 6.5.5
#
RFC 7231, 6.5.5
#

[net/http/status.go]
#	StatusNotAcceptable                = 406 // #
RFC 7231, 6.5.6
#
RFC 7231, 6.5.6
#

[net/http/status.go]
#	StatusProxyAuthRequired            = 407 // #
RFC 7235, 3.2
#
RFC 7235, 3.2
#

[net/http/status.go]
#	StatusRequestTimeout               = 408 // #
RFC 7231, 6.5.7
#
RFC 7231, 6.5.7
#

[net/http/status.go]
#	StatusConflict                     = 409 // #
RFC 7231, 6.5.8
#
RFC 7231, 6.5.8
#

[net/http/status.go]
#	StatusGone                         = 410 // #
RFC 7231, 6.5.9
#
RFC 7231, 6.5.9
#

[net/http/status.go]
#	StatusLengthRequired               = 411 // #
RFC 7231, 6.5.10
#
RFC 7231, 6.5.10
#

[net/http/status.go]
#	StatusPreconditionFailed           = 412 // #
RFC 7232, 4.2
#
RFC 7232, 4.2
#

[net/http/status.go]
#	StatusRequestEntityTooLarge        = 413 // #
RFC 7231, 6.5.11
#
RFC 7231, 6.5.11
#

[net/http/status.go]
#	StatusRequestURITooLong            = 414 // #
RFC 7231, 6.5.12
#
RFC 7231, 6.5.12
#

[net/http/status.go]
#	StatusUnsupportedMediaType         = 415 // #
RFC 7231, 6.5.13
#
RFC 7231, 6.5.13
#

[net/http/status.go]
#	StatusRequestedRangeNotSatisfiable = 416 // #
RFC 7233, 4.4
#
RFC 7233, 4.4
#

[net/http/status.go]
#	StatusExpectationFailed            = 417 // #
RFC 7231, 6.5.14
#
RFC 7231, 6.5.14
#

[net/http/status.go]
#	StatusTeapot                       = 418 // #
RFC 7168, 2.3.3
#
RFC 7168, 2.3.3
#

[net/http/status.go]
#	StatusMisdirectedRequest           = 421 // #
RFC 7540, 9.1.2
#
RFC 7540, 9.1.2
#

[net/http/status.go]
#	StatusUnprocessableEntity          = 422 // #
RFC 4918, 11.2
#
RFC 4918, 11.2
#

[net/http/status.go]
#	StatusLocked                       = 423 // #
RFC 4918, 11.3
#
RFC 4918, 11.3
#

[net/http/status.go]
#	StatusFailedDependency             = 424 // #
RFC 4918, 11.4
#
RFC 4918, 11.4
#

[net/http/status.go]
#	StatusTooEarly                     = 425 // #
RFC 8470, 5.2.
#
RFC 8470, 5.2.
#

[net/http/status.go]
#	StatusUpgradeRequired              = 426 // #
RFC 7231, 6.5.15
#
RFC 7231, 6.5.15
#

[net/http/status.go]
#	StatusPreconditionRequired         = 428 // #
RFC 6585, 3
#
RFC 6585, 3
#

[net/http/status.go]
#	StatusTooManyRequests              = 429 // #
RFC 6585, 4
#
RFC 6585, 4
#

[net/http/status.go]
#	StatusRequestHeaderFieldsTooLarge  = 431 // #
RFC 6585, 5
#
RFC 6585, 5
#

[net/http/status.go]
#	StatusUnavailableForLegalReasons   = 451 // #
RFC 7725, 3
#
RFC 7725, 3
#

[net/http/status.go]
#	StatusInternalServerError           = 500 // #
RFC 7231, 6.6.1
#
RFC 7231, 6.6.1
#

[net/http/status.go]
#	StatusNotImplemented                = 501 // #
RFC 7231, 6.6.2
#
RFC 7231, 6.6.2
#

[net/http/status.go]
#	StatusBadGateway                    = 502 // #
RFC 7231, 6.6.3
#
RFC 7231, 6.6.3
#

[net/http/status.go]
#	StatusServiceUnavailable            = 503 // #
RFC 7231, 6.6.4
#
RFC 7231, 6.6.4
#

[net/http/status.go]
#	StatusGatewayTimeout                = 504 // #
RFC 7231, 6.6.5
#
RFC 7231, 6.6.5
#

[net/http/status.go]
#	StatusHTTPVersionNotSupported       = 505 // #
RFC 7231, 6.6.6
#
RFC 7231, 6.6.6
#

[net/http/status.go]
#	StatusVariantAlsoNegotiates         = 506 // #
RFC 2295, 8.1
#
RFC 2295, 8.1
#

[net/http/status.go]
#	StatusInsufficientStorage           = 507 // #
RFC 4918, 11.5
#
RFC 4918, 11.5
#

[net/http/status.go]
#	StatusLoopDetected                  = 508 // #
RFC 5842, 7.2
#
RFC 5842, 7.2
#

[net/http/status.go]
#	StatusNotExtended                   = 510 // #
RFC 2774, 7
#
RFC 2774, 7
#

[net/http/status.go]
#	StatusNetworkAuthenticationRequired = 511 // #
RFC 6585, 6
#
RFC 6585, 6
#

[net/http/status.go]
#	StatusContinue           = 100 // #
RFC 7231, 6.2.1
#
RFC 7231, 6.2.1
#

[net/http/status.go]
#	StatusSwitchingProtocols = 101 // #
RFC 7231, 6.2.2
#
RFC 7231, 6.2.2
#

[net/http/status.go]
#	StatusProcessing         = 102 // #
RFC 2518, 10.1
#
RFC 2518, 10.1
#

[net/http/status.go]
#	StatusEarlyHints         = 103 // #
RFC 8297
#
RFC 8297
#

[net/http/status.go]
#	StatusOK                   = 200 // #
RFC 7231, 6.3.1
#
RFC 7231, 6.3.1
#

[net/http/status.go]
#	StatusCreated              = 201 // #
RFC 7231, 6.3.2
#
RFC 7231, 6.3.2
#

[net/http/status.go]
#	StatusAccepted             = 202 // #
RFC 7231, 6.3.3
#
RFC 7231, 6.3.3
#

[net/http/status.go]
#	StatusNonAuthoritativeInfo = 203 // #
RFC 7231, 6.3.4
#
RFC 7231, 6.3.4
#

[net/http/status.go]
#	StatusNoContent            = 204 // #
RFC 7231, 6.3.5
#
RFC 7231, 6.3.5
#

[net/http/status.go]
#	StatusResetContent         = 205 // #
RFC 7231, 6.3.6
#
RFC 7231, 6.3.6
#

[net/http/status.go]
#	StatusPartialContent       = 206 // #
RFC 7233, 4.1
#
RFC 7233, 4.1
#

[net/http/status.go]
#	StatusMultiStatus          = 207 // #
RFC 4918, 11.1
#
RFC 4918, 11.1
#

[net/http/status.go]
#	StatusAlreadyReported      = 208 // #
RFC 5842, 7.1
#
RFC 5842, 7.1
#

[net/http/status.go]
#	StatusIMUsed               = 226 // #
RFC 3229, 10.4.1
#
RFC 3229, 10.4.1
#

[net/http/status.go]
#	StatusMultipleChoices   = 300 // #
RFC 7231, 6.4.1
#
RFC 7231, 6.4.1
#

[net/http/status.go]
#	StatusMovedPermanently  = 301 // #
RFC 7231, 6.4.2
#
RFC 7231, 6.4.2
#

[net/http/status.go]
#	StatusFound             = 302 // #
RFC 7231, 6.4.3
#
RFC 7231, 6.4.3
#

[net/http/status.go]
#	StatusSeeOther          = 303 // #
RFC 7231, 6.4.4
#
RFC 7231, 6.4.4
#

