* 1

#	// #
The operating system doesn't clean up
the file that announcing created, so
we have to clean it up ourselves.
There's a race here--we can't know for
sure whether someone else has come along
and replaced our socket name already--
but this sequence (remove then close)
is at least compatible with the auto-remove
sequence in ListenUnix. It's only non-Go
programs that can mess us up.
Even if there are racy calls to Close, we want to unlink only for the first one.
#
オペレーティングシステムはアナウンスが作成したファイルをクリーンアップしないので，私たちは自分でそれをクリーンアップしなければなりません。
ここに競争があります - 他の誰かがすでに私たちのソケット名を取り替えてしまったかどうかを確かに知ることはできません - しかしこのシーケンス (削除して閉じる) は少なくとも ListenUnix の自動削除シーケンスと互換性があります。
それは私たちを台無しにすることができるのは Go 以外のプログラムだけです。
Close への派手な呼び出しがあったとしても，最初のリンクについてのみリンク解除したいです。
#

#// #
SetUnlinkOnClose sets whether the underlying socket file should be removed
from the file system when the listener is closed.
#
SetUnlinkOnClose は，リスナーが閉じられたときに内部のソケットファイルをファイルシステムから削除するかどうかを設定します。
#

#// #
The default behavior is to unlink the socket file only when package net created it.
That is, when the listener and the underlying socket file were created by a call to
Listen or ListenUnix, then by default closing the listener will remove the socket file.
but if the listener was created by a call to FileListener to use an already existing
socket file, then by default closing the listener will not remove the socket file.
#
デフォルトの振る舞いは，パッケージ net がそれを作成したときだけソケットファイルをリンク解除することです。
つまり，リスナーと内部のソケットファイルが Listen または ListenUnix の呼び出しによって作成された場合，デフォルトではリスナーを閉じるとソケットファイルは削除されます。
しかし，既存のソケットファイルを使用するために FileListener を呼び出してリスナーを作成した場合，デフォルトではリスナーを閉じてもソケットファイルは削除されません。
#

