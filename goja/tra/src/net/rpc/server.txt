* 1

#	#
Package rpc provides access to the exported methods of an object across a
network or other I/O connection.  A server registers an object, making it visible
as a service with the name of the type of the object.  After registration, exported
methods of the object will be accessible remotely.  A server may register multiple
objects (services) of different types but it is an error to register multiple
objects of the same type.
#
rpc パッケージは，ネットワークや他の I/O 接続を越えてオブジェクトのエクスポートメソッドへのアクセスを提供します。
サーバーはオブジェクトを登録し，そのオブジェクトの型名でサービスを公開します。
登録後，エクスポートされたオブジェクトのメソッドは外部からアクセス可能になります。
サーバーは型の異なる複数のオブジェクト (サービス)を登録できますが，
同じ型のオブジェクトを複数登録するとエラーとなります。
#

#	#
Only methods that satisfy these criteria will be made available for remote access;
other methods will be ignored:
#
これらの基準を満たすメソッドだけがリモートアクセスに使用できるようになります。
他の方法は無視されます。
#

#		#
- the method's type is exported.
- the method is exported.
- the method has two arguments, both exported (or builtin) types.
- the method's second argument is a pointer.
- the method has return type error.
#
- メソッドの種類がエクスポートされます。
- メソッドはエクスポートされます。
- メソッドは 2 つの引数を持ち，両方ともエクスポートされた (または組み込みの) 型です。
- メソッドの 2 番目の引数はポインタです。
- メソッドに戻り型エラーがあります。
#

#	#
In effect, the method must look schematically like
#
実際には，このメソッドは概略的に次のようになります
#

#	#
where T1 and T2 can be marshaled by encoding/gob.
These requirements apply even if a different codec is used.
(In the future, these requirements may soften for custom codecs.)
#
T1 と T2 は encoding/gob によって整列化できます。
これらの要件は，異なるコーデックが使用されている場合でも適用されます。
(将来，カスタムコーデックの場合にはこれらの要件は緩和されるかもしれません。)
#

#	#
The method's first argument represents the arguments provided by the caller; the
second argument represents the result parameters to be returned to the caller.
The method's return value, if non-nil, is passed back as a string that the client
sees as if created by errors.New.  If an error is returned, the reply parameter
will not be sent back to the client.
#
メソッドの最初の引数は，呼び出し元によって渡された引数を表します。
2 番目の引数は，呼び出し元に返される結果パラメータを表します。
メソッドの戻り値が nil 以外の場合は， errors.New によって作成されたものとしてクライアントに認識される文字列として返されます。
エラーが返された場合， reply パラメータはクライアントに返送されません。
#

#	#
The server may handle requests on a single connection by calling ServeConn.  More
typically it will create a network listener and call Accept or, for an HTTP
listener, HandleHTTP and http.Serve.
#
サーバーは ServeConn を呼び出すことによって 1 つの接続でリクエストを処理できます。
より一般的には，ネットワークリスナーを作成して Accept を呼び出します。
HTTP リスナーの場合は HandleHTTP および http.Serve を呼び出します。
#

#	#
A client wishing to use the service establishes a connection and then invokes
NewClient on the connection.  The convenience function Dial (DialHTTP) performs
both steps for a raw network connection (an HTTP connection).  The resulting
Client object has two methods, Call and Go, that specify the service and method to
call, a pointer containing the arguments, and a pointer to receive the result
parameters.
#
サービスを使用したいクライアントは，接続を確立してから，その接続に対して NewClient を呼び出します。
便利な関数 Dial (DialHTTP) は，生のネットワーク接続 (HTTP 接続) に対して両方のステップを実行します。
結果の Client オブジェクトには，呼び出すサービスとメソッドを指定する Call と Go という 2 つのメソッド，引数を含むポインタ，および結果パラメータを受け取るポインタがあります。
#

#	#
The Call method waits for the remote call to complete while the Go method
launches the call asynchronously and signals completion using the Call
structure's Done channel.
#
Call メソッドはリモート呼び出しが完了するのを待ち， Go メソッドは非同期に呼び出しを開始し， Call 構造体の Done チャンネルを使用して完了を通知します。
#

#	#
Unless an explicit codec is set up, package encoding/gob is used to
transport the data.
#
明示的なコーデックが設定されていない限り，データの転送にパッケージ encoding/gob が使用されます。
#

#	#
Here is a simple example.  A server wishes to export an object of type Arith:
#
これは簡単な例です。
サーバーが Arith 型のオブジェクトをエクスポートしたいとします。
#

#	#
The server calls (for HTTP service):
#
サーバー呼び出し (HTTP サービス用):
#

#	#
At this point, clients can see a service "Arith" with methods "Arith.Multiply" and
"Arith.Divide".  To invoke one, a client first dials the server:
#
この時点で，クライアントはメソッド "Arith.Multiply" と "Arith.Divide" を持つサービス "Arith" を見ることができます。
起動するには，まずクライアントがサーバーにダイヤルします。
#

#	#
Then it can make a remote call:
#
それからそれはリモートコールをかけることができます:
#

#	#
or
#
または
#

#		#
// Asynchronous call
quotient := new(Quotient)
divCall := client.Go("Arith.Divide", args, quotient, nil)
replyCall := <-divCall.Done	// will be equal to divCall
// check errors, print, etc.
#
// 非同期呼び出し
quotient := new(Quotient)
divCall := client.Go("Arith.Divide", args, quotient, nil)
replyCall := <-divCall.Done	// divCall と同じになる
// エラーチェック，表示など
#

#	#
A server implementation will often provide a simple, type-safe wrapper for the
client.
#
サーバーの実装は，多くの場合，クライアントに対して単純で型セーフなラッパーを提供します。
#

#	#
The net/rpc package is frozen and is not accepting new features.
#
net/rpc パッケージはフリーズしており，新しい機能を受け付けていません。
#

#	// #
Defaults used by HandleHTTP
#
HandleHTTP によって使用されるデフォルト
#

#// #
Precompute the reflect type for error. Can't use error directly
because Typeof takes an empty interface value. This is annoying.
#
反映型をエラー用に事前計算します。
Typeof は空のインターフェース値をとるため，直接エラーを使用することはできません。
これは迷惑です。
#

#	sync.Mutex // #
protects counters
#
カウンターを保護する
#

#	name   string                 // #
name of service
#
サービス名
#

#	rcvr   reflect.Value          // #
receiver of methods for the service
#
サービスのメソッドの受信者
#

#	typ    reflect.Type           // #
type of the receiver
#
受信者の型
#

#	method map[string]*methodType // #
registered methods
#
登録メソッド
#

#// #
Request is a header written before every RPC call. It is used internally
but documented here as an aid to debugging, such as when analyzing
network traffic.
#
リクエストはすべての RPC 呼び出しの前に書かれたヘッダです。
これは内部的に使用されますが，ネットワークトラフィックを分析する場合など，デバッグの補助としてここで文書化されています。
#

#	ServiceMethod string   // #
format: "Service.Method"
#
フォーマット : "Service.Method"
#

#	Seq           uint64   // #
sequence number chosen by client
#
クライアントが選択したシーケンス番号
#

#	next          *Request // #
for free list in Server
#
Server のフリーリスト
#

#// #
Response is a header written before every RPC return. It is used internally
but documented here as an aid to debugging, such as when analyzing
network traffic.
#
Response は， RPC が戻るたびに書き込まれるヘッダーです。
これは内部的に使用されますが，ネットワークトラフィックを分析する場合など，デバッグの補助としてここで文書化されています。
#

#	ServiceMethod string    // #
echoes that of the Request
#
リクエストのそれを反映する
#

#	Seq           uint64    // #
echoes that of the request
#
リクエストのそれを反映する
#

#	Error         string    // #
error, if any.
#
もしあれば，エラー。
#

#	next          *Response // #
for free list in Server
#
Server のフリーリスト
#

#// #
Server represents an RPC Server.
#
Server は RPC サーバーを表します。
#

#	reqLock    sync.Mutex // #
protects freeReq
#
freeReq を保護します
#

#	respLock   sync.Mutex // #
protects freeResp
#
freeResp を保護します
#

#// #
NewServer returns a new Server.
#
NewServer は新しいサーバーを返します。
#

#// #
DefaultServer is the default instance of *Server.
#
DefaultServer は *Server のデフォルトインスタンスです。
#

#// #
Is this an exported - upper case - name?
#
これはエクスポートされた大文字の名前ですか ?
#

#// #
Is this type exported or a builtin?
#
この型はエクスポートされていますか，それとも組み込みですか ?
#

#	// #
PkgPath will be non-empty even for an exported type,
so we need to check the type name as well.
#
エクスポートされた型でも PkgPath は空ではないので，型名も確認する必要があります。
#

#// #
Register publishes in the server the set of methods of the
receiver value that satisfy the following conditions:
#
Register は，次の条件を満たす受信側値の一連のメソッドをサーバーに公開します。
#

#//	#
- exported method of exported type
- two arguments, both of exported type
- the second argument is a pointer
- one return value, of type error
#
- エクスポートされた型のエクスポートされたメソッド
- 2 つの引数，両方ともエクスポートされた型
- 2 番目の引数はポインタです
- 1 つの戻り値， error 型
#

#// #
It returns an error if the receiver is not an exported type or has
no suitable methods. It also logs the error using package log.
The client accesses each method using a string of the form "Type.Method",
where Type is the receiver's concrete type.
#
レシーバがエクスポートされた型ではないか，適切なメソッドがない場合はエラーを返します。
また，パッケージログを使用してエラーを記録します。
クライアントは， "Type.Method" の形式の文字列を使用して各メソッドにアクセスします。
ここで， Type は受信側の具象型です。
#

#// #
RegisterName is like Register but uses the provided name for the type
instead of the receiver's concrete type.
#
RegisterName は Register に似ていますが，受信側の具象型の代わりに渡された型の名前を使用します。
#

#	// #
Install the methods
#
メソッドをインストールする
#

#		// #
To help the user, see if a pointer receiver would work.
#
ユーザーを手助けするために，ポインターレシーバーが機能するかどうか確認してください。
#

#// #
suitableMethods returns suitable Rpc methods of typ, it will report
error using log if reportErr is true.
#
properMethods は typ の適切な Rpc メソッドを返します。
reportErr が true の場合， log を使用してエラーを報告します。
#

#		// #
Method must be exported.
#
メソッドをエクスポートする必要があります。
#

#		// #
Method needs three ins: receiver, *args, *reply.
#
メソッドは 3 つの入力を必要とします : レシーバ, *args, *reply。
#

#		// #
First arg need not be a pointer.
#
最初の引数はポインタである必要はありません。
#

#		// #
Second arg must be a pointer.
#
2 番目の引数はポインタでなければなりません。
#

#		// #
Reply type must be exported.
#
返信型をエクスポートする必要があります。
#

#		// #
Method needs one out.
#
メソッドは 1 つの出力が必要です。
#

#		// #
The return type of the method must be error.
#
メソッドの戻り型はエラーでなければなりません。
#

#// #
A value sent as a placeholder for the server's response value when the server
receives an invalid request. It is never decoded by the client since the Response
contains an error when it is used.
#
サーバーが無効なリクエストを受信したときに，サーバーのレスポンス値のプレースホルダーとして送信された値。
Response は使用時にエラーを含むため，クライアントによってデコードされることはありません。
#

#	// #
Encode the response header
#
レスポンスヘッダをエンコードする
#

#	// #
Invoke the method, providing a new value for the reply.
#
メソッドを呼び出して，返信に新しい値を指定します。
#

#	// #
The return value for the method is an error.
#
メソッドの戻り値はエラーです。
#

#			// #
Gob couldn't encode the header. Should not happen, so if it does,
shut down the connection to signal that the connection is broken.
#
Gob はヘッダーをエンコードできませんでした。
起こらないはずなので，もしそうなら，接続が切断されたことを知らせるために接続をシャットダウンします。
#

#			// #
Was a gob problem encoding the body but the header has been written.
Shut down the connection to signal that the connection is broken.
#
ボディをエンコードする際に問題が発生しましたが，ヘッダーは書き込まれました。
接続が切断されたことを知らせるために接続をシャットダウンします。
#

#		// #
Only call c.rwc.Close once; otherwise the semantics are undefined.
#
c.rwc.Close を一度だけ呼び出します。
そうでなければ意味論は未定義である。
#

#// #
ServeConn runs the server on a single connection.
ServeConn blocks, serving the connection until the client hangs up.
The caller typically invokes ServeConn in a go statement.
ServeConn uses the gob wire format (see package gob) on the
connection. To use an alternate codec, use ServeCodec.
See NewClient's comment for information about concurrent access.
#
ServeConn はサーバーを単一接続で実行します。
ServeConn はブロックし，クライアントが接続を切るまで接続を処理します。
呼び出し元は通常 go ステートメントで ServeConn を呼び出します。
ServeConn は，接続にゴブワイヤーフォーマット (gob パッケージを参照) を使用します。
代替コーデックを使用するには， ServeCodec を使用してください。
平行アクセスについては， NewClient のコメントを参照してください。
#

#// #
ServeCodec is like ServeConn but uses the specified codec to
decode requests and encode responses.
#
ServeCodec は ServeConn に似ていますが，指定されたコーデックを使用してリクエストをデコードし，レスポンスをエンコードします。
#

#			// #
send a response if we actually managed to read a header.
#
実際にヘッダーを読み取れた場合は，レスポンスを送信してください。
#

#	// #
We've seen that there are no more requests.
Wait for responses to be sent before closing codec.
#
これ以上リクエストがないことを確認しました。
コーデックを閉じる前にレスポンスが送信されるのを待ちます。
#

#// #
ServeRequest is like ServeCodec but synchronously serves a single request.
It does not close the codec upon completion.
#
ServeRequest は ServeCodec に似ていますが， 1 つのリクエストを同期的に処理します。
完了時にコーデックを閉じません。
#

#		// #
send a response if we actually managed to read a header.
#
実際にヘッダーを読み取れた場合は，レスポンスを送信してください。
#

#		// #
discard body
#
ボディを捨てる
#

#	// #
Decode the argument value.
#
引数値をデコードします。
#

#	argIsValue := false // #
if true, need to indirect before calling.
#
true の場合，呼び出す前に間接的にする必要があります。
#

#	// #
argv guaranteed to be a pointer now.
#
argv はポインタになることを保証しました。
#

#	// #
Grab the request header.
#
リクエストヘッダを取得します。
#

#	// #
We read the header successfully. If we see an error now,
we can still recover and move on to the next request.
#
ヘッダを読みました。
エラーが発生した場合でも，回復して次のリクエストに進むことができます。
#

#	// #
Look up the request.
#
リクエストを調べます。
#

#// #
Accept accepts connections on the listener and serves requests
for each incoming connection. Accept blocks until the listener
returns a non-nil error. The caller typically invokes Accept in a
go statement.
#
Accept はリスナー上の接続を受け入れ，着信接続ごとにリクエストを処理します。
リスナーが nil 以外のエラーを返すまでブロックを受け入れます。
呼び出し元は通常 go ステートメントで Accept を呼び出します。
#

#// #
Register publishes the receiver's methods in the DefaultServer.
#
Register は DefaultServer に受信側のメソッドを公開します。
#

#// #
A ServerCodec implements reading of RPC requests and writing of
RPC responses for the server side of an RPC session.
The server calls ReadRequestHeader and ReadRequestBody in pairs
to read requests from the connection, and it calls WriteResponse to
write a response back. The server calls Close when finished with the
connection. ReadRequestBody may be called with a nil
argument to force the body of the request to be read and discarded.
See NewClient's comment for information about concurrent access.
#
ServerCodec は， RPC セッションのサーバー側に対して RPC リクエストの読み取りと RPC レスポンスの書き込みを実装します。
サーバーは ReadRequestHeader と ReadRequestBody をペアで呼び出して接続からのリクエストを読み取り， WriteResponse を呼び出してレスポンスを書き戻します。
接続が終了すると，サーバーは Close を呼び出します。
ReadRequestBody を nil 引数で呼び出すと，リクエストのボディを強制的に読み取り，破棄することができます。
平行アクセスについては， NewClient のコメントを参照してください。
#

#	// #
Close can be called multiple times and must be idempotent.
#
Close は複数回呼び出すことができ，べき等でなければなりません。
#

#// #
ServeConn runs the DefaultServer on a single connection.
ServeConn blocks, serving the connection until the client hangs up.
The caller typically invokes ServeConn in a go statement.
ServeConn uses the gob wire format (see package gob) on the
connection. To use an alternate codec, use ServeCodec.
See NewClient's comment for information about concurrent access.
#
ServeConn は DefaultServer を単一接続で実行します。
ServeConn はブロックし，クライアントが接続を切るまで接続を処理します。
呼び出し元は通常 go ステートメントで ServeConn を呼び出します。
ServeConn は，接続にゴブワイヤーフォーマット (gob パッケージを参照) を使用します。
代替コーデックを使用するには， ServeCodec を使用してください。
平行アクセスについては， NewClient のコメントを参照してください。
#

#// #
Accept accepts connections on the listener and serves requests
to DefaultServer for each incoming connection.
Accept blocks; the caller typically invokes it in a go statement.
#
Accept はリスナー上の接続を受け入れ，着信接続ごとに DefaultServer へのリクエストを処理します。
ブロックを受け入れます。
呼び出し元は通常， go ステートメントでそれを呼び出します。
#

#// #
Can connect to RPC service using HTTP CONNECT to rpcPath.
#
HTTP CONNECT to rpcPath を使用して RPC サービスに接続できます。
#

#// #
ServeHTTP implements an http.Handler that answers RPC requests.
#
ServeHTTP は， RPC リクエストにレスポンスする http.Handler を実装します。
#

#// #
HandleHTTP registers an HTTP handler for RPC messages on rpcPath,
and a debugging handler on debugPath.
It is still necessary to invoke http.Serve(), typically in a go statement.
#
HandleHTTP は， rpcPath に RPC メッセージの HTTP ハンドラを， debugPath にデバッグハンドラを登録します。
通常は go ステートメントで http.Serve() を呼び出す必要があります。
#

#// #
HandleHTTP registers an HTTP handler for RPC messages to DefaultServer
on DefaultRPCPath and a debugging handler on DefaultDebugPath.
It is still necessary to invoke http.Serve(), typically in a go statement.
#
HandleHTTP は， RPC メッセージの HTTP ハンドラーを DefaultRPCPath 上の DefaultServer に，および DefaultDebugPath 上のデバッグハンドラーに登録します。
通常は go ステートメントで http.Serve() を呼び出す必要があります。
#

[net/rpc/server.go]
##
		func (t *T) MethodName(argType T1, replyType *T2) error
#
		func (t *T) MethodName(argType T1, replyType *T2) error
#

[net/rpc/server.go]
##
		package server
#
		package server
#

[net/rpc/server.go]
##
		import "errors"
#
		import "errors"
#

[net/rpc/server.go]
##
		type Args struct {
			A, B int
		}
#
		type Args struct {
			A, B int
		}
#

[net/rpc/server.go]
##
		type Quotient struct {
			Quo, Rem int
		}
#
		type Quotient struct {
			Quo, Rem int
		}
#

[net/rpc/server.go]
##
		type Arith int
#
		type Arith int
#

[net/rpc/server.go]
##
		func (t *Arith) Multiply(args *Args, reply *int) error {
			*reply = args.A * args.B
			return nil
		}
#
		func (t *Arith) Multiply(args *Args, reply *int) error {
			*reply = args.A * args.B
			return nil
		}
#

[net/rpc/server.go]
##
		func (t *Arith) Divide(args *Args, quo *Quotient) error {
			if args.B == 0 {
				return errors.New("divide by zero")
			}
			quo.Quo = args.A / args.B
			quo.Rem = args.A % args.B
			return nil
		}
#
		func (t *Arith) Divide(args *Args, quo *Quotient) error {
			if args.B == 0 {
				return errors.New("divide by zero")
			}
			quo.Quo = args.A / args.B
			quo.Rem = args.A % args.B
			return nil
		}
#

[net/rpc/server.go]
##
		arith := new(Arith)
		rpc.Register(arith)
		rpc.HandleHTTP()
		l, e := net.Listen("tcp", ":1234")
		if e != nil {
			log.Fatal("listen error:", e)
		}
		go http.Serve(l, nil)
#
		arith := new(Arith)
		rpc.Register(arith)
		rpc.HandleHTTP()
		l, e := net.Listen("tcp", ":1234")
		if e != nil {
			log.Fatal("listen error:", e)
		}
		go http.Serve(l, nil)
#

[net/rpc/server.go]
##
		client, err := rpc.DialHTTP("tcp", serverAddress + ":1234")
		if err != nil {
			log.Fatal("dialing:", err)
		}
#
		client, err := rpc.DialHTTP("tcp", serverAddress + ":1234")
		if err != nil {
			log.Fatal("dialing:", err)
		}
#

[net/rpc/server.go]
##
		// Synchronous call
		args := &server.Args{7,8}
		var reply int
		err = client.Call("Arith.Multiply", args, &reply)
		if err != nil {
			log.Fatal("arith error:", err)
		}
		fmt.Printf("Arith: %d*%d=%d", args.A, args.B, reply)
#
		// Synchronous call
		args := &server.Args{7,8}
		var reply int
		err = client.Call("Arith.Multiply", args, &reply)
		if err != nil {
			log.Fatal("arith error:", err)
		}
		fmt.Printf("Arith: %d*%d=%d", args.A, args.B, reply)
#

[net/rpc/server.go]
#	serviceMap sync.Map   // #
map[string]*service
#
map[string]*service
#

