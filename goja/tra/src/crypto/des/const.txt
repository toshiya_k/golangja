* 1

#// #
Package des implements the Data Encryption Standard (DES) and the
Triple Data Encryption Algorithm (TDEA) as defined
in U.S. Federal Information Processing Standards Publication 46-3.
#
des パッケージは，米国連邦情報処理標準出版物 (U.S. Federal Information Processing Standards Publication) 46-3
で定義されたデータ暗号化標準 (DES)
と トリプルデータ暗号化アルゴリズム (TDEA) を実装します。
#

#// #
DES is cryptographically broken and should not be used for secure
applications.
#
DES は暗号学的に破られているため，セキュアなアプリケーションには使用しないでください。
#

#// #
Used to perform an initial permutation of a 64-bit input block.
#
64 ビット入力ブロックの初期置換を実行するために使用されます。
#

#// #
Used to perform a final permutation of a 4-bit preoutput block. This is the
inverse of initialPermutation
#
4 ビットの事前出力ブロックの最終置換を実行するために使用されます。
これは initialPermutation の逆です
#

#// #
Used to expand an input block of 32 bits, producing an output block of 48
bits.
#
32 ビットの入力ブロックを拡張して 48 ビットの出力ブロックを生成するために使用されます。
#

#// #
Yields a 32-bit output from a 32-bit input
#
32 ビット入力から 32 ビット出力を生成
#

#// #
Used in the key schedule to select 56 bits
from a 64-bit input.
#
キースケジュールで 64 ビット入力から 56 ビットを選択するために使用されます。
#

#// #
Used in the key schedule to produce each subkey by selecting 48 bits from
the 56-bit input
#
56 ビットの入力から 48 ビットを選択して各サブキーを作成するためにキースケジュールで使用されます。
#

#// #
8 S-boxes composed of 4 rows and 16 columns
Used in the DES cipher function
#
DES 暗号関数で使用される 4 行 16 列の 8 個の S ボックス
#

#	// #
S-box 1
#
S ボックス 1
#

#	// #
S-box 2
#
S ボックス 2
#

#	// #
S-box 3
#
S ボックス 3
#

#	// #
S-box 4
#
S ボックス 4
#

#	// #
S-box 5
#
S ボックス 5
#

#	// #
S-box 6
#
S ボックス 6
#

#	// #
S-box 7
#
S ボックス 7
#

#	// #
S-box 8
#
S ボックス 8
#

#// #
Size of left rotation per round in each half of the key schedule
#
キースケジュールの各半分におけるラウンドごとの左回転のサイズ
#

