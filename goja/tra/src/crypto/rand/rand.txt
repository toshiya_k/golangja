* 1

#// #
Package rand implements a cryptographically secure
random number generator.
#
rand パッケージは，暗号論的に安全な乱数生成器を実装します。
#

#// #
Reader is a global, shared instance of a cryptographically
secure random number generator.
#
Reader は，暗号学的に安全な乱数ジェネレータのグローバルな共有インスタンスです。
#

#// #
On Linux and FreeBSD, Reader uses getrandom(2) if available, /dev/urandom otherwise.
On OpenBSD, Reader uses getentropy(2).
On other Unix-like systems, Reader reads from /dev/urandom.
On Windows systems, Reader uses the CryptGenRandom API.
On Wasm, Reader uses the Web Crypto API.
#
Linux と FreeBSD では， Reader は利用可能ならば getrandom(2) を，そうでなければ /dev/urandom を使います。
OpenBSD では， Reader は getentropy(2) を使用します。
他の Unix 系システムでは， Reader は /dev/urandom から読み込みます。
Windows システムでは， Reader は CryptGenRandom API を使用します。
Wasm では， Reader は Web Crypto API を使用します。
#

#// #
Read is a helper function that calls Reader.Read using io.ReadFull.
On return, n == len(b) if and only if err == nil.
#
Read は， io.ReadFull を使用して Reader.Read を呼び出すヘルパー関数です。
err == nil の場合は，またその場合に限り， n == len(b) となります。
#

