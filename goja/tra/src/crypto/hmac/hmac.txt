* 1

##
Package hmac implements the Keyed-Hash Message Authentication Code (HMAC) as
defined in U.S. Federal Information Processing Standards Publication 198.
An HMAC is a cryptographic hash that uses a key to sign a message.
The receiver verifies the hash by recomputing it using the same key.
#
hmac パッケージは，米国連邦情報処理規格 (U.S. Federal Information Processing Standards Publication) 198 で定義された鍵付きハッシュメッセージ認証コード (HMAC: Keyed-Hash Message Authentication Code) を実装します。
HMAC は，メッセージに署名するためにキーを使用する暗号化ハッシュです。
受信者は，同じキーを使用してハッシュを再計算することによってハッシュを検証します。
#

##
Receivers should be careful to use Equal to compare MACs in order to avoid
timing side-channels:
#
受信側は，タイミングサイドチャンネルを避けるために， MAC を比較するために Equal を使用するように注意する必要があります。
#

#	#
// ValidMAC reports whether messageMAC is a valid HMAC tag for message.
func ValidMAC(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC)
}
#
// ValidMAC は， messageMAC が message に対して有効な HMAC タグかどうかを報告します。
func ValidMAC(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC)
}
#

#// #
FIPS 198-1:
https://csrc.nist.gov/publications/fips/fips198-1/FIPS-198-1_final.pdf
#
FIPS 198-1:
https://csrc.nist.gov/publications/fips/fips198-1/FIPS-198-1_final.pdf
#

#// #
key is zero padded to the block size of the hash function
ipad = 0x36 byte repeated for key length
opad = 0x5c byte repeated for key length
hmac = H([key ^ opad] H([key ^ ipad] text))
#
 ハッシュ関数のブロックサイズだけkey をゼロ埋めします。
ipad = 鍵長さだけ0x36 バイトを繰り返す
opad = 鍵長さだけ0x5c バイトを繰り返す
hmac = H([key ^ opad] H([key ^ ipad] text))
#

#// #
New returns a new HMAC hash using the given hash.Hash type and key.
Note that unlike other hash implementations in the standard library,
the returned Hash does not implement encoding.BinaryMarshaler
or encoding.BinaryUnmarshaler.
#
New は与えられた hash.Hash type と key を使って新しい HMAC ハッシュを返します。
標準ライブラリの他のハッシュ実装とは異なり，返される Hash は encoding.BinaryMarshaler または encoding.BinaryUnmarshaler を実装していません。
#

#		// #
If key is too big, hash it.
#
キーが大きすぎる場合はハッシュしてください。
#

#// #
Equal compares two MACs for equality without leaking timing information.
#
Equal は，タイミング情報を漏らさずに 2 つの MAC が等しいかどうか比較します。
#

#	// #
We don't have to be constant time if the lengths of the MACs are
different as that suggests that a completely different hash function
was used.
#
MAC の長さが異なる場合は，まったく異なるハッシュ関数が使用されたことが示唆されるため，一定の時間である必要はありません。
#

