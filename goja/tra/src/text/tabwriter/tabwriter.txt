* 1

#// #
Package tabwriter implements a write filter (tabwriter.Writer) that
translates tabbed columns in input into properly aligned text.
#
tabwriter パッケージは，タブ区切りカラムを整列したテキストに変換する書き込みフィルタ
(tabwriter.Writer) を実装します。
#

#// #
The package is using the Elastic Tabstops algorithm described at
http://nickgravgaard.com/elastictabstops/index.html.
#
このパッケージは， http://nickgravgaard.com/elastictabstops/index.html に記載されている Elastic Tabstops アルゴリズムを使用しています。
#

#// #
The text/tabwriter package is frozen and is not accepting new features.
#
text/tabwriter パッケージはフリーズしており，新しい機能を受け付けていません。
#

#// #
Filter implementation
#
フィルタの実装
#

#// #
A cell represents a segment of text terminated by tabs or line breaks.
The text itself is stored in a separate buffer; cell only describes the
segment's size in bytes, its width in runes, and whether it's an htab
('\t') terminated cell.
#
cell は，タブまたは改行で終了するテキストのセグメントを表します。
テキスト自体は別のバッファに格納されています。
cell は，セグメントのサイズ (バイト単位) ，その幅 (ルーン単位) ，および htab('\t') で終了するセルかどうかだけを示します。
#

#	size  int  // #
cell size in bytes
#
セルサイズ (バイト)
#

#	width int  // #
cell width in runes
#
ルーン文字のセル幅
#

#	htab  bool // #
true if the cell is terminated by an htab ('\t')
#
セルが htab ('\t') で終わっている場合は true
#

#// #
A Writer is a filter that inserts padding around tab-delimited
columns in its input to align them in the output.
#
Writer は，入力内のタブ区切り列の周囲にパディングを挿入して出力内で整列させるフィルタです。
#

#// #
The Writer treats incoming bytes as UTF-8-encoded text consisting
of cells terminated by horizontal ('\t') or vertical ('\v') tabs,
and newline ('\n') or formfeed ('\f') characters; both newline and
formfeed act as line breaks.
#
Writer は，着信バイトを，水平 ('\t') または垂直 ('\v') のタブ，改行 ('\n') または改ページ ('\f') で終了するセルで構成される UTF-8 エンコードテキストとして扱います。
改行も改ページも改行として機能します。
#

#// #
Tab-terminated cells in contiguous lines constitute a column. The
Writer inserts padding as needed to make all cells in a column have
the same width, effectively aligning the columns. It assumes that
all characters have the same width, except for tabs for which a
tabwidth must be specified. Column cells must be tab-terminated, not
tab-separated: non-tab terminated trailing text at the end of a line
forms a cell but that cell is not part of an aligned column.
For instance, in this example (where | stands for a horizontal tab):
#
連続した行のタブ終了セルは列を構成します。
Writer は，列内のすべてのセルを同じ幅にするために必要に応じてパディングを挿入し，列を効果的に整列させます。
タブ幅を指定しなければならないタブを除いて，すべての文字が同じ幅を持つと仮定します。
列セルはタブ区切りではなくタブ区切りである必要があります。
行末のタブで終端されていない末尾テキストはセルを形成しますが，そのセルは整列列の一部ではありません。
例えば，この例では (| は水平タブを表します):
#

#// #
the b and c are in distinct columns (the b column is not contiguous
all the way). The d and e are not in a column at all (there's no
terminating tab, nor would the column be contiguous).
#
b と c は別の列にあります (b 列はずっと連続しているわけではありません) 。
d と e はまったく列に入っていません (終了タブはありませんし，列が連続することもありません) 。
#

#// #
The Writer assumes that all Unicode code points have the same width;
this may not be true in some fonts or if the string contains combining
characters.
#
Writer は，すべての Unicode コードポイントが同じ幅を持つと仮定します。
一部のフォントや，文字列に結合文字が含まれている場合，これは当てはまりません。
#

#// #
If DiscardEmptyColumns is set, empty columns that are terminated
entirely by vertical (or "soft") tabs are discarded. Columns
terminated by horizontal (or "hard") tabs are not affected by
this flag.
#
DiscardEmptyColumns が設定されている場合，垂直 (または "ソフト") タブで完全に終了している空の列は破棄されます。
水平 (または "ハード") タブで終了している列は，このフラグの影響を受けません。
#

#// #
If a Writer is configured to filter HTML, HTML tags and entities
are passed through. The widths of tags and entities are
assumed to be zero (tags) and one (entities) for formatting purposes.
#
Writer が HTML をフィルタ処理するように設定されている場合は， HTML タグとエンティティがパススルーされます。
書式設定のために，タグとエンティティの幅は 0(タグ) と 1(エンティティ) と見なされます。
#

#// #
A segment of text may be escaped by bracketing it with Escape
characters. The tabwriter passes escaped text segments through
unchanged. In particular, it does not interpret any tabs or line
breaks within the segment. If the StripEscape flag is set, the
Escape characters are stripped from the output; otherwise they
are passed through as well. For the purpose of formatting, the
width of the escaped text is always computed excluding the Escape
characters.
#
テキストのセグメントは，エスケープ文字で囲むことによってエスケープすることができます。
タブライターは，エスケープされたテキストセグメントを変更せずに通過させます。
特に，セグメント内のタブや改行は解釈されません。
StripEscape フラグが設定されていると，エスケープ文字は出力から取り除かれます。
そうでなければ，それらは同様に通過します。
フォーマットの目的で，エスケープ文字の幅はエスケープ文字を除いて常に計算されます。
#

#// #
The formfeed character acts like a newline but it also terminates
all columns in the current line (effectively calling Flush). Tab-
terminated cells in the next line start new columns. Unless found
inside an HTML tag or inside an escaped text segment, formfeed
characters appear as newlines in the output.
#
改ページ文字は改行のように機能しますが，現在行のすべての列も終了します (実質的に Flush を呼び出します) 。
次の行のタブで終わるセルは新しい列を始めます。
HTML タグ内またはエスケープテキストセグメント内に見つからない限り，改ページ文字は改行として出力に表示されます。
#

#// #
The Writer must buffer input internally, because proper spacing
of one line may depend on the cells in future lines. Clients must
call Flush when done calling Write.
#
1 行の適切な間隔は将来の行のセルに依存するため， Writer は内部で入力をバッファする必要があります。
Write の呼び出しが完了したら，クライアントは Flush を呼び出す必要があります。
#

#	// #
configuration
#
構成
#

#	// #
current state
#
現在の状態
#

#	buf     []byte   // #
collected text excluding tabs or line breaks
#
タブや改行を除いて収集されたテキスト
#

#	pos     int      // #
buffer position up to which cell.width of incomplete cell has been computed
#
不完全なセルの cell.width まで計算されたバッファ位置
#

#	cell    cell     // #
current incomplete cell; cell.width is up to buf[pos] excluding ignored sections
#
現在の不完全セル cell.width は，無視されるセクションを除いて buf[pos] までです。
#

#	endChar byte     // #
terminating char of escaped sequence (Escape for escapes, '>', ';' for HTML tags/entities, or 0)
#
エスケープシーケンスの終了文字 (エスケープの場合はエスケープ， HTML タグ / エンティティの場合は '>', ';' ，または 0)
#

#	lines   [][]cell // #
list of lines; each line is a list of cells
#
行のリスト各行はセルのリストです
#

#	widths  []int    // #
list of column widths in runes - re-used during formatting
#
ルーンの列幅のリスト - フォーマット時に再利用
#

#// #
addLine adds a new line.
flushed is a hint indicating whether the underlying writer was just flushed.
If so, the previous line is not likely to be a good indicator of the new line's cells.
#
addLine は新しい行を追加します。
フラッシュは，内部のライターがフラッシュされたばかりかどうかを示すヒントです。
もしそうなら，前の行は新しい行のセルの良い指標ではないでしょう。
#

#	// #
Grow slice instead of appending,
as that gives us an opportunity
to re-use an existing []cell.
#
追加する代わりにスライスを大きくすると，既存の []cell を再利用することができます。
#

#		// #
The previous line is probably a good indicator
of how many cells the current line will have.
If the current line's capacity is smaller than that,
abandon it and make a new one.
#
前の行は，おそらく現在の行に含まれるセルの数を示す良い指標です。
現在のラインの容量がそれより小さい場合，それを放棄して新しいものを作ります。
#

#// #
Reset the current state.
#
現在の状態をリセットします。
#

#// #
Internal representation (current state):
#
内部表現 (現在の状態):
#

#// #
- all text written is appended to buf; tabs and line breaks are stripped away
- at any given time there is a (possibly empty) incomplete cell at the end
  (the cell starts after a tab or line break)
- cell.size is the number of bytes belonging to the cell so far
- cell.width is text width in runes of that cell from the start of the cell to
  position pos; html tags and entities are excluded from this width if html
  filtering is enabled
- the sizes and widths of processed text are kept in the lines list
  which contains a list of cells for each line
- the widths list is a temporary list with current widths used during
  formatting; it is kept in Writer because it's re-used
#
- 書かれたテキストはすべて buf に追加されます。 タブと改行は取り除かれます
- いつでも最後に (おそらく空の) 不完全なセルがあります (タブまたは改行の後にセルが始まります) 。
- cell.size はこれまでのセルに属するバイト数です。
- cell.width は，セルの先頭から位置 pos までのセルのルーメンでのテキスト幅です。
  HTML フィルタリングが有効になっている場合， HTML タグとエンティティはこの幅から除外されます
- 処理されたテキストのサイズと幅は各行のセルのリストを含む行リストに保持されます。
- 幅のリストはフォーマット中に使われる現在の幅の一時的なリストです。再利用されるので Writer に保存されます
#

#// #
Formatting can be controlled with these flags.
#
フォーマットはこれらのフラグで制御できます。
#

#	// #
Ignore html tags and treat entities (starting with '&'
and ending in ';') as single characters (width = 1).
#
HTML タグを無視し，エンティティ ("&" で始まり ";" で終わる) を 1 つの文字 (width = 1) として扱います。
#

#	// #
Strip Escape characters bracketing escaped text segments
instead of passing them through unchanged with the text.
#
エスケープされたテキストセグメントのエスケープ文字を，テキストを変更せずにそのまま通過させるのではなく取り除きます。
#

#	// #
Force right-alignment of cell content.
Default is left-alignment.
#
セルの内容を強制的に右揃えにします。
デフォルトは左揃えです。
#

#	// #
Handle empty columns as if they were not present in
the input in the first place.
#
空の列を最初の入力に存在しない場合と同様に処理します。
#

#	// #
Always use tabs for indentation columns (i.e., padding of
leading empty cells on the left) independent of padchar.
#
padchar とは関係なく，常にインデント列にタブを使用してください (つまり，左側の先頭の空のセルのパディング) 。
#

#	// #
Print a vertical bar ('|') between columns (after formatting).
Discarded columns appear as zero-width columns ("||").
#
列の間に垂直バー ('|') を表示します (フォーマット後) 。
破棄された列は，幅 0 の列 ("||") として表示されます。
#

#// #
A Writer must be initialized with a call to Init. The first parameter (output)
specifies the filter output. The remaining parameters control the formatting:
#
Writer は Init を呼び出して初期化する必要があります。
最初のパラメーター (出力) はフィルター出力を指定します。
残りのパラメータはフォーマットを制御します。
#

#//	#
minwidth	minimal cell width including any padding
tabwidth	width of tab characters (equivalent number of spaces)
padding		padding added to a cell before computing its width
padchar		ASCII char used for padding
		if padchar == '\t', the Writer will assume that the
		width of a '\t' in the formatted output is tabwidth,
		and cells are left-aligned independent of align_left
		(for correct-looking results, tabwidth must correspond
		to the tab width in the viewer displaying the result)
flags		formatting control
#
minwidth	パディングを含めた最小セル幅
tabwidth	タブ幅 (等価のスペース数)
padding		幅を計算する前にセルに加えられたパディング
padchar		パディングに使う ASCII 文字
	padchar == '\t' の場合， Writer は，'\t' のセルの幅をフォーマットされた出力のタブ幅とみなし，セルは align_left とは無関係に左寄せになります (見栄えのする結果を得るために，タブ幅は結果を表示するビューアのタブ幅に対応している必要があります)
flags		フォーマットコントロール
#

#		// #
tab padding enforces left-alignment
#
タブのパディングは左揃えを強制します
#

#// #
debugging support (keep code around)
#
デバッグサポート (コードを回避する)
#

#// #
local error wrapper so we can distinguish errors we want to return
as errors from genuine panics (which we don't want to return as errors)
#
ローカルエラーラッパーなので，エラーとして返すエラーを本物のパニックと区別することができます (エラーとして返すことは望ましくありません) 。
#

#		// #
padding is done with tabs
#
パディングはタブで行われます
#

#			return // #
tabs have no width - can't do any padding
#
タブに幅がない - パディングができない
#

#		// #
make cellw the smallest multiple of b.tabwidth
#
cellw を b.tabwidth の最小倍数にする
#

#		n := cellw - textw // #
amount of padding
#
パディング量
#

#	// #
padding is done with non-tab characters
#
パディングはタブ以外の文字で行われます
#

#		// #
if TabIndent is set, use tabs to pad leading empty cells
#
TabIndent が設定されている場合は，タブを使用して先頭の空のセルを埋めます。
#

#				// #
indicate column break
#
列区切りを示す
#

#				// #
empty cell
#
空のセル
#

#				// #
non-empty cell
#
空でないセル
#

#				if b.flags&AlignRight == 0 { // #
align left
#
左寄せ
#

#				} else { // #
align right
#
右寄せ
#

#			// #
last buffered line - we don't have a newline, so just write
any outstanding buffered data
#
最後のバッファ行 - 改行がないので，未処理のバッファデータを書くだけです。
#

#			// #
not the last line - write newline
#
最後の行ではない - 改行を書く
#

#// #
Format the text between line0 and line1 (excluding line1); pos
is the buffer position corresponding to the beginning of line0.
Returns the buffer position corresponding to the beginning of
line1 and an error, if any.
#
line0 と line1 の間のテキストをフォーマットします (line1 を除く) 。
pos は， line0 の先頭に対応するバッファ位置です。
line1 の先頭に対応するバッファ位置と，もしあればエラーを返します。
#

#		// #
cell exists in this column => this line
has more cells than the previous line
(the last cell per line is ignored because cells are
tab-terminated; the last cell per line describes the
text before the newline/formfeed and does not belong
to a column)
#
セルはこの列に存在します => この行は前の行よりもセルが多くなります (行末のセルはタブで終了するため無視されます。
行ごとの最後のセルは改行 / 改ページの前のテキストを表します。
カラム)
#

#		// #
print unprinted lines until beginning of block
#
未表示の行をブロックの先頭まで表示する
#

#		// #
column block begin
#
列ブロックの始まり
#

#		width := b.minwidth // #
minimal column width
#
最小列幅
#

#		discardable := true // #
true if all cells in this column are empty and "soft"
#
この列のすべてのセルが空で " 柔らかい " 場合は true
#

#			// #
cell exists in this column
#
この列にセルが存在します
#

#			// #
update width
#
幅を更新する
#

#			// #
update discardable
#
更新は破棄可能
#

#		// #
column block end
#
列ブロックの終わり
#

#		// #
discard empty columns if necessary
#
必要なら空の列を捨てる
#

#		// #
format and print all columns to the right of this column
(we know the widths of this column and all columns to the left)
#
この列の右側にすべての列をフォーマットして表示します (この列の幅と左側にあるすべての列の幅がわかります) 。
#

#		b.widths = append(b.widths, width) // #
push width
#
プッシュ幅
#

#		b.widths = b.widths[0 : len(b.widths)-1] // #
pop width
#
ポップ幅
#

#	// #
print unprinted lines until end
#
表示されていない行を最後まで表示する
#

#// #
Append text to current cell.
#
現在のセルにテキストを追加します。
#

#// #
Update the cell width.
#
セル幅を更新します。
#

#// #
To escape a text segment, bracket it with Escape characters.
For instance, the tab in this string "Ignore this tab: \xff\t\xff"
does not terminate a cell and constitutes a single character of
width one for formatting purposes.
#
テキストセグメントをエスケープするには，エスケープ文字で囲みます。
たとえば，この文字列内のタブ " 無視するタブ :\xff\t\xff" はセルを終了させず，書式設定のために幅 1 の単一文字を構成します。
#

#// #
The value 0xff was chosen because it cannot appear in a valid UTF-8 sequence.
#
有効な UTF-8 シーケンスに表示できないため，値 0xff が選択されました。
#

#// #
Start escaped mode.
#
エスケープモードを開始します。
#

#// #
Terminate escaped mode. If the escaped text was an HTML tag, its width
is assumed to be zero for formatting purposes; if it was an HTML entity,
its width is assumed to be one. In all other cases, the width is the
unicode width of the text.
#
エスケープモードを終了します。
エスケープされたテキストが HTML タグの場合，その幅はフォーマット目的でゼロと見なされます。
HTML エンティティの場合，その幅は 1 と見なされます。
それ以外の場合はすべて，幅はテキストの Unicode 幅です。
#

#			b.cell.width -= 2 // #
don't count the Escape chars
#
エスケープ文字を数えないでください
#

#	case '>': // #
tag of zero width
#
幅ゼロのタグ
#

#		b.cell.width++ // #
entity, count as one rune
#
実体， 1 つのルーンとして数える
#

#// #
Terminate the current cell by adding it to the list of cells of the
current line. Returns the number of cells in that line.
#
現在のセルを現在の行のセルのリストに追加して終了します。
その行のセル数を返します。
#

#// #
Flush should be called after the last call to Write to ensure
that any data buffered in the Writer is written to output. Any
incomplete escape sequence at the end is considered
complete for formatting purposes.
#
Writer にバッファされているデータが確実に出力に書き込まれるようにするために，最後の Write 呼び出しの後に Flush を呼び出す必要があります。
末尾の不完全なエスケープシーケンスは，フォーマット目的では完全と見なされます。
#

#	defer b.reset() // #
even in the presence of errors
#
エラーがあっても
#

#	// #
add current cell if not empty
#
空でなければ現在のセルを追加
#

#			// #
inside escape - terminate it even if incomplete
#
内部エスケープ - 不完全であっても終了
#

#	// #
format contents of buffer
#
バッファのフォーマット内容
#

#// #
Write writes buf to the writer b.
The only errors returned are ones encountered
while writing to the underlying output stream.
#
Write は buf をライターに書き込みます。
返される唯一のエラーは，内部の出力ストリームへの書き込み中に発生したエラーです。
#

#	// #
split text into cells
#
テキストをセルに分割
#

#			// #
outside escape
#
エスケープ外
#

#				// #
end of cell
#
セルの終わり
#

#				n = i + 1 // #
ch consumed
#
ch が消費された
#

#					// #
terminate line
#
終点
#

#						// #
A '\f' always forces a flush. Otherwise, if the previous
line has only one cell which does not have an impact on
the formatting of the following lines (the last cell per
line is ignored by format()), thus we can flush the
Writer contents.
#
'\f' は常にフラッシュを強制します。
そうではなく，前の行に次の行のフォーマットに影響を与えないセルが 1 つしかない場合 (1 行の最後のセルは format() によって無視される) ，したがって Writer の内容をフラッシュできます。
#

#							// #
indicate section break
#
セクション区切りを示す
#

#				// #
start of escaped sequence
#
エスケープシーケンスの開始
#

#					n++ // #
strip Escape
#
Escape を取り除く
#

#				// #
possibly an html tag/entity
#
おそらく HTML タグ / エンティティ
#

#					// #
begin of tag/entity
#
タグ / エンティティの先頭
#

#			// #
inside escape
#
エスケープの中
#

#				// #
end of tag/entity
#
タグ / エンティティの終わり
#

#					j = i // #
strip Escape
#
Escape を取り除く
#

#	// #
append leftover text
#
残りのテキストを追加する
#

#// #
NewWriter allocates and initializes a new tabwriter.Writer.
The parameters are the same as for the Init function.
#
NewWriter は新しい tabwriter.Writer を割り当てて初期化します。
パラメータは Init 関数の場合と同じです。
#

[text/tabwriter/tabwriter.go]
#//	#
aaaa|bbb|d
aa  |b  |dd
a   |
aa  |cccc|eee
#
aaaa|bbb|d
aa  |b  |dd
a   |
aa  |cccc|eee
#

[text/tabwriter/tabwriter.go]
#// #
                   |<---------- size ---------->|
                   |                            |
                   |<- width ->|<- ignored ->|  |
                   |           |             |  |
[---processed---tab------------<tag>...</tag>...]
^                  ^                         ^
|                  |                         |
buf                start of incomplete cell  pos
#
                   |<---------- size ---------->|
                   |                            |
                   |<- width ->|<- ignored ->|  |
                   |           |             |  |
[---processed---tab------------<tag>...</tag>...]
^                  ^                         ^
|                  |                         |
buf                start of incomplete cell  pos
#

[text/tabwriter/tabwriter.go]
#			// #
If Flush ran into a panic, we still need to reset.
#
Flush がパニックに陥った場合でも，リセットする必要があります。
#

[text/tabwriter/tabwriter.go]
#// #
flush is the internal version of Flush, with a named return value which we
don't want to expose.
#
flush は Flush の内部バージョンであり，公開したくない名前の戻り値があります。
#

[text/tabwriter/tabwriter.go]
#// #
flushNoDefers is like flush, but without a deferred handlePanic call. This
can be called from other methods which already have their own deferred
handlePanic calls, such as Write, and avoid the extra defer work.
#
flushNoDefers は flush に似ていますが，遅延遅延 handlePanic 呼び出しはありません。これは，書き込みなどの独自の遅延 handlePanic 呼び出しを既に持っている他のメソッドから呼び出すことができ，余分な遅延作業を回避できます。
#

