* 1

##
Why Generics?
#
なぜジェネリックを導入するのか
#

#* #
Introduction
#
はじめに
#

##
[This is a version of a talk presented at Gophercon 2019.
Video link to follow when available.]
#
[これは Gophercon 2019 で発表された講演です。動画が見れるようになったら，リンクをここに貼ります。]
#

##
This article is about what it would mean to add generics to Go, and
why I think we should do it.
I'll also touch on an update to a possible design for
adding generics to Go.
#
Go にジェネリックを追加することが何を意味するのか，なぜ追加すべきと思うのかについてこの記事で説明します。
Go にジェネリックを追加する設計のアップデートについても触れます。
#

##
Go was released on November 10, 2009.
Less than 24 hours later we saw the
[[https://groups.google.com/d/msg/golang-nuts/70-pdwUUrbI/onMsQspcljcJ][first comment about generics]].
(That comment also mentions exceptions, which we added to the
language, in the form of `panic` and `recover`, in early 2010.)
#
Go は 2009 年 11 月 10 日にリリースされました。
24時間以内に[[https://groups.google.com/d/msg/golang-nuts/70-pdwUUrbI/onMsQspcljcJ][ジェネリックについての最初のコメント]]がありました。
（そのコメントは例外についても言及しており， 2010 年初めに `panic` と `recover` の形で追加しました。）
#

##
In three years of Go surveys, lack of generics has always been listed
as one of the top three problems to fix in the language.
#
Go の 3 年間の調査で，ジェネリックの欠如は，解決すべきトップ 3 の問題の 1 つとして常に挙げられてきました。
#

#* #
Why generics?
#
なぜジェネリクスが必要なのか
#

##
But what does it mean to add generics, and why would we want it?
#
しかし、ジェネリックを追加するとはどういう意味ですか、そしてなぜそれを望むのでしょうか。
#

